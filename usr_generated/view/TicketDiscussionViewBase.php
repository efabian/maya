<?php
require_once 'MayaSecurity.php';
require_once 'paths.cfg.php';
require_once 'Configurations.php';
require_once 'TicketDiscussion.php';
require_once 'Ticket.php';
require_once 'User.php';
class TicketDiscussionViewBase 
{ 
    public $security;
    public $ticket_discussion;
    public $page;
    public $path;
    public $ticket_discussions;
    public $container;
    public $item_per_page;
    public $pages;
    public $search_text;
    public $select_filters;
    public $sort_filters;
    public $sort_orders;
    public function getTicketId($ticket_discussion)
    {
         echo $ticket_discussion->getTicket();
    }
    public function getUserId($ticket_discussion)
    {
         echo $ticket_discussion->getUser();
    }
    public function getMessage($ticket_discussion)
    {
        echo $ticket_discussion->getMessage();
    }
    public function getCreated($ticket_discussion)
    {
        echo $ticket_discussion->getCreated();
    }
    public function renderId($ticket_discussion,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$ticket_discussion->getId();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='id'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='id' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='id' value='".$value."' ></input>";
            }
        }
    }
    public function renderTicketIdSelect($ticket_discussion,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'chosen'),$option_filters=array())
    {
        $sids_str=$ticket_discussion->getTicketId();
        
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }
        if ($hidden || $readonly)
        {
            echo "<input type='hidden' $attrs name='ticket_id'  value='$sids_str' />";
            if ($readonly)
            {
                echo "<span >".$ticket_discussion->getTicket()."</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<select $attrs class='$class edit_input'  name='ticket_id' >\n";
            } else
            {
                echo "<select $attrs class='$class'  name='ticket_id' >\n";
            }
            $selected_ids = explode(",",$sids_str);
            $ticket = new Ticket();
            $objs=$ticket->findAll($option_filters);
            $data_names = array();
            foreach ($objs as $option)
            {
                $name = $option->getName();
                $oid = $option->getId();
                if ($oid)
                {
                    if (in_array($oid,$selected_ids))
                    {
                        echo "<option selected='selected' value='$oid' >$name</option>\n";
                        $data_names[] = $name;
                    } else
                    {
                        echo "<option value='$oid' >$name</option>\n";
                    }
                }
            }
            echo "</select>\n";
            if ($edit) echo "<span class='view_input'>".implode(',',$data_names)."</span>";
       }
    }
    public function renderTicketIdFilter()
    {
		echo "<select  class='filter' name='ticket_discussions_select_filters[ticket_id]' value='' > \r\n";
		echo "<option value=''>All</option> \r\n";
	
		if (in_array('0',$this->ticket_id_lst)) 
		{
			if (array_key_exists('ticket_id',$this->select_filters) && ($this->select_filters['ticket_id']=='0'))
			{
				echo "<option selected='selected' value='0' >Unassigned</option>";
			} else
			{
				echo "<option value='0' >Unassigned</option>";
			}
		}
		$ticket_handle = new Ticket();
		$tickets = $ticket_handle->findAll(array('id'=>$this->ticket_id_lst),array('id'=>'ASC'));
		foreach ($tickets as $ticket)
		{
			if (($this->select_filters['ticket_id']) && ($this->select_filters['ticket_id']==$ticket->getId()))
			{
				echo "<option selected='selected' value='".$ticket->getId()."' >".$ticket->getName()."</option>";
			} else
			{
				echo "<option value='".$ticket->getId()."' >".$ticket->getName()."</option>";
			}
		}
		echo "</select> \r\n";		
	}
	
    public function renderUserIdSelect($ticket_discussion,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'chosen'),$option_filters=array())
    {
        $sids_str=$ticket_discussion->getUserId();
        
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }
        if ($hidden || $readonly)
        {
            echo "<input type='hidden' $attrs name='user_id'  value='$sids_str' />";
            if ($readonly)
            {
                echo "<span >".$ticket_discussion->getUser()."</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<select $attrs class='$class edit_input'  name='user_id' >\n";
            } else
            {
                echo "<select $attrs class='$class'  name='user_id' >\n";
            }
            $selected_ids = explode(",",$sids_str);
            $user = new User();
            $objs=$user->findAll($option_filters);
            $data_names = array();
            foreach ($objs as $option)
            {
                $name = $option->getName();
                $oid = $option->getId();
                if ($oid)
                {
                    if (in_array($oid,$selected_ids))
                    {
                        echo "<option selected='selected' value='$oid' >$name</option>\n";
                        $data_names[] = $name;
                    } else
                    {
                        echo "<option value='$oid' >$name</option>\n";
                    }
                }
            }
            echo "</select>\n";
            if ($edit) echo "<span class='view_input'>".implode(',',$data_names)."</span>";
       }
    }
    public function renderUserIdFilter()
    {
		echo "<select  class='filter' name='ticket_discussions_select_filters[user_id]' value='' > \r\n";
		echo "<option value=''>All</option> \r\n";
	
		if (in_array('0',$this->user_id_lst)) 
		{
			if (array_key_exists('user_id',$this->select_filters) && ($this->select_filters['user_id']=='0'))
			{
				echo "<option selected='selected' value='0' >Unassigned</option>";
			} else
			{
				echo "<option value='0' >Unassigned</option>";
			}
		}
		$user_handle = new User();
		$users = $user_handle->findAll(array('id'=>$this->user_id_lst),array('lastname'=>'ASC'));
		foreach ($users as $user)
		{
			if (($this->select_filters['user_id']) && ($this->select_filters['user_id']==$user->getId()))
			{
				echo "<option selected='selected' value='".$user->getId()."' >".$user->getName()."</option>";
			} else
			{
				echo "<option value='".$user->getId()."' >".$user->getName()."</option>";
			}
		}
		echo "</select> \r\n";		
	}
	
    public function renderMessageTextarea($ticket_discussion,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'jwysiwyg'))
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$ticket_discussion->getMessage();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='message'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<textarea id='edit_message' $attrs class='$class  edit_input' name='message' >".$value."</textarea>";                
            } else
            {
                echo "<textarea id='edit_message' $attrs type='text' class='$class ' name='message' >".$value."</textarea>";
            }
        }
    }
    public function __construct($param=array('container'=>'div.page'))
    {
        foreach($param as $field=>$value)
        {
            $this->$field=$value;
        }
        if (!isset($this->container) || (!$this->container))
        {
            $this->container='div.page';
        }
        $this->paths = new Paths();
        $this->config = new Configurations();
    }
    
    public function init_edit()
    { 
        $this->security=new MayaSecurity();
        if (array_key_exists("id",$_GET))
        {
            $this->id=$_GET["id"];
        } else
        {
            $this->id=$_REQUEST["id"];
        }
        $this->ticket_discussion = new TicketDiscussion();
        $this->ticket_discussion->find(array("id"=>$this->id));
        $this->page=$_REQUEST["selected"];
        $this->path=$_REQUEST["path"];
        $this->current_user=new User();
        $this->current_user->find(array("id"=>$_SESSION["user_id"]));
        $this->edit_form_attributes='';
        $this->edit_button_name='Update';
        $this->edit_button_url='?command=editRest&object=TicketDiscussion';
        $this->edit_button_next_url='?command=display_rest&path=Admin.dir/TicketDiscussions.dir/Edit TicketDiscussion.hid.rest.php&id='.$this->ticket_discussion->getId();
        $this->edit_delete_button_url='?command=deleteRest&object=TicketDiscussion';
        $this->edit_delete_button_next_url='?command=display_rest&path=Admin.dir/TicketDiscussions.dir/List TicketDiscussions.rest.php';
    } 
    public function init_new()
    { 
        $this->security=new MayaSecurity();
        $this->ticket_discussion = new TicketDiscussion();
        $this->page=$_REQUEST["selected"];
        $this->path=$_REQUEST["path"];
        $this->current_user=new User();
        $this->current_user->find(array("id"=>$_SESSION["user_id"]));
        $this->new_form_attributes='';
        $this->new_button_name='Create';
        $this->new_button_url='?command=newRest&object=TicketDiscussion';
        $this->new_button_next_url='?command=display_rest&path=Admin.dir/TicketDiscussions.dir/List TicketDiscussions.rest.php';
    } 
    public function init_list()
    { 
        $this->security=new MayaSecurity();
        $valid_filters=array("id","ticket_id","user_id","message","created");
        $this->page=$_REQUEST["ticket_discussions_selected"];
        $this->path=$_REQUEST["ticket_discussions_path"];
        $this->list_form_attributes='';
        $this->list_button_new_url='?command=display_rest&path=Admin.dir/TicketDiscussions.dir/New TicketDiscussion.rest.php';
        $this->list_button_new_name='New';
		$this->form_list='form_list';
		$this->action='?command=display_php&path=Admin.dir/TicketDiscussions.dir/List TicketDiscussions.rest.php';
        if (!$this->container) $this->container='div.page';
        $ticket_discussion = new TicketDiscussion();
        if (!isset($_SESSION['list_ticket_discussions']))
        {
            $_SESSION['list_ticket_discussions']=array();
            $_SESSION['list_ticket_discussions']['sort_filters']=array();
            $_SESSION['list_ticket_discussions']['sort_order']='';
            $_SESSION['list_ticket_discussions']['select_filters']=array();
            $_SESSION['list_ticket_discussions']['search_texts']=array();
            $_SESSION['list_ticket_discussions']['page']='1';
        }

        if (isset($_REQUEST['ticket_discussions_sort_filters']))
        {
            $sort_lists = $_REQUEST['ticket_discussions_sort_filters'];
            $_SESSION['list_ticket_discussions']['sort_filters']=$sort_lists;
        }
        else
        {
            $sort_lists=$_SESSION['list_ticket_discussions']['sort_filters'];
        }

        if (isset($_REQUEST['ticket_discussions_sort_order']))
        {
            $sort_order = $_REQUEST['ticket_discussions_sort_order'];
            $_SESSION['list_ticket_discussions']['sort_order']=$sort_order;
        }
        else
        {
            $sort_order = $_SESSION['list_ticket_discussions']['sort_order'];
        }
        
        if (!$sort_lists) 
        {
            $sort_lists = array('id'=>'DESC');
            $sort_order = 'id';
        }
        
        $this->sort_filters = array();
        if ($sort_order)
        {
            $this->sort_orders = explode(',',$sort_order);
            foreach ($this->sort_orders as $s_order)
            {
                $this->sort_filters[$s_order]=$sort_lists[$s_order];
            }
        } else $this->sort_orders = array();

		if (!isset($this->select_filters)) $this->select_filters=array();
        if (isset($_REQUEST['ticket_discussions_select_filters']))
        {
            foreach($_REQUEST['ticket_discussions_select_filters'] as $key=>$value)
            {
                if (!(is_numeric($key)) && in_array($key,$valid_filters))
                {
                    $this->select_filters[$key] = $value;
                }
            }
            $_SESSION['list_ticket_discussions']['select_filters']=$this->select_filters;
        }
        else
        {
            $this->select_filters=array_merge($_SESSION['list_ticket_discussions']['select_filters'],$this->select_filters);
        }
        
        


        $ticket_discussions_all = $ticket_discussion->findAll(array(),array(),100000,1,array("ticket_id","user_id"),false);
        $this->ticket_id_lst=array();
        $this->user_id_lst=array();

        foreach ($ticket_discussions_all as $item)
        {
                    $ticket_id=$item->getTicketId();
        $this->ticket_id_lst[$ticket_id]=$ticket_id;
        $user_id=$item->getUserId();
        $this->user_id_lst[$user_id]=$user_id;

        }


        $search='';
        if (isset($_REQUEST['ticket_discussions_search_texts']))
        {
            $this->search_texts = $_REQUEST['ticket_discussions_search_texts'];
            $_SESSION['list_ticket_discussions']['search_texts']=$this->search_texts;
        }
        else
        {
            $this->search_texts = $_SESSION['list_ticket_discussions']['search_texts'];
        }
        if (is_array($this->search_texts))
        {
            foreach($this->search_texts as $field=>$value)
            {
                if (trim($value))
                {
                    if ($search)
                    {
                        $search.=" AND $field like '%$value%' ";
                    }
                    else
                    {
                        $search=" $field like '%$value%' ";
                    }
                }
            }
        }
        if ($search)
        {
            $this->select_filters[] = $search;
        }


        if (!($this->item_per_page)) $this->item_per_page = 30;
        $this->pages=$ticket_discussion->getPageCount($this->select_filters,$this->sort_filters,$this->item_per_page,true);
        
        $this->page = 1;//default to 1 in case not provided below
        if (isset($_REQUEST['ticket_discussions_page']))
        {
            $this->page = $_REQUEST['ticket_discussions_page'];
            $_SESSION['list_ticket_discussions']['page']=$this->page;
        } else
        {
            $this->page = $_SESSION['list_ticket_discussions']['page'];
            if (array_key_exists('list_ticket_discussions',$_SESSION) && array_key_exists('page',$_SESSION['list_ticket_discussions']))
            {
                $this->page = $_SESSION['list_ticket_discussions']['page'];
            }            
        }
        if (($this->page < 1) || ($this->page > $this->pages)) $this->page = 1;

        
        $this->ticket_discussions = $ticket_discussion->findAll($this->select_filters,$this->sort_filters,$this->item_per_page,$this->page);
    } 
    public function renderPaginator()
    { 
        include 'PaginatorView.php'; 
        include 'PaginatorController.php'; 
    } 
    public function renderCsrf()
    { 
        echo '<input type="hidden" name="'.$this->security->getCsrfName().'" value="'.$this->security->getCsrfValue().'"/>'; 
    } 
    public function renderActions($ticket_discussion,$class="",$edit=TRUE)
    {
        if ($edit)
        {
            echo "<a class='mx- $class' mx-container='".$this->container."' mx-click='?command=display_rest&base_path=Admin.dir/TicketDiscussions.dir&path=Edit TicketDiscussion.hid.rest.php&id=".$ticket_discussion->getId()."&object=TicketDiscussion' href='#' ><li class='fa fa-pencil'></li> edit</a>"; 
        } else
        {
            echo "<a class='mx- $class' mx-container='".$this->container."' mx-click='?command=display_rest&base_path=Admin.dir/TicketDiscussions.dir&path=View TicketDiscussion.hid.rest.php&id=".$ticket_discussion->getId()."&object=TicketDiscussion' href='#' ><li class='fa fa-eye'></li> view</a>"; 
        }
    }
    
	public function renderEditControls()
    {
        if ($this->edit_button_next_url)
        { 
            $next_after_update='mx-next="'.$this->edit_button_next_url.'"';
        } else $next_after_update='';
        if ($this->edit_delete_button_next_url)
        {
            $next_after_delete='mx-next="'.$this->edit_delete_button_next_url.'"';
        } else $next_after_delete='';
		echo '<div class="control" style="height:80px">';
		echo '<a class="button mx-" mx-click="'.$this->edit_button_url.'"  href="#" mx-container="div.page" '.$next_after_update.' ><li class="fa fa-save"></li> '.$this->edit_button_name.'</a>';
		echo '<a class="button mx- danger" mx-confirm="Are you sure to delete?" mx-click="'.$this->edit_delete_button_url.'" href="#" mx-container="'.$this->container.'" '.$next_after_delete.' ><li class="fa fa-trash" ></li> Delete </a>';
		echo '</div>';
	}    
	
	public function renderNewControls()
	{
        if ($this->new_button_next_url)
        {
            $next_after_create='mx-next="'.$this->new_button_next_url.'"';
        } else
        {
            $next_after_create='';
        }
		echo '<div class="control" style="height:80px">';
		echo '<a class="button mx-" mx-click="'.$this->new_button_url.'" href="#" '.$next_after_create.' mx-container="'.$this->container.'"><li class="fa fa-plus"></li> '.$this->new_button_name.' </a>';
		echo '</div>';
	}
    
    public function renderListControls($new_after_new='')
    {
        echo '<div class="control" style="height:80px">'."\r\n";
        echo '<a class="button mx-" mx-container="'.$this->container.'" mx-click="'.$this->list_button_new_url.'" href="#" ><i class="fa fa-plus"></i> '.$this->list_button_new_name.'</a>'."\r\n";
        echo '</div>'."\r\n";  
    }
    
    public function renderCustomEdit()
    {
    }

    public function renderCustomNew()
    {
    }

    public function renderCustomlist()
    {
    }
    
    public function renderSearchText($field,$type)
    {
		echo '<div class="search_filter_header">';
		$value='';
		if (isset($this->search_texts) && array_key_exists($field,$this->search_texts)) $value=$this->search_texts[$field];
		echo '<input class="search_text '.$type.'" type="text" name="ticket_discussions_search_texts['.$field.']" value="'.$value.'"></input><a class="search_text_button" ><li class="fa fa-search"></li></a>';
		echo '</div>';
    }

}
