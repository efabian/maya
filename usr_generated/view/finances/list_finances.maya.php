<?php
/**  
 * Used to list all Finance
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */
require_once 'Finance.php';
require_once 'FinanceView.php';
require_once 'Role.php'; 
if (!isset($allowed)) $allowed=array("admin");
if (!isset($allowed_edit)) $allowed_edit=array("admin");
$role = new Role();
if (!$role->isUserAnyOf($allowed))
{

	$role->showUnauthorizedMessage($allowed,"?command=display_php&base_path=usr/view&path=List Finances.rest.php");
} else
{
if (!isset($finance_view)) 
{ 
    $finance_view = new FinanceView();
    $finance_view->init_list();
} 
 if (!isset($headers)) $headers = ["id","description","source","destination","expense_type","income_type","transaction_date","created"];
 if (array_key_exists("finances_container",$_REQUEST)) $finance_view->container = $_REQUEST["finances_container"];
?>
<form id='<?php echo $finance_view->form_list;?>' <?php echo $finance_view->list_form_attributes;?> class="form list" action="<?php echo $finance_view->action;?>" method="POST">
<input type='hidden' name='finances_container' value='<?php echo $finance_view->container;?>' />
<?php $finance_view->renderCsrf();?>
<?php $finance_view->renderCustomList();?>
<table class='odd_even_row_alternate_color full_width data' >
<input class='field_pages' field_pages='<?php echo $finance_view->pages; ?>' type='hidden' value='<?php echo $finance_view->pages; ?>' name='finances_pages' />
<input class='field_page' field_page='<?php echo $finance_view->page; ?>' type='hidden' value='<?php echo $finance_view->page; ?>' name='finances_page' /> 
<thead><tr><th>#</th><th>Actions</th>
<?php if (in_array('id',$headers)) { ?> <th id='header_id'  field='id'><a class='toggle_sort fa fa-sort' href='#'> id</a>             <input id="finances_sort_id" type="hidden" field="id" class="sort" name="finances_sort_filters[id]" value='<?php echo strtoupper($finance_view->sort_filters['id']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('description',$headers)) { ?> <th id='header_description'  field='description'><a class='toggle_sort fa fa-sort' href='#'> description</a>             <input id="finances_sort_description" type="hidden" field="description" class="sort" name="finances_sort_filters[description]" value='<?php echo strtoupper($finance_view->sort_filters['description']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('expense',$headers)) { ?> <th id='header_expense'  field='expense'> expense <?php } ?>
<?php if (in_array('income',$headers)) { ?> <th id='header_income'  field='income'> income <?php } ?>
<?php if (in_array('source',$headers)) { ?> <th id='header_source'  field='source'><a class='toggle_sort fa fa-sort' href='#'> source</a>             <input id="finances_sort_source" type="hidden" field="source" class="sort" name="finances_sort_filters[source]" value='<?php echo strtoupper($finance_view->sort_filters['source']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('destination',$headers)) { ?> <th id='header_destination'  field='destination'><a class='toggle_sort fa fa-sort' href='#'> destination</a>             <input id="finances_sort_destination" type="hidden" field="destination" class="sort" name="finances_sort_filters[destination]" value='<?php echo strtoupper($finance_view->sort_filters['destination']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('expense_type',$headers)) { ?> <th id='header_expense_type'  field='expense_type'><a class='toggle_sort fa fa-sort' href='#'> expense_type</a>             <input id="finances_sort_expense_type" type="hidden" field="expense_type" class="sort" name="finances_sort_filters[expense_type]" value='<?php echo strtoupper($finance_view->sort_filters['expense_type']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('income_type',$headers)) { ?> <th id='header_income_type'  field='income_type'><a class='toggle_sort fa fa-sort' href='#'> income_type</a>             <input id="finances_sort_income_type" type="hidden" field="income_type" class="sort" name="finances_sort_filters[income_type]" value='<?php echo strtoupper($finance_view->sort_filters['income_type']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('transaction_date',$headers)) { ?> <th id='header_transaction_date'  field='transaction_date'><a class='toggle_sort fa fa-sort' href='#'> transaction_date</a>             <input id="finances_sort_transaction_date" type="hidden" field="transaction_date" class="sort" name="finances_sort_filters[transaction_date]" value='<?php echo strtoupper($finance_view->sort_filters['transaction_date']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('created',$headers)) { ?> <th id='header_created'  field='created'><a class='toggle_sort fa fa-sort' href='#'> created</a>             <input id="finances_sort_created" type="hidden" field="created" class="sort" name="finances_sort_filters[created]" value='<?php echo strtoupper($finance_view->sort_filters['created']);?>' style="display:none;">
</th> <?php } ?>
</tr>
<tr><th></th><th><button id="clear_search_and_sort" type="button" class="button"><i class="fa fa-times"></i> <span style="color:#DDDDDD;text-shadow:none"><i class="fa fa-sort"><i class="fa fa-search"></span></button></th><?php if (in_array('id',$headers)) { ?> <th><?php $finance_view->renderSearchText('id','number'); ?></th> <?php } ?>
<?php if (in_array('description',$headers)) { ?> <th><?php $finance_view->renderSearchText('description','textarea'); ?></th> <?php } ?>
<?php if (in_array('expense',$headers)) { ?><th></th> <?php }?>
<?php if (in_array('income',$headers)) { ?><th></th> <?php }?>
<?php if (in_array('source',$headers)) { ?> <th><?php $finance_view->renderSearchText('source','textarea'); ?></th> <?php } ?>
<?php if (in_array('destination',$headers)) { ?> <th><?php $finance_view->renderSearchText('destination','textarea'); ?></th> <?php } ?>
<?php if (in_array('expense_type',$headers)) { ?><th><?php $finance_view->renderExpenseTypeEnumFilter(); ?></th> <?php }?>
<?php if (in_array('income_type',$headers)) { ?><th><?php $finance_view->renderIncomeTypeEnumFilter(); ?></th> <?php }?>
<?php if (in_array('transaction_date',$headers)) { ?> <th><?php $finance_view->renderSearchText('transaction_date','date'); ?></th> <?php } ?>
<?php if (in_array('created',$headers)) { ?><th></th> <?php }?>
</tr></thead>
<tbody>
 <?php
    $current_user=new User();
    $id = $_SESSION["user_id"];
    $current_user=$current_user->find(array("id"=>$id));
    $i=($finance_view->page-1)*$finance_view->item_per_page;
    foreach ($finance_view->finances as $finance)
    { 
        $i=$i+1;
        $path_edit = "Edit Finance";
?>
<tr class="id_<?php echo $finance->getId();?>" ><td><?php echo $i ;?></td><td><?php $finance_view->renderActions($finance,"",$role->isUserAnyOf($allowed_edit));?></td><?php if (in_array('id',$headers)) { ?><td><a class='mx-' mx-container='<?php echo $finance_view->container;?>' mx-click='?command=display_rest&base_path=Admin.dir/Finances.dir&path=Edit Finance.hid.rest.php&id=<?php echo $finance->getId();?>&object=Finance' href='#' ><?php echo $finance->getId();?></a></td> <?php }?>

<?php if (in_array('description',$headers)) { ?>
	<td><?php $finance_view->getDescription($finance);?></td> <?php }?>
<?php if (in_array('expense',$headers)) { ?>
	<td><?php $finance_view->getExpense($finance);?></td> <?php }?>
<?php if (in_array('income',$headers)) { ?>
	<td><?php $finance_view->getIncome($finance);?></td> <?php }?>
<?php if (in_array('source',$headers)) { ?>
	<td><?php $finance_view->getSource($finance);?></td> <?php }?>
<?php if (in_array('destination',$headers)) { ?>
	<td><?php $finance_view->getDestination($finance);?></td> <?php }?>
<?php if (in_array('expense_type',$headers)) { ?>
	<td><?php $finance_view->getExpenseType($finance);?></td> <?php }?>
<?php if (in_array('income_type',$headers)) { ?>
	<td><?php $finance_view->getIncomeType($finance);?></td> <?php }?>
<?php if (in_array('transaction_date',$headers)) { ?>
	<td><?php $finance_view->getTransactionDate($finance);?></td> <?php }?>
<?php if (in_array('created',$headers)) { ?>
	<td><?php $finance_view->getCreated($finance);?></td> <?php }?>
</tr>
<?php } ?>
</tbody>
</table><input class="sort_order" type="hidden" name="finances_sort_order" value="" />
<?php $finance_view->renderListControls(); ?>
</form>
<?php 
    $finance_view->renderPaginator();
?>
<script>
$(document).ready(function() 
{
	var container = '<?php echo $finance_view->container;?>';
	var form_list = '<?php echo $finance_view->form_list;?>';
    
    var sort_filters = [<?PHP 
                        $i=0;
                        foreach ($finance_view->sort_orders as $order) 
                        {
                            if ($i==0)
                            {
                                echo "'$order'";
                            } else echo ",'$order'";
                            $i=$i+1;
                        }?>];
    $('input.sort_order').val(sort_filters.toString());
    $('form').find('a.toggle_sort').each( function()
    {
        var sort_handle = $(this).closest('th').find('input.sort');
        if (sort_handle.val()=='ASC')
        {
            $(this).removeClass('fa-sort-desc');
            $(this).removeClass('fa-sort');
            $(this).addClass('fa-sort-asc');
        } else if (sort_handle.val()=='DESC')
        {
            $(this).removeClass('fa-sort-asc');
            $(this).removeClass('fa-sort');
            $(this).addClass('fa-sort-desc');
        } else
        {
            $(this).removeClass('fa-sort-desc');
            $(this).removeClass('fa-sort-asc');
            $(this).addClass('fa-sort');                
        }        
        $(this).click( function()
        {
            var sort_handle = $(this).closest('th').find('input.sort');
            if ($(this).hasClass('fa-sort-asc'))
            {
                sort_handle.val('DESC');
            } else
            if ($(this).hasClass('fa-sort-desc'))
            {
                sort_handle.val('');
            } else
            if ($(this).hasClass('fa-sort'))
            {
                sort_handle.val('ASC');             
            }


            if (sort_handle.hasClass('sort'))
            {
                var index = sort_filters.indexOf(sort_handle.attr('field'));
                if (index>=0)
                {
                    sort_filters.splice(index, 1);
                } 
                if ((sort_handle.val()=='ASC')||(sort_handle.val()=='DESC'))
                {
                    sort_filters.unshift(sort_handle.attr('field'));
                }
            }
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }
            
            var page=parseInt($('input.field_page').val());
            var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            loadPost(url_rest,$('#'+form_list),container);
            
        });
    });
    $('form').find('select.filter').each( function()
    {
        $(this).change( function()
        {
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }

            var page=parseInt($('input.field_page').val());
            var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            loadPost(url_rest,$('#'+form_list),container);
            
        });
    });    
	$('form').find('.search_text_button').each( function()
    {
        $(this).click( function()
        {
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }

            var page=parseInt($('input.field_page').val());
            var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            loadPost(url_rest,$('#'+form_list),container);
            
        });
    });   
    $("#clear_search_and_sort").click( function()
    {
        $("table.data").find("input.search_text").each( function()
        {
            $(this).val("");
        });
        $("table.data").find("select.filter").each( function()
        {
            $(this).val("");
        });			
        $("table.data").find("input.sort").each( function()
        {
            $(this).val("");
        });
        $("table.data").find("a.toglle_sort").each( function()
        {
            if ($(this).hasClass("fa-sort-asc"))
            {
                $(this).removeClass("fa-sort-asc");
            }
            if ($(this).hasClass("fa-sort-desc"))
            {
                $(this).removeClass("fa-sort-desc");
            }
            if ($(this).hasClass("fa-sort"))
            {
            
            }
            else
            {
                $(this).addClass("fa-sort-asc");
            }
            
            $(this).removeClass("fa-sort-asc");
        });
        
        if (sort_filters.length>0)
        {
            $('input.sort_order').val(sort_filters.toString());
        }

        var page=parseInt($('input.field_page').val());
        var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
        var url_rest = url.replace('display_php','display_rest');
        loadPost(url_rest,$('#'+form_list),container);        
    });         
});
</script>
<?php
}?>
