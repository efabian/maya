<?php 
/**  
 * Used to edit individual Finance
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link https://maya.docph.net/
 */
require_once 'Role.php'; 
if (!isset($allowed_edit)) $allowed_edit=array("admin");
if (!isset($allowed)) $allowed=array("admin");
$role = new Role();
if (!$role->isUserAnyOf($allowed))
{
	$role->showUnauthorizedMessage($allowed,"?command=display_php&base_path=usr/view&path=Edit Finance.rest.php");
} else
{
if (!isset($finance_view))
{
	require_once 'FinanceView.php'; 
	$finance_view = new FinanceView(); 
	$finance_view->init_edit();
}
?> 



<form id="edit_finance" <?php echo $finance_view->edit_form_attributes;?> class="edit" action="?command=edit&object=Finance&base_path=<?php echo $_REQUEST["base_path"];?>&path=<?php echo $path;?>" method="POST">
<input class="field_pages" field_pages="1" type="hidden" value="1" name="pages" />
<?php $finance_view->renderCsrf();?> 
<?php $finance_view->renderCustomEdit();?> 
<table class='odd_even_row_alternate_color full_width data' >
<thead><th>Field</th><th>Data <?php if ($role->isUserAnyOf($allowed_edit)) {?><button type='button' id='toggle_edit' class='button' onclick='toggleEdit()'><li class='fa fa-pencil'></li> Enable Edit</button> <?php }?></th></thead>
<tbody>
<tr id='tr_id' ><td class='label'>Id </td><td id='td_id' ><input type='number' style='display:none' name='id' value='<?php echo $finance_view->finance->getId();?>' ></input><?php echo $finance_view->finance->getId();?></td></tr>
<?php
if (!isset($edit_remove_description))
{?>
<tr id='tr_description'><td class='label'>Description </td><td id='td_description'><?php
    $edit_description=true;
    if (!isset($description_attributes)) 
    { 
        $description_attributes=array("placeholder"=>"Description ");
    } 
    $finance_view->renderDescriptionTextarea($finance_view->finance,isset($edit_description),isset($readonly_description),isset($hidden_description),$description_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_expense))
{?>
<tr id='tr_expense'><td class='label'>Expense </td><td id='td_expense'><?php
    $edit_expense=true;
    if (!isset($expense_attributes)) 
    { 
        $expense_attributes=array("placeholder"=>"Expense ");
    } 
    $finance_view->renderExpenseTextarea($finance_view->finance,isset($edit_expense),isset($readonly_expense),isset($hidden_expense),$expense_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_income))
{?>
<tr id='tr_income'><td class='label'>Income </td><td id='td_income'><?php
    $edit_income=true;
    if (!isset($income_attributes)) 
    { 
        $income_attributes=array("placeholder"=>"Income ");
    } 
    $finance_view->renderIncomeTextarea($finance_view->finance,isset($edit_income),isset($readonly_income),isset($hidden_income),$income_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_source))
{?>
<tr id='tr_source'><td class='label'>Source </td><td id='td_source'><?php
    $edit_source=true;
    if (!isset($source_attributes)) 
    { 
        $source_attributes=array("placeholder"=>"Source ");
    } 
    $finance_view->renderSourceTextarea($finance_view->finance,isset($edit_source),isset($readonly_source),isset($hidden_source),$source_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_destination))
{?>
<tr id='tr_destination'><td class='label'>Destination </td><td id='td_destination'><?php
    $edit_destination=true;
    if (!isset($destination_attributes)) 
    { 
        $destination_attributes=array("placeholder"=>"Destination ");
    } 
    $finance_view->renderDestinationTextarea($finance_view->finance,isset($edit_destination),isset($readonly_destination),isset($hidden_destination),$destination_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_expense_type))
{?>
<tr id='tr_expense_type'><td class='label'>Expense Type </td><td id='td_expense_type'><?php
    $edit_expense_type=true;
    $expense_type_attributes=array("class"=>"chosen");
    $finance_view->renderExpenseTypeEnum($finance_view->finance,isset($edit_expense_type),isset($readonly_expense_type),isset($hidden_expense_type),$expense_type_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_income_type))
{?>
<tr id='tr_income_type'><td class='label'>Income Type </td><td id='td_income_type'><?php
    $edit_income_type=true;
    $income_type_attributes=array("class"=>"chosen");
    $finance_view->renderIncomeTypeEnum($finance_view->finance,isset($edit_income_type),isset($readonly_income_type),isset($hidden_income_type),$income_type_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_transaction_date))
{?>
<tr id='tr_transaction_date'><td class='label'>Transaction Date </td><td id='td_transaction_date'><?php
    $edit_transaction_date=true;
    if (!isset($transaction_date_attributes)) 
    { 
        $transaction_date_attributes=array("placeholder"=>"Transaction Date ");
    } 
    $finance_view->renderTransactionDate($finance_view->finance,isset($edit_transaction_date),isset($readonly_transaction_date),isset($hidden_transaction_date),$transaction_date_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_created))
{?>
<tr id='tr_created'><td class='label'>Created </td><td id='td_created'><?php echo $finance_view->finance->getCreated();?></td></tr>
<?php
} ?>
</tbody>
</table>
<div class="custom_widget">
<?php if ($finance_custom_widget) include "$finance_custom_widget"; ?>
</div>
<?php if ($role->isUserAnyOf($allowed_edit)) { $finance_view->renderEditControls(); }?>
</form>
<?php
}?>
