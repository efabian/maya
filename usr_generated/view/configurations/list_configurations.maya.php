<?php
/**  
 * Used to list all Configuration
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */
require_once 'Configuration.php';
require_once 'ConfigurationView.php';
require_once 'Role.php'; 
if (!isset($allowed)) $allowed=array("admin");
if (!isset($allowed_edit)) $allowed_edit=array("admin");
$role = new Role();
if (!$role->isUserAnyOf($allowed))
{

	$role->showUnauthorizedMessage($allowed,"?command=display_php&base_path=usr/view&path=List Configurations.rest.php");
} else
{
if (!isset($configuration_view)) 
{ 
    $configuration_view = new ConfigurationView();
    $configuration_view->init_list();
} 
 if (!isset($headers)) $headers = ["id","name","value","description"];
 if (array_key_exists("configurations_container",$_REQUEST)) $configuration_view->container = $_REQUEST["configurations_container"];
?>
<form id='<?php echo $configuration_view->form_list;?>' <?php echo $configuration_view->list_form_attributes;?> class="form list" action="<?php echo $configuration_view->action;?>" method="POST">
<input type='hidden' name='configurations_container' value='<?php echo $configuration_view->container;?>' />
<?php $configuration_view->renderCsrf();?>
<?php $configuration_view->renderCustomList();?>
<table class='odd_even_row_alternate_color full_width data' >
<input class='field_pages' field_pages='<?php echo $configuration_view->pages; ?>' type='hidden' value='<?php echo $configuration_view->pages; ?>' name='configurations_pages' />
<input class='field_page' field_page='<?php echo $configuration_view->page; ?>' type='hidden' value='<?php echo $configuration_view->page; ?>' name='configurations_page' /> 
<thead><tr><th>#</th><th>Actions</th>
<?php if (in_array('id',$headers)) { ?> <th id='header_id'  field='id'><a class='toggle_sort fa fa-sort' href='#'> id</a>             <input id="configurations_sort_id" type="hidden" field="id" class="sort" name="configurations_sort_filters[id]" value='<?php echo strtoupper($configuration_view->sort_filters['id']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('name',$headers)) { ?> <th id='header_name'  field='name'><a class='toggle_sort fa fa-sort' href='#'> name</a>             <input id="configurations_sort_name" type="hidden" field="name" class="sort" name="configurations_sort_filters[name]" value='<?php echo strtoupper($configuration_view->sort_filters['name']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('value',$headers)) { ?> <th id='header_value'  field='value'><a class='toggle_sort fa fa-sort' href='#'> value</a>             <input id="configurations_sort_value" type="hidden" field="value" class="sort" name="configurations_sort_filters[value]" value='<?php echo strtoupper($configuration_view->sort_filters['value']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('description',$headers)) { ?> <th id='header_description'  field='description'><a class='toggle_sort fa fa-sort' href='#'> description</a>             <input id="configurations_sort_description" type="hidden" field="description" class="sort" name="configurations_sort_filters[description]" value='<?php echo strtoupper($configuration_view->sort_filters['description']);?>' style="display:none;">
</th> <?php } ?>
</tr>
<tr><th></th><th><button id="clear_search_and_sort" type="button" class="button"><i class="fa fa-times"></i> <span style="color:#DDDDDD;text-shadow:none"><i class="fa fa-sort"><i class="fa fa-search"></span></button></th><?php if (in_array('id',$headers)) { ?> <th><?php $configuration_view->renderSearchText('id','number'); ?></th> <?php } ?>
<?php if (in_array('name',$headers)) { ?> <th><?php $configuration_view->renderSearchText('name','text'); ?></th> <?php } ?>
<?php if (in_array('value',$headers)) { ?> <th><?php $configuration_view->renderSearchText('value','textarea'); ?></th> <?php } ?>
<?php if (in_array('description',$headers)) { ?> <th><?php $configuration_view->renderSearchText('description','textarea'); ?></th> <?php } ?>
</tr></thead>
<tbody>
 <?php
    $current_user=new User();
    $id = $_SESSION["user_id"];
    $current_user=$current_user->find(array("id"=>$id));
    $i=($configuration_view->page-1)*$configuration_view->item_per_page;
    foreach ($configuration_view->configurations as $configuration)
    { 
        $i=$i+1;
        $path_edit = "Edit Configuration";
?>
<tr class="id_<?php echo $configuration->getId();?>" ><td><?php echo $i ;?></td><td><?php $configuration_view->renderActions($configuration,"",$role->isUserAnyOf($allowed_edit));?></td><?php if (in_array('id',$headers)) { ?><td><a class='mx-' mx-container='<?php echo $configuration_view->container;?>' mx-click='?command=display_rest&base_path=Admin.dir/Configurations.dir&path=Edit Configuration.hid.rest.php&id=<?php echo $configuration->getId();?>&object=Configuration' href='#' ><?php echo $configuration->getId();?></a></td> <?php }?>

<?php if (in_array('name',$headers)) { ?>
	<td><?php $configuration_view->getName($configuration);?></td> <?php }?>
<?php if (in_array('value',$headers)) { ?>
	<td><?php $configuration_view->getValue($configuration);?></td> <?php }?>
<?php if (in_array('description',$headers)) { ?>
	<td><?php $configuration_view->getDescription($configuration);?></td> <?php }?>
</tr>
<?php } ?>
</tbody>
</table><input class="sort_order" type="hidden" name="configurations_sort_order" value="" />
<?php $configuration_view->renderListControls(); ?>
</form>
<?php 
    $configuration_view->renderPaginator();
?>
<script>
$(document).ready(function() 
{
	var container = '<?php echo $configuration_view->container;?>';
	var form_list = '<?php echo $configuration_view->form_list;?>';
    
    var sort_filters = [<?PHP 
                        $i=0;
                        foreach ($configuration_view->sort_orders as $order) 
                        {
                            if ($i==0)
                            {
                                echo "'$order'";
                            } else echo ",'$order'";
                            $i=$i+1;
                        }?>];
    $('input.sort_order').val(sort_filters.toString());
    $('form').find('a.toggle_sort').each( function()
    {
        var sort_handle = $(this).closest('th').find('input.sort');
        if (sort_handle.val()=='ASC')
        {
            $(this).removeClass('fa-sort-desc');
            $(this).removeClass('fa-sort');
            $(this).addClass('fa-sort-asc');
        } else if (sort_handle.val()=='DESC')
        {
            $(this).removeClass('fa-sort-asc');
            $(this).removeClass('fa-sort');
            $(this).addClass('fa-sort-desc');
        } else
        {
            $(this).removeClass('fa-sort-desc');
            $(this).removeClass('fa-sort-asc');
            $(this).addClass('fa-sort');                
        }        
        $(this).click( function()
        {
            var sort_handle = $(this).closest('th').find('input.sort');
            if ($(this).hasClass('fa-sort-asc'))
            {
                sort_handle.val('DESC');
            } else
            if ($(this).hasClass('fa-sort-desc'))
            {
                sort_handle.val('');
            } else
            if ($(this).hasClass('fa-sort'))
            {
                sort_handle.val('ASC');             
            }


            if (sort_handle.hasClass('sort'))
            {
                var index = sort_filters.indexOf(sort_handle.attr('field'));
                if (index>=0)
                {
                    sort_filters.splice(index, 1);
                } 
                if ((sort_handle.val()=='ASC')||(sort_handle.val()=='DESC'))
                {
                    sort_filters.unshift(sort_handle.attr('field'));
                }
            }
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }
            
            var page=parseInt($('input.field_page').val());
            var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            loadPost(url_rest,$('#'+form_list),container);
            
        });
    });
    $('form').find('select.filter').each( function()
    {
        $(this).change( function()
        {
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }

            var page=parseInt($('input.field_page').val());
            var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            loadPost(url_rest,$('#'+form_list),container);
            
        });
    });    
	$('form').find('.search_text_button').each( function()
    {
        $(this).click( function()
        {
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }

            var page=parseInt($('input.field_page').val());
            var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            loadPost(url_rest,$('#'+form_list),container);
            
        });
    });   
    $("#clear_search_and_sort").click( function()
    {
        $("table.data").find("input.search_text").each( function()
        {
            $(this).val("");
        });
        $("table.data").find("select.filter").each( function()
        {
            $(this).val("");
        });			
        $("table.data").find("input.sort").each( function()
        {
            $(this).val("");
        });
        $("table.data").find("a.toglle_sort").each( function()
        {
            if ($(this).hasClass("fa-sort-asc"))
            {
                $(this).removeClass("fa-sort-asc");
            }
            if ($(this).hasClass("fa-sort-desc"))
            {
                $(this).removeClass("fa-sort-desc");
            }
            if ($(this).hasClass("fa-sort"))
            {
            
            }
            else
            {
                $(this).addClass("fa-sort-asc");
            }
            
            $(this).removeClass("fa-sort-asc");
        });
        
        if (sort_filters.length>0)
        {
            $('input.sort_order').val(sort_filters.toString());
        }

        var page=parseInt($('input.field_page').val());
        var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
        var url_rest = url.replace('display_php','display_rest');
        loadPost(url_rest,$('#'+form_list),container);        
    });         
});
</script>
<?php
}?>
