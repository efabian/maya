<?php 
/**  
 * Used to edit individual Configuration
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link https://maya.docph.net/
 */
require_once 'Role.php'; 
if (!isset($allowed_edit)) $allowed_edit=array("admin");
if (!isset($allowed)) $allowed=array("admin");
$role = new Role();
if (!$role->isUserAnyOf($allowed))
{
	$role->showUnauthorizedMessage($allowed,"?command=display_php&base_path=usr/view&path=Edit Configuration.rest.php");
} else
{
if (!isset($configuration_view))
{
	require_once 'ConfigurationView.php'; 
	$configuration_view = new ConfigurationView(); 
	$configuration_view->init_edit();
}
?> 



<form id="edit_configuration" <?php echo $configuration_view->edit_form_attributes;?> class="edit" action="?command=edit&object=Configuration&base_path=<?php echo $_REQUEST["base_path"];?>&path=<?php echo $path;?>" method="POST">
<input class="field_pages" field_pages="1" type="hidden" value="1" name="pages" />
<?php $configuration_view->renderCsrf();?> 
<?php $configuration_view->renderCustomEdit();?> 
<table class='odd_even_row_alternate_color full_width data' >
<thead><th>Field</th><th>Data <?php if ($role->isUserAnyOf($allowed_edit)) {?><button type='button' id='toggle_edit' class='button' onclick='toggleEdit()'><li class='fa fa-pencil'></li> Enable Edit</button> <?php }?></th></thead>
<tbody>
<tr id='tr_id' ><td class='label'>Id </td><td id='td_id' ><input type='number' style='display:none' name='id' value='<?php echo $configuration_view->configuration->getId();?>' ></input><?php echo $configuration_view->configuration->getId();?></td></tr>
<?php
if (!isset($edit_remove_name))
{?>
<tr id='tr_name'><td class='label'>Name </td><td id='td_name'><?php
    $edit_name=true;
    if (!isset($name_attributes)) 
    { 
        $name_attributes=array("placeholder"=>"Name ");
    } 
    $configuration_view->renderName($configuration_view->configuration,isset($edit_name),isset($readonly_name),isset($hidden_name), $name_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_value))
{?>
<tr id='tr_value'><td class='label'>Value </td><td id='td_value'><?php
    $edit_value=true;
    if (!isset($value_attributes)) 
    { 
        $value_attributes=array("placeholder"=>"Value ");
    } 
    $configuration_view->renderValueTextarea($configuration_view->configuration,isset($edit_value),isset($readonly_value),isset($hidden_value),$value_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_description))
{?>
<tr id='tr_description'><td class='label'>Description </td><td id='td_description'><?php
    $edit_description=true;
    if (!isset($description_attributes)) 
    { 
        $description_attributes=array("placeholder"=>"Description ");
    } 
    $configuration_view->renderDescriptionTextarea($configuration_view->configuration,isset($edit_description),isset($readonly_description),isset($hidden_description),$description_attributes);
?>
</td></tr>
<?php
} ?>
</tbody>
</table>
<div class="custom_widget">
<?php if ($configuration_custom_widget) include "$configuration_custom_widget"; ?>
</div>
<?php if ($role->isUserAnyOf($allowed_edit)) { $configuration_view->renderEditControls(); }?>
</form>
<?php
}?>
