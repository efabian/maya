<?php 
/**  
 * Used to create new Configuration
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */
require_once 'Role.php'; 
if (!isset($allowed)) $allowed=array("admin");
$role = new Role();
if (!$role->isUserAnyOf($allowed))
{
	$role->showUnauthorizedMessage($allowed,"?command=display_php&base_path=usr/view&path=New Configuration.rest.php");
} else
{
if (!isset($configuration_view)) 
{ 
    require_once 'ConfigurationView.php'; 
    $configuration_view = new ConfigurationView(); 
    $configuration_view->init_new();
} ?> 
<form id="new_configuration"  <?php echo $configuration_view->new_form_attributes;?> class="new" action="?command=new&object=Configuration&base_path=<?php echo $_REQUEST["base_path"];?>&path=<?php echo $path;?>" method="POST">
<?php $configuration_view->renderCsrf();?> 
<?php $configuration_view->renderCustomNew();?> 
<table class='odd_even_row_alternate_color full_width data' >
<thead><th>Field</th><th>Data</th></thead>
<tbody>
<?php
if (!isset($new_remove_name))
{?>
<tr id='tr_name'><td class='label'>Name </td><td id='td_name'><?php
    if (!isset($name_attributes)) 
    { 
        $name_attributes=array("placeholder"=>"Name ");
    } 
    $configuration_view->renderName($configuration_view->configuration,isset($edit_name),isset($readonly_name),isset($hidden_name),$name_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_value))
{?>
<tr id='tr_value'><td class='label'>Value </td><td id='td_value'><?php
    if (!isset($value_attributes)) 
    { 
        $value_attributes=array("placeholder"=>"Value ");
    } 
    $configuration_view->renderValueTextarea($configuration_view->configuration,isset($edit_value),isset($readonly_value),isset($hidden_value),$value_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_description))
{?>
<tr id='tr_description'><td class='label'>Description </td><td id='td_description'><?php
    if (!isset($description_attributes)) 
    { 
        $description_attributes=array("placeholder"=>"Description ");
    } 
    $configuration_view->renderDescriptionTextarea($configuration_view->configuration,isset($edit_description),isset($readonly_description),isset($hidden_description),$description_attributes);
?>
</td></tr>
<?php
} ?>
</tbody>
</table><div class="custom_widget">
<?php if ($configuration_custom_widget) include "$configuration_custom_widget"; ?>
</div>
<?php $configuration_view->renderNewControls(); ?>
</form>
</div>
<?php
}?>
