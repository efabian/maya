<?php 
/**  
 * Used to create new BunkerSupply
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */
require_once 'Role.php'; 
if (!isset($allowed)) $allowed=array("admin");
$role = new Role();
if (!$role->isUserAnyOf($allowed))
{
	$role->showUnauthorizedMessage($allowed,"?command=display_php&base_path=usr/view&path=New BunkerSupply.rest.php");
} else
{
if (!isset($bunker_supply_view)) 
{ 
    require_once 'BunkerSupplyView.php'; 
    $bunker_supply_view = new BunkerSupplyView(); 
    $bunker_supply_view->init_new();
} ?> 
<form id="new_bunker_supply"  <?php echo $bunker_supply_view->new_form_attributes;?> class="new" action="?command=new&object=BunkerSupply&base_path=<?php echo $_REQUEST["base_path"];?>&path=<?php echo $path;?>" method="POST">
<input class="field_pages" field_pages="1" type="hidden" value="1" name="pages" />
<?php $bunker_supply_view->renderCsrf();?> 
<?php $bunker_supply_view->renderCustomNew();?> 
<table class='odd_even_row_alternate_color full_width data' >
<thead><th>Field</th><th>Data</th></thead>
<tbody>
<?php
if (!isset($new_remove_name))
{?>
<tr id='tr_name'><td class='label'>Name </td><td id='td_name'><?php
    if (!isset($name_attributes)) 
    { 
        $name_attributes=array("placeholder"=>"Name ");
    } 
    $bunker_supply_view->renderName($bunker_supply_view->bunker_supply,isset($edit_name),isset($readonly_name),isset($hidden_name),$name_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_description))
{?>
<tr id='tr_description'><td class='label'>Description </td><td id='td_description'><?php
    if (!isset($description_attributes)) 
    { 
        $description_attributes=array("placeholder"=>"Description ");
    } 
    $bunker_supply_view->renderDescriptionTextarea($bunker_supply_view->bunker_supply,isset($edit_description),isset($readonly_description),isset($hidden_description),$description_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_user_id))
{?>
<tr id='tr_user_id'><td class='label'>User </td><td id='td_user_id'><?php
    $bunker_supply_view->renderUserIdSelect($bunker_supply_view->bunker_supply,isset($edit_user_id),isset($readonly_user_id),isset($hidden_user_id));
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_expiry_date))
{?>
<tr id='tr_expiry_date'><td class='label'>Expiry Date </td><td id='td_expiry_date'><?php
    if (!isset($expiry_date_attributes)) 
    { 
        $expiry_date_attributes=array("placeholder"=>"Expiry Date ");
    } 
    $bunker_supply_view->renderExpiryDate($bunker_supply_view->bunker_supply,isset($edit_expiry_date),isset($readonly_expiry_date),isset($hidden_expiry_date),$expiry_date_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_created))
{?>
<tr id='tr_created'><td class='label'>Created </td><td id='td_created'><?php echo $bunker_supply_view->bunker_supply->getCreated();?></td></tr>
<?php
} ?>
</tbody>
</table><div class="custom_widget">
<?php if ($bunker_supply_custom_widget) include "$bunker_supply_custom_widget"; ?>
</div>
<?php $bunker_supply_view->renderNewControls(); ?>
</form>
</div>
<?php
}?>
