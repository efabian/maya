<?php
/**  
 * Used to list all BunkerSupply
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */
require_once 'BunkerSupply.php';
require_once 'BunkerSupplyView.php';
require_once 'Role.php'; 
if (!isset($allowed)) $allowed=array("admin");
if (!isset($allowed_edit)) $allowed_edit=array("admin");
$role = new Role();
if (!$role->isUserAnyOf($allowed))
{

	$role->showUnauthorizedMessage($allowed,"?command=display_php&base_path=usr/view&path=List BunkerSupplies.rest.php");
} else
{
if (!isset($bunker_supply_view)) 
{ 
    $bunker_supply_view = new BunkerSupplyView();
    $bunker_supply_view->init_list();
} 
 if (!isset($headers)) $headers = ["id","name","description","user_id","expiry_date","created"];
?>
<form id='form_list' <?php echo $bunker_supply_view->list_form_attributes;?> class='form list' action="?command=display_php&base_path=Admin.dir/BunkerSupplies.dir&path=List BunkerSupplies.rest.php" method="POST">
<table class='odd_even_row_alternate_color full_width data' >
<?php $bunker_supply_view->renderCsrf();?>
<?php $bunker_supply_view->renderCustomList();?>
<input class='field_pages' field_pages='<?php echo $bunker_supply_view->pages; ?>' type='hidden' value='<?php echo $bunker_supply_view->pages; ?>' name='pages' />
<input class='field_page' field_page='<?php echo $bunker_supply_view->page; ?>' type='hidden' value='<?php echo $bunker_supply_view->page; ?>' name='page' /> 
<thead><tr><th>#</th><th>Actions</th>
<?php if (in_array('id',$headers)) { ?> <th id='header_id'  field='id'><a class='toggle_sort fa fa-sort' href='#'> id</a>             <input id="sort_id" type="hidden" field="id" class="sort" name="sort_filters[id]" value='<?php echo strtoupper($bunker_supply_view->sort_filters['id']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('name',$headers)) { ?> <th id='header_name'  field='name'><a class='toggle_sort fa fa-sort' href='#'> name</a>             <input id="sort_name" type="hidden" field="name" class="sort" name="sort_filters[name]" value='<?php echo strtoupper($bunker_supply_view->sort_filters['name']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('description',$headers)) { ?> <th id='header_description'  field='description'><a class='toggle_sort fa fa-sort' href='#'> description</a>             <input id="sort_description" type="hidden" field="description" class="sort" name="sort_filters[description]" value='<?php echo strtoupper($bunker_supply_view->sort_filters['description']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('user_id',$headers)) { ?> <th id='header_user_id'  field='user_id'><a class='toggle_sort fa fa-sort' href='#'> users</a>             <input id="sort_user_id" type="hidden" field="user_id" class="sort" name="sort_filters[user_id]" value='<?php echo strtoupper($bunker_supply_view->sort_filters['user_id']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('expiry_date',$headers)) { ?> <th id='header_expiry_date'  field='expiry_date'><a class='toggle_sort fa fa-sort' href='#'> expiry_date</a>             <input id="sort_expiry_date" type="hidden" field="expiry_date" class="sort" name="sort_filters[expiry_date]" value='<?php echo strtoupper($bunker_supply_view->sort_filters['expiry_date']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('created',$headers)) { ?> <th id='header_created'  field='created'><a class='toggle_sort fa fa-sort' href='#'> created</a>             <input id="sort_created" type="hidden" field="created" class="sort" name="sort_filters[created]" value='<?php echo strtoupper($bunker_supply_view->sort_filters['created']);?>' style="display:none;">
</th> <?php } ?>
</tr>
<tr><th></th><th><button id="clear_search_and_sort" type="button" class="button"><i class="fa fa-times"></i> <span style="color:#DDDDDD;text-shadow:none"><i class="fa fa-sort"><i class="fa fa-search"></span></button></th><?php if (in_array('id',$headers)) { ?> <th>    <div class="search_filter_header">
    <input class="search_text" type="text" name="search_texts[id]" 
    value="<?php if (isset($bunker_supply_view->search_texts) && array_key_exists('id',$bunker_supply_view->search_texts)) echo $bunker_supply_view->search_texts['id'];?>"></input><a class="search_text_button" ><li class="fa fa-search"></li></a>
    </div>
</th> <?php } ?>
<?php if (in_array('name',$headers)) { ?> <th>    <div class="search_filter_header">
    <input class="search_text" type="text" name="search_texts[name]" 
    value="<?php if (isset($bunker_supply_view->search_texts) && array_key_exists('name',$bunker_supply_view->search_texts)) echo $bunker_supply_view->search_texts['name'];?>"></input><a class="search_text_button" ><li class="fa fa-search"></li></a>
    </div>
</th> <?php } ?>
<?php if (in_array('description',$headers)) { ?> <th>    <div class="search_filter_header">
    <input class="search_text" type="text" name="search_texts[description]" 
    value="<?php if (isset($bunker_supply_view->search_texts) && array_key_exists('description',$bunker_supply_view->search_texts)) echo $bunker_supply_view->search_texts['description'];?>"></input><a class="search_text_button" ><li class="fa fa-search"></li></a>
    </div>
</th> <?php } ?>
<?php if (in_array('user_id',$headers)) { ?><th><?php $bunker_supply_view->renderUserIdFilter(); ?></th> <?php }?>
<?php if (in_array('expiry_date',$headers)) { ?> <th>    <div class="search_filter_header">
    <input class="search_text date" type="text" name="search_texts[expiry_date]" 
    value="<?php if (isset($bunker_supply_view->search_texts) && array_key_exists('expiry_date',$bunker_supply_view->search_texts)) echo $bunker_supply_view->search_texts['expiry_date'];?>"></input><a class="search_text_button" ><li class="fa fa-search"></li></a>
    </div>
</th> <?php } ?>
<?php if (in_array('created',$headers)) { ?><th></th> <?php }?>
</tr></thead>
<tbody>
 <?php
    $current_user=new User();
    $id = $_SESSION["user_id"];
    $current_user=$current_user->find(array("id"=>$id));
    $i=0;
    foreach ($bunker_supply_view->bunker_supplies as $bunker_supply)
    { 
        $i=$i+1;
        $path_edit = "Edit BunkerSupply";
?>
<tr>
<td><?php echo $i;?></td><td><?php $bunker_supply_view->renderActions($bunker_supply,"",$role->isUserAnyOf($allowed_edit));?></td><?php if (in_array('id',$headers)) { ?><td><a class='click_action' container='<?php echo $bunker_supply_view->container;?>' url='?command=display_rest&base_path=Admin.dir/BunkerSupplies.dir&path=Edit BunkerSupply.hid.rest.php&id=<?php echo $bunker_supply->getId();?>&object=BunkerSupply' href='#' ><?php echo $bunker_supply->getId();?></a></td> <?php }?>

<?php if (in_array('name',$headers)) { ?>
	<td><?php $bunker_supply_view->getName($bunker_supply);?></td> <?php }?>
<?php if (in_array('description',$headers)) { ?>
	<td><?php $bunker_supply_view->getDescription($bunker_supply);?></td> <?php }?>
<?php if (in_array('user_id',$headers)) { ?>
	<td><?php $bunker_supply_view->getUserId($bunker_supply);?></td> <?php }?>
<?php if (in_array('expiry_date',$headers)) { ?>
	<td><?php $bunker_supply_view->getExpiryDate($bunker_supply);?></td> <?php }?>
<?php if (in_array('created',$headers)) { ?>
	<td><?php $bunker_supply_view->getCreated($bunker_supply);?></td> <?php }?>
</tr>
<?php } ?>
</tbody>
</table><input class="sort_order" type="hidden" name="sort_order" value="" />
<?PHP 
        $bunker_supply_view->renderPaginator();
?>
<?php $bunker_supply_view->renderListControls(); ?>
</form>
<script>
$(document).ready(function() 
{
    var sort_filters = [<?PHP 
                        $i=0;
                        foreach ($bunker_supply_view->sort_orders as $order) 
                        {
                            if ($i==0)
                            {
                                echo "'$order'";
                            } else echo ",'$order'";
                            $i=$i+1;
                        }?>];
    $('input.sort_order').val(sort_filters.toString());
    $('form').find('a.toggle_sort').each( function()
    {
        var sort_handle = $(this).closest('th').find('input.sort');
        if (sort_handle.val()=='ASC')
        {
            $(this).removeClass('fa-sort-desc');
            $(this).removeClass('fa-sort');
            $(this).addClass('fa-sort-asc');
        } else if (sort_handle.val()=='DESC')
        {
            $(this).removeClass('fa-sort-asc');
            $(this).removeClass('fa-sort');
            $(this).addClass('fa-sort-desc');
        } else
        {
            $(this).removeClass('fa-sort-desc');
            $(this).removeClass('fa-sort-asc');
            $(this).addClass('fa-sort');                
        }        
        $(this).click( function()
        {
            var sort_handle = $(this).closest('th').find('input.sort');
            if ($(this).hasClass('fa-sort-asc'))
            {
                sort_handle.val('DESC');
            } else
            if ($(this).hasClass('fa-sort-desc'))
            {
                sort_handle.val('');
            } else
            if ($(this).hasClass('fa-sort'))
            {
                sort_handle.val('ASC');             
            }


            if (sort_handle.hasClass('sort'))
            {
                var index = sort_filters.indexOf(sort_handle.attr('field'));
                if (index>=0)
                {
                    sort_filters.splice(index, 1);
                } 
                if ((sort_handle.val()=='ASC')||(sort_handle.val()=='DESC'))
                {
                    sort_filters.unshift(sort_handle.attr('field'));
                }
            }
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }
            
            var page=parseInt($('input.field_page').val());
            var url=$('#form_list').attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            loadPost(url_rest,$('#form_list'),'div.page');
            
        });
    });
    $('form').find('select.filter').each( function()
    {
        $(this).change( function()
        {
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }

            var page=parseInt($('input.field_page').val());
            var url=$('#form_list').attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            loadPost(url_rest,$('#form_list'),'div.page');
            
        });
    });    
	$('form').find('.search_text_button').each( function()
    {
        $(this).click( function()
        {
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }

            var page=parseInt($('input.field_page').val());
            var url=$('#form_list').attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            loadPost(url_rest,$('#form_list'),'div.page');
            
        });
    });   
    $("#clear_search_and_sort").click( function()
    {
        $("table.data").find("input.search_text").each( function()
        {
            $(this).val("");
        });
        $("table.data").find("select.filter").each( function()
        {
            $(this).val("");
        });			
        $("table.data").find("input.sort").each( function()
        {
            $(this).val("");
        });
        $("table.data").find("a.toglle_sort").each( function()
        {
            if ($(this).hasClass("fa-sort-asc"))
            {
                $(this).removeClass("fa-sort-asc");
            }
            if ($(this).hasClass("fa-sort-desc"))
            {
                $(this).removeClass("fa-sort-desc");
            }
            if ($(this).hasClass("fa-sort"))
            {
            
            }
            else
            {
                $(this).addClass("fa-sort-asc");
            }
            
            $(this).removeClass("fa-sort-asc");
        });
        
        if (sort_filters.length>0)
        {
            $('input.sort_order').val(sort_filters.toString());
        }

        var page=parseInt($('input.field_page').val());
        var url=$('#form_list').attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
        var url_rest = url.replace('display_php','display_rest');
        loadPost(url_rest,$('#form_list'),'div.page');        
    });         
});
</script>
<?php
}?>
