<?php
/**  
 * Used to list all Information
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */
require_once 'Information.php';
require_once 'InformationView.php';
require_once 'Role.php'; 
if (!isset($allowed)) $allowed=array("admin");
if (!isset($allowed_edit)) $allowed_edit=array("admin");
$role = new Role();
if (!$role->isUserAnyOf($allowed))
{

	$role->showUnauthorizedMessage($allowed,"?command=display_php&base_path=usr/view&path=List Informations.rest.php");
} else
{
if (!isset($information_view)) 
{ 
    $information_view = new InformationView();
    $information_view->init_list();
} 
 if (!isset($headers)) $headers = ["id","user_id","name","salt","active","created"];
 if (array_key_exists("informations_container",$_REQUEST)) $information_view->container = $_REQUEST["informations_container"];
?>
<form id='<?php echo $information_view->form_list;?>' <?php echo $information_view->list_form_attributes;?> class="form list" action="<?php echo $information_view->action;?>" method="POST">
<input type='hidden' name='informations_container' value='<?php echo $information_view->container;?>' />
<?php $information_view->renderCsrf();?>
<?php $information_view->renderCustomList();?>
<table class='odd_even_row_alternate_color full_width data' >
<input class='field_pages' field_pages='<?php echo $information_view->pages; ?>' type='hidden' value='<?php echo $information_view->pages; ?>' name='informations_pages' />
<input class='field_page' field_page='<?php echo $information_view->page; ?>' type='hidden' value='<?php echo $information_view->page; ?>' name='informations_page' /> 
<thead><tr><th>#</th><th>Actions</th>
<?php if (in_array('id',$headers)) { ?> <th id='header_id'  field='id'><a class='toggle_sort fa fa-sort' href='#'> id</a>             <input id="informations_sort_id" type="hidden" field="id" class="sort" name="informations_sort_filters[id]" value='<?php echo strtoupper($information_view->sort_filters['id']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('user_id',$headers)) { ?> <th id='header_user_id'  field='user_id'><a class='toggle_sort fa fa-sort' href='#'> users</a>             <input id="informations_sort_user_id" type="hidden" field="user_id" class="sort" name="informations_sort_filters[user_id]" value='<?php echo strtoupper($information_view->sort_filters['user_id']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('name',$headers)) { ?> <th id='header_name'  field='name'><a class='toggle_sort fa fa-sort' href='#'> name</a>             <input id="informations_sort_name" type="hidden" field="name" class="sort" name="informations_sort_filters[name]" value='<?php echo strtoupper($information_view->sort_filters['name']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('value',$headers)) { ?> <th id='header_value'  field='value'> value <?php } ?>
<?php if (in_array('token',$headers)) { ?> <th id='header_token'  field='token'> token <?php } ?>
<?php if (in_array('salt',$headers)) { ?> <th id='header_salt'  field='salt'><a class='toggle_sort fa fa-sort' href='#'> salt</a>             <input id="informations_sort_salt" type="hidden" field="salt" class="sort" name="informations_sort_filters[salt]" value='<?php echo strtoupper($information_view->sort_filters['salt']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('active',$headers)) { ?> <th id='header_active'  field='active'><a class='toggle_sort fa fa-sort' href='#'> active</a>             <input id="informations_sort_active" type="hidden" field="active" class="sort" name="informations_sort_filters[active]" value='<?php echo strtoupper($information_view->sort_filters['active']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('created',$headers)) { ?> <th id='header_created'  field='created'><a class='toggle_sort fa fa-sort' href='#'> created</a>             <input id="informations_sort_created" type="hidden" field="created" class="sort" name="informations_sort_filters[created]" value='<?php echo strtoupper($information_view->sort_filters['created']);?>' style="display:none;">
</th> <?php } ?>
</tr>
<tr><th></th><th><button id="clear_search_and_sort" type="button" class="button"><i class="fa fa-times"></i> <span style="color:#DDDDDD;text-shadow:none"><i class="fa fa-sort"><i class="fa fa-search"></span></button></th><?php if (in_array('id',$headers)) { ?> <th><?php $information_view->renderSearchText('id','number'); ?></th> <?php } ?>
<?php if (in_array('user_id',$headers)) { ?><th><?php $information_view->renderUserIdFilter(); ?></th> <?php }?>
<?php if (in_array('name',$headers)) { ?> <th><?php $information_view->renderSearchText('name','text'); ?></th> <?php } ?>
<?php if (in_array('value',$headers)) { ?><th></th> <?php }?>
<?php if (in_array('token',$headers)) { ?><th></th> <?php }?>
<?php if (in_array('salt',$headers)) { ?> <th><?php $information_view->renderSearchText('salt','text'); ?></th> <?php } ?>
<?php if (in_array('active',$headers)) { ?> <th><?php $information_view->renderSearchText('active','number'); ?></th> <?php } ?>
<?php if (in_array('created',$headers)) { ?><th></th> <?php }?>
</tr></thead>
<tbody>
 <?php
    $current_user=new User();
    $id = $_SESSION["user_id"];
    $current_user=$current_user->find(array("id"=>$id));
    $i=($information_view->page-1)*$information_view->item_per_page;
    foreach ($information_view->informations as $information)
    { 
        $i=$i+1;
        $path_edit = "Edit Information";
?>
<tr class="id_<?php echo $information->getId();?>" ><td><?php echo $i ;?></td><td><?php $information_view->renderActions($information,"",$role->isUserAnyOf($allowed_edit));?></td><?php if (in_array('id',$headers)) { ?><td><a class='mx-' mx-container='<?php echo $information_view->container;?>' mx-click='?command=display_rest&base_path=Admin.dir/Informations.dir&path=Edit Information.hid.rest.php&id=<?php echo $information->getId();?>&object=Information' href='#' ><?php echo $information->getId();?></a></td> <?php }?>

<?php if (in_array('user_id',$headers)) { ?>
	<td><?php $information_view->getUserId($information);?></td> <?php }?>
<?php if (in_array('name',$headers)) { ?>
	<td><?php $information_view->getName($information);?></td> <?php }?>
<?php if (in_array('value',$headers)) { ?>
	<td><?php $information_view->getValue($information);?></td> <?php }?>
<?php if (in_array('token',$headers)) { ?>
	<td><?php $information_view->getToken($information);?></td> <?php }?>
<?php if (in_array('salt',$headers)) { ?>
	<td><?php $information_view->getSalt($information);?></td> <?php }?>
<?php if (in_array('active',$headers)) { ?>
	<td><?php $information_view->getActive($information);?></td> <?php }?>
<?php if (in_array('created',$headers)) { ?>
	<td><?php $information_view->getCreated($information);?></td> <?php }?>
</tr>
<?php } ?>
</tbody>
</table><input class="sort_order" type="hidden" name="informations_sort_order" value="" />
<?php $information_view->renderListControls(); ?>
</form>
<?php 
    $information_view->renderPaginator();
?>
<script>
$(document).ready(function() 
{
	var container = '<?php echo $information_view->container;?>';
	var form_list = '<?php echo $information_view->form_list;?>';
    
    var sort_filters = [<?PHP 
                        $i=0;
                        foreach ($information_view->sort_orders as $order) 
                        {
                            if ($i==0)
                            {
                                echo "'$order'";
                            } else echo ",'$order'";
                            $i=$i+1;
                        }?>];
    $('input.sort_order').val(sort_filters.toString());
    $('form').find('a.toggle_sort').each( function()
    {
        var sort_handle = $(this).closest('th').find('input.sort');
        if (sort_handle.val()=='ASC')
        {
            $(this).removeClass('fa-sort-desc');
            $(this).removeClass('fa-sort');
            $(this).addClass('fa-sort-asc');
        } else if (sort_handle.val()=='DESC')
        {
            $(this).removeClass('fa-sort-asc');
            $(this).removeClass('fa-sort');
            $(this).addClass('fa-sort-desc');
        } else
        {
            $(this).removeClass('fa-sort-desc');
            $(this).removeClass('fa-sort-asc');
            $(this).addClass('fa-sort');                
        }        
        $(this).click( function()
        {
            var sort_handle = $(this).closest('th').find('input.sort');
            if ($(this).hasClass('fa-sort-asc'))
            {
                sort_handle.val('DESC');
            } else
            if ($(this).hasClass('fa-sort-desc'))
            {
                sort_handle.val('');
            } else
            if ($(this).hasClass('fa-sort'))
            {
                sort_handle.val('ASC');             
            }


            if (sort_handle.hasClass('sort'))
            {
                var index = sort_filters.indexOf(sort_handle.attr('field'));
                if (index>=0)
                {
                    sort_filters.splice(index, 1);
                } 
                if ((sort_handle.val()=='ASC')||(sort_handle.val()=='DESC'))
                {
                    sort_filters.unshift(sort_handle.attr('field'));
                }
            }
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }
            
            var page=parseInt($('input.field_page').val());
            var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            //loadPost(url_rest,$('#'+form_list),container);
            loadToSelector(url_rest,container,'#'+form_list,'',loader_img);
        });
    });
    $('form').find('select.filter').each( function()
    {
        $(this).change( function()
        {
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }

            var page=parseInt($('input.field_page').val());
            var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            //loadPost(url_rest,$('#'+form_list),container);
            loadToSelector(url_rest,container,'#'+form_list,'',loader_img);
        });
    });    
	$('form').find('.search_text_button').each( function()
    {
        $(this).click( function()
        {
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }

            var page=parseInt($('input.field_page').val());
            var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            //loadPost(url_rest,$('#'+form_list),container);
            loadToSelector(url_rest,container,'#'+form_list,'',loader_img);
        });
    });   
    $("#clear_search_and_sort").click( function()
    {
        $("table.data").find("input.search_text").each( function()
        {
            $(this).val("");
        });
        $("table.data").find("select.filter").each( function()
        {
            $(this).val("");
        });			
        $("table.data").find("input.sort").each( function()
        {
            $(this).val("");
        });
        $("table.data").find("a.toglle_sort").each( function()
        {
            if ($(this).hasClass("fa-sort-asc"))
            {
                $(this).removeClass("fa-sort-asc");
            }
            if ($(this).hasClass("fa-sort-desc"))
            {
                $(this).removeClass("fa-sort-desc");
            }
            if ($(this).hasClass("fa-sort"))
            {
            
            }
            else
            {
                $(this).addClass("fa-sort-asc");
            }
            
            $(this).removeClass("fa-sort-asc");
        });
        
        if (sort_filters.length>0)
        {
            $('input.sort_order').val(sort_filters.toString());
        }

        var page=parseInt($('input.field_page').val());
        var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
        var url_rest = url.replace('display_php','display_rest');
        //loadPost(url_rest,$('#'+form_list),container);    
        loadToSelector(url_rest,container,'#'+form_list,'',loader_img);    
    });         
});
</script>
<?php
}?>
