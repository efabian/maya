<?php 
/**  
 * Used to edit individual Information
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link https://maya.docph.net/
 */
require_once 'Role.php'; 
if (!isset($allowed_edit)) $allowed_edit=array("admin");
if (!isset($allowed)) $allowed=array("admin");
$role = new Role();
if (!$role->isUserAnyOf($allowed))
{
	$role->showUnauthorizedMessage($allowed,"?command=display_php&base_path=usr/view&path=Edit Information.rest.php");
} else
{
if (!isset($information_view))
{
	require_once 'InformationView.php'; 
	$information_view = new InformationView(); 
	$information_view->init_edit();
}
?> 



<form id="edit_information" <?php echo $information_view->edit_form_attributes;?> class="edit" action="?command=edit&object=Information&base_path=<?php echo $_REQUEST["base_path"];?>&path=<?php echo $path;?>" method="POST">
<input class="field_pages" field_pages="1" type="hidden" value="1" name="pages" />
<?php $information_view->renderCsrf();?> 
<?php $information_view->renderCustomEdit();?> 
<table class='odd_even_row_alternate_color full_width data' >
<thead><th>Field</th><th>Data <?php if ($role->isUserAnyOf($allowed_edit)) {?><button type='button' id='toggle_edit' class='button' onclick='toggleEdit()'><li class='fa fa-pencil'></li> Enable Edit</button> <?php }?></th></thead>
<tbody>
<tr id='tr_id' ><td class='label'>Id </td><td id='td_id' ><input type='number' style='display:none' name='id' value='<?php echo $information_view->information->getId();?>' ></input><?php echo $information_view->information->getId();?></td></tr>
<?php
if (!isset($edit_remove_user_id))
{?>
<tr id='tr_user_id'><td class='label'>User </td><td id='td_user_id'><?php
    $edit_user_id=true;
    $information_view->renderUserIdSelect($information_view->information,isset($edit_user_id),isset($readonly_user_id),isset($hidden_user_id));
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_name))
{?>
<tr id='tr_name'><td class='label'>Name </td><td id='td_name'><?php
    $edit_name=true;
    if (!isset($name_attributes)) 
    { 
        $name_attributes=array("placeholder"=>"Name ");
    } 
    $information_view->renderName($information_view->information,isset($edit_name),isset($readonly_name),isset($hidden_name), $name_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_value))
{?>
<tr id='tr_value'><td class='label'>Value </td><td id='td_value'><?php
    $edit_value=true;
    if (!isset($value_attributes)) 
    { 
        $value_attributes=array("placeholder"=>"Value ");
    } 
    $information_view->renderValueTextarea($information_view->information,isset($edit_value),isset($readonly_value),isset($hidden_value),$value_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_token))
{?>
<tr id='tr_token'><td class='label'>Token </td><td id='td_token'><?php
    $edit_token=true;
    if (!isset($token_attributes)) 
    { 
        $token_attributes=array("placeholder"=>"Token ");
    } 
    $information_view->renderTokenTextarea($information_view->information,isset($edit_token),isset($readonly_token),isset($hidden_token),$token_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_salt))
{?>
<tr id='tr_salt'><td class='label'>Salt </td><td id='td_salt'><?php
    $edit_salt=true;
    if (!isset($salt_attributes)) 
    { 
        $salt_attributes=array("placeholder"=>"Salt ");
    } 
    $information_view->renderSalt($information_view->information,isset($edit_salt),isset($readonly_salt),isset($hidden_salt), $salt_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_active))
{?>
<tr id='tr_active'><td class='label'>Active </td><td id='td_active'><?php
    $edit_active=true;
    if (!isset($active_attributes)) 
    { 
        $active_attributes=array("placeholder"=>"Active ");
    } 
    $information_view->renderActive($information_view->information,isset($edit_active),isset($readonly_active),isset($hidden_active), $active_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_created))
{?>
<tr id='tr_created'><td class='label'>Created </td><td id='td_created'><?php echo $information_view->information->getCreated();?></td></tr>
<?php
} ?>
</tbody>
</table>
<div class="custom_widget">
<?php if ($information_custom_widget) include "$information_custom_widget"; ?>
</div>
<?php if ($role->isUserAnyOf($allowed_edit)) { $information_view->renderEditControls(); }?>
</form>
<?php
}?>
