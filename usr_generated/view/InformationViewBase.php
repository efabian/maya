<?php
require_once 'MayaSecurity.php';
require_once 'paths.cfg.php';
require_once 'Configurations.php';
require_once 'Information.php';
require_once 'User.php';
class InformationViewBase 
{ 
    public $security;
    public $information;
    public $page;
    public $path;
    public $informations;
    public $container;
    public $item_per_page;
    public $pages;
    public $search_text;
    public $select_filters;
    public $sort_filters;
    public $sort_orders;
    public function getUserId($information)
    {
         echo $information->getUser();
    }
    public function getName($information)
    {
        echo $information->getName();
    }
    public function getValue($information)
    {
        echo $information->getValue();
    }
    public function getToken($information)
    {
        echo $information->getToken();
    }
    public function getSalt($information)
    {
        echo $information->getSalt();
    }
    public function getActive($information)
    {
        echo $information->getActive();
    }
    public function getCreated($information)
    {
        echo $information->getCreated();
    }
    public function renderId($information,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$information->getId();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='id'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='id' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='id' value='".$value."' ></input>";
            }
        }
    }
    public function renderUserIdSelect($information,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'chosen'),$option_filters=array())
    {
        $sids_str=$information->getUserId();
        
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }
        if ($hidden || $readonly)
        {
            echo "<input type='hidden' $attrs name='user_id'  value='$sids_str' />";
            if ($readonly)
            {
                echo "<span >".$information->getUser()."</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<select $attrs class='$class edit_input'  name='user_id' >\n";
            } else
            {
                echo "<select $attrs class='$class'  name='user_id' >\n";
            }
            $selected_ids = explode(",",$sids_str);
            $user = new User();
            $objs=$user->findAll($option_filters);
            $data_names = array();
            foreach ($objs as $option)
            {
                $name = $option->getName();
                $oid = $option->getId();
                if ($oid)
                {
                    if (in_array($oid,$selected_ids))
                    {
                        echo "<option selected='selected' value='$oid' >$name</option>\n";
                        $data_names[] = $name;
                    } else
                    {
                        echo "<option value='$oid' >$name</option>\n";
                    }
                }
            }
            echo "</select>\n";
            if ($edit) echo "<span class='view_input'>".implode(',',$data_names)."</span>";
       }
    }
    public function renderUserIdFilter()
    {
		echo "<select  class='filter' name='informations_select_filters[user_id]' value='' > \r\n";
		echo "<option value=''>All</option> \r\n";
	
		if (in_array('0',$this->user_id_lst)) 
		{
			if (array_key_exists('user_id',$this->select_filters) && ($this->select_filters['user_id']=='0'))
			{
				echo "<option selected='selected' value='0' >Unassigned</option>";
			} else
			{
				echo "<option value='0' >Unassigned</option>";
			}
		}
		$user_handle = new User();
		$users = $user_handle->findAll(array('id'=>$this->user_id_lst),array('lastname'=>'ASC'));
		foreach ($users as $user)
		{
			if (($this->select_filters['user_id']) && ($this->select_filters['user_id']==$user->getId()))
			{
				echo "<option selected='selected' value='".$user->getId()."' >".$user->getName()."</option>";
			} else
			{
				echo "<option value='".$user->getId()."' >".$user->getName()."</option>";
			}
		}
		echo "</select> \r\n";		
	}
	
    public function renderName($information,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$information->getName();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='name'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='name' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='name' value='".$value."' ></input>";
            }
        }
    }
    public function renderValueTextarea($information,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'jwysiwyg'))
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$information->getValue();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='value'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<textarea id='edit_value' $attrs class='$class  edit_input' name='value' >".$value."</textarea>";                
            } else
            {
                echo "<textarea id='edit_value' $attrs type='text' class='$class ' name='value' >".$value."</textarea>";
            }
        }
    }
    public function renderTokenTextarea($information,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'jwysiwyg'))
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$information->getToken();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='token'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<textarea id='edit_token' $attrs class='$class  edit_input' name='token' >".$value."</textarea>";                
            } else
            {
                echo "<textarea id='edit_token' $attrs type='text' class='$class ' name='token' >".$value."</textarea>";
            }
        }
    }
    public function renderSalt($information,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$information->getSalt();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='salt'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='salt' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='salt' value='".$value."' ></input>";
            }
        }
    }
    public function renderActive($information,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$information->getActive();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='active'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='active' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='active' value='".$value."' ></input>";
            }
        }
    }
    public function __construct($param=array('container'=>'div.page'))
    {
        foreach($param as $field=>$value)
        {
            $this->$field=$value;
        }
        if (!isset($this->container) || (!$this->container))
        {
            $this->container='div.page';
        }
        $this->paths = new Paths();
        $this->config = new Configurations();
    }
    
    public function init_edit()
    { 
        $this->security=new MayaSecurity();
        if (array_key_exists("id",$_GET))
        {
            $this->id=$_GET["id"];
        } else
        {
            $this->id=$_REQUEST["id"];
        }
        $this->information = new Information();
        $this->information->find(array("id"=>$this->id));
        $this->page=$_REQUEST["selected"];
        $this->path=$_REQUEST["path"];
        $this->current_user=new User();
        $this->current_user->find(array("id"=>$_SESSION["user_id"]));
        $this->edit_form_attributes='';
        $this->edit_button_name='Update';
        $this->edit_button_url='?command=editRest&object=Information';
        $this->edit_button_next_url='?command=display_rest&path=Admin.dir/Informations.dir/Edit Information.hid.rest.php&id='.$this->information->getId();
        $this->edit_delete_button_url='?command=deleteRest&object=Information';
        $this->edit_delete_button_next_url='?command=display_rest&path=Admin.dir/Informations.dir/List Informations.rest.php';
    } 
    public function init_new()
    { 
        $this->security=new MayaSecurity();
        $this->information = new Information();
        $this->page=$_REQUEST["selected"];
        $this->path=$_REQUEST["path"];
        $this->current_user=new User();
        $this->current_user->find(array("id"=>$_SESSION["user_id"]));
        $this->new_form_attributes='';
        $this->new_button_name='Create';
        $this->new_button_url='?command=newRest&object=Information';
        $this->new_button_next_url='?command=display_rest&path=Admin.dir/Informations.dir/List Informations.rest.php';
    } 
    public function init_list()
    { 
        $this->security=new MayaSecurity();
        $valid_filters=array("id","user_id","name","value","token","salt","active","created");
        $this->page=$_REQUEST["informations_selected"];
        $this->path=$_REQUEST["informations_path"];
        $this->list_form_attributes='';
        $this->list_button_new_url='?command=display_rest&path=Admin.dir/Informations.dir/New Information.rest.php';
        $this->list_button_new_name='New';
		$this->form_list='form_list';
		$this->action='?command=display_php&path=Admin.dir/Informations.dir/List Informations.rest.php';
        if (!$this->container) $this->container='div.page';
        $information = new Information();
        if (!isset($_SESSION['list_informations']))
        {
            $_SESSION['list_informations']=array();
            $_SESSION['list_informations']['sort_filters']=array();
            $_SESSION['list_informations']['sort_order']='';
            $_SESSION['list_informations']['select_filters']=array();
            $_SESSION['list_informations']['search_texts']=array();
            $_SESSION['list_informations']['page']='1';
        }

        if (isset($_REQUEST['informations_sort_filters']))
        {
            $sort_lists = $_REQUEST['informations_sort_filters'];
            $_SESSION['list_informations']['sort_filters']=$sort_lists;
        }
        else
        {
            $sort_lists=$_SESSION['list_informations']['sort_filters'];
        }

        if (isset($_REQUEST['informations_sort_order']))
        {
            $sort_order = $_REQUEST['informations_sort_order'];
            $_SESSION['list_informations']['sort_order']=$sort_order;
        }
        else
        {
            $sort_order = $_SESSION['list_informations']['sort_order'];
        }
        
        if (!$sort_lists) 
        {
            $sort_lists = array('id'=>'DESC');
            $sort_order = 'id';
        }
        
        $this->sort_filters = array();
        if ($sort_order)
        {
            $this->sort_orders = explode(',',$sort_order);
            foreach ($this->sort_orders as $s_order)
            {
                $this->sort_filters[$s_order]=$sort_lists[$s_order];
            }
        } else $this->sort_orders = array();

		if (!isset($this->select_filters)) $this->select_filters=array();
        if (isset($_REQUEST['informations_select_filters']))
        {
            foreach($_REQUEST['informations_select_filters'] as $key=>$value)
            {
                if (!(is_numeric($key)) && in_array($key,$valid_filters))
                {
                    $this->select_filters[$key] = $value;
                }
            }
            $_SESSION['list_informations']['select_filters']=$this->select_filters;
        }
        else
        {
            $this->select_filters=array_merge($_SESSION['list_informations']['select_filters'],$this->select_filters);
        }
        
        


        $informations_all = $information->findAll(array(),array(),100000,1,array("user_id"),false);
        $this->user_id_lst=array();

        foreach ($informations_all as $item)
        {
                    $user_id=$item->getUserId();
        $this->user_id_lst[$user_id]=$user_id;

        }


        $search='';
        if (isset($_REQUEST['informations_search_texts']))
        {
            $this->search_texts = $_REQUEST['informations_search_texts'];
            $_SESSION['list_informations']['search_texts']=$this->search_texts;
        }
        else
        {
            $this->search_texts = $_SESSION['list_informations']['search_texts'];
        }
        if (is_array($this->search_texts))
        {
            foreach($this->search_texts as $field=>$value)
            {
                if (trim($value))
                {
                    if ($search)
                    {
                        $search.=" AND $field like '%$value%' ";
                    }
                    else
                    {
                        $search=" $field like '%$value%' ";
                    }
                }
            }
        }
        if ($search)
        {
            $this->select_filters[] = $search;
        }


        if (!($this->item_per_page)) $this->item_per_page = 30;
        $this->pages=$information->getPageCount($this->select_filters,$this->sort_filters,$this->item_per_page,true);
        
        $this->page = 1;//default to 1 in case not provided below
        if (isset($_REQUEST['informations_page']))
        {
            $this->page = $_REQUEST['informations_page'];
            $_SESSION['list_informations']['page']=$this->page;
        } else
        {
            $this->page = $_SESSION['list_informations']['page'];
            if (array_key_exists('list_informations',$_SESSION) && array_key_exists('page',$_SESSION['list_informations']))
            {
                $this->page = $_SESSION['list_informations']['page'];
            }            
        }
        if (($this->page < 1) || ($this->page > $this->pages)) $this->page = 1;

        
        $this->informations = $information->findAll($this->select_filters,$this->sort_filters,$this->item_per_page,$this->page);
    } 
    public function renderPaginator()
    { 
        include 'PaginatorView.php'; 
        include 'PaginatorController.php'; 
    } 
    public function renderCsrf()
    { 
        echo '<input type="hidden" name="'.$this->security->getCsrfName().'" value="'.$this->security->getCsrfValue().'"/>'; 
    } 
    public function renderActions($information,$class="",$edit=TRUE)
    {
        if ($edit)
        {
            echo "<a class='mx- $class' mx-container='".$this->container."' mx-click='?command=display_rest&base_path=Admin.dir/Informations.dir&path=Edit Information.hid.rest.php&id=".$information->getId()."&object=Information' href='#' ><li class='fa fa-pencil'></li> edit</a>"; 
        } else
        {
            echo "<a class='mx- $class' mx-container='".$this->container."' mx-click='?command=display_rest&base_path=Admin.dir/Informations.dir&path=View Information.hid.rest.php&id=".$information->getId()."&object=Information' href='#' ><li class='fa fa-eye'></li> view</a>"; 
        }
    }
    
	public function renderEditControls()
    {
        if ($this->edit_button_next_url)
        { 
            $next_after_update='mx-next="'.$this->edit_button_next_url.'"';
        } else $next_after_update='';
        if ($this->edit_delete_button_next_url)
        {
            $next_after_delete='mx-next="'.$this->edit_delete_button_next_url.'"';
        } else $next_after_delete='';
		echo '<div class="control" style="height:80px">';
		echo '<a class="button mx-" mx-click="'.$this->edit_button_url.'"  href="#" mx-container="div.page" '.$next_after_update.' ><li class="fa fa-save"></li> '.$this->edit_button_name.'</a>';
		echo '<a class="button mx- danger" mx-confirm="Are you sure to delete?" mx-click="'.$this->edit_delete_button_url.'" href="#" mx-container="'.$this->container.'" '.$next_after_delete.' ><li class="fa fa-trash" ></li> Delete </a>';
		echo '</div>';
	}    
	
	public function renderNewControls()
	{
        if ($this->new_button_next_url)
        {
            $next_after_create='mx-next="'.$this->new_button_next_url.'"';
        } else
        {
            $next_after_create='';
        }
		echo '<div class="control" style="height:80px">';
		echo '<a class="button mx-" mx-click="'.$this->new_button_url.'" href="#" '.$next_after_create.' mx-container="'.$this->container.'"><li class="fa fa-plus"></li> '.$this->new_button_name.' </a>';
		echo '</div>';
	}
    
    public function renderListControls($new_after_new='')
    {
        echo '<div class="control" style="height:80px">'."\r\n";
        echo '<a class="button mx-" mx-container="'.$this->container.'" mx-click="'.$this->list_button_new_url.'" href="#" ><i class="fa fa-plus"></i> '.$this->list_button_new_name.'</a>'."\r\n";
        echo '</div>'."\r\n";  
    }
    
    public function renderCustomEdit()
    {
    }

    public function renderCustomNew()
    {
    }

    public function renderCustomlist()
    {
    }
    
    public function renderSearchText($field,$type)
    {
		echo '<div class="search_filter_header">';
		$value='';
		if (isset($this->search_texts) && array_key_exists($field,$this->search_texts)) $value=$this->search_texts[$field];
		echo '<input class="search_text '.$type.'" type="text" name="informations_search_texts['.$field.']" value="'.$value.'"></input><a class="search_text_button" ><li class="fa fa-search"></li></a>';
		echo '</div>';
    }

}
