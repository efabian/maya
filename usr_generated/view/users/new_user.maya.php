<?php 
/**  
 * Used to create new User
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */
require_once 'Role.php'; 
if (!isset($allowed)) $allowed=array("admin");
$role = new Role();
if (!$role->isUserAnyOf($allowed))
{
	$role->showUnauthorizedMessage($allowed,"?command=display_php&base_path=usr/view&path=New User.rest.php");
} else
{
if (!isset($user_view)) 
{ 
    require_once 'UserView.php'; 
    $user_view = new UserView(); 
    $user_view->init_new();
} ?> 
<form id="new_user"  <?php echo $user_view->new_form_attributes;?> class="new" action="?command=new&object=User&base_path=<?php echo $_REQUEST["base_path"];?>&path=<?php echo $path;?>" method="POST">
<?php $user_view->renderCsrf();?> 
<?php $user_view->renderCustomNew();?> 
<table class='odd_even_row_alternate_color full_width data' >
<thead><th>Field</th><th>Data</th></thead>
<tbody>
<?php
if (!isset($new_remove_firstname))
{?>
<tr id='tr_firstname'><td class='label'>Firstname </td><td id='td_firstname'><?php
    if (!isset($firstname_attributes)) 
    { 
        $firstname_attributes=array("placeholder"=>"Firstname ");
    } 
    $user_view->renderFirstname($user_view->user,isset($edit_firstname),isset($readonly_firstname),isset($hidden_firstname),$firstname_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_lastname))
{?>
<tr id='tr_lastname'><td class='label'>Lastname </td><td id='td_lastname'><?php
    if (!isset($lastname_attributes)) 
    { 
        $lastname_attributes=array("placeholder"=>"Lastname ");
    } 
    $user_view->renderLastname($user_view->user,isset($edit_lastname),isset($readonly_lastname),isset($hidden_lastname),$lastname_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_email))
{?>
<tr id='tr_email'><td class='label'>Email </td><td id='td_email'><?php
    if (!isset($email_attributes)) 
    { 
        $email_attributes=array("placeholder"=>"Email ");
    } 
    $user_view->renderEmail($user_view->user,isset($edit_email),isset($readonly_email),isset($hidden_email),$email_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_profile))
{?>
<tr id='tr_profile'><td class='label'>Profile </td><td id='td_profile'><?php
    if (!isset($profile_attributes)) 
    { 
        $profile_attributes=array("placeholder"=>"Profile ");
    } 
    $user_view->renderProfileTextarea($user_view->user,isset($edit_profile),isset($readonly_profile),isset($hidden_profile),$profile_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_created))
{?>
<tr id='tr_created'><td class='label'>Created </td><td id='td_created'><?php echo $user_view->user->getCreated();?></td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_modified))
{?>
<tr id='tr_modified'><td class='label'>Modified </td><td id='td_modified'><?php $now = new DateTime("now");
echo $user_view->user->getModified();
echo '<input type="hidden" value="'.$now->format("Y-m-d H:i:s").'" >';?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_login))
{?>
<tr id='tr_login'><td class='label'>Login </td><td id='td_login'><?php
    if (!isset($login_attributes)) 
    { 
        $login_attributes=array("placeholder"=>"Login ");
    } 
    $user_view->renderLogin($user_view->user,isset($edit_login),isset($readonly_login),isset($hidden_login),$login_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_role))
{?>
<tr id='tr_role'><td class='label'>Role </td><td id='td_role'><?php
    if (!isset($role_attributes)) 
    { 
        $role_attributes=array("placeholder"=>"Role ");
    } 
    $user_view->renderRoleValue($user_view->user,isset($edit_role),isset($readonly_role),isset($hidden_role),$role_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_salt))
{?>
<tr id='tr_salt'><td class='label'>Salt </td><td id='td_salt'><?php
    if (!isset($salt_attributes)) 
    { 
        $salt_attributes=array("placeholder"=>"Salt ");
    } 
    $user_view->renderSalt($user_view->user,isset($edit_salt),isset($readonly_salt),isset($hidden_salt),$salt_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_ip))
{?>
<tr id='tr_ip'><td class='label'>Ip </td><td id='td_ip'><?php
    if (!isset($ip_attributes)) 
    { 
        $ip_attributes=array("placeholder"=>"Ip ");
    } 
    $user_view->renderIp($user_view->user,isset($edit_ip),isset($readonly_ip),isset($hidden_ip),$ip_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_updateip))
{?>
<tr id='tr_updateip'><td class='label'>Updateip </td><td id='td_updateip'><?php
    if (!isset($updateip_attributes)) 
    { 
        $updateip_attributes=array("placeholder"=>"Updateip ");
    } 
    $user_view->renderUpdateip($user_view->user,isset($edit_updateip),isset($readonly_updateip),isset($hidden_updateip),$updateip_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_loginip))
{?>
<tr id='tr_loginip'><td class='label'>Loginip </td><td id='td_loginip'><?php
    if (!isset($loginip_attributes)) 
    { 
        $loginip_attributes=array("placeholder"=>"Loginip ");
    } 
    $user_view->renderLoginip($user_view->user,isset($edit_loginip),isset($readonly_loginip),isset($hidden_loginip),$loginip_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_resetkey))
{?>
<tr id='tr_resetkey'><td class='label'>Resetkey </td><td id='td_resetkey'><?php
    if (!isset($resetkey_attributes)) 
    { 
        $resetkey_attributes=array("placeholder"=>"Resetkey ");
    } 
    $user_view->renderResetkey($user_view->user,isset($edit_resetkey),isset($readonly_resetkey),isset($hidden_resetkey),$resetkey_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_securekey))
{?>
<tr id='tr_securekey'><td class='label'>Securekey </td><td id='td_securekey'><?php
    if (!isset($securekey_attributes)) 
    { 
        $securekey_attributes=array("placeholder"=>"Securekey ");
    } 
    $user_view->renderSecurekeyTextarea($user_view->user,isset($edit_securekey),isset($readonly_securekey),isset($hidden_securekey),$securekey_attributes);
?>
</td></tr>
<?php
} ?>
<?php if (!isset($new_remove_password)) { ?>
<tr><td class='label' colspan='2'>Change Password</td></tr>
<tr><td class='label'>password</td><td><input id='profile_password' class='' type='password' name='password' value='' style='max-width:200px' ></input><button id='show_profile_password' type='button' class=' button' href='#' style='color:gray;background-color:white'> <i class='fa fa-eye'></i></button></td></tr>
<tr><td class='label'>confirm password</td><td><input id='profile_password_confirm' type='password' class='' name='confirm_password' value='' style='max-width:200px' ></input><span id='password_match' class=' fa fa-times' name='match' style='margin-left:10px;width:30px'></span></td></tr>
<script>
$( function()
{
	$("#show_profile_password").click( function()
	{
		showProfilePassword("profile_password");
	});
	
	$("#profile_password").keyup( function()
	{
		if ($("#profile_password").val()==$("#profile_password_confirm").val()) 
		{
			$("#password_match").removeClass("fa-times");
			$("#password_match").addClass("fa-check");
			$("#password_match").css("background-color:green");
			
		} else
		{
			$("#password_match").removeClass("fa-check");
			$("#password_match").addClass("fa-times");
			$("#password_match").css("background-color:red");
		}
		
	});
	
	$("#profile_password_confirm").keyup( function()
	{
		if ($("#profile_password").val()==$("#profile_password_confirm").val()) 
		{
			$("#password_match").removeClass("fa-times");
			$("#password_match").addClass("fa-check");
			$("#password_match").css("background-color:green");
			
		} else
		{
			$("#password_match").removeClass("fa-check");
			$("#password_match").addClass("fa-times");
			$("#password_match").css("background-color:red");
		}
	});
	
});
function showProfilePassword(id) 
{
	var pw = document.getElementById(id);
	var pwc = document.getElementById(id+"_confirm");
	if (pw.type === "password") 
	{
		pw.type = "text";
		pwc.type = "text";
		setTimeout(function() { showProfilePassword(id); },1500);
	} else 
	{
		pw.type = "password";
		pwc.type = "password";
	}
}
</script>
<?php } ?>
</tbody>
</table><div class="custom_widget">
<?php if ($user_custom_widget) include "$user_custom_widget"; ?>
</div>
<?php $user_view->renderNewControls(); ?>
</form>
</div>
<?php
}?>
