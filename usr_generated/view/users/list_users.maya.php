<?php
/**  
 * Used to list all User
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */
require_once 'User.php';
require_once 'UserView.php';
require_once 'Role.php'; 
if (!isset($allowed)) $allowed=array("admin");
if (!isset($allowed_edit)) $allowed_edit=array("admin");
$role = new Role();
if (!$role->isUserAnyOf($allowed))
{

	$role->showUnauthorizedMessage($allowed,"?command=display_php&base_path=usr/view&path=List Users.rest.php");
} else
{
if (!isset($user_view)) 
{ 
    $user_view = new UserView();
    $user_view->init_list();
} 
 if (!isset($headers)) $headers = ["id","firstname","lastname","email","profile","created","modified","login","role","salt","password","ip","updateip","loginip","resetkey","securekey"];
 if (array_key_exists("users_container",$_REQUEST)) $user_view->container = $_REQUEST["users_container"];
?>
<form id='<?php echo $user_view->form_list;?>' <?php echo $user_view->list_form_attributes;?> class="form list" action="<?php echo $user_view->action;?>" method="POST">
<input type='hidden' name='users_container' value='<?php echo $user_view->container;?>' />
<?php $user_view->renderCsrf();?>
<?php $user_view->renderCustomList();?>
<table class='odd_even_row_alternate_color full_width data' >
<input class='field_pages' field_pages='<?php echo $user_view->pages; ?>' type='hidden' value='<?php echo $user_view->pages; ?>' name='users_pages' />
<input class='field_page' field_page='<?php echo $user_view->page; ?>' type='hidden' value='<?php echo $user_view->page; ?>' name='users_page' /> 
<thead><tr><th>#</th><th>Actions</th>
<?php if (in_array('id',$headers)) { ?> <th id='header_id'  field='id'><a class='toggle_sort fa fa-sort' href='#'> id</a>             <input id="users_sort_id" type="hidden" field="id" class="sort" name="users_sort_filters[id]" value='<?php echo strtoupper($user_view->sort_filters['id']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('icon',$headers)) echo '<th>Icon</th>';?>
<?php if (in_array('firstname',$headers)) { ?> <th id='header_firstname'  field='firstname'><a class='toggle_sort fa fa-sort' href='#'> firstname</a>             <input id="users_sort_firstname" type="hidden" field="firstname" class="sort" name="users_sort_filters[firstname]" value='<?php echo strtoupper($user_view->sort_filters['firstname']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('lastname',$headers)) { ?> <th id='header_lastname'  field='lastname'><a class='toggle_sort fa fa-sort' href='#'> lastname</a>             <input id="users_sort_lastname" type="hidden" field="lastname" class="sort" name="users_sort_filters[lastname]" value='<?php echo strtoupper($user_view->sort_filters['lastname']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('email',$headers)) { ?> <th id='header_email'  field='email'><a class='toggle_sort fa fa-sort' href='#'> email</a>             <input id="users_sort_email" type="hidden" field="email" class="sort" name="users_sort_filters[email]" value='<?php echo strtoupper($user_view->sort_filters['email']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('profile',$headers)) { ?> <th id='header_profile'  field='profile'><a class='toggle_sort fa fa-sort' href='#'> profile</a>             <input id="users_sort_profile" type="hidden" field="profile" class="sort" name="users_sort_filters[profile]" value='<?php echo strtoupper($user_view->sort_filters['profile']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('created',$headers)) { ?> <th id='header_created'  field='created'><a class='toggle_sort fa fa-sort' href='#'> created</a>             <input id="users_sort_created" type="hidden" field="created" class="sort" name="users_sort_filters[created]" value='<?php echo strtoupper($user_view->sort_filters['created']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('modified',$headers)) { ?> <th id='header_modified'  field='modified'><a class='toggle_sort fa fa-sort' href='#'> modified</a>             <input id="users_sort_modified" type="hidden" field="modified" class="sort" name="users_sort_filters[modified]" value='<?php echo strtoupper($user_view->sort_filters['modified']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('login',$headers)) { ?> <th id='header_login'  field='login'><a class='toggle_sort fa fa-sort' href='#'> login</a>             <input id="users_sort_login" type="hidden" field="login" class="sort" name="users_sort_filters[login]" value='<?php echo strtoupper($user_view->sort_filters['login']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('role',$headers)) { ?> <th id='header_role'  field='role'><a class='toggle_sort fa fa-sort' href='#'> role</a>             <input id="users_sort_role" type="hidden" field="role" class="sort" name="users_sort_filters[role]" value='<?php echo strtoupper($user_view->sort_filters['role']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('salt',$headers)) { ?> <th id='header_salt'  field='salt'><a class='toggle_sort fa fa-sort' href='#'> salt</a>             <input id="users_sort_salt" type="hidden" field="salt" class="sort" name="users_sort_filters[salt]" value='<?php echo strtoupper($user_view->sort_filters['salt']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('password',$headers)) { ?> <th id='header_password'  field='password'><a class='toggle_sort fa fa-sort' href='#'> password</a>             <input id="users_sort_password" type="hidden" field="password" class="sort" name="users_sort_filters[password]" value='<?php echo strtoupper($user_view->sort_filters['password']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('ip',$headers)) { ?> <th id='header_ip'  field='ip'><a class='toggle_sort fa fa-sort' href='#'> ip</a>             <input id="users_sort_ip" type="hidden" field="ip" class="sort" name="users_sort_filters[ip]" value='<?php echo strtoupper($user_view->sort_filters['ip']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('updateip',$headers)) { ?> <th id='header_updateip'  field='updateip'><a class='toggle_sort fa fa-sort' href='#'> updateip</a>             <input id="users_sort_updateip" type="hidden" field="updateip" class="sort" name="users_sort_filters[updateip]" value='<?php echo strtoupper($user_view->sort_filters['updateip']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('loginip',$headers)) { ?> <th id='header_loginip'  field='loginip'><a class='toggle_sort fa fa-sort' href='#'> loginip</a>             <input id="users_sort_loginip" type="hidden" field="loginip" class="sort" name="users_sort_filters[loginip]" value='<?php echo strtoupper($user_view->sort_filters['loginip']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('resetkey',$headers)) { ?> <th id='header_resetkey'  field='resetkey'><a class='toggle_sort fa fa-sort' href='#'> resetkey</a>             <input id="users_sort_resetkey" type="hidden" field="resetkey" class="sort" name="users_sort_filters[resetkey]" value='<?php echo strtoupper($user_view->sort_filters['resetkey']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('securekey',$headers)) { ?> <th id='header_securekey'  field='securekey'><a class='toggle_sort fa fa-sort' href='#'> securekey</a>             <input id="users_sort_securekey" type="hidden" field="securekey" class="sort" name="users_sort_filters[securekey]" value='<?php echo strtoupper($user_view->sort_filters['securekey']);?>' style="display:none;">
</th> <?php } ?>
</tr>
<tr><th></th><th><button id="clear_search_and_sort" type="button" class="button"><i class="fa fa-times"></i> <span style="color:#DDDDDD;text-shadow:none"><i class="fa fa-sort"><i class="fa fa-search"></span></button></th><?php if (in_array('id',$headers)) { ?> <th><?php $user_view->renderSearchText('id','number'); ?></th> <?php } ?>
<?php if (in_array('icon',$headers)) echo '<th></th>';?>
<?php if (in_array('firstname',$headers)) { ?> <th><?php $user_view->renderSearchText('firstname','text'); ?></th> <?php } ?>
<?php if (in_array('lastname',$headers)) { ?> <th><?php $user_view->renderSearchText('lastname','text'); ?></th> <?php } ?>
<?php if (in_array('email',$headers)) { ?> <th><?php $user_view->renderSearchText('email','text'); ?></th> <?php } ?>
<?php if (in_array('profile',$headers)) { ?> <th><?php $user_view->renderSearchText('profile','textarea'); ?></th> <?php } ?>
<?php if (in_array('created',$headers)) { ?><th></th> <?php }?>
<?php if (in_array('modified',$headers)) { ?><th></th> <?php }?>
<?php if (in_array('login',$headers)) { ?><th></th> <?php }?>
<?php if (in_array('role',$headers)) { ?><th><?php $user_view->renderRoleFilter(); ?></th> <?php }?>
<?php if (in_array('salt',$headers)) { ?> <th><?php $user_view->renderSearchText('salt','text'); ?></th> <?php } ?>
<?php if (in_array('password',$headers)) { ?><th></th> <?php }?>
<?php if (in_array('ip',$headers)) { ?> <th><?php $user_view->renderSearchText('ip','text'); ?></th> <?php } ?>
<?php if (in_array('updateip',$headers)) { ?> <th><?php $user_view->renderSearchText('updateip','text'); ?></th> <?php } ?>
<?php if (in_array('loginip',$headers)) { ?> <th><?php $user_view->renderSearchText('loginip','text'); ?></th> <?php } ?>
<?php if (in_array('resetkey',$headers)) { ?> <th><?php $user_view->renderSearchText('resetkey','text'); ?></th> <?php } ?>
<?php if (in_array('securekey',$headers)) { ?> <th><?php $user_view->renderSearchText('securekey','textarea'); ?></th> <?php } ?>
</tr></thead>
<tbody>
 <?php
    $current_user=new User();
    $id = $_SESSION["user_id"];
    $current_user=$current_user->find(array("id"=>$id));
    $i=($user_view->page-1)*$user_view->item_per_page;
    foreach ($user_view->users as $user)
    { 
        $i=$i+1;
        $path_edit = "Edit User";
?>
<tr class="id_<?php echo $user->getId();?>" ><td><?php echo $i ;?></td><td><?php $user_view->renderActions($user,"",$role->isUserAnyOf($allowed_edit));?></td><?php if (in_array('id',$headers)) { ?><td><a class='mx-' mx-container='<?php echo $user_view->container;?>' mx-click='?command=display_rest&base_path=Admin.dir/Users.dir&path=Edit User.hid.rest.php&id=<?php echo $user->getId();?>&object=User' href='#' ><?php echo $user->getId();?></a></td> <?php }?>
<?php if (in_array("icon",$headers)) {?><td><a class="mx-" mx-container="<?php echo $user_view->container;?>" mx-click="?command=display_rest&path=<?php echo $user_view->paths->admin;?>/Users.dir/Edit User.hid.rest.php&id=<?php echo $user->getId();?>&object=User" ><img class='profile_icon' src='img/displaypng.php?id=<?php echo $id;?>&image=<?php echo $user->getImageIcon();?>&key=<?php echo urlencode($current_user->getSecurekey());?>' /></a></td>
<?php }?>
<?php if (in_array('firstname',$headers)) { ?>
	<td><?php $user_view->getFirstname($user);?></td> <?php }?>
<?php if (in_array('lastname',$headers)) { ?>
	<td><?php $user_view->getLastname($user);?></td> <?php }?>
<?php if (in_array('email',$headers)) { ?>
	<td><?php $user_view->getEmail($user);?></td> <?php }?>
<?php if (in_array('profile',$headers)) { ?>
	<td><?php $user_view->getProfile($user);?></td> <?php }?>
<?php if (in_array('created',$headers)) { ?>
	<td><?php $user_view->getCreated($user);?></td> <?php }?>
<?php if (in_array('modified',$headers)) { ?>
	<td><?php $user_view->getModified($user);?></td> <?php }?>
<?php if (in_array('login',$headers)) { ?>
	<td><?php $user_view->getLogin($user);?></td> <?php }?>
<?php if (in_array('role',$headers)) { ?>
	<td><?php $user_view->getRole($user);?></td> <?php }?>
<?php if (in_array('salt',$headers)) { ?>
	<td><?php $user_view->getSalt($user);?></td> <?php }?>
<?php if (in_array('password',$headers)) { ?>
	<td><?php $user_view->getPassword($user);?></td> <?php }?>
<?php if (in_array('ip',$headers)) { ?>
	<td><?php $user_view->getIp($user);?></td> <?php }?>
<?php if (in_array('updateip',$headers)) { ?>
	<td><?php $user_view->getUpdateip($user);?></td> <?php }?>
<?php if (in_array('loginip',$headers)) { ?>
	<td><?php $user_view->getLoginip($user);?></td> <?php }?>
<?php if (in_array('resetkey',$headers)) { ?>
	<td><?php $user_view->getResetkey($user);?></td> <?php }?>
<?php if (in_array('securekey',$headers)) { ?>
	<td><?php $user_view->getSecurekey($user);?></td> <?php }?>
</tr>
<?php } ?>
</tbody>
</table><input class="sort_order" type="hidden" name="users_sort_order" value="" />
<?php $user_view->renderListControls(); ?>
</form>
<?php 
    $user_view->renderPaginator();
?>
<script>
$(document).ready(function() 
{
	var container = '<?php echo $user_view->container;?>';
	var form_list = '<?php echo $user_view->form_list;?>';
    
    var sort_filters = [<?PHP 
                        $i=0;
                        foreach ($user_view->sort_orders as $order) 
                        {
                            if ($i==0)
                            {
                                echo "'$order'";
                            } else echo ",'$order'";
                            $i=$i+1;
                        }?>];
    $('input.sort_order').val(sort_filters.toString());
    $('form').find('a.toggle_sort').each( function()
    {
        var sort_handle = $(this).closest('th').find('input.sort');
        if (sort_handle.val()=='ASC')
        {
            $(this).removeClass('fa-sort-desc');
            $(this).removeClass('fa-sort');
            $(this).addClass('fa-sort-asc');
        } else if (sort_handle.val()=='DESC')
        {
            $(this).removeClass('fa-sort-asc');
            $(this).removeClass('fa-sort');
            $(this).addClass('fa-sort-desc');
        } else
        {
            $(this).removeClass('fa-sort-desc');
            $(this).removeClass('fa-sort-asc');
            $(this).addClass('fa-sort');                
        }        
        $(this).click( function()
        {
            var sort_handle = $(this).closest('th').find('input.sort');
            if ($(this).hasClass('fa-sort-asc'))
            {
                sort_handle.val('DESC');
            } else
            if ($(this).hasClass('fa-sort-desc'))
            {
                sort_handle.val('');
            } else
            if ($(this).hasClass('fa-sort'))
            {
                sort_handle.val('ASC');             
            }


            if (sort_handle.hasClass('sort'))
            {
                var index = sort_filters.indexOf(sort_handle.attr('field'));
                if (index>=0)
                {
                    sort_filters.splice(index, 1);
                } 
                if ((sort_handle.val()=='ASC')||(sort_handle.val()=='DESC'))
                {
                    sort_filters.unshift(sort_handle.attr('field'));
                }
            }
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }
            
            var page=parseInt($('input.field_page').val());
            var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            loadPost(url_rest,$('#'+form_list),container);
            
        });
    });
    $('form').find('select.filter').each( function()
    {
        $(this).change( function()
        {
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }

            var page=parseInt($('input.field_page').val());
            var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            loadPost(url_rest,$('#'+form_list),container);
            
        });
    });    
	$('form').find('.search_text_button').each( function()
    {
        $(this).click( function()
        {
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }

            var page=parseInt($('input.field_page').val());
            var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            loadPost(url_rest,$('#'+form_list),container);
            
        });
    });   
    $("#clear_search_and_sort").click( function()
    {
        $("table.data").find("input.search_text").each( function()
        {
            $(this).val("");
        });
        $("table.data").find("select.filter").each( function()
        {
            $(this).val("");
        });			
        $("table.data").find("input.sort").each( function()
        {
            $(this).val("");
        });
        $("table.data").find("a.toglle_sort").each( function()
        {
            if ($(this).hasClass("fa-sort-asc"))
            {
                $(this).removeClass("fa-sort-asc");
            }
            if ($(this).hasClass("fa-sort-desc"))
            {
                $(this).removeClass("fa-sort-desc");
            }
            if ($(this).hasClass("fa-sort"))
            {
            
            }
            else
            {
                $(this).addClass("fa-sort-asc");
            }
            
            $(this).removeClass("fa-sort-asc");
        });
        
        if (sort_filters.length>0)
        {
            $('input.sort_order').val(sort_filters.toString());
        }

        var page=parseInt($('input.field_page').val());
        var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
        var url_rest = url.replace('display_php','display_rest');
        loadPost(url_rest,$('#'+form_list),container);        
    });         
});
</script>
<?php
}?>
