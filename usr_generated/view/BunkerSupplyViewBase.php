<?php
require_once 'MayaSecurity.php';
require_once 'paths.cfg.php';
require_once 'Configurations.php';
require_once 'BunkerSupply.php';
require_once 'User.php';
class BunkerSupplyViewBase 
{ 
    public $security;
    public $bunker_supply;
    public $page;
    public $path;
    public $bunker_supplies;
    public $container;
    public $item_per_page;
    public $pages;
    public $search_text;
    public $select_filters;
    public $sort_filters;
    public $sort_orders;
    public function getName($bunker_supply)
    {
        echo $bunker_supply->getName();
    }
    public function getDescription($bunker_supply)
    {
        echo $bunker_supply->getDescription();
    }
    public function getUserId($bunker_supply)
    {
         echo $bunker_supply->getUser();
    }
    public function getExpiryDate($bunker_supply)
    {
        echo $bunker_supply->getExpiryDate();
    }
    public function getCreated($bunker_supply)
    {
        echo $bunker_supply->getCreated();
    }
    public function renderId($bunker_supply,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$bunker_supply->getId();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='id'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='id' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='id' value='".$value."' ></input>";
            }
        }
    }
    public function renderName($bunker_supply,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$bunker_supply->getName();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='name'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='name' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='name' value='".$value."' ></input>";
            }
        }
    }
    public function renderDescriptionTextarea($bunker_supply,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'jwysiwyg'))
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$bunker_supply->getDescription();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='description'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<textarea id='edit_description' $attrs class='$class  edit_input' name='description' >".$value."</textarea>";                
            } else
            {
                echo "<textarea id='edit_description' $attrs type='text' class='$class ' name='description' >".$value."</textarea>";
            }
        }
    }
    public function renderUserIdSelect($bunker_supply,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'chosen'),$option_filters=array())
    {
        $sids_str=$bunker_supply->getUserId();
        
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }
        if ($hidden || $readonly)
        {
            echo "<input type='hidden' $attrs name='user_id'  value='$sids_str' />";
            if ($readonly)
            {
                echo "<span >".$bunker_supply->getUser()."</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<select $attrs class='$class edit_input'  name='user_id' >\n";
            } else
            {
                echo "<select $attrs class='$class'  name='user_id' >\n";
            }
            $selected_ids = explode(",",$sids_str);
            $user = new User();
            $objs=$user->findAll($option_filters);
            $data_names = array();
            foreach ($objs as $option)
            {
                $name = $option->getName();
                $oid = $option->getId();
                if ($oid)
                {
                    if (in_array($oid,$selected_ids))
                    {
                        echo "<option selected='selected' value='$oid' >$name</option>\n";
                        $data_names[] = $name;
                    } else
                    {
                        echo "<option value='$oid' >$name</option>\n";
                    }
                }
            }
            echo "</select>\n";
            if ($edit) echo "<span class='view_input'>".implode(',',$data_names)."</span>";
       }
    }
    public function renderUserIdFilter()
    {
		echo "<select  class='filter' name='select_filters[user_id]' value='' > \r\n";
		echo "<option value=''>All</option> \r\n";
	
		if (in_array('0',$this->user_id_lst)) 
		{
			if (array_key_exists('user_id',$this->select_filters) && ($this->select_filters['user_id']=='0'))
			{
				echo "<option selected='selected' value='0' >Unassigned</option>";
			} else
			{
				echo "<option value='0' >Unassigned</option>";
			}
		}
		$user_handle = new User();
		$users = $user_handle->findAll(array('id'=>$this->user_id_lst),array('lastname'=>'ASC'));
		foreach ($users as $user)
		{
			if (($this->select_filters['user_id']) && ($this->select_filters['user_id']==$user->getId()))
			{
				echo "<option selected='selected' value='".$user->getId()."' >".$user->getName()."</option>";
			} else
			{
				echo "<option value='".$user->getId()."' >".$user->getName()."</option>";
			}
		}
		echo "</select> \r\n";		
	}
	
    public function renderExpiryDate($bunker_supply,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$bunker_supply->getExpiryDate();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='expiry_date'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class date edit_input' name='expiry_date' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class date' name='expiry_date' value='".$value."' ></input>";
            }
        }
    }
    public function __construct($param=array('container'=>'div.page'))
    {
        foreach($param as $field=>$value)
        {
            $this->$field=$value;
        }
        if (!isset($this->container) || (!$this->container))
        {
            $this->container='div.page';
        }
        $this->paths = new Paths();
        $this->config = new Configurations();
    }
    
    public function init_edit()
    { 
        $this->security=new MayaSecurity();
        if (array_key_exists("id",$_GET))
        {
            $this->id=$_GET["id"];
        } else
        {
            $this->id=$_REQUEST["id"];
        }
        $this->bunker_supply = new BunkerSupply();
        $this->bunker_supply->find(array("id"=>$this->id));
        $this->page=$_REQUEST["selected"];
        $this->path=$_REQUEST["path"];
        $this->current_user=new User();
        $this->current_user->find(array("id"=>$_SESSION["user_id"]));
        $this->edit_form_attributes='';
        $this->edit_button_name='Update';
        $this->edit_button_url='?command=editRest&object=BunkerSupply';
        $this->edit_button_next_url='?command=display_rest&path=Admin.dir/BunkerSupplies.dir/Edit BunkerSupply.hid.rest.php&id='.$this->bunker_supply->getId();
        $this->edit_delete_button_url='?command=deleteRest&object=BunkerSupply';
        $this->edit_delete_button_next_url='?command=display_rest&path=Admin.dir/BunkerSupplies.dir/List BunkerSupplies.rest.php';
    } 
    public function init_new()
    { 
        $this->security=new MayaSecurity();
        $this->bunker_supply = new BunkerSupply();
        $this->page=$_REQUEST["selected"];
        $this->path=$_REQUEST["path"];
        $this->current_user=new User();
        $this->current_user->find(array("id"=>$_SESSION["user_id"]));
        $this->new_form_attributes='';
        $this->new_button_name='Create';
        $this->new_button_url='?command=newRest&object=BunkerSupply';
        $this->new_button_next_url='?command=display_rest&path=Admin.dir/BunkerSupplies.dir/List BunkerSupplies.rest.php';
    } 
    public function init_list()
    { 
        $this->security=new MayaSecurity();
        $valid_filters=array("id","name","description","user_id","expiry_date","created");
        $this->page=$_REQUEST["selected"];
        $this->path=$_REQUEST["path"];
        $this->list_form_attributes='';
        $this->list_button_new_url='?command=display_rest&path=Admin.dir/BunkerSupplies.dir/New BunkerSupply.rest.php';
        $this->list_button_new_name='New';

        if (!$this->container) $this->container='div.page';
        $bunker_supply = new BunkerSupply();
        if (!isset($_SESSION['list_bunker_supplies']))
        {
            $_SESSION['list_bunker_supplies']=array();
            $_SESSION['list_bunker_supplies']['sort_filters']=array();
            $_SESSION['list_bunker_supplies']['sort_order']='';
            $_SESSION['list_bunker_supplies']['select_filters']=array();
            $_SESSION['list_bunker_supplies']['search_texts']=array();
            $_SESSION['list_bunker_supplies']['page']='1';
        }

        if (isset($_REQUEST['sort_filters']))
        {
            $sort_lists = $_REQUEST['sort_filters'];
            $_SESSION['list_bunker_supplies']['sort_filters']=$sort_lists;
        }
        else
        {
            $sort_lists=$_SESSION['list_bunker_supplies']['sort_filters'];
        }

        if (isset($_REQUEST['sort_order']))
        {
            $sort_order = $_REQUEST['sort_order'];
            $_SESSION['list_bunker_supplies']['sort_order']=$sort_order;
        }
        else
        {
            $sort_order = $_SESSION['list_bunker_supplies']['sort_order'];
        }
        
        if (!$sort_lists) 
        {
            $sort_lists = array('id'=>'DESC');
            $sort_order = 'id';
        }
        
        $this->sort_filters = array();
        if ($sort_order)
        {
            $this->sort_orders = explode(',',$sort_order);
            foreach ($this->sort_orders as $s_order)
            {
                $this->sort_filters[$s_order]=$sort_lists[$s_order];
            }
        } else $this->sort_orders = array();

		if (!isset($this->select_filters)) $this->select_filters=array();
        if (isset($_REQUEST['select_filters']))
        {
            foreach($_REQUEST['select_filters'] as $key=>$value)
            {
                if (!(is_numeric($key)) && in_array($key,$valid_filters))
                {
                    $this->select_filters[$key] = $value;
                }
            }
            $_SESSION['list_bunker_supplies']['select_filters']=$this->select_filters;
        }
        else
        {
            $this->select_filters=$_SESSION['list_bunker_supplies']['select_filters'];
        }
        
        


        $bunker_supplies_all = $bunker_supply->findAll(array(),array(),100000,1,array("user_id"),false);
        $this->user_id_lst=array();

        foreach ($bunker_supplies_all as $item)
        {
                    $user_id=$item->getUserId();
        $this->user_id_lst[$user_id]=$user_id;

        }


        if (!($this->item_per_page)) $this->item_per_page = 30;
        $this->pages=$bunker_supply->getPageCount($this->select_filters,$this->sort_filters,$this->item_per_page,true);
        if (isset($_REQUEST['page']))
        {
            $this->page = $_REQUEST['page'];
            if ($this->page < 1) $this->page = 1;
            $_SESSION['list_bunker_supplies']['page']=$this->page;
        } else
        {
            $this->page = $_SESSION['list_bunker_supplies']['page'];
        }
        if ($this->page > $this->pages) $this->page = 1;

        
        $search='';
        if (isset($_REQUEST['search_texts']))
        {
            $this->search_texts = $_REQUEST['search_texts'];
            $_SESSION['list_bunker_supplies']['search_texts']=$this->search_texts;
        }
        else
        {
            $this->search_texts = $_SESSION['list_bunker_supplies']['search_texts'];
        }
        if (is_array($this->search_texts))
        {
            foreach($this->search_texts as $field=>$value)
            {
                if (trim($value))
                {
                    if ($search)
                    {
                        $search.=" AND $field like '%$value%' ";
                    }
                    else
                    {
                        $search=" $field like '%$value%' ";
                    }
                }
            }
        }
        if ($search)
        {
            $this->select_filters[] = $search;
        }

        
        $this->bunker_supplies = $bunker_supply->findAll($this->select_filters,$this->sort_filters,$this->item_per_page,$this->page);
    } 
    public function renderPaginator()
    { 
        include 'PaginatorView.php'; 
        include 'PaginatorController.php'; 
    } 
    public function renderCsrf()
    { 
        echo '<input type="hidden" name="'.$this->security->getCsrfName().'" value="'.$this->security->getCsrfValue().'"/>'; 
    } 
    public function renderActions($bunker_supply,$class="",$edit=TRUE)
    {
        if ($edit)
        {
            echo "<a class='click_action $class' container='".$this->container."' url='?command=display_rest&base_path=Admin.dir/BunkerSupplies.dir&path=Edit BunkerSupply.hid.rest.php&id=".$bunker_supply->getId()."&object=BunkerSupply' href='#' ><li class='fa fa-pencil'></li> edit</a>"; 
        } else
        {
            echo "<a class='click_action $class' container='".$this->container."' url='?command=display_rest&base_path=Admin.dir/BunkerSupplies.dir&path=View BunkerSupply.hid.rest.php&id=".$bunker_supply->getId()."&object=BunkerSupply' href='#' ><li class='fa fa-eye'></li> view</a>"; 
        }
    }
    
	public function renderEditControls()
    {
        if ($this->edit_button_next_url)
        { 
            $next_after_update='next="'.$this->edit_button_next_url.'"';
        } else $next_after_update='';
        if ($this->edit_delete_button_next_url)
        {
            $next_after_delete='next="'.$this->edit_delete_button_next_url.'"';
        } else $next_after_delete='';
		echo '<div class="control" style="height:80px">';
		echo '<a class="button click_action" url="'.$this->edit_button_url.'"  href="#" container="div.page" '.$next_after_update.' ><li class="fa fa-save"></li> '.$this->edit_button_name.'</a>';
		echo '<a class="button click_action danger confirm" url="'.$this->edit_delete_button_url.'" href="#" container="'.$this->container.'" '.$next_after_delete.' ><li class="fa fa-trash" ></li> Delete </a>';
		echo '</div>';
	}    
	
	public function renderNewControls()
	{
        if ($this->new_button_next_url)
        {
            $next_after_create='next="'.$this->new_button_next_url.'"';
        } else
        {
            $next_after_create='';
        }
		echo '<div class="control" style="height:80px">';
		echo '<a class="button click_action" url="'.$this->new_button_url.'" href="#" '.$next_after_create.' container="'.$this->container.'"><li class="fa fa-plus"></li> '.$this->new_button_name.' </a>';
		echo '</div>';
	}
    
    public function renderListControls($new_after_new='')
    {
        echo '<div class="control" style="height:80px">'."\r\n";
        echo '<a class="button click_action" container="'.$this->container.'" url="'.$this->list_button_new_url.'" href="#" ><i class="fa fa-plus"></i> '.$this->list_button_new_name.'</a>'."\r\n";
        echo '</div>'."\r\n";  
    }
    
    public function renderCustomEdit()
    {
    }

    public function renderCustomNew()
    {
    }

    public function renderCustomlist()
    {
    }

}
