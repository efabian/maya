<?php
require_once 'MayaSecurity.php';
require_once 'paths.cfg.php';
require_once 'Configurations.php';
require_once 'Finance.php';
class FinanceViewBase 
{ 
    public $security;
    public $finance;
    public $page;
    public $path;
    public $finances;
    public $container;
    public $item_per_page;
    public $pages;
    public $search_text;
    public $select_filters;
    public $sort_filters;
    public $sort_orders;
    public function getDescription($finance)
    {
        echo $finance->getDescription();
    }
    public function getExpense($finance)
    {
        echo $finance->getExpense();
    }
    public function getIncome($finance)
    {
        echo $finance->getIncome();
    }
    public function getSource($finance)
    {
        echo $finance->getSource();
    }
    public function getDestination($finance)
    {
        echo $finance->getDestination();
    }
    public function getExpenseType($finance)
    {
        echo $finance->getExpenseType();
    }
    public function getIncomeType($finance)
    {
        echo $finance->getIncomeType();
    }
    public function getTransactionDate($finance)
    {
        echo $finance->getTransactionDate();
    }
    public function getCreated($finance)
    {
        echo $finance->getCreated();
    }
    public function renderId($finance,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$finance->getId();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='id'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='id' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='id' value='".$value."' ></input>";
            }
        }
    }
    public function renderDescriptionTextarea($finance,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'jwysiwyg'))
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$finance->getDescription();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='description'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<textarea id='edit_description' $attrs class='$class  edit_input' name='description' >".$value."</textarea>";                
            } else
            {
                echo "<textarea id='edit_description' $attrs type='text' class='$class ' name='description' >".$value."</textarea>";
            }
        }
    }
    public function renderExpenseTextarea($finance,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'jwysiwyg'))
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$finance->getExpense();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='expense'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<textarea id='edit_expense' $attrs class='$class  edit_input' name='expense' >".$value."</textarea>";                
            } else
            {
                echo "<textarea id='edit_expense' $attrs type='text' class='$class ' name='expense' >".$value."</textarea>";
            }
        }
    }
    public function renderIncomeTextarea($finance,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'jwysiwyg'))
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$finance->getIncome();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='income'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<textarea id='edit_income' $attrs class='$class  edit_input' name='income' >".$value."</textarea>";                
            } else
            {
                echo "<textarea id='edit_income' $attrs type='text' class='$class ' name='income' >".$value."</textarea>";
            }
        }
    }
    public function renderSourceTextarea($finance,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'jwysiwyg'))
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$finance->getSource();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='source'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<textarea id='edit_source' $attrs class='$class  edit_input' name='source' >".$value."</textarea>";                
            } else
            {
                echo "<textarea id='edit_source' $attrs type='text' class='$class ' name='source' >".$value."</textarea>";
            }
        }
    }
    public function renderDestinationTextarea($finance,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'jwysiwyg'))
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$finance->getDestination();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='destination'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<textarea id='edit_destination' $attrs class='$class  edit_input' name='destination' >".$value."</textarea>";                
            } else
            {
                echo "<textarea id='edit_destination' $attrs type='text' class='$class ' name='destination' >".$value."</textarea>";
            }
        }
    }
    public function renderExpenseTypeEnum($finance,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'chosen'),$option_filters=array())
    {
		$class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }
    
        $val=$finance->getExpenseType();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='expense_type'  value='$val' />";
            if ($readonly)
            {
                echo "<span >$val</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<select $attrs class='$class edit_input'  name='expense_type' >\n";
            } else
            {
                echo "<select $attrs class='$class'  name='expense_type' >\n";
            } 
			$options=array('basic_needs','real_estate_purchase','lent','donation','mama_dor','mama_caring','kyle','xavier','cairi','sinaman_diwan','barili_bukid','vehicle','others');
			foreach($options as $option)
			{
				if ($val==$option)
				{
					echo "<option selected='selected' value='$option' >$option</option>\n";
				} else
				{
					echo "<option value='$option' >$option</option>\n";
				}
			}
			echo "</select>\n";
            if ($edit) echo "<span class='view_input'>$val</span>";
            
        }
    }
    public function renderExpenseTypeEnumFilter()
    {
		echo "<select  class='filter' name='finances_select_filters[expense_type]' value='' > \r\n";
        echo "<option value=''>All</option> \r\n";
		$options=array('basic_needs','real_estate_purchase','lent','donation','mama_dor','mama_caring','kyle','xavier','cairi','sinaman_diwan','barili_bukid','vehicle','others');
        foreach($options as $option)
		{
			
			if ((array_key_exists('expense_type',$this->select_filters)) && ($this->select_filters['expense_type']==$option))
			{
				echo "<option selected='selected' value='".$option."' >".$option."</option>";
			} 
			else
			{
				echo "<option value='".$option."' >".$option."</option>";
			}
		}
		echo "</select> \r\n";		
	}
	
    public function renderIncomeTypeEnum($finance,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'chosen'),$option_filters=array())
    {
		$class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }
    
        $val=$finance->getIncomeType();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='income_type'  value='$val' />";
            if ($readonly)
            {
                echo "<span >$val</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<select $attrs class='$class edit_input'  name='income_type' >\n";
            } else
            {
                echo "<select $attrs class='$class'  name='income_type' >\n";
            } 
			$options=array('nekpa','local_work','lent_payment','donated','found','sale','others');
			foreach($options as $option)
			{
				if ($val==$option)
				{
					echo "<option selected='selected' value='$option' >$option</option>\n";
				} else
				{
					echo "<option value='$option' >$option</option>\n";
				}
			}
			echo "</select>\n";
            if ($edit) echo "<span class='view_input'>$val</span>";
            
        }
    }
    public function renderIncomeTypeEnumFilter()
    {
		echo "<select  class='filter' name='finances_select_filters[income_type]' value='' > \r\n";
        echo "<option value=''>All</option> \r\n";
		$options=array('nekpa','local_work','lent_payment','donated','found','sale','others');
        foreach($options as $option)
		{
			
			if ((array_key_exists('income_type',$this->select_filters)) && ($this->select_filters['income_type']==$option))
			{
				echo "<option selected='selected' value='".$option."' >".$option."</option>";
			} 
			else
			{
				echo "<option value='".$option."' >".$option."</option>";
			}
		}
		echo "</select> \r\n";		
	}
	
    public function renderTransactionDate($finance,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$finance->getTransactionDate();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='transaction_date'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class date edit_input' name='transaction_date' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class date' name='transaction_date' value='".$value."' ></input>";
            }
        }
    }
    public function __construct($param=array('container'=>'div.page'))
    {
        foreach($param as $field=>$value)
        {
            $this->$field=$value;
        }
        if (!isset($this->container) || (!$this->container))
        {
            $this->container='div.page';
        }
        $this->paths = new Paths();
        $this->config = new Configurations();
    }
    
    public function init_edit()
    { 
        $this->security=new MayaSecurity();
        if (array_key_exists("id",$_GET))
        {
            $this->id=$_GET["id"];
        } else
        {
            $this->id=$_REQUEST["id"];
        }
        $this->finance = new Finance();
        $this->finance->find(array("id"=>$this->id));
        $this->page=$_REQUEST["selected"];
        $this->path=$_REQUEST["path"];
        $this->current_user=new User();
        $this->current_user->find(array("id"=>$_SESSION["user_id"]));
        $this->edit_form_attributes='';
        $this->edit_button_name='Update';
        $this->edit_button_url='?command=editRest&object=Finance';
        $this->edit_button_next_url='?command=display_rest&path=Admin.dir/Finances.dir/Edit Finance.hid.rest.php&id='.$this->finance->getId();
        $this->edit_delete_button_url='?command=deleteRest&object=Finance';
        $this->edit_delete_button_next_url='?command=display_rest&path=Admin.dir/Finances.dir/List Finances.rest.php';
    } 
    public function init_new()
    { 
        $this->security=new MayaSecurity();
        $this->finance = new Finance();
        $this->page=$_REQUEST["selected"];
        $this->path=$_REQUEST["path"];
        $this->current_user=new User();
        $this->current_user->find(array("id"=>$_SESSION["user_id"]));
        $this->new_form_attributes='';
        $this->new_button_name='Create';
        $this->new_button_url='?command=newRest&object=Finance';
        $this->new_button_next_url='?command=display_rest&path=Admin.dir/Finances.dir/List Finances.rest.php';
    } 
    public function init_list()
    { 
        $this->security=new MayaSecurity();
        $valid_filters=array("id","description","expense","income","source","destination","expense_type","income_type","transaction_date","created");
        $this->page=$_REQUEST["finances_selected"];
        $this->path=$_REQUEST["finances_path"];
        $this->list_form_attributes='';
        $this->list_button_new_url='?command=display_rest&path=Admin.dir/Finances.dir/New Finance.rest.php';
        $this->list_button_new_name='New';
		$this->form_list='form_list';
		$this->action='?command=display_php&path=Admin.dir/Finances.dir/List Finances.rest.php';
        if (!$this->container) $this->container='div.page';
        $finance = new Finance();
        if (!isset($_SESSION['list_finances']))
        {
            $_SESSION['list_finances']=array();
            $_SESSION['list_finances']['sort_filters']=array();
            $_SESSION['list_finances']['sort_order']='';
            $_SESSION['list_finances']['select_filters']=array();
            $_SESSION['list_finances']['search_texts']=array();
            $_SESSION['list_finances']['page']='1';
        }

        if (isset($_REQUEST['finances_sort_filters']))
        {
            $sort_lists = $_REQUEST['finances_sort_filters'];
            $_SESSION['list_finances']['sort_filters']=$sort_lists;
        }
        else
        {
            $sort_lists=$_SESSION['list_finances']['sort_filters'];
        }

        if (isset($_REQUEST['finances_sort_order']))
        {
            $sort_order = $_REQUEST['finances_sort_order'];
            $_SESSION['list_finances']['sort_order']=$sort_order;
        }
        else
        {
            $sort_order = $_SESSION['list_finances']['sort_order'];
        }
        
        if (!$sort_lists) 
        {
            $sort_lists = array('id'=>'DESC');
            $sort_order = 'id';
        }
        
        $this->sort_filters = array();
        if ($sort_order)
        {
            $this->sort_orders = explode(',',$sort_order);
            foreach ($this->sort_orders as $s_order)
            {
                $this->sort_filters[$s_order]=$sort_lists[$s_order];
            }
        } else $this->sort_orders = array();

		if (!isset($this->select_filters)) $this->select_filters=array();
        if (isset($_REQUEST['finances_select_filters']))
        {
            foreach($_REQUEST['finances_select_filters'] as $key=>$value)
            {
                if (!(is_numeric($key)) && in_array($key,$valid_filters))
                {
                    $this->select_filters[$key] = $value;
                }
            }
            $_SESSION['list_finances']['select_filters']=$this->select_filters;
        }
        else
        {
            $this->select_filters=array_merge($_SESSION['list_finances']['select_filters'],$this->select_filters);
        }
        
        


        $finances_all = $finance->findAll(array(),array(),100000,1,array("expense_type","income_type"),false);
        $this->expense_type_lst=array();
        $this->income_type_lst=array();

        foreach ($finances_all as $item)
        {
                    $expense_type=$item->getExpenseType();
        $this->expense_type_lst[$expense_type]=$expense_type;
        $income_type=$item->getIncomeType();
        $this->income_type_lst[$income_type]=$income_type;

        }


        $search='';
        if (isset($_REQUEST['finances_search_texts']))
        {
            $this->search_texts = $_REQUEST['finances_search_texts'];
            $_SESSION['list_finances']['search_texts']=$this->search_texts;
        }
        else
        {
            $this->search_texts = $_SESSION['list_finances']['search_texts'];
        }
        if (is_array($this->search_texts))
        {
            foreach($this->search_texts as $field=>$value)
            {
                if (trim($value))
                {
                    if ($search)
                    {
                        $search.=" AND $field like '%$value%' ";
                    }
                    else
                    {
                        $search=" $field like '%$value%' ";
                    }
                }
            }
        }
        if ($search)
        {
            $this->select_filters[] = $search;
        }


        if (!($this->item_per_page)) $this->item_per_page = 30;
        $this->pages=$finance->getPageCount($this->select_filters,$this->sort_filters,$this->item_per_page,true);
        
        $this->page = 1;//default to 1 in case not provided below
        if (isset($_REQUEST['finances_page']))
        {
            $this->page = $_REQUEST['finances_page'];
            $_SESSION['list_finances']['page']=$this->page;
        } else
        {
            $this->page = $_SESSION['list_finances']['page'];
            if (array_key_exists('list_finances',$_SESSION) && array_key_exists('page',$_SESSION['list_finances']))
            {
                $this->page = $_SESSION['list_finances']['page'];
            }            
        }
        if (($this->page < 1) || ($this->page > $this->pages)) $this->page = 1;

        
        $this->finances = $finance->findAll($this->select_filters,$this->sort_filters,$this->item_per_page,$this->page);
    } 
    public function renderPaginator()
    { 
        include 'PaginatorView.php'; 
        include 'PaginatorController.php'; 
    } 
    public function renderCsrf()
    { 
        echo '<input type="hidden" name="'.$this->security->getCsrfName().'" value="'.$this->security->getCsrfValue().'"/>'; 
    } 
    public function renderActions($finance,$class="",$edit=TRUE)
    {
        if ($edit)
        {
            echo "<a class='mx- $class' mx-container='".$this->container."' mx-click='?command=display_rest&base_path=Admin.dir/Finances.dir&path=Edit Finance.hid.rest.php&id=".$finance->getId()."&object=Finance' href='#' ><li class='fa fa-pencil'></li> edit</a>"; 
        } else
        {
            echo "<a class='mx- $class' mx-container='".$this->container."' mx-click='?command=display_rest&base_path=Admin.dir/Finances.dir&path=View Finance.hid.rest.php&id=".$finance->getId()."&object=Finance' href='#' ><li class='fa fa-eye'></li> view</a>"; 
        }
    }
    
	public function renderEditControls()
    {
        if ($this->edit_button_next_url)
        { 
            $next_after_update='mx-next="'.$this->edit_button_next_url.'"';
        } else $next_after_update='';
        if ($this->edit_delete_button_next_url)
        {
            $next_after_delete='mx-next="'.$this->edit_delete_button_next_url.'"';
        } else $next_after_delete='';
		echo '<div class="control" style="height:80px">';
		echo '<a class="button mx-" mx-click="'.$this->edit_button_url.'"  href="#" mx-container="div.page" '.$next_after_update.' ><li class="fa fa-save"></li> '.$this->edit_button_name.'</a>';
		echo '<a class="button mx- danger" mx-confirm="Are you sure to delete?" mx-click="'.$this->edit_delete_button_url.'" href="#" mx-container="'.$this->container.'" '.$next_after_delete.' ><li class="fa fa-trash" ></li> Delete </a>';
		echo '</div>';
	}    
	
	public function renderNewControls()
	{
        if ($this->new_button_next_url)
        {
            $next_after_create='mx-next="'.$this->new_button_next_url.'"';
        } else
        {
            $next_after_create='';
        }
		echo '<div class="control" style="height:80px">';
		echo '<a class="button mx-" mx-click="'.$this->new_button_url.'" href="#" '.$next_after_create.' mx-container="'.$this->container.'"><li class="fa fa-plus"></li> '.$this->new_button_name.' </a>';
		echo '</div>';
	}
    
    public function renderListControls($new_after_new='')
    {
        echo '<div class="control" style="height:80px">'."\r\n";
        echo '<a class="button mx-" mx-container="'.$this->container.'" mx-click="'.$this->list_button_new_url.'" href="#" ><i class="fa fa-plus"></i> '.$this->list_button_new_name.'</a>'."\r\n";
        echo '</div>'."\r\n";  
    }
    
    public function renderCustomEdit()
    {
    }

    public function renderCustomNew()
    {
    }

    public function renderCustomlist()
    {
    }
    
    public function renderSearchText($field,$type)
    {
		echo '<div class="search_filter_header">';
		$value='';
		if (isset($this->search_texts) && array_key_exists($field,$this->search_texts)) $value=$this->search_texts[$field];
		echo '<input class="search_text '.$type.'" type="text" name="finances_search_texts['.$field.']" value="'.$value.'"></input><a class="search_text_button" ><li class="fa fa-search"></li></a>';
		echo '</div>';
    }

}
