<?php
require_once 'MayaSecurity.php';
require_once 'paths.cfg.php';
require_once 'Configurations.php';
require_once 'User.php';
require_once 'Role.php';
class UserViewBase 
{ 
    public $security;
    public $user;
    public $page;
    public $path;
    public $users;
    public $container;
    public $item_per_page;
    public $pages;
    public $search_text;
    public $select_filters;
    public $sort_filters;
    public $sort_orders;
    public function getFirstname($user)
    {
        echo $user->getFirstname();
    }
    public function getLastname($user)
    {
        echo $user->getLastname();
    }
    public function getEmail($user)
    {
        echo $user->getEmail();
    }
    public function getProfile($user)
    {
        echo $user->getProfile();
    }
    public function getCreated($user)
    {
        echo $user->getCreated();
    }
    public function getModified($user)
    {
        echo $user->getModified();
    }
    public function getLogin($user)
    {
        echo $user->getLogin();
    }
    public function getRole($user)
    {
        $role = new Role();
echo $role->getRoleName($user->getRole());
    }
    public function getSalt($user)
    {
        echo $user->getSalt();
    }
    public function getPassword($user)
    {
        echo $user->getPassword();
    }
    public function getIp($user)
    {
        echo $user->getIp();
    }
    public function getUpdateip($user)
    {
        echo $user->getUpdateip();
    }
    public function getLoginip($user)
    {
        echo $user->getLoginip();
    }
    public function getResetkey($user)
    {
        echo $user->getResetkey();
    }
    public function getSecurekey($user)
    {
        echo $user->getSecurekey();
    }
    public function renderId($user,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$user->getId();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='id'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='id' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='id' value='".$value."' ></input>";
            }
        }
    }
    public function renderFirstname($user,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$user->getFirstname();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='firstname'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='firstname' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='firstname' value='".$value."' ></input>";
            }
        }
    }
    public function renderLastname($user,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$user->getLastname();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='lastname'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='lastname' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='lastname' value='".$value."' ></input>";
            }
        }
    }
    public function renderEmail($user,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$user->getEmail();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='email'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='email' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='email' value='".$value."' ></input>";
            }
        }
    }
    public function renderProfileTextarea($user,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'jwysiwyg'))
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$user->getProfile();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='profile'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<textarea id='edit_profile' $attrs class='$class  edit_input' name='profile' >".$value."</textarea>";                
            } else
            {
                echo "<textarea id='edit_profile' $attrs type='text' class='$class ' name='profile' >".$value."</textarea>";
            }
        }
    }
    public function renderLogin($user,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$user->getLogin();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='login'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class datetime edit_input' name='login' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class datetime' name='login' value='".$value."' ></input>";
            }
        }
    }
    public function renderRoleValue($user,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $role = new Role();
        $options=$role->getRoleNames();
        $role_names = array();
        $value=$user->getRole();
        if ($hidden | $readonly)
        {
            echo "<input $attrs type='hidden' name='role'  value='$value' />";
            if ($readonly)
            {
                echo "<span>".$role->getRoleName($value)."</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='edit_input'><?php echo $user->getRole();?>";
            } else
            {
               echo "<span>".$role->getRoleName($value)."</span>";
            }
            foreach ($options as $key=>$option)
            {
                if ($value & $key)
                {
                    echo "<label><input $attrs type='checkbox' class='$class' checked='checked' value='$key' name=role[]>$option </label>";
                    $role_names[] = $option;
                } else
                {
                    echo "<label><input $attrs type='checkbox' class='$class' value='$key' name=role[]>$option </label>";
                }
             }
             echo "</span>";
             echo '<span class="view_input">'.implode(", ",$role_names).'</span>';
        }
    }
    public function renderRoleFilter()
    {
		echo "<!-- ".json_encode($this->select_filters)." --> \r\n";
		echo "<select  class='filter' name='users_select_filters[role]' value='' > \r\n";
		echo "<option value=''>All</option> \r\n";
		
		$role = new Role();
		
		foreach ($role->getRoleNames() as $val=>$name)
		{
			
			if ((array_key_exists('role',$this->select_filters)) && ($this->select_filters['role']) && in_array($val,$this->select_filters['role']))
			{
				echo "<option selected='selected' value='".$val."' >".$name."</option>";
			} else if (($val==0) && (in_array("(role < 1)",$this->select_filters))) 
			{
				echo "<option selected='selected' value='".$val."' >".$name."</option>";
			} 
			else
			{
				echo "<option value='".$val."' >".$name."</option>";
			}
		}
		echo "</select> \r\n";		
	}
	
    public function renderSalt($user,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$user->getSalt();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='salt'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='salt' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='salt' value='".$value."' ></input>";
            }
        }
    }
    public function renderIp($user,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$user->getIp();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='ip'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='ip' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='ip' value='".$value."' ></input>";
            }
        }
    }
    public function renderUpdateip($user,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$user->getUpdateip();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='updateip'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='updateip' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='updateip' value='".$value."' ></input>";
            }
        }
    }
    public function renderLoginip($user,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$user->getLoginip();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='loginip'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='loginip' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='loginip' value='".$value."' ></input>";
            }
        }
    }
    public function renderResetkey($user,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$user->getResetkey();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='resetkey'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='resetkey' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='resetkey' value='".$value."' ></input>";
            }
        }
    }
    public function renderSecurekeyTextarea($user,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'jwysiwyg'))
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$user->getSecurekey();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='securekey'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<textarea id='edit_securekey' $attrs class='$class  edit_input' name='securekey' >".$value."</textarea>";                
            } else
            {
                echo "<textarea id='edit_securekey' $attrs type='text' class='$class ' name='securekey' >".$value."</textarea>";
            }
        }
    }
    public function __construct($param=array('container'=>'div.page'))
    {
        foreach($param as $field=>$value)
        {
            $this->$field=$value;
        }
        if (!isset($this->container) || (!$this->container))
        {
            $this->container='div.page';
        }
        $this->paths = new Paths();
        $this->config = new Configurations();
    }
    
    public function init_edit()
    { 
        $this->security=new MayaSecurity();
        if (array_key_exists("id",$_GET))
        {
            $this->id=$_GET["id"];
        } else
        {
            $this->id=$_REQUEST["id"];
        }
        $this->user = new User();
        $this->user->find(array("id"=>$this->id));
        $this->page=$_REQUEST["selected"];
        $this->path=$_REQUEST["path"];
        $this->current_user=new User();
        $this->current_user->find(array("id"=>$_SESSION["user_id"]));
        $this->edit_form_attributes='';
        $this->edit_button_name='Update';
        $this->edit_button_url='?command=editRest&object=User';
        $this->edit_button_next_url='?command=display_rest&path=Admin.dir/Users.dir/Edit User.hid.rest.php&id='.$this->user->getId();
        $this->edit_delete_button_url='?command=deleteRest&object=User';
        $this->edit_delete_button_next_url='?command=display_rest&path=Admin.dir/Users.dir/List Users.rest.php';
    } 
    public function init_new()
    { 
        $this->security=new MayaSecurity();
        $this->user = new User();
        $this->page=$_REQUEST["selected"];
        $this->path=$_REQUEST["path"];
        $this->current_user=new User();
        $this->current_user->find(array("id"=>$_SESSION["user_id"]));
        $this->new_form_attributes='';
        $this->new_button_name='Create';
        $this->new_button_url='?command=newRest&object=User';
        $this->new_button_next_url='?command=display_rest&path=Admin.dir/Users.dir/List Users.rest.php';
    } 
    public function init_list()
    { 
        $this->security=new MayaSecurity();
        $valid_filters=array("id","firstname","lastname","email","profile","created","modified","login","role","salt","password","ip","updateip","loginip","resetkey","securekey");
        $this->page=$_REQUEST["users_selected"];
        $this->path=$_REQUEST["users_path"];
        $this->list_form_attributes='';
        $this->list_button_new_url='?command=display_rest&path=Admin.dir/Users.dir/New User.rest.php';
        $this->list_button_new_name='New';
		$this->form_list='form_list';
		$this->action='?command=display_php&path=Admin.dir/Users.dir/List Users.rest.php';
        if (!$this->container) $this->container='div.page';
        $user = new User();
        if (!isset($_SESSION['list_users']))
        {
            $_SESSION['list_users']=array();
            $_SESSION['list_users']['sort_filters']=array();
            $_SESSION['list_users']['sort_order']='';
            $_SESSION['list_users']['select_filters']=array();
            $_SESSION['list_users']['search_texts']=array();
            $_SESSION['list_users']['page']='1';
        }

        if (isset($_REQUEST['users_sort_filters']))
        {
            $sort_lists = $_REQUEST['users_sort_filters'];
            $_SESSION['list_users']['sort_filters']=$sort_lists;
        }
        else
        {
            $sort_lists=$_SESSION['list_users']['sort_filters'];
        }

        if (isset($_REQUEST['users_sort_order']))
        {
            $sort_order = $_REQUEST['users_sort_order'];
            $_SESSION['list_users']['sort_order']=$sort_order;
        }
        else
        {
            $sort_order = $_SESSION['list_users']['sort_order'];
        }
        
        if (!$sort_lists) 
        {
            $sort_lists = array('id'=>'DESC');
            $sort_order = 'id';
        }
        
        $this->sort_filters = array();
        if ($sort_order)
        {
            $this->sort_orders = explode(',',$sort_order);
            foreach ($this->sort_orders as $s_order)
            {
                $this->sort_filters[$s_order]=$sort_lists[$s_order];
            }
        } else $this->sort_orders = array();

		if (!isset($this->select_filters)) $this->select_filters=array();
        if (isset($_REQUEST['users_select_filters']))
        {
            foreach($_REQUEST['users_select_filters'] as $key=>$value)
            {
                if (!(is_numeric($key)) && in_array($key,$valid_filters))
                {
                    $this->select_filters[$key] = $value;
                }
            }
            $_SESSION['list_users']['select_filters']=$this->select_filters;
        }
        else
        {
            $this->select_filters=array_merge($_SESSION['list_users']['select_filters'],$this->select_filters);
        }
        
        
		if (isset($this->select_filters)) 
		{
			if (array_key_exists('role',$this->select_filters))
			{
				if (strlen(trim($this->select_filters['role']))==0)
				{
					unset($this->select_filters['role']);
				}
				else if ($this->select_filters['role']=='0')
				{
					unset($this->select_filters['role']);
					$this->select_filters[]="(role < 1)";
				}
				else
				{
					$vals=array();
					for($i=0;$i<=15;$i++)
					{
						if ($i & $this->select_filters['role'])
						{
							$vals[] = $i;
						}
					}
					$this->select_filters['role']=$vals;
				}
				
			}
		}
        
        




        $search='';
        if (isset($_REQUEST['users_search_texts']))
        {
            $this->search_texts = $_REQUEST['users_search_texts'];
            $_SESSION['list_users']['search_texts']=$this->search_texts;
        }
        else
        {
            $this->search_texts = $_SESSION['list_users']['search_texts'];
        }
        if (is_array($this->search_texts))
        {
            foreach($this->search_texts as $field=>$value)
            {
                if (trim($value))
                {
                    if ($search)
                    {
                        $search.=" AND $field like '%$value%' ";
                    }
                    else
                    {
                        $search=" $field like '%$value%' ";
                    }
                }
            }
        }
        if ($search)
        {
            $this->select_filters[] = $search;
        }


        if (!($this->item_per_page)) $this->item_per_page = 30;
        $this->pages=$user->getPageCount($this->select_filters,$this->sort_filters,$this->item_per_page,true);
        
        $this->page = 1;//default to 1 in case not provided below
        if (isset($_REQUEST['users_page']))
        {
            $this->page = $_REQUEST['users_page'];
            $_SESSION['list_users']['page']=$this->page;
        } else
        {
            $this->page = $_SESSION['list_users']['page'];
            if (array_key_exists('list_users',$_SESSION) && array_key_exists('page',$_SESSION['list_users']))
            {
                $this->page = $_SESSION['list_users']['page'];
            }            
        }
        if (($this->page < 1) || ($this->page > $this->pages)) $this->page = 1;

        
        $this->users = $user->findAll($this->select_filters,$this->sort_filters,$this->item_per_page,$this->page);
    } 
    public function renderPaginator()
    { 
        include 'PaginatorView.php'; 
        include 'PaginatorController.php'; 
    } 
    public function renderCsrf()
    { 
        echo '<input type="hidden" name="'.$this->security->getCsrfName().'" value="'.$this->security->getCsrfValue().'"/>'; 
    } 
    public function renderActions($user,$class="",$edit=TRUE)
    {
        if ($edit)
        {
            echo "<a class='mx- $class' mx-container='".$this->container."' mx-click='?command=display_rest&base_path=Admin.dir/Users.dir&path=Edit User.hid.rest.php&id=".$user->getId()."&object=User' href='#' ><li class='fa fa-pencil'></li> edit</a>"; 
        } else
        {
            echo "<a class='mx- $class' mx-container='".$this->container."' mx-click='?command=display_rest&base_path=Admin.dir/Users.dir&path=View User.hid.rest.php&id=".$user->getId()."&object=User' href='#' ><li class='fa fa-eye'></li> view</a>"; 
        }
    }
    
	public function renderEditControls()
    {
        if ($this->edit_button_next_url)
        { 
            $next_after_update='mx-next="'.$this->edit_button_next_url.'"';
        } else $next_after_update='';
        if ($this->edit_delete_button_next_url)
        {
            $next_after_delete='mx-next="'.$this->edit_delete_button_next_url.'"';
        } else $next_after_delete='';
		echo '<div class="control" style="height:80px">';
		echo '<a class="button mx-" mx-click="'.$this->edit_button_url.'"  href="#" mx-container="div.page" '.$next_after_update.' ><li class="fa fa-save"></li> '.$this->edit_button_name.'</a>';
		echo '<a class="button mx- danger" mx-confirm="Are you sure to delete?" mx-click="'.$this->edit_delete_button_url.'" href="#" mx-container="'.$this->container.'" '.$next_after_delete.' ><li class="fa fa-trash" ></li> Delete </a>';
		echo '</div>';
	}    
	
	public function renderNewControls()
	{
        if ($this->new_button_next_url)
        {
            $next_after_create='mx-next="'.$this->new_button_next_url.'"';
        } else
        {
            $next_after_create='';
        }
		echo '<div class="control" style="height:80px">';
		echo '<a class="button mx-" mx-click="'.$this->new_button_url.'" href="#" '.$next_after_create.' mx-container="'.$this->container.'"><li class="fa fa-plus"></li> '.$this->new_button_name.' </a>';
		echo '</div>';
	}
    
    public function renderListControls($new_after_new='')
    {
        echo '<div class="control" style="height:80px">'."\r\n";
        echo '<a class="button mx-" mx-container="'.$this->container.'" mx-click="'.$this->list_button_new_url.'" href="#" ><i class="fa fa-plus"></i> '.$this->list_button_new_name.'</a>'."\r\n";
        echo '</div>'."\r\n";  
    }
    
    public function renderCustomEdit()
    {
    }

    public function renderCustomNew()
    {
    }

    public function renderCustomlist()
    {
    }
    
    public function renderSearchText($field,$type)
    {
		echo '<div class="search_filter_header">';
		$value='';
		if (isset($this->search_texts) && array_key_exists($field,$this->search_texts)) $value=$this->search_texts[$field];
		echo '<input class="search_text '.$type.'" type="text" name="users_search_texts['.$field.']" value="'.$value.'"></input><a class="search_text_button" ><li class="fa fa-search"></li></a>';
		echo '</div>';
    }


    public function renderUploadImage($user,$disable__icon=FALSE,$redirect_after_image_upload='',$img_attributes='',$edit_enabled=TRUE)
    {
        if (!isset($this->src_)) $this->src_='img/displaypng.php?id='.$this->current_user->getId().'&image='.$this->user->getImage('').'&key='.urlencode($this->current_user->getSecurekey()).'&rnd='.rand(1,100);    
        if (!isset($this->src__icon)) $this->src__icon='img/displaypng.php?id='.$this->current_user->getId().'&image='.$this->user->getImageIcon('').'&key='.urlencode($this->current_user->getSecurekey()).'&rnd='.rand(1,100);    
        if ($edit_enabled)
        {
            echo '<form id="upload_" action="?command=uploadImage&object=User&id='.$user->getId().'" class="form image round-corners" method="post" style="float:center; display:block" enctype="multipart/form-data" >';
        }
        echo '<div>
                <div style="display:inline-block" >
                    <img class="profile"  src="'.$this->src_.'" '.$img_attributes.'/>
                </div>';
                if (!(isset($disable__icon) && $disable__icon))
                {
                    echo '<div style="display:inline-block">';
                    echo '<img class="profile_icon"  src="'.$this->src__icon.'" />';
                    echo '<label style="display:block">Icon</label>';
                    echo '</div>';
                }
        if ($edit_enabled)
        {
                $this->renderCsrf();
            echo '<input type="file" name="file" id="file" class="button fa fa-plus" accept="image/*" />';
            echo '<input type="hidden" name="id" value="'.$this->user->getId().'" />';
            echo '<input type="hidden" name="redirect" value="'.$redirect_after_image_upload.'" />';
            echo '<textarea id="upload_content" type="submit" style="display:none" name="upload_content" value=""></textarea>';
            echo '<button type="submit" class="button" name="submit"><i class="fa fa-upload"></i>Upload</button>';
        }
        
            echo '</div>';
        if ($edit_enabled)
        {
            echo '</form>';
        }
    }

    public function renderImage($user)
    {
        echo '<span>'.$user->getImage().". (See upload button above this table)</span>\n";
    }
}
