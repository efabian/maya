<?php 
/**  
 * Used to edit individual Ticket
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link https://maya.docph.net/
 */
require_once 'Role.php'; 
if (!isset($allowed_edit)) $allowed_edit=array("admin");
if (!isset($allowed)) $allowed=array("admin");
$role = new Role();
if (!$role->isUserAnyOf($allowed))
{
	$role->showUnauthorizedMessage($allowed,"?command=display_php&base_path=usr/view&path=Edit Ticket.rest.php");
} else
{
if (!isset($ticket_view))
{
	require_once 'TicketView.php'; 
	$ticket_view = new TicketView(); 
	$ticket_view->init_edit();
}
?> 

<?php if (!isset($edit_remove_image)) 
{ 
   echo "<h5>Image</h5>";
   if (!isset($edit_redirect_image)) $edit_redirect_image="?command=display_php&base_path=".$_REQUEST["base_path"]."&path=".$_REQUEST["path"]."&id=".$ticket_view->ticket->getId();
   $ticket_view->renderUploadImage($ticket_view->ticket,TRUE,$edit_redirect_image,"",$role->isUserAnyOf($allowed_edit));
} ?>


<form id="edit_ticket" <?php echo $ticket_view->edit_form_attributes;?> class="edit" action="?command=edit&object=Ticket&base_path=<?php echo $_REQUEST["base_path"];?>&path=<?php echo $path;?>" method="POST">
<input class="field_pages" field_pages="1" type="hidden" value="1" name="pages" />
<?php $ticket_view->renderCsrf();?> 
<?php $ticket_view->renderCustomEdit();?> 
<table class='odd_even_row_alternate_color full_width data' >
<thead><th>Field</th><th>Data <?php if ($role->isUserAnyOf($allowed_edit)) {?><button type='button' id='toggle_edit' class='button' onclick='toggleEdit()'><li class='fa fa-pencil'></li> Enable Edit</button> <?php }?></th></thead>
<tbody>
<tr id='tr_id' ><td class='label'>Id </td><td id='td_id' ><input type='number' style='display:none' name='id' value='<?php echo $ticket_view->ticket->getId();?>' ></input><?php echo $ticket_view->ticket->getId();?></td></tr>
<?php
if (!isset($edit_remove_status))
{?>
<tr id='tr_status'><td class='label'>Status </td><td id='td_status'><?php
    $edit_status=true;
    $status_attributes=array("class"=>"chosen");
    $ticket_view->renderStatusEnum($ticket_view->ticket,isset($edit_status),isset($readonly_status),isset($hidden_status),$status_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_user_id_submitter))
{?>
<tr id='tr_user_id_submitter'><td class='label'>Submitter </td><td id='td_user_id_submitter'><?php
    $edit_user_id_submitter=true;
    $ticket_view->renderUserIdSubmitterSelect($ticket_view->ticket,isset($edit_user_id_submitter),isset($readonly_user_id_submitter),isset($hidden_user_id_submitter));
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_description))
{?>
<tr id='tr_description'><td class='label'>Description </td><td id='td_description'><?php
    $edit_description=true;
    if (!isset($description_attributes)) 
    { 
        $description_attributes=array("placeholder"=>"Description ");
    } 
    $ticket_view->renderDescriptionTextarea($ticket_view->ticket,isset($edit_description),isset($readonly_description),isset($hidden_description),$description_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($edit_remove_created))
{?>
<tr id='tr_created'><td class='label'>Created </td><td id='td_created'><?php echo $ticket_view->ticket->getCreated();?></td></tr>
<?php
} ?>
</tbody>
</table>
<div class="custom_widget">
<?php if ($ticket_custom_widget) include "$ticket_custom_widget"; ?>
</div>
<?php if ($role->isUserAnyOf($allowed_edit)) { $ticket_view->renderEditControls(); }?>
</form>
<?php
}?>
