<?php 
/**  
 * Used to create new TicketDiscussion
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */
require_once 'Role.php'; 
if (!isset($allowed)) $allowed=array("admin");
$role = new Role();
if (!$role->isUserAnyOf($allowed))
{
	$role->showUnauthorizedMessage($allowed,"?command=display_php&base_path=usr/view&path=New TicketDiscussion.rest.php");
} else
{
if (!isset($ticket_discussion_view)) 
{ 
    require_once 'TicketDiscussionView.php'; 
    $ticket_discussion_view = new TicketDiscussionView(); 
    $ticket_discussion_view->init_new();
} ?> 
<form id="new_ticket_discussion"  <?php echo $ticket_discussion_view->new_form_attributes;?> class="new" action="?command=new&object=TicketDiscussion&base_path=<?php echo $_REQUEST["base_path"];?>&path=<?php echo $path;?>" method="POST">
<?php $ticket_discussion_view->renderCsrf();?> 
<?php $ticket_discussion_view->renderCustomNew();?> 
<table class='odd_even_row_alternate_color full_width data' >
<thead><th>Field</th><th>Data</th></thead>
<tbody>
<?php
if (!isset($new_remove_ticket_id))
{?>
<tr id='tr_ticket_id'><td class='label'>Ticket </td><td id='td_ticket_id'><?php
    $ticket_discussion_view->renderTicketIdSelect($ticket_discussion_view->ticket_discussion,isset($edit_ticket_id),isset($readonly_ticket_id),isset($hidden_ticket_id));
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_user_id))
{?>
<tr id='tr_user_id'><td class='label'>User </td><td id='td_user_id'><?php
    $ticket_discussion_view->renderUserIdSelect($ticket_discussion_view->ticket_discussion,isset($edit_user_id),isset($readonly_user_id),isset($hidden_user_id));
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_message))
{?>
<tr id='tr_message'><td class='label'>Message </td><td id='td_message'><?php
    if (!isset($message_attributes)) 
    { 
        $message_attributes=array("placeholder"=>"Message ");
    } 
    $ticket_discussion_view->renderMessageTextarea($ticket_discussion_view->ticket_discussion,isset($edit_message),isset($readonly_message),isset($hidden_message),$message_attributes);
?>
</td></tr>
<?php
} ?>
<?php
if (!isset($new_remove_created))
{?>
<tr id='tr_created'><td class='label'>Created </td><td id='td_created'><?php echo $ticket_discussion_view->ticket_discussion->getCreated();?></td></tr>
<?php
} ?>
</tbody>
</table><div class="custom_widget">
<?php if ($ticket_discussion_custom_widget) include "$ticket_discussion_custom_widget"; ?>
</div>
<?php $ticket_discussion_view->renderNewControls(); ?>
</form>
</div>
<?php
}?>
