<?php
/**  
 * Used to list all TicketDiscussion
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */
require_once 'TicketDiscussion.php';
require_once 'TicketDiscussionView.php';
require_once 'Role.php'; 
if (!isset($allowed)) $allowed=array("admin");
if (!isset($allowed_edit)) $allowed_edit=array("admin");
$role = new Role();
if (!$role->isUserAnyOf($allowed))
{

	$role->showUnauthorizedMessage($allowed,"?command=display_php&base_path=usr/view&path=List TicketDiscussions.rest.php");
} else
{
if (!isset($ticket_discussion_view)) 
{ 
    $ticket_discussion_view = new TicketDiscussionView();
    $ticket_discussion_view->init_list();
} 
 if (!isset($headers)) $headers = ["id","ticket_id","user_id","message","created"];
 if (array_key_exists("ticket_discussions_container",$_REQUEST)) $ticket_discussion_view->container = $_REQUEST["ticket_discussions_container"];
?>
<form id='<?php echo $ticket_discussion_view->form_list;?>' <?php echo $ticket_discussion_view->list_form_attributes;?> class="form list" action="<?php echo $ticket_discussion_view->action;?>" method="POST">
<input type='hidden' name='ticket_discussions_container' value='<?php echo $ticket_discussion_view->container;?>' />
<?php $ticket_discussion_view->renderCsrf();?>
<?php $ticket_discussion_view->renderCustomList();?>
<table class='odd_even_row_alternate_color full_width data' >
<input class='field_pages' field_pages='<?php echo $ticket_discussion_view->pages; ?>' type='hidden' value='<?php echo $ticket_discussion_view->pages; ?>' name='ticket_discussions_pages' />
<input class='field_page' field_page='<?php echo $ticket_discussion_view->page; ?>' type='hidden' value='<?php echo $ticket_discussion_view->page; ?>' name='ticket_discussions_page' /> 
<thead><tr><th>#</th><th>Actions</th>
<?php if (in_array('id',$headers)) { ?> <th id='header_id'  field='id'><a class='toggle_sort fa fa-sort' href='#'> id</a>             <input id="ticket_discussions_sort_id" type="hidden" field="id" class="sort" name="ticket_discussions_sort_filters[id]" value='<?php echo strtoupper($ticket_discussion_view->sort_filters['id']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('ticket_id',$headers)) { ?> <th id='header_ticket_id'  field='ticket_id'><a class='toggle_sort fa fa-sort' href='#'> tickets</a>             <input id="ticket_discussions_sort_ticket_id" type="hidden" field="ticket_id" class="sort" name="ticket_discussions_sort_filters[ticket_id]" value='<?php echo strtoupper($ticket_discussion_view->sort_filters['ticket_id']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('user_id',$headers)) { ?> <th id='header_user_id'  field='user_id'><a class='toggle_sort fa fa-sort' href='#'> users</a>             <input id="ticket_discussions_sort_user_id" type="hidden" field="user_id" class="sort" name="ticket_discussions_sort_filters[user_id]" value='<?php echo strtoupper($ticket_discussion_view->sort_filters['user_id']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('message',$headers)) { ?> <th id='header_message'  field='message'><a class='toggle_sort fa fa-sort' href='#'> message</a>             <input id="ticket_discussions_sort_message" type="hidden" field="message" class="sort" name="ticket_discussions_sort_filters[message]" value='<?php echo strtoupper($ticket_discussion_view->sort_filters['message']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('created',$headers)) { ?> <th id='header_created'  field='created'><a class='toggle_sort fa fa-sort' href='#'> created</a>             <input id="ticket_discussions_sort_created" type="hidden" field="created" class="sort" name="ticket_discussions_sort_filters[created]" value='<?php echo strtoupper($ticket_discussion_view->sort_filters['created']);?>' style="display:none;">
</th> <?php } ?>
</tr>
<tr><th></th><th><button id="clear_search_and_sort" type="button" class="button"><i class="fa fa-times"></i> <span style="color:#DDDDDD;text-shadow:none"><i class="fa fa-sort"><i class="fa fa-search"></span></button></th><?php if (in_array('id',$headers)) { ?> <th><?php $ticket_discussion_view->renderSearchText('id','number'); ?></th> <?php } ?>
<?php if (in_array('ticket_id',$headers)) { ?><th><?php $ticket_discussion_view->renderTicketIdFilter(); ?></th> <?php }?>
<?php if (in_array('user_id',$headers)) { ?><th><?php $ticket_discussion_view->renderUserIdFilter(); ?></th> <?php }?>
<?php if (in_array('message',$headers)) { ?> <th><?php $ticket_discussion_view->renderSearchText('message','textarea'); ?></th> <?php } ?>
<?php if (in_array('created',$headers)) { ?><th></th> <?php }?>
</tr></thead>
<tbody>
 <?php
    $current_user=new User();
    $id = $_SESSION["user_id"];
    $current_user=$current_user->find(array("id"=>$id));
    $i=($ticket_discussion_view->page-1)*$ticket_discussion_view->item_per_page;
    foreach ($ticket_discussion_view->ticket_discussions as $ticket_discussion)
    { 
        $i=$i+1;
        $path_edit = "Edit TicketDiscussion";
?>
<tr class="id_<?php echo $ticket_discussion->getId();?>" ><td><?php echo $i ;?></td><td><?php $ticket_discussion_view->renderActions($ticket_discussion,"",$role->isUserAnyOf($allowed_edit));?></td><?php if (in_array('id',$headers)) { ?><td><a class='mx-' mx-container='<?php echo $ticket_discussion_view->container;?>' mx-click='?command=display_rest&base_path=Admin.dir/TicketDiscussions.dir&path=Edit TicketDiscussion.hid.rest.php&id=<?php echo $ticket_discussion->getId();?>&object=TicketDiscussion' href='#' ><?php echo $ticket_discussion->getId();?></a></td> <?php }?>

<?php if (in_array('ticket_id',$headers)) { ?>
	<td><?php $ticket_discussion_view->getTicketId($ticket_discussion);?></td> <?php }?>
<?php if (in_array('user_id',$headers)) { ?>
	<td><?php $ticket_discussion_view->getUserId($ticket_discussion);?></td> <?php }?>
<?php if (in_array('message',$headers)) { ?>
	<td><?php $ticket_discussion_view->getMessage($ticket_discussion);?></td> <?php }?>
<?php if (in_array('created',$headers)) { ?>
	<td><?php $ticket_discussion_view->getCreated($ticket_discussion);?></td> <?php }?>
</tr>
<?php } ?>
</tbody>
</table><input class="sort_order" type="hidden" name="ticket_discussions_sort_order" value="" />
<?php $ticket_discussion_view->renderListControls(); ?>
</form>
<?php 
    $ticket_discussion_view->renderPaginator();
?>
<script>
$(document).ready(function() 
{
	var container = '<?php echo $ticket_discussion_view->container;?>';
	var form_list = '<?php echo $ticket_discussion_view->form_list;?>';
    
    var sort_filters = [<?PHP 
                        $i=0;
                        foreach ($ticket_discussion_view->sort_orders as $order) 
                        {
                            if ($i==0)
                            {
                                echo "'$order'";
                            } else echo ",'$order'";
                            $i=$i+1;
                        }?>];
    $('input.sort_order').val(sort_filters.toString());
    $('form').find('a.toggle_sort').each( function()
    {
        var sort_handle = $(this).closest('th').find('input.sort');
        if (sort_handle.val()=='ASC')
        {
            $(this).removeClass('fa-sort-desc');
            $(this).removeClass('fa-sort');
            $(this).addClass('fa-sort-asc');
        } else if (sort_handle.val()=='DESC')
        {
            $(this).removeClass('fa-sort-asc');
            $(this).removeClass('fa-sort');
            $(this).addClass('fa-sort-desc');
        } else
        {
            $(this).removeClass('fa-sort-desc');
            $(this).removeClass('fa-sort-asc');
            $(this).addClass('fa-sort');                
        }        
        $(this).click( function()
        {
            var sort_handle = $(this).closest('th').find('input.sort');
            if ($(this).hasClass('fa-sort-asc'))
            {
                sort_handle.val('DESC');
            } else
            if ($(this).hasClass('fa-sort-desc'))
            {
                sort_handle.val('');
            } else
            if ($(this).hasClass('fa-sort'))
            {
                sort_handle.val('ASC');             
            }


            if (sort_handle.hasClass('sort'))
            {
                var index = sort_filters.indexOf(sort_handle.attr('field'));
                if (index>=0)
                {
                    sort_filters.splice(index, 1);
                } 
                if ((sort_handle.val()=='ASC')||(sort_handle.val()=='DESC'))
                {
                    sort_filters.unshift(sort_handle.attr('field'));
                }
            }
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }
            
            var page=parseInt($('input.field_page').val());
            var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            loadPost(url_rest,$('#'+form_list),container);
            
        });
    });
    $('form').find('select.filter').each( function()
    {
        $(this).change( function()
        {
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }

            var page=parseInt($('input.field_page').val());
            var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            loadPost(url_rest,$('#'+form_list),container);
            
        });
    });    
	$('form').find('.search_text_button').each( function()
    {
        $(this).click( function()
        {
            if (sort_filters.length>0)
            {
                $('input.sort_order').val(sort_filters.toString());
            }

            var page=parseInt($('input.field_page').val());
            var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
            var url_rest = url.replace('display_php','display_rest');
            loadPost(url_rest,$('#'+form_list),container);
            
        });
    });   
    $("#clear_search_and_sort").click( function()
    {
        $("table.data").find("input.search_text").each( function()
        {
            $(this).val("");
        });
        $("table.data").find("select.filter").each( function()
        {
            $(this).val("");
        });			
        $("table.data").find("input.sort").each( function()
        {
            $(this).val("");
        });
        $("table.data").find("a.toglle_sort").each( function()
        {
            if ($(this).hasClass("fa-sort-asc"))
            {
                $(this).removeClass("fa-sort-asc");
            }
            if ($(this).hasClass("fa-sort-desc"))
            {
                $(this).removeClass("fa-sort-desc");
            }
            if ($(this).hasClass("fa-sort"))
            {
            
            }
            else
            {
                $(this).addClass("fa-sort-asc");
            }
            
            $(this).removeClass("fa-sort-asc");
        });
        
        if (sort_filters.length>0)
        {
            $('input.sort_order').val(sort_filters.toString());
        }

        var page=parseInt($('input.field_page').val());
        var url=$('#'+form_list).attr('action')+'&page='+page+'&sort_order='+$('input.sort_order').val();
        var url_rest = url.replace('display_php','display_rest');
        loadPost(url_rest,$('#'+form_list),container);        
    });         
});
</script>
<?php
}?>
