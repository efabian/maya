<?php
require_once 'MayaSecurity.php';
require_once 'paths.cfg.php';
require_once 'Configurations.php';
require_once 'Ticket.php';
require_once 'User.php';
class TicketViewBase 
{ 
    public $security;
    public $ticket;
    public $page;
    public $path;
    public $tickets;
    public $container;
    public $item_per_page;
    public $pages;
    public $search_text;
    public $select_filters;
    public $sort_filters;
    public $sort_orders;
    public function getStatus($ticket)
    {
        echo $ticket->getStatus();
    }
    public function getUserIdSubmitter($ticket)
    {
         echo $ticket->getSubmitter();
    }
    public function getDescription($ticket)
    {
        echo $ticket->getDescription();
    }
    public function getCreated($ticket)
    {
        echo $ticket->getCreated();
    }
    public function renderId($ticket,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$ticket->getId();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='id'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<input $attrs type='text' class='$class  edit_input' name='id' value='".$value."' ></input>";
            } else
            {
                echo "<input $attrs type='text' class='$class ' name='id' value='".$value."' ></input>";
            }
        }
    }
    public function renderStatusEnum($ticket,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'chosen'),$option_filters=array())
    {
		$class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }
    
        $val=$ticket->getStatus();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='status'  value='$val' />";
            if ($readonly)
            {
                echo "<span >$val</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<select $attrs class='$class edit_input'  name='status' >\n";
            } else
            {
                echo "<select $attrs class='$class'  name='status' >\n";
            } 
			$options=array('active','closed','cancelled');
			foreach($options as $option)
			{
				if ($val==$option)
				{
					echo "<option selected='selected' value='$option' >$option</option>\n";
				} else
				{
					echo "<option value='$option' >$option</option>\n";
				}
			}
			echo "</select>\n";
            if ($edit) echo "<span class='view_input'>$val</span>";
            
        }
    }
    public function renderStatusEnumFilter()
    {
		echo "<select  class='filter' name='tickets_select_filters[status]' value='' > \r\n";
        echo "<option value=''>All</option> \r\n";
		$options=array('active','closed','cancelled');
        foreach($options as $option)
		{
			
			if ((array_key_exists('status',$this->select_filters)) && ($this->select_filters['status']==$option))
			{
				echo "<option selected='selected' value='".$option."' >".$option."</option>";
			} 
			else
			{
				echo "<option value='".$option."' >".$option."</option>";
			}
		}
		echo "</select> \r\n";		
	}
	
    public function renderUserIdSubmitterSelect($ticket,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'chosen'),$option_filters=array())
    {
        $sids_str=$ticket->getUserIdSubmitter();
        
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }
        if ($hidden || $readonly)
        {
            echo "<input type='hidden' $attrs name='user_id_submitter'  value='$sids_str' />";
            if ($readonly)
            {
                echo "<span >".$ticket->getSubmitter()."</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<select $attrs class='$class edit_input'  name='user_id_submitter' >\n";
            } else
            {
                echo "<select $attrs class='$class'  name='user_id_submitter' >\n";
            }
            $selected_ids = explode(",",$sids_str);
            $submitter = new User();
            $objs=$submitter->findAll($option_filters);
            $data_names = array();
            foreach ($objs as $option)
            {
                $name = $option->getName();
                $oid = $option->getId();
                if ($oid)
                {
                    if (in_array($oid,$selected_ids))
                    {
                        echo "<option selected='selected' value='$oid' >$name</option>\n";
                        $data_names[] = $name;
                    } else
                    {
                        echo "<option value='$oid' >$name</option>\n";
                    }
                }
            }
            echo "</select>\n";
            if ($edit) echo "<span class='view_input'>".implode(',',$data_names)."</span>";
       }
    }
    public function renderUserIdSubmitterFilter()
    {
		echo "<select  class='filter' name='tickets_select_filters[user_id_submitter]' value='' > \r\n";
		echo "<option value=''>All</option> \r\n";
	
		if (in_array('0',$this->user_id_submitter_lst)) 
		{
			if (array_key_exists('user_id_submitter',$this->select_filters) && ($this->select_filters['user_id_submitter']=='0'))
			{
				echo "<option selected='selected' value='0' >Unassigned</option>";
			} else
			{
				echo "<option value='0' >Unassigned</option>";
			}
		}
		$user_handle = new User();
		$users = $user_handle->findAll(array('id'=>$this->user_id_submitter_lst),array('lastname'=>'ASC'));
		foreach ($users as $user)
		{
			if (($this->select_filters['user_id_submitter']) && ($this->select_filters['user_id_submitter']==$user->getId()))
			{
				echo "<option selected='selected' value='".$user->getId()."' >".$user->getName()."</option>";
			} else
			{
				echo "<option value='".$user->getId()."' >".$user->getName()."</option>";
			}
		}
		echo "</select> \r\n";		
	}
	
    public function renderDescriptionTextarea($ticket,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'jwysiwyg'))
    {
        $class='';
        $attrs='';
        foreach($attributes as $name=>$value)
        {
            if ($name=='class')
            {
               $class=$attributes['class'];
            }
            else
            {
               $attrs.=$name.'="'.$attributes[$name].'" ';
            }
            
        }

        $value=$ticket->getDescription();
        if ($hidden || $readonly)
        {
            echo "<input $attrs type='hidden' name='description'  value='$value' />";
            if ($readonly)
            {
                echo "<span>$value</span>";
            }
        } else
        {
            if ($edit)
            {
                echo "<span class='view_input'>".$value."</span>";
                echo "<textarea id='edit_description' $attrs class='$class  edit_input' name='description' >".$value."</textarea>";                
            } else
            {
                echo "<textarea id='edit_description' $attrs type='text' class='$class ' name='description' >".$value."</textarea>";
            }
        }
    }
    public function __construct($param=array('container'=>'div.page'))
    {
        foreach($param as $field=>$value)
        {
            $this->$field=$value;
        }
        if (!isset($this->container) || (!$this->container))
        {
            $this->container='div.page';
        }
        $this->paths = new Paths();
        $this->config = new Configurations();
    }
    
    public function init_edit()
    { 
        $this->security=new MayaSecurity();
        if (array_key_exists("id",$_GET))
        {
            $this->id=$_GET["id"];
        } else
        {
            $this->id=$_REQUEST["id"];
        }
        $this->ticket = new Ticket();
        $this->ticket->find(array("id"=>$this->id));
        $this->page=$_REQUEST["selected"];
        $this->path=$_REQUEST["path"];
        $this->current_user=new User();
        $this->current_user->find(array("id"=>$_SESSION["user_id"]));
        $this->edit_form_attributes='';
        $this->edit_button_name='Update';
        $this->edit_button_url='?command=editRest&object=Ticket';
        $this->edit_button_next_url='?command=display_rest&path=Admin.dir/Tickets.dir/Edit Ticket.hid.rest.php&id='.$this->ticket->getId();
        $this->edit_delete_button_url='?command=deleteRest&object=Ticket';
        $this->edit_delete_button_next_url='?command=display_rest&path=Admin.dir/Tickets.dir/List Tickets.rest.php';
    } 
    public function init_new()
    { 
        $this->security=new MayaSecurity();
        $this->ticket = new Ticket();
        $this->page=$_REQUEST["selected"];
        $this->path=$_REQUEST["path"];
        $this->current_user=new User();
        $this->current_user->find(array("id"=>$_SESSION["user_id"]));
        $this->new_form_attributes='';
        $this->new_button_name='Create';
        $this->new_button_url='?command=newRest&object=Ticket';
        $this->new_button_next_url='?command=display_rest&path=Admin.dir/Tickets.dir/List Tickets.rest.php';
    } 
    public function init_list()
    { 
        $this->security=new MayaSecurity();
        $valid_filters=array("id","status","user_id_submitter","description","created");
        $this->page=$_REQUEST["tickets_selected"];
        $this->path=$_REQUEST["tickets_path"];
        $this->list_form_attributes='';
        $this->list_button_new_url='?command=display_rest&path=Admin.dir/Tickets.dir/New Ticket.rest.php';
        $this->list_button_new_name='New';
		$this->form_list='form_list';
		$this->action='?command=display_php&path=Admin.dir/Tickets.dir/List Tickets.rest.php';
        if (!$this->container) $this->container='div.page';
        $ticket = new Ticket();
        if (!isset($_SESSION['list_tickets']))
        {
            $_SESSION['list_tickets']=array();
            $_SESSION['list_tickets']['sort_filters']=array();
            $_SESSION['list_tickets']['sort_order']='';
            $_SESSION['list_tickets']['select_filters']=array();
            $_SESSION['list_tickets']['search_texts']=array();
            $_SESSION['list_tickets']['page']='1';
        }

        if (isset($_REQUEST['tickets_sort_filters']))
        {
            $sort_lists = $_REQUEST['tickets_sort_filters'];
            $_SESSION['list_tickets']['sort_filters']=$sort_lists;
        }
        else
        {
            $sort_lists=$_SESSION['list_tickets']['sort_filters'];
        }

        if (isset($_REQUEST['tickets_sort_order']))
        {
            $sort_order = $_REQUEST['tickets_sort_order'];
            $_SESSION['list_tickets']['sort_order']=$sort_order;
        }
        else
        {
            $sort_order = $_SESSION['list_tickets']['sort_order'];
        }
        
        if (!$sort_lists) 
        {
            $sort_lists = array('id'=>'DESC');
            $sort_order = 'id';
        }
        
        $this->sort_filters = array();
        if ($sort_order)
        {
            $this->sort_orders = explode(',',$sort_order);
            foreach ($this->sort_orders as $s_order)
            {
                $this->sort_filters[$s_order]=$sort_lists[$s_order];
            }
        } else $this->sort_orders = array();

		if (!isset($this->select_filters)) $this->select_filters=array();
        if (isset($_REQUEST['tickets_select_filters']))
        {
            foreach($_REQUEST['tickets_select_filters'] as $key=>$value)
            {
                if (!(is_numeric($key)) && in_array($key,$valid_filters))
                {
                    $this->select_filters[$key] = $value;
                }
            }
            $_SESSION['list_tickets']['select_filters']=$this->select_filters;
        }
        else
        {
            $this->select_filters=array_merge($_SESSION['list_tickets']['select_filters'],$this->select_filters);
        }
        
        


        $tickets_all = $ticket->findAll(array(),array(),100000,1,array("status","user_id_submitter"),false);
        $this->status_lst=array();
        $this->user_id_submitter_lst=array();

        foreach ($tickets_all as $item)
        {
                    $status=$item->getStatus();
        $this->status_lst[$status]=$status;
        $user_id_submitter=$item->getUserIdSubmitter();
        $this->user_id_submitter_lst[$user_id_submitter]=$user_id_submitter;

        }


        $search='';
        if (isset($_REQUEST['tickets_search_texts']))
        {
            $this->search_texts = $_REQUEST['tickets_search_texts'];
            $_SESSION['list_tickets']['search_texts']=$this->search_texts;
        }
        else
        {
            $this->search_texts = $_SESSION['list_tickets']['search_texts'];
        }
        if (is_array($this->search_texts))
        {
            foreach($this->search_texts as $field=>$value)
            {
                if (trim($value))
                {
                    if ($search)
                    {
                        $search.=" AND $field like '%$value%' ";
                    }
                    else
                    {
                        $search=" $field like '%$value%' ";
                    }
                }
            }
        }
        if ($search)
        {
            $this->select_filters[] = $search;
        }


        if (!($this->item_per_page)) $this->item_per_page = 30;
        $this->pages=$ticket->getPageCount($this->select_filters,$this->sort_filters,$this->item_per_page,true);
        
        $this->page = 1;//default to 1 in case not provided below
        if (isset($_REQUEST['tickets_page']))
        {
            $this->page = $_REQUEST['tickets_page'];
            $_SESSION['list_tickets']['page']=$this->page;
        } else
        {
            $this->page = $_SESSION['list_tickets']['page'];
            if (array_key_exists('list_tickets',$_SESSION) && array_key_exists('page',$_SESSION['list_tickets']))
            {
                $this->page = $_SESSION['list_tickets']['page'];
            }            
        }
        if (($this->page < 1) || ($this->page > $this->pages)) $this->page = 1;

        
        $this->tickets = $ticket->findAll($this->select_filters,$this->sort_filters,$this->item_per_page,$this->page);
    } 
    public function renderPaginator()
    { 
        include 'PaginatorView.php'; 
        include 'PaginatorController.php'; 
    } 
    public function renderCsrf()
    { 
        echo '<input type="hidden" name="'.$this->security->getCsrfName().'" value="'.$this->security->getCsrfValue().'"/>'; 
    } 
    public function renderActions($ticket,$class="",$edit=TRUE)
    {
        if ($edit)
        {
            echo "<a class='mx- $class' mx-container='".$this->container."' mx-click='?command=display_rest&base_path=Admin.dir/Tickets.dir&path=Edit Ticket.hid.rest.php&id=".$ticket->getId()."&object=Ticket' href='#' ><li class='fa fa-pencil'></li> edit</a>"; 
        } else
        {
            echo "<a class='mx- $class' mx-container='".$this->container."' mx-click='?command=display_rest&base_path=Admin.dir/Tickets.dir&path=View Ticket.hid.rest.php&id=".$ticket->getId()."&object=Ticket' href='#' ><li class='fa fa-eye'></li> view</a>"; 
        }
    }
    
	public function renderEditControls()
    {
        if ($this->edit_button_next_url)
        { 
            $next_after_update='mx-next="'.$this->edit_button_next_url.'"';
        } else $next_after_update='';
        if ($this->edit_delete_button_next_url)
        {
            $next_after_delete='mx-next="'.$this->edit_delete_button_next_url.'"';
        } else $next_after_delete='';
		echo '<div class="control" style="height:80px">';
		echo '<a class="button mx-" mx-click="'.$this->edit_button_url.'"  href="#" mx-container="div.page" '.$next_after_update.' ><li class="fa fa-save"></li> '.$this->edit_button_name.'</a>';
		echo '<a class="button mx- danger" mx-confirm="Are you sure to delete?" mx-click="'.$this->edit_delete_button_url.'" href="#" mx-container="'.$this->container.'" '.$next_after_delete.' ><li class="fa fa-trash" ></li> Delete </a>';
		echo '</div>';
	}    
	
	public function renderNewControls()
	{
        if ($this->new_button_next_url)
        {
            $next_after_create='mx-next="'.$this->new_button_next_url.'"';
        } else
        {
            $next_after_create='';
        }
		echo '<div class="control" style="height:80px">';
		echo '<a class="button mx-" mx-click="'.$this->new_button_url.'" href="#" '.$next_after_create.' mx-container="'.$this->container.'"><li class="fa fa-plus"></li> '.$this->new_button_name.' </a>';
		echo '</div>';
	}
    
    public function renderListControls($new_after_new='')
    {
        echo '<div class="control" style="height:80px">'."\r\n";
        echo '<a class="button mx-" mx-container="'.$this->container.'" mx-click="'.$this->list_button_new_url.'" href="#" ><i class="fa fa-plus"></i> '.$this->list_button_new_name.'</a>'."\r\n";
        echo '</div>'."\r\n";  
    }
    
    public function renderCustomEdit()
    {
    }

    public function renderCustomNew()
    {
    }

    public function renderCustomlist()
    {
    }
    
    public function renderSearchText($field,$type)
    {
		echo '<div class="search_filter_header">';
		$value='';
		if (isset($this->search_texts) && array_key_exists($field,$this->search_texts)) $value=$this->search_texts[$field];
		echo '<input class="search_text '.$type.'" type="text" name="tickets_search_texts['.$field.']" value="'.$value.'"></input><a class="search_text_button" ><li class="fa fa-search"></li></a>';
		echo '</div>';
    }


    public function renderUploadImage($ticket,$disable__icon=FALSE,$redirect_after_image_upload='',$img_attributes='',$edit_enabled=TRUE)
    {
        if (!isset($this->src_)) $this->src_='img/displaypng.php?id='.$this->current_user->getId().'&image='.$this->ticket->getImage('').'&key='.urlencode($this->current_user->getSecurekey()).'&rnd='.rand(1,100);    
        if (!isset($this->src__icon)) $this->src__icon='img/displaypng.php?id='.$this->current_user->getId().'&image='.$this->ticket->getImageIcon('').'&key='.urlencode($this->current_user->getSecurekey()).'&rnd='.rand(1,100);    
        if ($edit_enabled)
        {
            echo '<form id="upload_" action="?command=uploadImage&object=Ticket&id='.$ticket->getId().'" class="form image round-corners" method="post" style="float:center; display:block" enctype="multipart/form-data" >';
        }
        echo '<div>
                <div style="display:inline-block" >
                    <img class="profile"  src="'.$this->src_.'" '.$img_attributes.'/>
                </div>';
                if (!(isset($disable__icon) && $disable__icon))
                {
                    echo '<div style="display:inline-block">';
                    echo '<img class="profile_icon"  src="'.$this->src__icon.'" />';
                    echo '<label style="display:block">Icon</label>';
                    echo '</div>';
                }
        if ($edit_enabled)
        {
                $this->renderCsrf();
            echo '<input type="file" name="file" id="file" class="button fa fa-plus" accept="image/*" />';
            echo '<input type="hidden" name="id" value="'.$this->ticket->getId().'" />';
            echo '<input type="hidden" name="redirect" value="'.$redirect_after_image_upload.'" />';
            echo '<textarea id="upload_content" type="submit" style="display:none" name="upload_content" value=""></textarea>';
            echo '<button type="submit" class="button" name="submit"><i class="fa fa-upload"></i>Upload</button>';
        }
        
            echo '</div>';
        if ($edit_enabled)
        {
            echo '</form>';
        }
    }

    public function renderImage($ticket)
    {
        echo '<span>'.$ticket->getImage().". (See upload button above this table)</span>\n";
    }
}
