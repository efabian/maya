<?php 
/**  
 * [DESCRIPTION]
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */

require_once 'MayaSecurity.php';
require_once 'security.cfg.php';
require_once 'BunkerSupply.php';
require_once 'Next.php';
class MayaBunkerSuppliesController 
{
	
    public $security;
    
    public function __construct()
    {
        $this->security = new MayaSecurity();
    }

    

    protected $requirements;
    
    protected function setRequirements($reqs)
    {
        $this->requirements = $reqs;
    }
    
    protected function validate()
    {
        $success=TRUE;
        $message='';
        if (isset($this->requirements))
        {
            foreach($this->requirements as $field=>$req)
            {
                if (in_array('required',$req))
                {
                    if (!((array_key_exists($field,$_REQUEST)) && trim($_REQUEST[$field]))) 
                    {
                        $success = FALSE;
                        $message.= "Missing required $field.\r\n";
                    }
                }
                if (in_array($field,$_POST))
                {
                    if (in_array('email',$req))
                    {
                        if (!filter_var(trim($_POST[$field]), FILTER_VALIDATE_EMAIL))
                        {
                            $success = FALSE;
                            $message.= 'Your $field:'.$_POST[$field]." appears to be invalid email.\r\n";
                        }
                    }
                    if (in_array('numeric',$req))
                    {
                        if (!is_numeric(trim($_POST[$field])))
                        {
                            $success = FALSE;
                            $message.= "$field is not numeric .\r\n";
                        }
                    }            
                }
            }
        }
        
        $response = array('message'=>$message,'success'=>$success);
        
        return $response;
    }
    
    protected function getPost($bunker_supply)
    {
        
        if (array_key_exists("name",$_POST))  $bunker_supply->setName($_POST["name"]);
        if (array_key_exists("description",$_POST))  $bunker_supply->setDescription($_POST["description"]);
        if (array_key_exists("user_id",$_POST))  $bunker_supply->setUserId($_POST["user_id"]);
        if (array_key_exists("expiry_date",$_POST))  $bunker_supply->setExpiryDate($_POST["expiry_date"]);
;
        return $bunker_supply;
    }
        
    protected function getPostNew($bunker_supply)
    {
        return $this->getPost($bunker_supply);
    }
        
    public function create()
    {
        $response = $this->validate();
        if ($response['success'])
        {
            $bunker_supply= new BunkerSupply();
            $bunker_supply = $this->getPostNew($bunker_supply);
            $response = $this->save($bunker_supply);
        }
        
        return $response;
    }

    
    protected function getBunkerSupply()
    {
        $id = $_REQUEST['id'];
		$bunker_supply= new BunkerSupply();
		$bunker_supply = $bunker_supply->find(array('id'=>$id));
        return $bunker_supply;
    }
    
    protected function setDefaultIdRequirements()
    {
        if (!isset($this->requirements))
        {
            $this->requirements=array('id'=>array('required'));
        }
        if (!array_key_exists('id',$this->requirements))
        {
            $this->requirements = array_merge($this->requiremenst, array('id'=>array('required')));
        }
    }

    protected function setEditRequirements()
    {
        $this->setDefaultIdRequirements();
    }
    
    protected function setNewRequirements()
    {
        $this->requirements = array();
    }    
    
    protected function edit()
    {
        $response = $this->validate();
        if ($response['success']) 
        {
            $bunker_supply = $this->getBunkerSupply();
            if (is_object($bunker_supply) && ($bunker_supply->getId()))
            {
                $bunker_supply=$this->getPost($bunker_supply);
                $response = $this->save($bunker_supply);
            }
            else
            {
                $message='Cannot find $bunker_supply! '.$_SESSION['maya_notice'];
                $success=false;
                $response = array('message'=>$message,'success'=>$success);
            }
        }
        return $response;
    }
    
    
    protected function remove($bunker_supply)
    {
        if ($bunker_supply->getId()>0)
        {
            if ($bunker_supply->delete()==1)
            {
                
                $message='Successfully deleted '.$id;
                $success=true;
            } else
            {
                $message='SQL Error! '.$_SESSION['maya_notice'];
                $success=false;
            }
        } else
        {
            $message='Cannot find bunker_supply with id='.$id;
            $success=false;            
        }
        $response = array('message'=>$message,'success'=>$success);
        return $response;
    }    

    public function delete()     
    {
        $response = $this->validate();
        if ($response['success']) 
        {
            $bunker_supply = $this->getBunkerSupply();
            $response=$this->remove($bunker_supply);
        }
        return $response;
    }
    
    public function newRestAction()
    {
        $this->setNewRequirements();
        $response = $this->create();
        if (array_key_exists('id',$response))
        {
            $next = new Next();
            $next->url = '?command=display_rest&path=Admin.dir/BunkerSupplies.dir/Edit BunkerSupply.hid.rest.php&id='.$response['id'];
            $next->container = 'div.page';
            $response['next']=$next;
        }
        $this->json_response($response);
    }    
    
    public function editRestAction()     
    {
        $this->setEditRequirements();
        $response = $this->edit();
        $this->json_response($response);
    }    

    public function deleteRestAction()     
    {
        $this->setDefaultIdRequirements();
        $response=$this->delete();
        $this->json_response($response);        
    }

    public function newAction()
    {
        $this->setNewRequirements();
        $response = $this->create();
        return $this->bool_response($response);
    }

    public function editAction()     
    {
        $this->setEditRequirements();
        $response = $this->edit();
        return $this->bool_response($response);
    }
    
    public function deleteAction()     
    {
        $this->setDefaultIdRequirements();
        $response = $this->delete();
        return $this->bool_response($response);
    }



    public function save($bunker_supply)
    {
        if ($bunker_supply->save()>0)
        {

           $message="Successfully saved ".$bunker_supply->getName();
           $success=true;
           
        } else
        {
           $message='SQL Error! '.$_SESSION['maya_notice'];
           $success=false;
        }
        
        $response = array('message'=>$message,'success'=>$success,'id'=>$bunker_supply->getId());    
        return $response;
    }

    public function json_response($response)
    {
        header('Content-Type: application/json');
        echo json_encode($response);
        exit();           
    }
    
    public function bool_response($response)
    {
        $_SESSION['maya_notice']=$response['message'];
        return $response['success'];
    }
}

