<?php 
/**  
 * [DESCRIPTION]
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */

require_once 'MayaSecurity.php';
require_once 'security.cfg.php';
require_once 'Configurations.php';
require_once 'MayaImage.php';
require_once 'User.php';
require_once 'Next.php';
class MayaUsersController 
{
	
    public $security;
    
    public function __construct()
    {
        $this->security = new MayaSecurity();
    }

    
    public function loginAction($check_field)
    {
        $check_value = $_POST[$check_field];
        $password = $_POST['password'];    
        if ($_SESSION['user_id']==0)
        {
            $user = new User();
            $user->setId(0);
            $user = $user->find(array($check_field=>$check_value));
            if ($user->getId()>0)
            {
                $secure = new Secure_config();
                
                if ($this->security->verifyPassword($password,$user->getSalt(),$secure->secret_key,$user->getPassword()))
                {
                    $user=$this->login($user,$secure);
                    return $user;
                } else
                {
                    $_SESSION['maya_notice']='Sorry invalid credentials!';
                }
            } else
            {
                $_SESSION['maya_notice']='Identity not found!';
            }
        } else 
        {
            $_SESSION['maya_notice']='';
        }
        return false;
	}

    public function login($user,$secure)
    {
        $now = new DateTime('NOW');
        $user->setLogin($now->format('Y-m-d H:i:s'));
        $user->setLoginip($_SERVER['REMOTE_ADDR']);          
        $user->save();
              
        $_SESSION['user_id']=$user->getId();
        $_SESSION['user_name']=$user->getFirstname().' '.$user->getLastname();
        $_SESSION['user_email']=$user->getEmail();
        $_SESSION['user_role']=$user->getRole();
        $_SESSION['user_domain']=$secure->domain;
        $_SESSION['maya_notice']='Welcome '.$_SESSION['user_name'].'!';
        $_SESSION['site_map']='';
        return $user;
    }
    
    public function logoutAction()
    {
        $_SESSION['user_id']=0;
        $_SESSION['user_name']=0;
        $_SESSION['user_email']=0;
        $_SESSION['user_role']=0;
        $_SESSION['user_domain']='';
        $_SESSION['maya_notice']="Successful logout!<script>$( function() { setTimeout( function () { window.location.href = '?home=true';},3000)});</script>";
	} 
    
    public function registerRestAction()
    {
		$user= new User();
		$user= $this->getPostNew($user);
        $response = $this->save_new_user($user);
        if ($response['success'])
        {
            $message='Please monitor your email to confirm your registration as buyer.';
            $response['message']=$response['message']."\r\n".$message;
            
            $next = new Next();
            $next->silent = 1;
            $next->url = '?command=registerConfirmSendLinkRest&object=User&id='.$response['id'];
            $next->container = 'div.page';
            $response['next']=$next;
        }
        $this->json_response($response);
    }
    
    protected function sendLink($user,$command,$msg)
    {
        $config = new Configurations();
        $response=array();
        if ($user->getId())
        {
            
            
            if (trim($user->getResetKey()))
            {
                $key=$user->getResetKey();
            }
            else
            {
                $key = $this->security->generateKey(32);
                $user->setResetkey($key);
            }            
            
            $email = new Email();
            $email->setSubject('Password Reset');
            if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] != 'off')) 
            {
                $ht = 'https';
            } else
            {
                $ht='http';
            }
            $config_paths = new Paths();
            
            $link = $ht.'://'.$config->domain.'/?command='.$command.'&object=User&email='.$user->getEmail().'&resetkey='.urlencode($user->getResetkey());            
            $msg.=$link;
            $email->setMessage($msg);
            $email->addTo($user->getEmail());
            if ($config->office_email)
			{
				$email->addFrom($config->office_email);
			}
            if ($user->save())
            {
                if ($email->send())
                {
                    
                    $message='Reset details emailed to your primary email account.';
                    $success=TRUE;
                } else
                {
                    $message='Email send error!';
                    $success=false;
                }
            } else
            {
                $message='Unable to update user!';
                $success=false;
            }
        } else
        {
            $message='Unknown user!';
            $success=false;
        }    
        return array('success'=>$success,'message'=>$message);
    }

    public function registerConfirmSendLinkRestAction()
    {
        $id = $_GET['id'];
        if (!$id)
        {
            $message='User Id not provided!';
            $success=false;
        }
        $user= new User();
        $user = $user->find(array('id'=>$id));
        $msg="To confirm your account as buyer please visit the link below:\r\n";
        $response = $this->sendLink($user,'registrationConfirm',$msg);    
        $this->json_response($response);
    }
    

    public function forgotPasswordSendLinkAction()
    {
        $email = $_REQUEST['email'];
        if (!$email)
        {
            $_SESSION['maya_notice']='Email not provided!';
            return false;
        }
        $user= new User();
        $user = $user->find(array('email'=>$email));
        $msg="To reset your password please visit the link below:\r\n";
        $response = $this->sendLink($user,'forgotPasswordReceive',$msg);    
        if ($response['success'])
        {
            $_SESSION['maya_notice']=$response['message'];
            return $user;
        }    
        else
        {
            $_SESSION['maya_notice']=$response['message'];
            return FALSE;
        }
    }
    
    public function forgotPasswordReceiveActionGet()
    {
		$email = $_REQUEST['email'];
		$key = $_REQUEST['resetkey'];
		$user= new User();
        $user->find(array('email'=>$email));
        $user_key = $user->getResetkey();
        if (($key==$user_key) || ($user->getId()==$_SESSION['user_id'])) 
        {
            $secure = new Secure_config();
            $user->setResetkey('');
            $user=$this->login($user,$secure);
            $_SESSION['user_required_action']='change_password';
            $_SESSION['maya_notice']='Please update your password through your profile!';
            
            $paths = new Paths();
            $_GET['id']=$user->getId();
			return $paths->admin.'/Users.dir/Edit User.hid.rest.php';
        } else
        {
			$_SESSION['user_id']=0;
			$_SESSION['user_name']=0;
			$_SESSION['user_email']=0;
			$_SESSION['user_role']=0;
			$_SESSION['user_required_action']='';
			$_SESSION['maya_notice']='Invalid Credentials or link may have been used already!';
			return false;			
        }
	}

    public function registrationConfirmActionGet()
    {
		$email = $_REQUEST['email'];
		$key = $_REQUEST['resetkey'];
		$user= new User();
        $user->find(array('email'=>$email));
        $user_key = $user->getResetkey();
        if (($key==$user_key) || ($user->getId()==$_SESSION['user_id'])) 
        {
            $role = new Role();
            $role_value=0;
            if (defined('DEFAULT_REGISTRATION_ROLE'))
            {
                $role_value = $role->getValueFromName(DEFAULT_REGISTRATION_ROLE);
            }
            $user->setRole($role_value);
            $user->setResetkey('');
            $secure = new Secure_config();
            
            $user=$this->login($user,$secure);
            $_SESSION['user_required_action']='change_password';
            $_SESSION['maya_notice']='Please update your password through your profile!';
            
            $paths = new Paths();
            $_GET['id']=$user->getId();
			return $paths->admin.'/Users.dir/Edit User.hid.rest.php';
        } else
        {
			$_SESSION['user_id']=0;
			$_SESSION['user_name']=0;
			$_SESSION['user_email']=0;
			$_SESSION['user_role']=0;
			$_SESSION['user_required_action']='';
			$_SESSION['maya_notice']='Invalid Credentials or link may have been used already!';
			return false;			
        }
	}

	public function updatePasswordAction()
    {
		$user= new User();

		$user = $user->find(array('id'=>$_REQUEST['id']));
		$password = $_REQUEST['password'];
        $confirm_password = $_REQUEST['confirm_password'];
        if ((strlen($user->getEmail())>0) && (strcmp($password,$confirm_password)==0) && (strlen($password)>3))
        {
            $secure = new Secure_config();
            $user->setSalt($this->security->generateSalt(10));
            $user->setPassword($this->security->generateHash($confirm_password,$user->getSalt(),$secure->secret_key));
            $now = new DateTime('NOW');
            $user->setModified($now->format('Y-m-d H:i:s'));
            $user->setUpdateip($_SERVER['REMOTE_ADDR']);
            $user->setResetkey('');
            if ($user->save()>0)
            {
                $_SESSION['maya_notice']='Password successfully updated!';
                return $user;
            } else
            {
                $_SESSION['maya_notice']='Password update error!';
                return false;
            }
        } else
        {
            $_SESSION['maya_notice']='Password mismatched!';
            return false;
        }
    }
    
    public function testDriveAction()
    {
        $config = new Configurations();
        $role = new Role();
        if (defined('ENABLE_TEST_DRIVE') && ENABLE_TEST_DRIVE)
        {
            if (array_key_exists('email',$_POST) && trim($_POST['email']))
            {
                $message='';
                $test_role = strtolower(ENABLE_TEST_DRIVE);
                $user = new User();
                $email_add = strip_tags(trim($_POST['email']));
                
                $user->find(array('email'=>$email_add));
                
                $key = $this->security->generateKey(32);
                if ($user->getId())
                {
                    $message="You already have an existing account. Now setting to $test_role. ";
                    $user->setRole($role->$test_role);
                    if (!trim($user->getResetKey()))
                    {
                        $user->setResetkey($key);
                    }
                }
                else
                {
                    $user->setEmail($email_add);
                    $names=array('test',ENABLE_TEST_DRIVE);
                    $pos_at = strpos($email_add,'@');
                    if ($post_at)
                    {
                        $name=substr($email_add,0,$post_at+1);
                        $names=array($name,ENABLE_TEST_DRIVE);
                        if (strpos($name,'.'))
                        {
                            $names=explode('.',$name);
                        }
                        else if (strpos($name,'_'))
                        {
                            $names=explode('.',$name);
                        }
                    }
                    $user->setFirstname($names[0]);
                    if (count($names)>1)
                    {
                        $user->setLastname($names[1]);
                    }
                    
                    $user->setResetkey($key);
                }    
                
                
                $config = new Configurations();
                $email = new Email();
                $email->setSubject('Test as doctor');
                if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] != 'off')) 
                {
                    $ht = 'https';
                } else
                {
                    $ht='http';
                }
                $config_paths = new Paths();
                
                $link = $ht.'://'.$config->domain.'/?command=forgotPasswordReceive&object=User&email='.$user->getEmail().'&resetkey='.urlencode($user->getResetkey());            
                
                $msg="To automatically login please click the one-time login link below:<br>".$link;
                $email->setMessage($msg);
                $email->addTo($email_add);
                if ($config->office_email)
                {
                    $email->addFrom($config->office_email);
                }
                if ($user->save())
                {
                    if ($email->send())
                    {
                        
                        $message.='Login details emailed to your '.$email_add.' email account.';
                        $success=TRUE;
                    } else
                    {
                        $message.='Email send error!';
                        $success=FALSE;
                    }
                } else
                {
                    $message='Unable to update user! '.$_SESSION['maya_notice'];
                    $success=FALSE;
                }
        
                $response = array('success'=>$success,'message'=>$message);
            }
            else
            {
                $response = array('success'=>FALSE,'message'=>'Missing email!');
            }
        }
        else
        {
            $response = array('success'=>FALSE,'message'=>'Unsupported method!');
        }
        return $this->json_response($response);
    }  


    


    protected $requirements;
    
    protected function setRequirements($reqs)
    {
        $this->requirements = $reqs;
    }
    
    protected function validate()
    {
        $success=TRUE;
        $message='';
        if (isset($this->requirements))
        {
        
            foreach($this->requirements as $field=>$req)
            {
                if (in_array('required',$req))
                {
                    if (!((array_key_exists($field,$_REQUEST)) && trim($_REQUEST[$field]))) 
                    {
                        $success = FALSE;
                        $message.= "Missing required $field.\r\n";
                    }
                }
                if (in_array($field,$_POST))
                {
                    if (in_array('email',$req))
                    {
                        if (!filter_var(trim($_POST[$field]), FILTER_VALIDATE_EMAIL))
                        {
                            $success = FALSE;
                            $message.= 'Your $field:'.$_POST[$field]." appears to be invalid email.\r\n";
                        }
                    }
                    if (in_array('numeric',$req))
                    {
                        if (!is_numeric(trim($_POST[$field])))
                        {
                            $success = FALSE;
                            $message.= "$field is not numeric .\r\n";
                        }
                    }            
                }
            }
        }
        
        $response = array('message'=>$message,'success'=>$success);
        
        return $response;
    }
    
    protected function getPost($user)
    {
        
        if (array_key_exists("firstname",$_POST))  $user->setFirstname($_POST["firstname"]);
        if (array_key_exists("lastname",$_POST))  $user->setLastname($_POST["lastname"]);
        if (array_key_exists("email",$_POST))  $user->setEmail($_POST["email"]);
        if (array_key_exists("profile",$_POST))  $user->setProfile($_POST["profile"]);
        if (array_key_exists("role",$_POST))  $user->setRole($_POST["role"]);
        if (array_key_exists("ip",$_POST))  $user->setIp($_POST["ip"]);
        if (array_key_exists("updateip",$_POST))  $user->setUpdateip($_POST["updateip"]);
        if (array_key_exists("loginip",$_POST))  $user->setLoginip($_POST["loginip"]);
        if (array_key_exists("resetkey",$_POST))  $user->setResetkey($_POST["resetkey"]);
        if (array_key_exists("securekey",$_POST))  $user->setSecurekey($_POST["securekey"]);
;
        return $user;
    }
        
    protected function getPostNew($user)
    {
        return $this->getPost($user);
    }
        
    public function create()
    {
        $response = $this->validate();
        if ($response['success'])
        {
            $user= new User();
            $user = $this->getPostNew($user);
            $response = $this->save_new_user($user);
        }
        if (array_key_exists('id',$response))
        {
            $next = new Next();
            $next->url = '?command=display_rest&path=Admin.dir/Users.dir/Edit User.hid.rest.php&id='.$response['id'];
            $next->container = 'div.page';
            $response['next']=$next;
        }
        return $response;
    }
    
    protected function save_new_user($user)
    {
        $old_user = new User();
        $old_user = $old_user->find(array('email'=>$user->getEmail()));
        $confirm_password=$_POST['confirm_password'];
        if ($old_user->getId()>0)
        {
            $message='User with the same email='.$user->getEmail().'  already exist!';
            $success =false;
        } else 
        if (strcmp($_POST["password"],$confirm_password)===0)
        {
            if (strlen($confirm_password)<50)
            {
                $user->setSalt($this->security->generateSalt(10));
                $user->setPassword($this->security->generateHash($confirm_password,$user->getSalt(),$secure->secret_key));
                $user->setIp($_SERVER['REMOTE_ADDR']);
                $message.= "Profile and password updated! \r\n";
                if ($user->save()>0)
                {

                   $message='Successfully created '.$user->getName().'.';
                   $success=true;
                   
                } else
                {
                   $message='SQL Error! '.$_SESSION['error'].' '.$_SESSION['maya_notice'];
                   $success=false;
                }
            } else
            {
                $message.='Password too long! '.$_SESSION['error'];
                $success=false;
            }
        } else
        {
            $message='Password did not match!';
            $success =false;
        }        
        $response = array('message'=>$message,'success'=>$success,'id'=>$user->getId());
        return $response;
    }
    
    protected function getUser()
    {
        $id = $_REQUEST['id'];
		$user= new User();
		$user = $user->find(array('id'=>$id));
        return $user;
    }

    protected function setDefaultIdRequirements()
    {
        if (!isset($this->requirements))
        {
            $this->requirements=array('id'=>array('required'));
        }
        if (!array_key_exists('id',$this->requirements))
        {
            $this->requirements = array_merge($this->requiremenst, array('id'=>array('required')));
        }
    }

    protected function setEditRequirements()
    {
        $this->setDefaultIdRequirements();
        $edit_user_requirements = array('firstname'=>array('required'),'lastname'=>array('required'),'email'=>array('required','email'));
        $this->requirements = array_merge($this->requirements, $edit_user_requirements);
    }
    
    protected function setNewRequirements()
    {
        $this->requirements = array('firstname'=>array('required'),'lastname'=>array('required'),'password'=>array('required'),'email'=>array('required','email'));
    }  
    
    protected function edit()
    {
        $response = $this->validate();
        if ($response['success']) 
        {
            $user = $this->getUser();
            if (is_object($user) && ($user->getId()))
            {
                $user=$this->getPost($user);
                $response = $this->update_user($user);
            }
            else
            {
                $message='Cannot find $user! '.$_SESSION['maya_notice'];
                $success=false;
                $response = array('message'=>$message,'success'=>$success);
            }
        }
        return $response;
    }
    
    protected function update_user($user)
    {
        $user->setIp($_SERVER['REMOTE_ADDR']);
        
        $secure = new Secure_config();
        $confirm_password = $_POST["confirm_password"];
        $message = '';
        $success = true;
        if ((strlen($confirm_password)>0) || (strlen($_POST["password"])>0))
        {
            if (strcmp($_POST["password"],$confirm_password)===0)
            {
                if (strlen($confirm_password)<50)
                {
                    $user->setSalt($this->security->generateSalt(10));
                    $user->setPassword($this->security->generateHash($confirm_password,$user->getSalt(),$secure->secret_key));
                    $user->setIp($_SERVER['REMOTE_ADDR']);
                    $message.= "Profile and password updated! \r\n";
                } else
                {
                    $message.='Password too long! '.$_SESSION['error'];
                    $success=false;
                }
            } else
            {
                $message.='Password Error! '.$_SESSION['error'];
                $success=false;
            }
        } else
        {
            $message.="Profile updated!, password preserved. \r\n";
        }
            

        if ($success)
        {
            if ($user->save()>0)
            {
               $message.='Successfully saved '.$user->getName();
               $success=true;
            } else
            {
               $message.='SQL Error! '.$_SESSION['error'].' '.$_SESSION['maya_notice'];
               $success=false;
            }
        }
        $response = array('message'=>$message,'success'=>$success);
        return $response;         
    }
    
    protected function remove($user)
    {
        if ($user->getId()>0)
        {
            if ($user->delete()==1)
            {
                
                $message='Successfully deleted '.$id;
                $success=true;
            } else
            {
                $message='SQL Error! '.$_SESSION['maya_notice'];
                $success=false;
            }
        } else
        {
            $message='Cannot find user with id='.$id;
            $success=false;            
        }
        $response = array('message'=>$message,'success'=>$success);
        return $response;
    }    

    public function delete()     
    {
        $response = $this->validate();
        if ($response['success']) 
        {
            $user = $this->getUser();
            $response=$this->remove($user);
        }
        return $response;
    }   
    
    public function newRestAction()
    {
        $this->setNewRequirements();
        $response = $this->create();
        $this->json_response($response);
    }    
    
    public function editRestAction()     
    {
        $this->setEditRequirements();
        $response = $this->edit();
        $this->json_response($response);
    }    

    public function deleteRestAction()     
    {
        $this->setDefaultIdRequirements();
        $response=$this->delete();
        $this->json_response($response);        
    }

    public function newAction()
    {
        $this->setNewRequirements();
        $response = $this->create();
        return $this->bool_response($response);
    }

    public function editAction()     
    {
        $this->setEditRequirements();
        $response = $this->edit();
        return $this->bool_response($response);
    }
    
    public function deleteAction()     
    {
        $this->setDefaultIdRequirements();
        $response = $this->delete();
        return $this->bool_response($response);
    }   
    

    public function uploadImageAction()
    {
		$user = $this->getUser();;
        $this->uploadImage($user);
        if (isset($_POST['redirect']) && $_POST['redirect'])
        {
            $url = $_POST['redirect'];
            header('Location: '.$url);
            exit();
        }        
    }
    
    public function generateImageName($user,$folder)
    {
        $name = str_replace(',','_',str_replace(' ','',strip_tags($user->getId().'_users')));
        $name=strtolower($name).($folder?'_'.$folder:'');    
        return $name;
    }
    
    protected function uploadImage($user,$folder='',$force_png=TRUE,$max_width=480)
    {
        $current_image = $user->getImage($folder);
        if (!(strpos($current_image,'none.png')>0))  unlink($current_image);
        if (!$folder) 
        {
            $current_icon_image = $user->getImageIcon($folder);
            if (!(strpos($current_icon_image,'none_icon.png')>0))  unlink($current_icon_image);
        }
        $filename = $this->generateImageName($user,$folder);
        
        $paths = new Paths();
        $ext = strtolower(strrchr($_FILES["file"]["name"], '.'));
        $image = new MayaImage();
        $image->setFilename($filename.$ext);
        $image->setPath($paths->user_image."users/");
        
        if ($image->save())
        {
            $method='';
            if ($max_width && ($image->getWidth() > $max_width))
            {
                $image->setWidth(480);
                $method='force_width';
            }
            if (($ext!='.png')&&($force_png)&&($ext!='.svg')||($method)) //allow only png and svg
            {
                $image->resize($method); //all image must be stored as png
                $ext='.png';
            }
            if (!$folder) $this->createIcon($image,$filename);
            $_SESSION['maya_notice'].='Successfully uploaded image';
            return $filename.$ext;
        } else
        {
            $_SESSION['maya_notice'].='Cannot save image!';
            return false;
        }
        
    }
    
    protected function createIcon($image,$name)
    {
        $image->createIcon('64','64',$name.'_icon.png');    
    }



    public function save($user)
    {
        if ($user->save()>0)
        {

           $message="Successfully saved ".$user->getName();
           $success=true;
           
        } else
        {
           $message='SQL Error! '.$_SESSION['maya_notice'];
           $success=false;
        }
        
        $response = array('message'=>$message,'success'=>$success,'id'=>$user->getId());    
        return $response;
    }

    public function json_response($response)
    {
        header('Content-Type: application/json');
        echo json_encode($response);
        exit();           
    }
    
    public function bool_response($response)
    {
        $_SESSION['maya_notice']=$response['message'];
        return $response['success'];
    }
}

