<?php 
/** Main model of maya framework 
 * Object contaiting model objects for main maya framework. 
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package Maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */
require_once 'database.cfg.php';
class Billboard
{

    //@var int
    protected $id;

    //@var varchar
    protected $name;

    //@var varchar
    protected $content;

    //@var int
    protected $status;

    //@var int
    protected $user_id_author;

    public function getImage()
    {
        $paths = new Paths();
        $name = str_replace(',','_',str_replace(' ','',$this->getName()));
        $absolute_path = $paths->user_image.'billboards/'.$this->id.'_'.strtolower($name).'.png';
        if (file_exists($absolute_path))
        {
            return $absolute_path;
        } else
        {
            return $paths->user_image."billboards/none.png";
        }
    }
    
    public function getImageIcon()
    {
        $paths = new Paths();
        $name = str_replace(',','_',str_replace(' ','',$this->getName()));
        $absolute_path = $paths->user_image.'billboards/'.$this->id.'_'.strtolower($name).'_icon.png';
        if (file_exists($absolute_path))
        {
            return $absolute_path;
        } else
        {
            return $paths->user_image."billboards/none_icon.png";
        }
    }
    public function setId($id)
    {
       $this->id=$id;
       return $this;
    }
    public function getId()
    {
       return $this->id;
    }

    public function setName($name)
    {
       $this->name=$name;
       return $this;
    }
    public function getName()
    {
       return htmlentities($this->name,ENT_QUOTES);
    }

    public function setContent($content)
    {
       $this->content=$content;
       return $this;
    }
    public function getContent()
    {
       return htmlentities($this->content,ENT_QUOTES);
    }

    public function setStatus($status)
    {
       $this->status=$status;
       return $this;
    }
    public function getStatus()
    {
       return $this->status;
    }

    public function setUserIdAuthor($user_id_author)
    {
       $this->user_id_author=$user_id_author;
       return $this;
    }
    public function getUserIdAuthor()
    {
       return $this->user_id_author;
    }

    public  function insert()
    {
        $db = new db();
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        
        $name=$mysqli->real_escape_string($this->name);
        $content=$mysqli->real_escape_string($this->content);
        $status=$mysqli->real_escape_string($this->status);
        $user_id_author=$mysqli->real_escape_string($this->user_id_author);
        $sql="INSERT INTO billboards (name,content,status,user_id_author) VALUES ('$name','$content','$status','$user_id_author');";
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        }
        $mysqli->close();
        return true;
        

    }
    public  function update()
    {
        $db = new db();
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        
        $name=$mysqli->real_escape_string($this->name);
        $content=$mysqli->real_escape_string($this->content);
        $status=$mysqli->real_escape_string($this->status);
        $user_id_author=$mysqli->real_escape_string($this->user_id_author);
        $sql="UPDATE billboards SET name='$name',content='$content',status='$status',user_id_author='$user_id_author' WHERE id='$this->id'";
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        }
        $mysqli->close();
        return true;
        

    }
    public  function save()
    {
        if  ($this->id)
        {
            return $this->update();
        } else
        {
            return $this->insert();
        }
    }

    public  function delete()
    {
        $db = new db();
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        
        $sql="DELETE FROM billboards WHERE id='$this->id'";
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        }
        $mysqli->close();
        return true;
        

    }

    
    public function find($arr)
    {
        $db = new db();
        $sql="SELECT * from billboards WHERE ";
        $i=1;
        $len=count($arr);
        foreach($arr as $key=>$value)
        {
            if ($i<$len)
            {
                $sql.="`$key`='$value' AND";
            } else
            {
                $sql.="`$key`='$value' ";
            }
        }
        $sql.=" LIMIT 1 ;";
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        
        $res = $mysqli->query($sql);
    
        if ($mysqli->affected_rows>0)
        {
            $row = $res->fetch_array();
            
            $this->id=$row['id'];
            $this->name=$row['name'];
            $this->content=$row['content'];
            $this->status=$row['status'];
            $this->user_id_author=$row['user_id_author'];

        } else
        {
            
        }

        return $this;
    }   
    
     
    public function findAll($arr,$orders)
    {
        $db = new db();
        $sql="SELECT * from billboards ";
        $i=1;
        foreach ($arr as $id=>$val)
        {
            if (trim($arr[$id]))
            {
            }
            else
            {
                unset($arr[$id]);
            }
        }
        foreach ($orders as $id=>$val)
        {
            if (trim($orders[$id]))
            {
            }
            else
            {
                unset($orders[$id]);
            }
        }        
        $len=count($arr);
        if ($len>0)
        {
            foreach($arr as $key=>$value)
            {
                if ($i==1)
                {
                    $sql.="WHERE ";
                }

                if ($i<$len)
                {
                    $sql.="`$key`='$value' AND ";
                } else
                {
                    $sql.="`$key`='$value' ";
                }

                $i=$i+1;
            }
        } else
		{
			$sql.="WHERE `id` > 0 ";
		}
        $i=1;
        $olen=count($orders);
        if ($olen>0)
        {
            $sql.="ORDER BY ";
            foreach($orders as $key=>$value)
            {
                if ($i<$olen)
                {
                    $sql.="$key $value, ";
                } else
                {
                    $sql.="$key $value ";
                }
                $i=$i+1;
            }
        }
        $sql.=";";
        
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        
        $res = $mysqli->query($sql);
    
        $billboards = array();
        
        if ($mysqli->affected_rows>0)
        {
            while ($row = $res->fetch_array())
            {
                $billboard= new Billboard();
                
            $billboard->id=$row['id'];
            $billboard->name=$row['name'];
            $billboard->content=$row['content'];
            $billboard->status=$row['status'];
            $billboard->user_id_author=$row['user_id_author'];

                $billboards[]=$billboard;
            }
        } else
        {

        }

        return $billboards;
    } 
    
    public function findAllLike($arr,$orders)
    {
        $db = new db();
        $sql="SELECT * from billboards ";
        $i=1;
        foreach ($arr as $id=>$val)
        {
            if (trim($arr[$id]))
            {
            }
            else
            {
                unset($arr[$id]);
            }
        }
        foreach ($orders as $id=>$val)
        {
            if (trim($orders[$id]))
            {
            }
            else
            {
                unset($orders[$id]);
            }
        }        
        $len=count($arr);
        if ($len>0)
        {
            foreach($arr as $key=>$value)
            {
                if ($i==1)
                {
                    $sql.="WHERE ";
                }

                if ($i<$len)
                {
                    $sql.="`$key` LIKE '%$value%' AND ";
                } else
                {
                    $sql.="`$key` LIKE '%$value%' ";
                }

                $i=$i+1;
            }
        } else
		{
			$sql.="WHERE `id` > 0 ";
		}
        $i=1;
        $olen=count($orders);
        if ($olen>0)
        {
            $sql.="ORDER BY ";
            foreach($orders as $key=>$value)
            {
                if ($i<$olen)
                {
                    $sql.="$key $value, ";
                } else
                {
                    $sql.="$key $value ";
                }
                $i=$i+1;
            }
        }
        $sql.=";";
        
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        
        $res = $mysqli->query($sql);
    
        $billboards = array();
        
        if ($mysqli->affected_rows>0)
        {
            while ($row = $res->fetch_array())
            {
                $billboard= new Billboard();
                
            $billboard->id=$row['id'];
            $billboard->name=$row['name'];
            $billboard->content=$row['content'];
            $billboard->status=$row['status'];
            $billboard->user_id_author=$row['user_id_author'];

                $billboards[]=$billboard;
            }
        } else
        {

        }

        return $billboards;
    }
    
     


}
