<?php 
/**  
 * [DESCRIPTION]
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */

require_once 'database.cfg.php';
 
class LeadPublic
{

    //@var int
    public $id;

    //@var varchar
    public $lead_source;

    //@var int
    public $campaign_id;

    //@var varchar
    public $campaign;

    //@var int
    public $aid;

    //@var varchar
    public $partner;

    //@var varchar
    public $password;

    //@var timestamp
    public $date_created;

    //@var varchar
    public $firstname;

    //@var varchar
    public $lastname;

    //@var varchar
    public $email;

    //@var varchar
    public $street_address;

    //@var varchar
    public $city;

    //@var varchar
    public $state;

    //@var varchar
    public $zip_code;

    //@var varchar
    public $phone1;

    //@var varchar
    public $phone_line_type;

    //@var varchar
    public $credit_profile;

    //@var varchar
    public $first_mortgage_balance;

    //@var decimal
    public $first_mortgage_interest_rate;

    //@var varchar
    public $desired_loan_amount;

    //@var varchar
    public $property_type;

    //@var varchar
    public $loan_type;

    //@var varchar
    public $existing_property_value;

    //@var decimal
    public $lead_cost;

    //@var enum
    public $is_military;

    //@var varchar
    public $post_data;

    //@var varchar
    public $repost_data;

    //@var varchar
    public $response;

    //@var int
    public $user_id_submitter;

    //@var varchar
    public $submitter;

    //@var int
    public $ckm_subid;

    //@var varchar
    public $sr_token;

    //@var date
    public $origination;

    //@var varchar
    public $bankruptcy_type;

    //@var varchar
    public $ip_address;

    //@var varchar
    public $universal_leadid;

    //@var varchar
    public $notes;

    public function getCampaign()
    {
        require_once 'Campaign.php';
        $campaign_id=$this->campaign_id;
		$campaign = new Campaign(); 
        $campaign->find(array('id'=>$campaign_id));
        if ($campaign->getId()>0)
        {
            $this->campaign = $campaign->getName();
        } else
        {
            $this->campaign = "";
        }
        return $this->campaign;
    }
    public function getSubmitter()
    {
        require_once 'User.php';
        $user_id_submitter=$this->user_id_submitter;
		$user = new User(); 
        $user->find(array('id'=>$user_id_submitter));
        if ($user->getId()>0)
        {
            $this->submitter = $user->getName();
        } else
        {
            $this->submitter = "";
        }
        return $this->submitter;
    }
    public  function insert()
    {
        $db = new db();
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        

            $lead_source=$mysqli->real_escape_string($this->lead_source);
            $campaign_id=$mysqli->real_escape_string($this->campaign_id);
            $aid=$mysqli->real_escape_string($this->aid);
            $partner=$mysqli->real_escape_string($this->partner);
            $password=$mysqli->real_escape_string($this->password);
            $date_created=$mysqli->real_escape_string($this->date_created);
            $firstname=$mysqli->real_escape_string($this->firstname);
            $lastname=$mysqli->real_escape_string($this->lastname);
            $email=$mysqli->real_escape_string($this->email);
            $street_address=$mysqli->real_escape_string($this->street_address);
            $city=$mysqli->real_escape_string($this->city);
            $state=$mysqli->real_escape_string($this->state);
            $zip_code=$mysqli->real_escape_string($this->zip_code);
            $phone1=$mysqli->real_escape_string($this->phone1);
            $phone_line_type=$mysqli->real_escape_string($this->phone_line_type);
            $credit_profile=$mysqli->real_escape_string($this->credit_profile);
            $first_mortgage_balance=$mysqli->real_escape_string($this->first_mortgage_balance);
            $first_mortgage_interest_rate=$mysqli->real_escape_string($this->first_mortgage_interest_rate);
            $desired_loan_amount=$mysqli->real_escape_string($this->desired_loan_amount);
            $property_type=$mysqli->real_escape_string($this->property_type);
            $loan_type=$mysqli->real_escape_string($this->loan_type);
            $existing_property_value=$mysqli->real_escape_string($this->existing_property_value);
            $lead_cost=$mysqli->real_escape_string($this->lead_cost);
            $is_military=$mysqli->real_escape_string($this->is_military);
            $post_data=$mysqli->real_escape_string($this->post_data);
            $repost_data=$mysqli->real_escape_string($this->repost_data);
            $response=$mysqli->real_escape_string($this->response);
            $user_id_submitter=$mysqli->real_escape_string($this->user_id_submitter);
            $ckm_subid=$mysqli->real_escape_string($this->ckm_subid);
            $sr_token=$mysqli->real_escape_string($this->sr_token);
            $origination=$mysqli->real_escape_string($this->origination);
            $bankruptcy_type=$mysqli->real_escape_string($this->bankruptcy_type);
            $ip_address=$mysqli->real_escape_string($this->ip_address);
            $universal_leadid=$mysqli->real_escape_string($this->universal_leadid);
            $notes=$mysqli->real_escape_string($this->notes);
        $sql="INSERT INTO leads (lead_source,campaign_id,aid,partner,password,date_created,firstname,lastname,email,street_address,city,state,zip_code,phone1,phone_line_type,credit_profile,first_mortgage_balance,first_mortgage_interest_rate,desired_loan_amount,property_type,loan_type,existing_property_value,lead_cost,is_military,post_data,repost_data,response,user_id_submitter,ckm_subid,sr_token,origination,bankruptcy_type,ip_address,universal_leadid,notes) VALUES ('$lead_source','$campaign_id','$aid','$partner','$password','$date_created','$firstname','$lastname','$email','$street_address','$city','$state','$zip_code','$phone1','$phone_line_type','$credit_profile','$first_mortgage_balance','$first_mortgage_interest_rate','$desired_loan_amount','$property_type','$loan_type','$existing_property_value','$lead_cost','$is_military','$post_data','$repost_data','$response','$user_id_submitter','$ckm_subid','$sr_token','$origination','$bankruptcy_type','$ip_address','$universal_leadid','$notes');";
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        } else
        {
            $this->id = $mysqli->insert_id;
        }
        $mysqli->close();
        return true;
        

    }
    public  function update($data=array())
    {
        $db = new db();
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        
        $sql="UPDATE leads SET ";        
        $updates = '';
        if ((count($data)>0) && (array_key_exists('id',$data) || ($this->id>0)))
        {
            $where = '';
            foreach($data as $key=>$value)
            {
                if ($key=='id')
                {
                    $where="WHERE $key='$value';";
                } else
                {
                    if ($updates)
                    {
                        $updates.= ", $key='$value' ";
                    } else
                    {
                        $updates.= " $key='$value' ";
                    }
                }
            }
            if ($where)
            {
            } else
            {
                $where="WHERE id='".$this->id."';";
            }
            $sql.=$updates.$where;
        } else
        {
        
            $lead_source=$mysqli->real_escape_string($this->lead_source);
            $campaign_id=$mysqli->real_escape_string($this->campaign_id);
            $aid=$mysqli->real_escape_string($this->aid);
            $partner=$mysqli->real_escape_string($this->partner);
            $password=$mysqli->real_escape_string($this->password);
            $date_created=$mysqli->real_escape_string($this->date_created);
            $firstname=$mysqli->real_escape_string($this->firstname);
            $lastname=$mysqli->real_escape_string($this->lastname);
            $email=$mysqli->real_escape_string($this->email);
            $street_address=$mysqli->real_escape_string($this->street_address);
            $city=$mysqli->real_escape_string($this->city);
            $state=$mysqli->real_escape_string($this->state);
            $zip_code=$mysqli->real_escape_string($this->zip_code);
            $phone1=$mysqli->real_escape_string($this->phone1);
            $phone_line_type=$mysqli->real_escape_string($this->phone_line_type);
            $credit_profile=$mysqli->real_escape_string($this->credit_profile);
            $first_mortgage_balance=$mysqli->real_escape_string($this->first_mortgage_balance);
            $first_mortgage_interest_rate=$mysqli->real_escape_string($this->first_mortgage_interest_rate);
            $desired_loan_amount=$mysqli->real_escape_string($this->desired_loan_amount);
            $property_type=$mysqli->real_escape_string($this->property_type);
            $loan_type=$mysqli->real_escape_string($this->loan_type);
            $existing_property_value=$mysqli->real_escape_string($this->existing_property_value);
            $lead_cost=$mysqli->real_escape_string($this->lead_cost);
            $is_military=$mysqli->real_escape_string($this->is_military);
            $post_data=$mysqli->real_escape_string($this->post_data);
            $repost_data=$mysqli->real_escape_string($this->repost_data);
            $response=$mysqli->real_escape_string($this->response);
            $user_id_submitter=$mysqli->real_escape_string($this->user_id_submitter);
            $ckm_subid=$mysqli->real_escape_string($this->ckm_subid);
            $sr_token=$mysqli->real_escape_string($this->sr_token);
            $origination=$mysqli->real_escape_string($this->origination);
            $bankruptcy_type=$mysqli->real_escape_string($this->bankruptcy_type);
            $ip_address=$mysqli->real_escape_string($this->ip_address);
            $universal_leadid=$mysqli->real_escape_string($this->universal_leadid);
            $notes=$mysqli->real_escape_string($this->notes);

            $sql.="lead_source='$lead_source',campaign_id='$campaign_id',aid='$aid',partner='$partner',password='$password',date_created='$date_created',firstname='$firstname',lastname='$lastname',email='$email',street_address='$street_address',city='$city',state='$state',zip_code='$zip_code',phone1='$phone1',phone_line_type='$phone_line_type',credit_profile='$credit_profile',first_mortgage_balance='$first_mortgage_balance',first_mortgage_interest_rate='$first_mortgage_interest_rate',desired_loan_amount='$desired_loan_amount',property_type='$property_type',loan_type='$loan_type',existing_property_value='$existing_property_value',lead_cost='$lead_cost',is_military='$is_military',post_data='$post_data',repost_data='$repost_data',response='$response',user_id_submitter='$user_id_submitter',ckm_subid='$ckm_subid',sr_token='$sr_token',origination='$origination',bankruptcy_type='$bankruptcy_type',ip_address='$ip_address',universal_leadid='$universal_leadid',notes='$notes' WHERE id='$this->id'";

        }
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        } 
        $mysqli->close();
        return true;
        

    }
    public  function save()
    {
        if  ($this->id)
        {
            return $this->update();
        } else
        {
            return $this->insert();
        }
    }

    public  function delete()
    {
        $db = new db();
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        
        $sql="DELETE FROM leads WHERE id='$this->id'";
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        } 
        $mysqli->close();
        return true;
        

    }

    
    public function find($arr)
    {
        $db = new db();
        $sql="SELECT * from leads ";
        $i=1;
        $len=count($arr);
        foreach($arr as $key=>$val)
        {
            $value='';
            $operator = '=';
            if (is_array($val))
            {
                $field="`$key`";
                $value = "('".implode("','",$val)."')";
                $operator = 'IN';
            } else if (is_numeric($key) || empty($key) || (!isset($key))) 
            {
                $operator = '';
                $value="$val";
                $field='';
            } else
            {
                $field="`$key`";
                $field=$key;
                $value = "'$val'";
            }
            
            if ($i==1)
            {
                $sql.="WHERE $field $operator $value ";
            } else
            {
                $sql.="AND $field $operator $value ";
            } 

            $i=$i+1;
        }
        $sql.=" LIMIT 1 ;";
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        
        $res = $mysqli->query($sql);
    
        if ($mysqli->affected_rows>0)
        {
            $row = $res->fetch_array();
            
            $this->id=$row['id'];
            $this->lead_source=$row['lead_source'];
            $this->campaign_id=$row['campaign_id'];
            $this->aid=$row['aid'];
            $this->partner=$row['partner'];
            $this->password=$row['password'];
            $this->date_created=$row['date_created'];
            $this->firstname=$row['firstname'];
            $this->lastname=$row['lastname'];
            $this->email=$row['email'];
            $this->street_address=$row['street_address'];
            $this->city=$row['city'];
            $this->state=$row['state'];
            $this->zip_code=$row['zip_code'];
            $this->phone1=$row['phone1'];
            $this->phone_line_type=$row['phone_line_type'];
            $this->credit_profile=$row['credit_profile'];
            $this->first_mortgage_balance=$row['first_mortgage_balance'];
            $this->first_mortgage_interest_rate=$row['first_mortgage_interest_rate'];
            $this->desired_loan_amount=$row['desired_loan_amount'];
            $this->property_type=$row['property_type'];
            $this->loan_type=$row['loan_type'];
            $this->existing_property_value=$row['existing_property_value'];
            $this->lead_cost=$row['lead_cost'];
            $this->is_military=$row['is_military'];
            $this->post_data=$row['post_data'];
            $this->repost_data=$row['repost_data'];
            $this->response=$row['response'];
            $this->user_id_submitter=$row['user_id_submitter'];
            $this->ckm_subid=$row['ckm_subid'];
            $this->sr_token=$row['sr_token'];
            $this->origination=$row['origination'];
            $this->bankruptcy_type=$row['bankruptcy_type'];
            $this->ip_address=$row['ip_address'];
            $this->universal_leadid=$row['universal_leadid'];
            $this->notes=$row['notes'];
            $this->getCampaign();
            $this->getSubmitter();

        } else
        {
            
        }

        return $this;
    }   
    
     
    public function findAll($arr=array(),$orders=array(),$limit=100000,$page=1,$select,$debug=false)
    {
        $db = new db();
        
        $sel_str='*';
        if ($select)
        {
            if (in_array('id',$select))
            {
            } else
            {
                $select[]='id';
            }
            $sel_str=implode(',',$select);
        }
        $sql="SELECT ".$sel_str." from leads ";        
        
        $i=1;
        if ($page > 0)
        {
            $offset = ($page - 1)*$limit;
        } else
        {
            $offset = 0;
        }
        foreach ($arr as $id=>$val)
        {
            if (strlen($arr[$id])||(is_array($val)))
            {
            }
            else
            {
                unset($arr[$id]);
            }
        }
        foreach ($orders as $id=>$val)
        {
            if (strlen($orders[$id]))
            {
            }
            else
            {
                unset($orders[$id]);
            }
        }        
        $len=count($arr);
        if ($len>0)
        {
            foreach($arr as $key=>$val)
            {
                $operator = '=';
                if (is_array($val))
                {
                    $field="`$key`";
                    $value = "('".implode("','",$val)."')";
                    $operator = 'IN';
                } else if (is_numeric($key) || empty($key) || (!isset($key))) 
                {
                    $operator = '';
                    $value="$val";
                    $field='';
                } else
                {
                    $field="`$key`";
                    $field=$key;
                    $value = "'$val'";
                }
                
                if ($i==1)
                {
                    $sql.="WHERE $field $operator $value ";
                } else
                {
                    $sql.="AND $field $operator $value ";
                } 

                $i=$i+1;
            }
        } else
		{
			$sql.="WHERE `id` > 0 ";
		}
        $i=1;
        $olen=count($orders);
        if ($olen>0)
        {
            $sql.="ORDER BY ";
            foreach($orders as $key=>$value)
            {
                if ($i<$olen)
                {
                    $sql.="$key $value, ";
                } else
                {
                    $sql.="$key $value ";
                }
                $i=$i+1;
            }
        }
        $sql.=" LIMIT $limit OFFSET $offset";
        
        $sql.=";";
        
        if ($debug) die($sql);
        
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        
        $res = $mysqli->query($sql);
    
        $leads = array();
        
        if ($mysqli->affected_rows>0)
        {
            while ($row = $res->fetch_array())
            {
                $lead= new LeadPublic();
                
                $lead->id=$row['id'];
                $lead->lead_source=$row['lead_source'];
                $lead->campaign_id=$row['campaign_id'];
                $lead->aid=$row['aid'];
                $lead->partner=$row['partner'];
                $lead->password=$row['password'];
                $lead->date_created=$row['date_created'];
                $lead->firstname=$row['firstname'];
                $lead->lastname=$row['lastname'];
                $lead->email=$row['email'];
                $lead->street_address=$row['street_address'];
                $lead->city=$row['city'];
                $lead->state=$row['state'];
                $lead->zip_code=$row['zip_code'];
                $lead->phone1=$row['phone1'];
                $lead->phone_line_type=$row['phone_line_type'];
                $lead->credit_profile=$row['credit_profile'];
                $lead->first_mortgage_balance=$row['first_mortgage_balance'];
                $lead->first_mortgage_interest_rate=$row['first_mortgage_interest_rate'];
                $lead->desired_loan_amount=$row['desired_loan_amount'];
                $lead->property_type=$row['property_type'];
                $lead->loan_type=$row['loan_type'];
                $lead->existing_property_value=$row['existing_property_value'];
                $lead->lead_cost=$row['lead_cost'];
                $lead->is_military=$row['is_military'];
                $lead->post_data=$row['post_data'];
                $lead->repost_data=$row['repost_data'];
                $lead->response=$row['response'];
                $lead->user_id_submitter=$row['user_id_submitter'];
                $lead->ckm_subid=$row['ckm_subid'];
                $lead->sr_token=$row['sr_token'];
                $lead->origination=$row['origination'];
                $lead->bankruptcy_type=$row['bankruptcy_type'];
                $lead->ip_address=$row['ip_address'];
                $lead->universal_leadid=$row['universal_leadid'];
                $lead->notes=$row['notes'];
                $lead->getCampaign();
                $lead->getSubmitter();

                $leads[]=$lead;
            }
        } else
        {

        }

        return $leads;
    } 
    
    public function findAllLike($arr=array(),$orders=array(),$limit=100000,$page=1)
    {
        $db = new db();
        $sql="SELECT * from leads ";
        $i=1;
        $offset = ($page - 1)*$limit;
        foreach ($arr as $id=>$val)
        {
            if (trim($arr[$id]))
            {
            }
            else
            {
                unset($arr[$id]);
            }
        }
        foreach ($orders as $id=>$val)
        {
            if (trim($orders[$id]))
            {
            }
            else
            {
                unset($orders[$id]);
            }
        }       
        $len=count($arr); 
        if ($len>0)
        {
            foreach($arr as $key=>$val)
            {               
                $operator = 'LIKE';
                if (is_array($val))
                {
                    $value = "('".implode("','",$val)."')";
                    $operator = 'IN';
                } else
                {
                    $value = "'%$val%'";
                }

                if ($i==1)
                {
                    $sql.="WHERE `$key` $operator $value ";
                    
                } else
                {
                    $sql.="$operator `$key` $operator $value ";
                }

                $i=$i+1;
            }
        } else
		{
			$sql.="WHERE `id` > 0 ";
		}
        $i=1;
        $olen=count($orders);
        if ($olen>0)
        {
            $sql.="ORDER BY ";
            foreach($orders as $key=>$value)
            {
                if ($i<$olen)
                {
                    $sql.="$key $value, ";
                } else
                {
                    $sql.="$key $value ";
                }
                $i=$i+1;
            }
        }
        $sql.=" LIMIT $limit OFFSET $offset";
        $sql.=";";
        
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        
        $res = $mysqli->query($sql);
    
        $leads = array();
        
        if ($mysqli->affected_rows>0)
        {
            while ($row = $res->fetch_array())
            {
                $lead= new LeadPublic();
                
                $lead->id=$row['id'];
                $lead->lead_source=$row['lead_source'];
                $lead->campaign_id=$row['campaign_id'];
                $lead->aid=$row['aid'];
                $lead->partner=$row['partner'];
                $lead->password=$row['password'];
                $lead->date_created=$row['date_created'];
                $lead->firstname=$row['firstname'];
                $lead->lastname=$row['lastname'];
                $lead->email=$row['email'];
                $lead->street_address=$row['street_address'];
                $lead->city=$row['city'];
                $lead->state=$row['state'];
                $lead->zip_code=$row['zip_code'];
                $lead->phone1=$row['phone1'];
                $lead->phone_line_type=$row['phone_line_type'];
                $lead->credit_profile=$row['credit_profile'];
                $lead->first_mortgage_balance=$row['first_mortgage_balance'];
                $lead->first_mortgage_interest_rate=$row['first_mortgage_interest_rate'];
                $lead->desired_loan_amount=$row['desired_loan_amount'];
                $lead->property_type=$row['property_type'];
                $lead->loan_type=$row['loan_type'];
                $lead->existing_property_value=$row['existing_property_value'];
                $lead->lead_cost=$row['lead_cost'];
                $lead->is_military=$row['is_military'];
                $lead->post_data=$row['post_data'];
                $lead->repost_data=$row['repost_data'];
                $lead->response=$row['response'];
                $lead->user_id_submitter=$row['user_id_submitter'];
                $lead->ckm_subid=$row['ckm_subid'];
                $lead->sr_token=$row['sr_token'];
                $lead->origination=$row['origination'];
                $lead->bankruptcy_type=$row['bankruptcy_type'];
                $lead->ip_address=$row['ip_address'];
                $lead->universal_leadid=$row['universal_leadid'];
                $lead->notes=$row['notes'];
                $lead->getCampaign();
                $lead->getSubmitter();

                $leads[]=$lead;
            }
        } else
        {

        }

        return $leads;
    }
    
    public function getPageCount($arr=array(),$orders=array(),$limit=100,$equals_array_value=true,$clear=true)
    {
        $id = 'lead_pages';
        if (($_SESSION[$id]) && (!$clear))
        {
            return $_SESSION[$id]; 
        } else
        {
            $db = new db();
            $sql="SELECT count(*) from leads ";
            $i=1;
            
            foreach ($arr as $id=>$val)
            {
                if (trim($arr[$id]))
                {
                }
                else
                {
                    unset($arr[$id]);
                }
            }
            foreach ($orders as $id=>$val)
            {
                if (trim($orders[$id]))
                {
                }
                else
                {
                    unset($orders[$id]);
                }
            }        
            $len=count($arr);
            $prev_key='';
            if ($len>0)
            {
                if ($equals_array_value)
                {
                    foreach($arr as $key=>$val)
                    {
                        $operator = '=';
                        if (is_array($val))
                        {
                            $value = "('".implode("','",$val)."')";
                            $operator = 'IN';
                        } else
                        {
                            $value = "'$val'";
                        }
                        
                        if ($i==1)
                        {
                            $sql.="WHERE `$key` $operator $value ";
                        } else
                        {
                            $sql.="AND `$key` $operator $value ";
                        } 

                        $i=$i+1;
                    }
                } else
                {
                    foreach($arr as $key=>$val)
                    {               
                        $operator = 'LIKE';
                        if (is_array($val))
                        {
                            $value = "('".implode("','",$val)."')";
                            $operator = 'IN';
                        } else
                        {
                            $value = "'%$val%'";
                        }

                        if ($i==1)
                        {
                            $sql.="WHERE `$key` $operator $value ";
                            
                        } else
                        {
                            $sql.="$operator `$key` $operator $value ";
                        }

                        $i=$i+1;
                    }
                }
            } else
            {
                $sql.="WHERE `id` > 0 ";
            }
            $i=1;
            $olen=count($orders);
            if ($olen>0)
            {
                $sql.="ORDER BY ";
                foreach($orders as $key=>$value)
                {
                    if ($i<$olen)
                    {
                        $sql.="$key $value, ";
                    } else
                    {
                        $sql.="$key $value ";
                    }
                    $i=$i+1;
                }
            }
            
            
            $sql.=";";
            
            $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
            
            $res = $mysqli->query($sql);
            if ($mysqli->affected_rows>0)
            {
               $row = $res->fetch_row();
               $pages = ceil($row[0]/$limit);
            
               if ($pages<1) $pages = 1;
            
               $_SESSION[$id] = $pages;
            }
            
            return ($pages);
        }
    }     

    public function getName()
    { 
        return 'Lead:'.$this->id;
    }

}