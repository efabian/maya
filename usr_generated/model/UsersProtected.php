<?php 
/**  
 * [DESCRIPTION]
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */

require_once 'database.cfg.php';
class UsersProtected
{

    //@var int
    protected $id;

    //@var varchar
    protected $firstname;

    //@var varchar
    protected $lastname;

    //@var varchar
    protected $email;

    //@var varchar
    protected $profile;

    //@var timestamp
    protected $created;

    //@var datetime
    protected $modified;

    //@var datetime
    protected $login;

    //@var int
    protected $role;

    //@var varchar
    protected $salt;

    //@var varchar
    protected $password;

    //@var varchar
    protected $ip;

    //@var varchar
    protected $updateip;

    //@var varchar
    protected $loginip;

    //@var varchar
    protected $resetkey;

    //@var varchar
    protected $securekey;

    public function __construct()
    {
       $now=new DateTime('NOW');
       $this->created=$now->format('Y-m-d H:i:s');
       return $this;
    }
    public function getImage()
    {
        $paths = new Paths();
        $name = str_replace(',','_',str_replace(' ','',$this->getName()));
        $absolute_path = $paths->user_image.'users/'.$this->id.'_'.strtolower($name).'.png';
        if (file_exists($absolute_path))
        {
            return $absolute_path;
        } else
        {
            return $paths->user_image."users/none.png";
        }
    }
    
    public function getImageIcon()
    {
        $paths = new Paths();
        $name = str_replace(',','_',str_replace(' ','',$this->getName()));
        $absolute_path = $paths->user_image.'users/'.$this->id.'_'.strtolower($name).'_icon.png';
        if (file_exists($absolute_path))
        {
            return $absolute_path;
        } else
        {
            return $paths->user_image."users/none_icon.png";
        }
    }
    public function setId($id)
    {
       $this->id=$id;
       return $this;
    }
    public function getId()
    {
       return $this->id;
    }

    public function setFirstname($firstname)
    {
       $this->firstname=$firstname;
       return $this;
    }
    public function getFirstname()
    {
       return htmlentities($this->firstname,ENT_QUOTES);
    }

    public function setLastname($lastname)
    {
       $this->lastname=$lastname;
       return $this;
    }
    public function getLastname()
    {
       return htmlentities($this->lastname,ENT_QUOTES);
    }

    public function setEmail($email)
    {
       $this->email=$email;
       return $this;
    }
    public function getEmail()
    {
       return $this->email;
    }

    public function setProfile($profile)
    {
       $this->profile=$profile;
       return $this;
    }
    public function getProfile()
    {
       return htmlentities($this->profile,ENT_QUOTES);
    }

    public function setCreated($created)
    {
       $this->created=$created;
       return $this;
    }
    public function getCreated()
    {
       return $this->created;
    }

    public function setModified($modified)
    {
       $this->modified=$modified;
       return $this;
    }
    public function getModified()
    {
       return $this->modified;
    }

    public function setLogin($login)
    {
       $this->login=$login;
       return $this;
    }
    public function getLogin()
    {
       return $this->login;
    }

    public function setRole($role)
    {
       $this->role=$role;
       return $this;
    }
    public function getRole()
    {
       return $this->role;
    }

    public function setSalt($salt)
    {
       $this->salt=$salt;
       return $this;
    }
    public function getSalt()
    {
       return $this->salt;
    }

    public function setPassword($password)
    {
       $this->password=$password;
       return $this;
    }
    public function getPassword()
    {
       return $this->password;
    }

    public function setIp($ip)
    {
       $this->ip=$ip;
       return $this;
    }
    public function getIp()
    {
       return htmlentities($this->ip,ENT_QUOTES);
    }

    public function setUpdateip($updateip)
    {
       $this->updateip=$updateip;
       return $this;
    }
    public function getUpdateip()
    {
       return htmlentities($this->updateip,ENT_QUOTES);
    }

    public function setLoginip($loginip)
    {
       $this->loginip=$loginip;
       return $this;
    }
    public function getLoginip()
    {
       return htmlentities($this->loginip,ENT_QUOTES);
    }

    public function setResetkey($resetkey)
    {
       $this->resetkey=$resetkey;
       return $this;
    }
    public function getResetkey()
    {
       return htmlentities($this->resetkey,ENT_QUOTES);
    }

    public function setSecurekey($securekey)
    {
       $this->securekey=$securekey;
       return $this;
    }
    public function getSecurekey()
    {
       return htmlentities($this->securekey,ENT_QUOTES);
    }

    public  function insert()
    {
        $db = new db();
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        

            $firstname=$mysqli->real_escape_string($this->firstname);
            $lastname=$mysqli->real_escape_string($this->lastname);
            $email=$mysqli->real_escape_string($this->email);
            $profile=$mysqli->real_escape_string($this->profile);
            $created=$mysqli->real_escape_string($this->created);
            $modified=$mysqli->real_escape_string($this->modified);
            $login=$mysqli->real_escape_string($this->login);
            $role=$mysqli->real_escape_string($this->role);
            $salt=$mysqli->real_escape_string($this->salt);
            $password=$mysqli->real_escape_string($this->password);
            $ip=$mysqli->real_escape_string($this->ip);
            $updateip=$mysqli->real_escape_string($this->updateip);
            $loginip=$mysqli->real_escape_string($this->loginip);
            $resetkey=$mysqli->real_escape_string($this->resetkey);
            $securekey=$mysqli->real_escape_string($this->securekey);
        $sql="INSERT INTO users (firstname,lastname,email,profile,created,modified,login,role,salt,password,ip,updateip,loginip,resetkey,securekey) VALUES ('$firstname','$lastname','$email','$profile','$created','$modified','$login','$role','$salt','$password','$ip','$updateip','$loginip','$resetkey','$securekey');";
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        } else
        {
            $this->id = $mysqli->insert_id;
        }
        $mysqli->close();
        return true;
        

    }
    public  function update($data=array())
    {
        $db = new db();
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        
        $sql="UPDATE users SET ";        
        $updates = '';
        if ((count($data)>0) && (array_key_exists('id',$data) || ($this->id>0)))
        {
            $where = '';
            foreach($data as $key=>$value)
            {
                if ($key=='id')
                {
                    $where="WHERE $key='$value';";
                } else
                {
                    if ($updates)
                    {
                        $updates.= ", $key='$value' ";
                    } else
                    {
                        $updates.= " $key='$value' ";
                    }
                }
            }
            if ($where)
            {
            } else
            {
                $where="WHERE id='".$this->id."';";
            }
            $sql.=$updates.$where;
        } else
        {
        
            $firstname=$mysqli->real_escape_string($this->firstname);
            $lastname=$mysqli->real_escape_string($this->lastname);
            $email=$mysqli->real_escape_string($this->email);
            $profile=$mysqli->real_escape_string($this->profile);
            $created=$mysqli->real_escape_string($this->created);
            $modified=$mysqli->real_escape_string($this->modified);
            $login=$mysqli->real_escape_string($this->login);
            $role=$mysqli->real_escape_string($this->role);
            $salt=$mysqli->real_escape_string($this->salt);
            $password=$mysqli->real_escape_string($this->password);
            $ip=$mysqli->real_escape_string($this->ip);
            $updateip=$mysqli->real_escape_string($this->updateip);
            $loginip=$mysqli->real_escape_string($this->loginip);
            $resetkey=$mysqli->real_escape_string($this->resetkey);
            $securekey=$mysqli->real_escape_string($this->securekey);

            $sql.="firstname='$firstname',lastname='$lastname',email='$email',profile='$profile',created='$created',modified='$modified',login='$login',role='$role',salt='$salt',password='$password',ip='$ip',updateip='$updateip',loginip='$loginip',resetkey='$resetkey',securekey='$securekey' WHERE id='$this->id'";

        }
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        } 
        $mysqli->close();
        return true;
        

    }
    public  function save()
    {
        if  ($this->id)
        {
            return $this->update();
        } else
        {
            return $this->insert();
        }
    }

    public  function delete()
    {
        $db = new db();
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        
        $sql="DELETE FROM users WHERE id='$this->id'";
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        } 
        $mysqli->close();
        return true;
        

    }

    
    public function find($arr)
    {
        $db = new db();
        $sql="SELECT * from users ";
        $i=1;
        $len=count($arr);
        foreach($arr as $key=>$val)
        {
            $value='';
            $operator = '=';
            if (is_array($val))
            {
                $field="`$key`";
                $value = "('".implode("','",$val)."')";
                $operator = 'IN';
            } else if (is_numeric($key) || empty($key) || (!isset($key))) 
            {
                $operator = '';
                $value="$val";
                $field='';
            } else
            {
                $field="`$key`";
                $field=$key;
                $value = "'$val'";
            }
            
            if ($i==1)
            {
                $sql.="WHERE $field $operator $value ";
            } else
            {
                $sql.="AND $field $operator $value ";
            } 

            $i=$i+1;
        }
        $sql.=" LIMIT 1 ;";
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        
        $res = $mysqli->query($sql);
    
        if ($mysqli->affected_rows>0)
        {
            $row = $res->fetch_array();
            
            $this->id=$row['id'];
            $this->firstname=$row['firstname'];
            $this->lastname=$row['lastname'];
            $this->email=$row['email'];
            $this->profile=$row['profile'];
            $this->created=$row['created'];
            $this->modified=$row['modified'];
            $this->login=$row['login'];
            $this->role=$row['role'];
            $this->salt=$row['salt'];
            $this->password=$row['password'];
            $this->ip=$row['ip'];
            $this->updateip=$row['updateip'];
            $this->loginip=$row['loginip'];
            $this->resetkey=$row['resetkey'];
            $this->securekey=$row['securekey'];

        } else
        {
            
        }

        return $this;
    }   
    
     
    public function findAll($arr=array(),$orders=array(),$limit=1000,$page=1)
    {
        $db = new db();
        $sql="SELECT * from users ";
        $i=1;
        if ($page > 0)
        {
            $offset = ($page - 1)*$limit;
        } else
        {
            $offset = 0;
        }
        foreach ($arr as $id=>$val)
        {
            if (strlen($arr[$id])||(is_array($val)))
            {
            }
            else
            {
                unset($arr[$id]);
            }
        }
        foreach ($orders as $id=>$val)
        {
            if (strlen($orders[$id]))
            {
            }
            else
            {
                unset($orders[$id]);
            }
        }        
        $len=count($arr);
        if ($len>0)
        {
            foreach($arr as $key=>$val)
            {
                $operator = '=';
                if (is_array($val))
                {
                    $field="`$key`";
                    $value = "('".implode("','",$val)."')";
                    $operator = 'IN';
                } else if (is_numeric($key) || empty($key) || (!isset($key))) 
                {
                    $operator = '';
                    $value="$val";
                    $field='';
                } else
                {
                    $field="`$key`";
                    $field=$key;
                    $value = "'$val'";
                }
                
                if ($i==1)
                {
                    $sql.="WHERE $field $operator $value ";
                } else
                {
                    $sql.="AND $field $operator $value ";
                } 

                $i=$i+1;
            }
        } else
		{
			$sql.="WHERE `id` > 0 ";
		}
        $i=1;
        $olen=count($orders);
        if ($olen>0)
        {
            $sql.="ORDER BY ";
            foreach($orders as $key=>$value)
            {
                if ($i<$olen)
                {
                    $sql.="$key $value, ";
                } else
                {
                    $sql.="$key $value ";
                }
                $i=$i+1;
            }
        }
        $sql.=" LIMIT $limit OFFSET $offset";
        
        $sql.=";";
        
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        
        $res = $mysqli->query($sql);
    
        $userss = array();
        
        if ($mysqli->affected_rows>0)
        {
            while ($row = $res->fetch_array())
            {
                $users= new Users();
                
                $users->id=$row['id'];
                $users->firstname=$row['firstname'];
                $users->lastname=$row['lastname'];
                $users->email=$row['email'];
                $users->profile=$row['profile'];
                $users->created=$row['created'];
                $users->modified=$row['modified'];
                $users->login=$row['login'];
                $users->role=$row['role'];
                $users->salt=$row['salt'];
                $users->password=$row['password'];
                $users->ip=$row['ip'];
                $users->updateip=$row['updateip'];
                $users->loginip=$row['loginip'];
                $users->resetkey=$row['resetkey'];
                $users->securekey=$row['securekey'];

                $userss[]=$users;
            }
        } else
        {

        }

        return $userss;
    } 
    
    public function findAllLike($arr=array(),$orders=array(),$limit=1000,$page=1)
    {
        $db = new db();
        $sql="SELECT * from users ";
        $i=1;
        $offset = ($page - 1)*$limit;
        foreach ($arr as $id=>$val)
        {
            if (trim($arr[$id]))
            {
            }
            else
            {
                unset($arr[$id]);
            }
        }
        foreach ($orders as $id=>$val)
        {
            if (trim($orders[$id]))
            {
            }
            else
            {
                unset($orders[$id]);
            }
        }       
        $len=count($arr); 
        if ($len>0)
        {
            foreach($arr as $key=>$val)
            {               
                $operator = 'LIKE';
                if (is_array($val))
                {
                    $value = "('".implode("','",$val)."')";
                    $operator = 'IN';
                } else
                {
                    $value = "'%$val%'";
                }

                if ($i==1)
                {
                    $sql.="WHERE `$key` $operator $value ";
                    
                } else
                {
                    $sql.="$operator `$key` $operator $value ";
                }

                $i=$i+1;
            }
        } else
		{
			$sql.="WHERE `id` > 0 ";
		}
        $i=1;
        $olen=count($orders);
        if ($olen>0)
        {
            $sql.="ORDER BY ";
            foreach($orders as $key=>$value)
            {
                if ($i<$olen)
                {
                    $sql.="$key $value, ";
                } else
                {
                    $sql.="$key $value ";
                }
                $i=$i+1;
            }
        }
        $sql.=" LIMIT $limit OFFSET $offset";
        $sql.=";";
        
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        
        $res = $mysqli->query($sql);
    
        $userss = array();
        
        if ($mysqli->affected_rows>0)
        {
            while ($row = $res->fetch_array())
            {
                $users= new Users();
                
                $users->id=$row['id'];
                $users->firstname=$row['firstname'];
                $users->lastname=$row['lastname'];
                $users->email=$row['email'];
                $users->profile=$row['profile'];
                $users->created=$row['created'];
                $users->modified=$row['modified'];
                $users->login=$row['login'];
                $users->role=$row['role'];
                $users->salt=$row['salt'];
                $users->password=$row['password'];
                $users->ip=$row['ip'];
                $users->updateip=$row['updateip'];
                $users->loginip=$row['loginip'];
                $users->resetkey=$row['resetkey'];
                $users->securekey=$row['securekey'];

                $userss[]=$users;
            }
        } else
        {

        }

        return $userss;
    }
    
    public function getPageCount($arr=array(),$orders=array(),$limit=100,$equals_array_value=true,$clear=true)
    {
        $id = 'users_pages';
        if (($_SESSION[$id]) && (!$clear))
        {
            return $_SESSION[$id]; 
        } else
        {
            $db = new db();
            $sql="SELECT count(*) from users ";
            $i=1;
            
            foreach ($arr as $id=>$val)
            {
                if (trim($arr[$id]))
                {
                }
                else
                {
                    unset($arr[$id]);
                }
            }
            foreach ($orders as $id=>$val)
            {
                if (trim($orders[$id]))
                {
                }
                else
                {
                    unset($orders[$id]);
                }
            }        
            $len=count($arr);
            $prev_key='';
            if ($len>0)
            {
                if ($equals_array_value)
                {
                    foreach($arr as $key=>$val)
                    {
                        $operator = '=';
                        if (is_array($val))
                        {
                            $value = "('".implode("','",$val)."')";
                            $operator = 'IN';
                        } else
                        {
                            $value = "'$val'";
                        }
                        
                        if ($i==1)
                        {
                            $sql.="WHERE `$key` $operator $value ";
                        } else
                        {
                            $sql.="AND `$key` $operator $value ";
                        } 

                        $i=$i+1;
                    }
                } else
                {
                    foreach($arr as $key=>$val)
                    {               
                        $operator = 'LIKE';
                        if (is_array($val))
                        {
                            $value = "('".implode("','",$val)."')";
                            $operator = 'IN';
                        } else
                        {
                            $value = "'%$val%'";
                        }

                        if ($i==1)
                        {
                            $sql.="WHERE `$key` $operator $value ";
                            
                        } else
                        {
                            $sql.="$operator `$key` $operator $value ";
                        }

                        $i=$i+1;
                    }
                }
            } else
            {
                $sql.="WHERE `id` > 0 ";
            }
            $i=1;
            $olen=count($orders);
            if ($olen>0)
            {
                $sql.="ORDER BY ";
                foreach($orders as $key=>$value)
                {
                    if ($i<$olen)
                    {
                        $sql.="$key $value, ";
                    } else
                    {
                        $sql.="$key $value ";
                    }
                    $i=$i+1;
                }
            }
            
            
            $sql.=";";
            
            $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
            
            $res = $mysqli->query($sql);
            $row = $res->fetch_row();
            $pages = ceil($row[0]/$limit);
            
            if ($pages<1) $pages = 1;
            
            $_SESSION[$id] = $pages;
            
            return ($pages);
        }
    }     

    public function getName()
    { 
        return 'Users:'.$this->id;
    }

}