<?php 
/**  
 * [DESCRIPTION]
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */

require_once 'database.cfg.php';
require_once 'User.php'; 

class BunkerSupplyPublic
{

    //@var int
    public $id;

    //@var varchar
    public $name;

    //@var text
    public $description;

    //@var int
    public $user_id;

    //@var varchar
    public $user;

    //@var date
    public $expiry_date;

    //@var timestamp
    public $created;

    public function getUser()
    {
        require_once 'User.php';
        $user_id=$this->user_id;
		$user = new User(); 
        $user->find(array('id'=>$user_id));
        if ($user->getId()>0)
        {
            $this->user = $user->getName();
        } else
        {
            $this->user = "";
        }
        return $this->user;
    }
    public  function insert()
    {
        $mysqli = new mysqli($this->db->host,$this->db->user, $this->db->password, $this->db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        

            $name=$mysqli->real_escape_string($this->name);
            $description=$mysqli->real_escape_string($this->description);
            $user_id=$mysqli->real_escape_string($this->user_id);
            $expiry_date=$mysqli->real_escape_string($this->expiry_date);
        $sql="INSERT INTO bunker_supplies (name,description,user_id,expiry_date) VALUES ('$name','$description','$user_id','$expiry_date');";
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        } else
        {
            $this->id = $mysqli->insert_id;
        }
        $mysqli->close();
        return true;
        

    }
    public  function update($data=array())
    {
        $mysqli = new mysqli($this->db->host,$this->db->user, $this->db->password, $this->db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        
        $sql="UPDATE bunker_supplies SET ";        
        $updates = '';
        if ((count($data)>0) && (array_key_exists('id',$data) || ($this->id>0)))
        {
            $where = '';
            foreach($data as $key=>$value)
            {
                if ($key=='id')
                {
                    $where="WHERE $key='$value';";
                } else
                {
                    if ($updates)
                    {
                        $updates.= ", $key='$value' ";
                    } else
                    {
                        $updates.= " $key='$value' ";
                    }
                }
            }
            if ($where)
            {
            } else
            {
                $where="WHERE id='".$this->id."';";
            }
            $sql.=$updates.$where;
        } else
        {
        
            $name=$mysqli->real_escape_string($this->name);
            $description=$mysqli->real_escape_string($this->description);
            $user_id=$mysqli->real_escape_string($this->user_id);
            $expiry_date=$mysqli->real_escape_string($this->expiry_date);

            $sql.="name='$name',description='$description',user_id='$user_id',expiry_date='$expiry_date' WHERE id='$this->id'";

        }
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        } 
        $mysqli->close();
        return true;
        

    }
    public  function save()
    {
        if  ($this->id)
        {
            return $this->update();
        } else
        {
            return $this->insert();
        }
    }

    public  function delete()
    {
        $mysqli = new mysqli($this->db->host,$this->db->user, $this->db->password, $this->db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        
        $sql="DELETE FROM bunker_supplies WHERE id='$this->id'";
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        } 
        $mysqli->close();
        return true;
        

    }
    public $db;
    
    public function __construct()
    {
       $now=new DateTime('NOW');
       $this->created=$now->format('Y-m-d H:i:s');
       $this->db = new db();

       return $this;
    }
    
    public function find($filter,$orders=array())
    {
        $sql="SELECT * from bunker_supplies ";
        $i=1;
        $len=count($filter);
        foreach($filter as $key=>$val)
        {
            $value='';
            $operator = '=';
            if (is_array($val))
            {
                $field="`$key`";
                $value = "('".implode("','",$val)."')";
                $operator = 'IN';
            } else if (is_numeric($key) || empty($key) || (!isset($key))) 
            {
                $operator = '';
                $value="$val";
                $field='';
            } else if (property_exists($this,$key))
            {
                $field=$key;
                $value = "'$val'";
            } else
            {
                $operator = '';
                $field=$key;
                $value = "'$val'";
            }
            
            if ($i==1)
            {
                $sql.="WHERE $field $operator $value ";
            } else
            {
                $sql.="AND $field $operator $value ";
            } 

            $i=$i+1;
        }
        $olen=count($orders);
        if ($olen>0)
        {
            $sql.="ORDER BY ";
            foreach($orders as $key=>$value)
            {
                if ($i<$olen)
                {
                    $sql.="$key $value, ";
                } else
                {
                    $sql.="$key $value ";
                }
                $i=$i+1;
            }
        }        
        
        $sql.=" LIMIT 1 ;";
        $mysqli = new mysqli($this->db->host,$this->db->user, $this->db->password, $this->db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return $this;
        }
        
        $res = $mysqli->query($sql);
    
        if ($mysqli->affected_rows>0)
        {
            $row = $res->fetch_array();
            
            $this->id=$row['id'];
            $this->name=$row['name'];
            $this->description=$row['description'];
            $this->user_id=$row['user_id'];
            $this->expiry_date=$row['expiry_date'];
            $this->created=$row['created'];

        } else
        {
            
        }
        $mysqli->close();
        return $this;
    }   
    
     
    public function findAll($filter=array(),$orders=array(),$limit=100000,$page=1,$select=array(),$debug=false)
    {
        $sel_str='*';
        if ($select)
        {
            $sel_str=implode(',',$select);
        }
        $sql="SELECT ".$sel_str." from bunker_supplies ";        
        
        $i=1;
        if ($page > 0)
        {
            $offset = ($page - 1)*$limit;
        } else
        {
            $offset = 0;
        }
        foreach ($filter as $id=>$val)
        {
            if (($val)||(is_string($filter[$id]) && strlen($filter[$id]))) 
            {
            }
            else
            {
                unset($filter[$id]);
            }
        }
        foreach ($orders as $id=>$val)
        {
            if (strlen($orders[$id]))
            {
            }
            else
            {
                unset($orders[$id]);
            }
        }        
        $len=count($filter);
        if ($len>0)
        {
            foreach($filter as $key=>$val)
            {
                $operator = '=';
                if (is_array($val))
                {
                    $field="`$key`";
                    $value = "('".implode("','",$val)."')";
                    $operator = 'IN';
                } else if (is_numeric($key) || empty($key) || (!isset($key))) 
                {
                    $operator = '';
                    $value="$val";
                    $field='';
                } else if (property_exists($this,$key))
                {
                    $field=$key;
                    $value = "'$val'";
                } else
                {
                    $operator = '';
                    $field=$key;
                    $value = "'$val'";
                }
                
                if ($i==1)
                {
                    $sql.="WHERE $field $operator $value ";
                } else
                {
                    $sql.="AND $field $operator $value ";
                } 

                $i=$i+1;
            }
        } else
		{
			$sql.="WHERE `id` > 0 ";
		}
        $i=1;
        $olen=count($orders);
        if ($olen>0)
        {
            $sql.="ORDER BY ";
            foreach($orders as $key=>$value)
            {
                if ($i<$olen)
                {
                    $sql.="$key $value, ";
                } else
                {
                    $sql.="$key $value ";
                }
                $i=$i+1;
            }
        }
        if ($limit>0)
        {
            $sql.=" LIMIT $limit OFFSET $offset";
        }
        
        
        $sql.=";";
        
        if ($debug) die($sql);
        
        $mysqli = new mysqli($this->db->host,$this->db->user, $this->db->password, $this->db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return array();
        }        
        
        $res = $mysqli->query($sql);
    
        $bunker_supplies = array();
        
        if ($mysqli->affected_rows>0)
        {
            while ($row = $res->fetch_array())
            {
                $bunker_supply= new BunkerSupply();
                
                $bunker_supply->id=$row['id'];
                $bunker_supply->name=$row['name'];
                $bunker_supply->description=$row['description'];
                $bunker_supply->user_id=$row['user_id'];
                $bunker_supply->expiry_date=$row['expiry_date'];
                $bunker_supply->created=$row['created'];

                $bunker_supplies[]=$bunker_supply;
            }
        } else
        {

        }
        $mysqli->close();
        return $bunker_supplies;
    } 
    
    public function getAll($param=array('filter'=>array(),'select'=>array(),'sort'=>array(),'limit'=>0,'page'=>0))
    {
        $filter=array();
        if (array_key_exists('filter',$param))
        {
            $filter = $param['filter'];
        }
        $select=array();
        if (array_key_exists('select',$param))
        {
            $select=$param['select'];
        }
        $sort=array();
        if (array_key_exists('sort',$param))
        {
            $sort=$param['sort'];
        }
        $limit=0;
        if (array_key_exists('limit',$param))
        {
            $limit=$param['limit'];
        }
        $page=0;
        if (array_key_exists('page',$param))
        {
            $page=$param['page'];
        }
        $debug=false;
        if (array_key_exists('debug',$param))
        {
            $debug=$param['debug'];
        }
        return $this->findAll($filter,$sort,$limit,$page,$select,$debug);
    }    

    public function getPageCount($filter=array(),$orders=array(),$item_per_page=100,$clear=true,$max=10000)
    {
        $id = 'bunker_supply_pages';
        if (($_SESSION[$id]) && (!$clear))
        {
            return $_SESSION[$id]; 
        } else
        {
            $res_count = 0;
            $not_done = 1;
            $loop = 0;
            if (((int)$max)==0) $max=10000;
            for($not_done=1;$not_done;)
            {
                $offset = $loop*$max;
                if ($not_done)
                {
                    $res = $this->getCount($filter,$max,$offset);
                    if ($res>0)
                    {
                        $res_count = $res_count + $res;
                    }
                    if ($res<$max)
                    {
                        $not_done = 0; //done already
                    }
                }
                $loop = $loop+1;
             }
            
            if ($res_count>0)
            {
               $pages = ceil($res_count/$item_per_page);
            
               if ($pages<1) $pages = 1;
            
               $_SESSION[$id] = $pages;
            }
            else
            {
               if ($pages<1) $pages = 1;
            
               $_SESSION[$id] = $pages;
            }
            
            return ($pages);
        }        
        
    } 
        
    public function getCount($filter=array(),$limit=100000,$offset=0)
    {
        $mysqli = new mysqli($this->db->host,$this->db->user, $this->db->password, $this->db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return 0;
        }
                
        $sql="SELECT count(id) from bunker_supplies ";
 

        foreach ($filter as $id=>$val)
        {
            if (strlen($filter[$id])||(is_array($val)))
            {
            }
            else
            {
                unset($filter[$id]);
            }
        }

        $len=count($filter);
        if ($len>0)
        {
            $i=1;
            foreach($filter as $key=>$val)
            {
                $operator = '=';
                if (is_array($val))
                {
                    $field="`$key`";
                    $value = "('".implode("','",$val)."')";
                    $operator = 'IN';
                } else if (is_numeric($key) || empty($key) || (!isset($key))) 
                {
                    $operator = '';
                    $value="$val";
                    $field='';
                } else if (property_exists($this,$key))
                {
                    $field=$key;
                    $value = "'$val'";
                } else
                {
                    $operator = '';
                    $field=$key;
                    $value = "'$val'";
                }
                
                if ($i==1)
                {
                    $sql.="WHERE $field $operator $value ";
                } else
                {
                    $sql.="AND $field $operator $value ";
                } 

                $i=$i+1;
            }
        } else
		{
			$sql.="WHERE `id` > 0 ";
		}
 
        $sql.=" LIMIT $limit OFFSET $offset";
 
        $res = $mysqli->query($sql);
        
        if ($res)
        {
            $row = $res->fetch_array();
            return $row["count(id)"];
        }
        $mysqli->close();        
        return 0;
        
    }
    
    function getOptionsAndSelected($selected=array(),$filter=array())
    {
        $objs=$this->findAll($filter);
        $data_names = array();
        $options = '';
        foreach ($objs as $option)
        {
            $name = $option->getName();
            $oid = $option->getId();
            if ($oid)
            {
                if (in_array($oid,$selected))
                {
                    $options.="<option selected='selected' value='$oid' >$name</option>\n";

                    $data_names[] = $name;
                } else
                {
                    $options.="<option value='$oid' >$name</option>\n";

                }
            }
        }     
        return (object) array('options'=>$options,'selected'=>implode(', ',$data_names));
    }


}