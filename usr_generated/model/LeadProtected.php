<?php 
/**  
 * [DESCRIPTION]
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */

require_once 'database.cfg.php';
class LeadProtected
{

    //@var int
    protected $id;

    //@var varchar
    protected $lead_source;

    //@var int
    protected $campaign_id;

    //@var int
    protected $aid;

    //@var varchar
    protected $partner;

    //@var varchar
    protected $password;

    //@var timestamp
    protected $date_created;

    //@var varchar
    protected $firstname;

    //@var varchar
    protected $lastname;

    //@var varchar
    protected $email;

    //@var varchar
    protected $street_address;

    //@var varchar
    protected $city;

    //@var varchar
    protected $state;

    //@var varchar
    protected $zip_code;

    //@var varchar
    protected $phone1;

    //@var varchar
    protected $phone_line_type;

    //@var varchar
    protected $credit_profile;

    //@var varchar
    protected $first_mortgage_balance;

    //@var decimal
    protected $first_mortgage_interest_rate;

    //@var varchar
    protected $desired_loan_amount;

    //@var varchar
    protected $property_type;

    //@var varchar
    protected $loan_type;

    //@var varchar
    protected $existing_property_value;

    //@var decimal
    protected $lead_cost;

    //@var enum
    protected $is_military;

    //@var varchar
    protected $post_data;

    //@var varchar
    protected $repost_data;

    //@var varchar
    protected $response;

    //@var int
    protected $user_id_submitter;

    //@var int
    protected $ckm_subid;

    //@var varchar
    protected $sr_token;

    //@var date
    protected $origination;

    //@var varchar
    protected $bankruptcy_type;

    //@var varchar
    protected $ip_address;

    //@var varchar
    protected $universal_leadid;

    //@var varchar
    protected $notes;

    public function setId($id)
    {
       $this->id=$id;
       return $this;
    }
    public function getId()
    {
       return $this->id;
    }

    public function setLeadSource($lead_source)
    {
       $this->lead_source=$lead_source;
       return $this;
    }
    public function getLeadSource()
    {
       return htmlentities($this->lead_source,ENT_QUOTES);
    }

    public function setCampaignId($campaign_id)
    {
       $this->campaign_id=$campaign_id;
       return $this;
    }
    public function getCampaignId()
    {
       return $this->campaign_id;
    }

    public function setAid($aid)
    {
       $this->aid=$aid;
       return $this;
    }
    public function getAid()
    {
       return $this->aid;
    }

    public function setPartner($partner)
    {
       $this->partner=$partner;
       return $this;
    }
    public function getPartner()
    {
       return htmlentities($this->partner,ENT_QUOTES);
    }

    public function setPassword($password)
    {
       $this->password=$password;
       return $this;
    }
    public function getPassword()
    {
       return $this->password;
    }

    public function setDateCreated($date_created)
    {
       $this->date_created=$date_created;
       return $this;
    }
    public function getDateCreated()
    {
       return $this->date_created;
    }

    public function setFirstname($firstname)
    {
       $this->firstname=$firstname;
       return $this;
    }
    public function getFirstname()
    {
       return htmlentities($this->firstname,ENT_QUOTES);
    }

    public function setLastname($lastname)
    {
       $this->lastname=$lastname;
       return $this;
    }
    public function getLastname()
    {
       return htmlentities($this->lastname,ENT_QUOTES);
    }

    public function setEmail($email)
    {
       $this->email=$email;
       return $this;
    }
    public function getEmail()
    {
       return $this->email;
    }

    public function setStreetAddress($street_address)
    {
       $this->street_address=$street_address;
       return $this;
    }
    public function getStreetAddress()
    {
       return htmlentities($this->street_address,ENT_QUOTES);
    }

    public function setCity($city)
    {
       $this->city=$city;
       return $this;
    }
    public function getCity()
    {
       return htmlentities($this->city,ENT_QUOTES);
    }

    public function setState($state)
    {
       $this->state=$state;
       return $this;
    }
    public function getState()
    {
       return htmlentities($this->state,ENT_QUOTES);
    }

    public function setZipCode($zip_code)
    {
       $this->zip_code=$zip_code;
       return $this;
    }
    public function getZipCode()
    {
       return htmlentities($this->zip_code,ENT_QUOTES);
    }

    public function setPhone1($phone1)
    {
       $this->phone1=$phone1;
       return $this;
    }
    public function getPhone1()
    {
       return htmlentities($this->phone1,ENT_QUOTES);
    }

    public function setPhoneLineType($phone_line_type)
    {
       $this->phone_line_type=$phone_line_type;
       return $this;
    }
    public function getPhoneLineType()
    {
       return htmlentities($this->phone_line_type,ENT_QUOTES);
    }

    public function setCreditProfile($credit_profile)
    {
       $this->credit_profile=$credit_profile;
       return $this;
    }
    public function getCreditProfile()
    {
       return htmlentities($this->credit_profile,ENT_QUOTES);
    }

    public function setFirstMortgageBalance($first_mortgage_balance)
    {
       $this->first_mortgage_balance=$first_mortgage_balance;
       return $this;
    }
    public function getFirstMortgageBalance()
    {
       return htmlentities($this->first_mortgage_balance,ENT_QUOTES);
    }

    public function setFirstMortgageInterestRate($first_mortgage_interest_rate)
    {
       $this->first_mortgage_interest_rate=$first_mortgage_interest_rate;
       return $this;
    }
    public function getFirstMortgageInterestRate()
    {
       return $this->first_mortgage_interest_rate;
    }

    public function setDesiredLoanAmount($desired_loan_amount)
    {
       $this->desired_loan_amount=$desired_loan_amount;
       return $this;
    }
    public function getDesiredLoanAmount()
    {
       return htmlentities($this->desired_loan_amount,ENT_QUOTES);
    }

    public function setPropertyType($property_type)
    {
       $this->property_type=$property_type;
       return $this;
    }
    public function getPropertyType()
    {
       return htmlentities($this->property_type,ENT_QUOTES);
    }

    public function setLoanType($loan_type)
    {
       $this->loan_type=$loan_type;
       return $this;
    }
    public function getLoanType()
    {
       return htmlentities($this->loan_type,ENT_QUOTES);
    }

    public function setExistingPropertyValue($existing_property_value)
    {
       $this->existing_property_value=$existing_property_value;
       return $this;
    }
    public function getExistingPropertyValue()
    {
       return htmlentities($this->existing_property_value,ENT_QUOTES);
    }

    public function setLeadCost($lead_cost)
    {
       $this->lead_cost=$lead_cost;
       return $this;
    }
    public function getLeadCost()
    {
       return $this->lead_cost;
    }

    public function setIsMilitary($is_military)
    {
       $this->is_military=$is_military;
       return $this;
    }
    public function getIsMilitary()
    {
       return $this->is_military;
    }

    public function setPostData($post_data)
    {
       $this->post_data=$post_data;
       return $this;
    }
    public function getPostData()
    {
       return htmlentities($this->post_data,ENT_QUOTES);
    }

    public function setRepostData($repost_data)
    {
       $this->repost_data=$repost_data;
       return $this;
    }
    public function getRepostData()
    {
       return htmlentities($this->repost_data,ENT_QUOTES);
    }

    public function setResponse($response)
    {
       $this->response=$response;
       return $this;
    }
    public function getResponse()
    {
       return htmlentities($this->response,ENT_QUOTES);
    }

    public function setUserIdSubmitter($user_id_submitter)
    {
       $this->user_id_submitter=$user_id_submitter;
       return $this;
    }
    public function getUserIdSubmitter()
    {
       return $this->user_id_submitter;
    }

    public function setCkmSubid($ckm_subid)
    {
       $this->ckm_subid=$ckm_subid;
       return $this;
    }
    public function getCkmSubid()
    {
       return $this->ckm_subid;
    }

    public function setSrToken($sr_token)
    {
       $this->sr_token=$sr_token;
       return $this;
    }
    public function getSrToken()
    {
       return htmlentities($this->sr_token,ENT_QUOTES);
    }

    public function setOrigination($origination)
    {
       $this->origination=$origination;
       return $this;
    }
    public function getOrigination()
    {
       return $this->origination;
    }

    public function setBankruptcyType($bankruptcy_type)
    {
       $this->bankruptcy_type=$bankruptcy_type;
       return $this;
    }
    public function getBankruptcyType()
    {
       return htmlentities($this->bankruptcy_type,ENT_QUOTES);
    }

    public function setIpAddress($ip_address)
    {
       $this->ip_address=$ip_address;
       return $this;
    }
    public function getIpAddress()
    {
       return htmlentities($this->ip_address,ENT_QUOTES);
    }

    public function setUniversalLeadid($universal_leadid)
    {
       $this->universal_leadid=$universal_leadid;
       return $this;
    }
    public function getUniversalLeadid()
    {
       return htmlentities($this->universal_leadid,ENT_QUOTES);
    }

    public function setNotes($notes)
    {
       $this->notes=$notes;
       return $this;
    }
    public function getNotes()
    {
       return htmlentities($this->notes,ENT_QUOTES);
    }

    public  function insert()
    {
        $db = new db();
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        

            $lead_source=$mysqli->real_escape_string($this->lead_source);
            $campaign_id=$mysqli->real_escape_string($this->campaign_id);
            $aid=$mysqli->real_escape_string($this->aid);
            $partner=$mysqli->real_escape_string($this->partner);
            $password=$mysqli->real_escape_string($this->password);
            $date_created=$mysqli->real_escape_string($this->date_created);
            $firstname=$mysqli->real_escape_string($this->firstname);
            $lastname=$mysqli->real_escape_string($this->lastname);
            $email=$mysqli->real_escape_string($this->email);
            $street_address=$mysqli->real_escape_string($this->street_address);
            $city=$mysqli->real_escape_string($this->city);
            $state=$mysqli->real_escape_string($this->state);
            $zip_code=$mysqli->real_escape_string($this->zip_code);
            $phone1=$mysqli->real_escape_string($this->phone1);
            $phone_line_type=$mysqli->real_escape_string($this->phone_line_type);
            $credit_profile=$mysqli->real_escape_string($this->credit_profile);
            $first_mortgage_balance=$mysqli->real_escape_string($this->first_mortgage_balance);
            $first_mortgage_interest_rate=$mysqli->real_escape_string($this->first_mortgage_interest_rate);
            $desired_loan_amount=$mysqli->real_escape_string($this->desired_loan_amount);
            $property_type=$mysqli->real_escape_string($this->property_type);
            $loan_type=$mysqli->real_escape_string($this->loan_type);
            $existing_property_value=$mysqli->real_escape_string($this->existing_property_value);
            $lead_cost=$mysqli->real_escape_string($this->lead_cost);
            $is_military=$mysqli->real_escape_string($this->is_military);
            $post_data=$mysqli->real_escape_string($this->post_data);
            $repost_data=$mysqli->real_escape_string($this->repost_data);
            $response=$mysqli->real_escape_string($this->response);
            $user_id_submitter=$mysqli->real_escape_string($this->user_id_submitter);
            $ckm_subid=$mysqli->real_escape_string($this->ckm_subid);
            $sr_token=$mysqli->real_escape_string($this->sr_token);
            $origination=$mysqli->real_escape_string($this->origination);
            $bankruptcy_type=$mysqli->real_escape_string($this->bankruptcy_type);
            $ip_address=$mysqli->real_escape_string($this->ip_address);
            $universal_leadid=$mysqli->real_escape_string($this->universal_leadid);
            $notes=$mysqli->real_escape_string($this->notes);
        $sql="INSERT INTO leads (lead_source,campaign_id,aid,partner,password,date_created,firstname,lastname,email,street_address,city,state,zip_code,phone1,phone_line_type,credit_profile,first_mortgage_balance,first_mortgage_interest_rate,desired_loan_amount,property_type,loan_type,existing_property_value,lead_cost,is_military,post_data,repost_data,response,user_id_submitter,ckm_subid,sr_token,origination,bankruptcy_type,ip_address,universal_leadid,notes) VALUES ('$lead_source','$campaign_id','$aid','$partner','$password','$date_created','$firstname','$lastname','$email','$street_address','$city','$state','$zip_code','$phone1','$phone_line_type','$credit_profile','$first_mortgage_balance','$first_mortgage_interest_rate','$desired_loan_amount','$property_type','$loan_type','$existing_property_value','$lead_cost','$is_military','$post_data','$repost_data','$response','$user_id_submitter','$ckm_subid','$sr_token','$origination','$bankruptcy_type','$ip_address','$universal_leadid','$notes');";
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        } else
        {
            $this->id = $mysqli->insert_id;
        }
        $mysqli->close();
        return true;
        

    }
    public  function update($data=array())
    {
        $db = new db();
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        
        $sql="UPDATE leads SET ";        
        $updates = '';
        if ((count($data)>0) && (array_key_exists('id',$data) || ($this->id>0)))
        {
            $where = '';
            foreach($data as $key=>$value)
            {
                if ($key=='id')
                {
                    $where="WHERE $key='$value';";
                } else
                {
                    if ($updates)
                    {
                        $updates.= ", $key='$value' ";
                    } else
                    {
                        $updates.= " $key='$value' ";
                    }
                }
            }
            if ($where)
            {
            } else
            {
                $where="WHERE id='".$this->id."';";
            }
            $sql.=$updates.$where;
        } else
        {
        
            $lead_source=$mysqli->real_escape_string($this->lead_source);
            $campaign_id=$mysqli->real_escape_string($this->campaign_id);
            $aid=$mysqli->real_escape_string($this->aid);
            $partner=$mysqli->real_escape_string($this->partner);
            $password=$mysqli->real_escape_string($this->password);
            $date_created=$mysqli->real_escape_string($this->date_created);
            $firstname=$mysqli->real_escape_string($this->firstname);
            $lastname=$mysqli->real_escape_string($this->lastname);
            $email=$mysqli->real_escape_string($this->email);
            $street_address=$mysqli->real_escape_string($this->street_address);
            $city=$mysqli->real_escape_string($this->city);
            $state=$mysqli->real_escape_string($this->state);
            $zip_code=$mysqli->real_escape_string($this->zip_code);
            $phone1=$mysqli->real_escape_string($this->phone1);
            $phone_line_type=$mysqli->real_escape_string($this->phone_line_type);
            $credit_profile=$mysqli->real_escape_string($this->credit_profile);
            $first_mortgage_balance=$mysqli->real_escape_string($this->first_mortgage_balance);
            $first_mortgage_interest_rate=$mysqli->real_escape_string($this->first_mortgage_interest_rate);
            $desired_loan_amount=$mysqli->real_escape_string($this->desired_loan_amount);
            $property_type=$mysqli->real_escape_string($this->property_type);
            $loan_type=$mysqli->real_escape_string($this->loan_type);
            $existing_property_value=$mysqli->real_escape_string($this->existing_property_value);
            $lead_cost=$mysqli->real_escape_string($this->lead_cost);
            $is_military=$mysqli->real_escape_string($this->is_military);
            $post_data=$mysqli->real_escape_string($this->post_data);
            $repost_data=$mysqli->real_escape_string($this->repost_data);
            $response=$mysqli->real_escape_string($this->response);
            $user_id_submitter=$mysqli->real_escape_string($this->user_id_submitter);
            $ckm_subid=$mysqli->real_escape_string($this->ckm_subid);
            $sr_token=$mysqli->real_escape_string($this->sr_token);
            $origination=$mysqli->real_escape_string($this->origination);
            $bankruptcy_type=$mysqli->real_escape_string($this->bankruptcy_type);
            $ip_address=$mysqli->real_escape_string($this->ip_address);
            $universal_leadid=$mysqli->real_escape_string($this->universal_leadid);
            $notes=$mysqli->real_escape_string($this->notes);

            $sql.="lead_source='$lead_source',campaign_id='$campaign_id',aid='$aid',partner='$partner',password='$password',date_created='$date_created',firstname='$firstname',lastname='$lastname',email='$email',street_address='$street_address',city='$city',state='$state',zip_code='$zip_code',phone1='$phone1',phone_line_type='$phone_line_type',credit_profile='$credit_profile',first_mortgage_balance='$first_mortgage_balance',first_mortgage_interest_rate='$first_mortgage_interest_rate',desired_loan_amount='$desired_loan_amount',property_type='$property_type',loan_type='$loan_type',existing_property_value='$existing_property_value',lead_cost='$lead_cost',is_military='$is_military',post_data='$post_data',repost_data='$repost_data',response='$response',user_id_submitter='$user_id_submitter',ckm_subid='$ckm_subid',sr_token='$sr_token',origination='$origination',bankruptcy_type='$bankruptcy_type',ip_address='$ip_address',universal_leadid='$universal_leadid',notes='$notes' WHERE id='$this->id'";

        }
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        } 
        $mysqli->close();
        return true;
        

    }
    public  function save()
    {
        if  ($this->id)
        {
            return $this->update();
        } else
        {
            return $this->insert();
        }
    }

    public  function delete()
    {
        $db = new db();
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        
        $sql="DELETE FROM leads WHERE id='$this->id'";
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        } 
        $mysqli->close();
        return true;
        

    }

    
    public function find($arr)
    {
        $db = new db();
        $sql="SELECT * from leads ";
        $i=1;
        $len=count($arr);
        foreach($arr as $key=>$val)
        {
            $value='';
            $operator = '=';
            if (is_array($val))
            {
                $field="`$key`";
                $value = "('".implode("','",$val)."')";
                $operator = 'IN';
            } else if (is_numeric($key) || empty($key) || (!isset($key))) 
            {
                $operator = '';
                $value="$val";
                $field='';
            } else
            {
                $field="`$key`";
                $field=$key;
                $value = "'$val'";
            }
            
            if ($i==1)
            {
                $sql.="WHERE $field $operator $value ";
            } else
            {
                $sql.="AND $field $operator $value ";
            } 

            $i=$i+1;
        }
        $sql.=" LIMIT 1 ;";
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        
        $res = $mysqli->query($sql);
    
        if ($mysqli->affected_rows>0)
        {
            $row = $res->fetch_array();
            
            $this->id=$row['id'];
            $this->lead_source=$row['lead_source'];
            $this->campaign_id=$row['campaign_id'];
            $this->aid=$row['aid'];
            $this->partner=$row['partner'];
            $this->password=$row['password'];
            $this->date_created=$row['date_created'];
            $this->firstname=$row['firstname'];
            $this->lastname=$row['lastname'];
            $this->email=$row['email'];
            $this->street_address=$row['street_address'];
            $this->city=$row['city'];
            $this->state=$row['state'];
            $this->zip_code=$row['zip_code'];
            $this->phone1=$row['phone1'];
            $this->phone_line_type=$row['phone_line_type'];
            $this->credit_profile=$row['credit_profile'];
            $this->first_mortgage_balance=$row['first_mortgage_balance'];
            $this->first_mortgage_interest_rate=$row['first_mortgage_interest_rate'];
            $this->desired_loan_amount=$row['desired_loan_amount'];
            $this->property_type=$row['property_type'];
            $this->loan_type=$row['loan_type'];
            $this->existing_property_value=$row['existing_property_value'];
            $this->lead_cost=$row['lead_cost'];
            $this->is_military=$row['is_military'];
            $this->post_data=$row['post_data'];
            $this->repost_data=$row['repost_data'];
            $this->response=$row['response'];
            $this->user_id_submitter=$row['user_id_submitter'];
            $this->ckm_subid=$row['ckm_subid'];
            $this->sr_token=$row['sr_token'];
            $this->origination=$row['origination'];
            $this->bankruptcy_type=$row['bankruptcy_type'];
            $this->ip_address=$row['ip_address'];
            $this->universal_leadid=$row['universal_leadid'];
            $this->notes=$row['notes'];

        } else
        {
            
        }

        return $this;
    }   
    
     
    public function findAll($arr=array(),$orders=array(),$limit=100000,$page=1,$select,$debug=false)
    {
        $db = new db();
        
        $sel_str='*';
        if ($select)
        {
            if (in_array('id',$select))
            {
            } else
            {
                $select[]='id';
            }
            $sel_str=implode(',',$select);
        }
        $sql="SELECT ".$sel_str." from leads ";        
        
        $i=1;
        if ($page > 0)
        {
            $offset = ($page - 1)*$limit;
        } else
        {
            $offset = 0;
        }
        foreach ($arr as $id=>$val)
        {
            if (strlen($arr[$id])||(is_array($val)))
            {
            }
            else
            {
                unset($arr[$id]);
            }
        }
        foreach ($orders as $id=>$val)
        {
            if (strlen($orders[$id]))
            {
            }
            else
            {
                unset($orders[$id]);
            }
        }        
        $len=count($arr);
        if ($len>0)
        {
            foreach($arr as $key=>$val)
            {
                $operator = '=';
                if (is_array($val))
                {
                    $field="`$key`";
                    $value = "('".implode("','",$val)."')";
                    $operator = 'IN';
                } else if (is_numeric($key) || empty($key) || (!isset($key))) 
                {
                    $operator = '';
                    $value="$val";
                    $field='';
                } else
                {
                    $field="`$key`";
                    $field=$key;
                    $value = "'$val'";
                }
                
                if ($i==1)
                {
                    $sql.="WHERE $field $operator $value ";
                } else
                {
                    $sql.="AND $field $operator $value ";
                } 

                $i=$i+1;
            }
        } else
		{
			$sql.="WHERE `id` > 0 ";
		}
        $i=1;
        $olen=count($orders);
        if ($olen>0)
        {
            $sql.="ORDER BY ";
            foreach($orders as $key=>$value)
            {
                if ($i<$olen)
                {
                    $sql.="$key $value, ";
                } else
                {
                    $sql.="$key $value ";
                }
                $i=$i+1;
            }
        }
        $sql.=" LIMIT $limit OFFSET $offset";
        
        $sql.=";";
        
        if ($debug) die($sql);
        
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        
        $res = $mysqli->query($sql);
    
        $leads = array();
        
        if ($mysqli->affected_rows>0)
        {
            while ($row = $res->fetch_array())
            {
                $lead= new Lead();
                
                $lead->id=$row['id'];
                $lead->lead_source=$row['lead_source'];
                $lead->campaign_id=$row['campaign_id'];
                $lead->aid=$row['aid'];
                $lead->partner=$row['partner'];
                $lead->password=$row['password'];
                $lead->date_created=$row['date_created'];
                $lead->firstname=$row['firstname'];
                $lead->lastname=$row['lastname'];
                $lead->email=$row['email'];
                $lead->street_address=$row['street_address'];
                $lead->city=$row['city'];
                $lead->state=$row['state'];
                $lead->zip_code=$row['zip_code'];
                $lead->phone1=$row['phone1'];
                $lead->phone_line_type=$row['phone_line_type'];
                $lead->credit_profile=$row['credit_profile'];
                $lead->first_mortgage_balance=$row['first_mortgage_balance'];
                $lead->first_mortgage_interest_rate=$row['first_mortgage_interest_rate'];
                $lead->desired_loan_amount=$row['desired_loan_amount'];
                $lead->property_type=$row['property_type'];
                $lead->loan_type=$row['loan_type'];
                $lead->existing_property_value=$row['existing_property_value'];
                $lead->lead_cost=$row['lead_cost'];
                $lead->is_military=$row['is_military'];
                $lead->post_data=$row['post_data'];
                $lead->repost_data=$row['repost_data'];
                $lead->response=$row['response'];
                $lead->user_id_submitter=$row['user_id_submitter'];
                $lead->ckm_subid=$row['ckm_subid'];
                $lead->sr_token=$row['sr_token'];
                $lead->origination=$row['origination'];
                $lead->bankruptcy_type=$row['bankruptcy_type'];
                $lead->ip_address=$row['ip_address'];
                $lead->universal_leadid=$row['universal_leadid'];
                $lead->notes=$row['notes'];

                $leads[]=$lead;
            }
        } else
        {

        }

        return $leads;
    } 
    
    public function findAllLike($arr=array(),$orders=array(),$limit=100000,$page=1)
    {
        $db = new db();
        $sql="SELECT * from leads ";
        $i=1;
        $offset = ($page - 1)*$limit;
        foreach ($arr as $id=>$val)
        {
            if (trim($arr[$id]))
            {
            }
            else
            {
                unset($arr[$id]);
            }
        }
        foreach ($orders as $id=>$val)
        {
            if (trim($orders[$id]))
            {
            }
            else
            {
                unset($orders[$id]);
            }
        }       
        $len=count($arr); 
        if ($len>0)
        {
            foreach($arr as $key=>$val)
            {               
                $operator = 'LIKE';
                if (is_array($val))
                {
                    $value = "('".implode("','",$val)."')";
                    $operator = 'IN';
                } else
                {
                    $value = "'%$val%'";
                }

                if ($i==1)
                {
                    $sql.="WHERE `$key` $operator $value ";
                    
                } else
                {
                    $sql.="$operator `$key` $operator $value ";
                }

                $i=$i+1;
            }
        } else
		{
			$sql.="WHERE `id` > 0 ";
		}
        $i=1;
        $olen=count($orders);
        if ($olen>0)
        {
            $sql.="ORDER BY ";
            foreach($orders as $key=>$value)
            {
                if ($i<$olen)
                {
                    $sql.="$key $value, ";
                } else
                {
                    $sql.="$key $value ";
                }
                $i=$i+1;
            }
        }
        $sql.=" LIMIT $limit OFFSET $offset";
        $sql.=";";
        
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        
        $res = $mysqli->query($sql);
    
        $leads = array();
        
        if ($mysqli->affected_rows>0)
        {
            while ($row = $res->fetch_array())
            {
                $lead= new Lead();
                
                $lead->id=$row['id'];
                $lead->lead_source=$row['lead_source'];
                $lead->campaign_id=$row['campaign_id'];
                $lead->aid=$row['aid'];
                $lead->partner=$row['partner'];
                $lead->password=$row['password'];
                $lead->date_created=$row['date_created'];
                $lead->firstname=$row['firstname'];
                $lead->lastname=$row['lastname'];
                $lead->email=$row['email'];
                $lead->street_address=$row['street_address'];
                $lead->city=$row['city'];
                $lead->state=$row['state'];
                $lead->zip_code=$row['zip_code'];
                $lead->phone1=$row['phone1'];
                $lead->phone_line_type=$row['phone_line_type'];
                $lead->credit_profile=$row['credit_profile'];
                $lead->first_mortgage_balance=$row['first_mortgage_balance'];
                $lead->first_mortgage_interest_rate=$row['first_mortgage_interest_rate'];
                $lead->desired_loan_amount=$row['desired_loan_amount'];
                $lead->property_type=$row['property_type'];
                $lead->loan_type=$row['loan_type'];
                $lead->existing_property_value=$row['existing_property_value'];
                $lead->lead_cost=$row['lead_cost'];
                $lead->is_military=$row['is_military'];
                $lead->post_data=$row['post_data'];
                $lead->repost_data=$row['repost_data'];
                $lead->response=$row['response'];
                $lead->user_id_submitter=$row['user_id_submitter'];
                $lead->ckm_subid=$row['ckm_subid'];
                $lead->sr_token=$row['sr_token'];
                $lead->origination=$row['origination'];
                $lead->bankruptcy_type=$row['bankruptcy_type'];
                $lead->ip_address=$row['ip_address'];
                $lead->universal_leadid=$row['universal_leadid'];
                $lead->notes=$row['notes'];

                $leads[]=$lead;
            }
        } else
        {

        }

        return $leads;
    }
    
    public function getPageCount($arr=array(),$orders=array(),$limit=100,$equals_array_value=true,$clear=true)
    {
        $id = 'lead_pages';
        if (($_SESSION[$id]) && (!$clear))
        {
            return $_SESSION[$id]; 
        } else
        {
            $db = new db();
            $sql="SELECT count(*) from leads ";
            $i=1;
            
            foreach ($arr as $id=>$val)
            {
                if (trim($arr[$id]))
                {
                }
                else
                {
                    unset($arr[$id]);
                }
            }
            foreach ($orders as $id=>$val)
            {
                if (trim($orders[$id]))
                {
                }
                else
                {
                    unset($orders[$id]);
                }
            }        
            $len=count($arr);
            $prev_key='';
            if ($len>0)
            {
                if ($equals_array_value)
                {
                    foreach($arr as $key=>$val)
                    {
                        $operator = '=';
                        if (is_array($val))
                        {
                            $value = "('".implode("','",$val)."')";
                            $operator = 'IN';
                        } else
                        {
                            $value = "'$val'";
                        }
                        
                        if ($i==1)
                        {
                            $sql.="WHERE `$key` $operator $value ";
                        } else
                        {
                            $sql.="AND `$key` $operator $value ";
                        } 

                        $i=$i+1;
                    }
                } else
                {
                    foreach($arr as $key=>$val)
                    {               
                        $operator = 'LIKE';
                        if (is_array($val))
                        {
                            $value = "('".implode("','",$val)."')";
                            $operator = 'IN';
                        } else
                        {
                            $value = "'%$val%'";
                        }

                        if ($i==1)
                        {
                            $sql.="WHERE `$key` $operator $value ";
                            
                        } else
                        {
                            $sql.="$operator `$key` $operator $value ";
                        }

                        $i=$i+1;
                    }
                }
            } else
            {
                $sql.="WHERE `id` > 0 ";
            }
            $i=1;
            $olen=count($orders);
            if ($olen>0)
            {
                $sql.="ORDER BY ";
                foreach($orders as $key=>$value)
                {
                    if ($i<$olen)
                    {
                        $sql.="$key $value, ";
                    } else
                    {
                        $sql.="$key $value ";
                    }
                    $i=$i+1;
                }
            }
            
            
            $sql.=";";
            
            $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
            
            $res = $mysqli->query($sql);
            if ($mysqli->affected_rows>0)
            {
               $row = $res->fetch_row();
               $pages = ceil($row[0]/$limit);
            
               if ($pages<1) $pages = 1;
            
               $_SESSION[$id] = $pages;
            }
            
            return ($pages);
        }
    }     

    public function getName()
    { 
        return 'Lead:'.$this->id;
    }

}