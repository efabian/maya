<?php 
require_once 'MayaSecurity.php';
require_once 'security.cfg.php';
require_once 'MayaImage.php';
require_once 'Event.php';
class EventsController 
{
    private $security;
    
    public function __construct()
    {
        $this->security = new MayaSecurity();
    }

    public function findById($id)
    {
		$db = new db();
        $sql="SELECT * from events WHERE id='$id' LIMIT 1";

        $res = $mysqli->query($sql);

        $event='';
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->affected_rows>0)
        {
			while ($row = $res->fetch_array())
            {
                $event=new Event();
				
                $event->setId($row["id"]);
                $event->setName($row["name"]);
                $event->setDescription($row["description"]);
                $event->setUserIdAuthor($row["user_id_author"]);
                $event->setDate($row["date"]);
                $event->setStatus($row["status"]);
                $event->setHistory($row["history"]);

				continue;
			}
        } else
        {

        }

        return $event;
    }

    public function find($arr)
    {
		$db = new db();
        $sql="SELECT * from events WHERE ";
        $i=1;
        $len=count($arr);
        if ($len>0)
        {
			foreach($arr as $key=>$value)
			{
				if ($i<$len)
				{
					$sql.="`$key`='$value' AND";
				} else
				{
					$sql.="`$key`='$value' ;";
				}
			}
		} else
		{
			$sql.=" `id` > 0; ";
		}
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);

        $res = $mysqli->query($sql);
    
        $events = array();
        
        if ($mysqli->affected_rows>0)
        {
            while ($row = $res->fetch_array())
            {
                $event= new Event();
                
                $event->setId($row["id"]);
                $event->setName($row["name"]);
                $event->setDescription($row["description"]);
                $event->setUserIdAuthor($row["user_id_author"]);
                $event->setDate($row["date"]);
                $event->setStatus($row["status"]);
                $event->setHistory($row["history"]);
                $events[]=$event;
            }
        } else
        {

        }

        return $events;
    }
    

    public function newAction()
    {
		$event= new Event();
		
		
        $event->setName($_POST["name"]);
        $event->setDescription($_POST["description"]);
        $event->setUserIdAuthor($_POST["user_id_author"]);
        $event->setDate($_POST["date"]);
        $event->setStatus($_POST["status"]);
        $event->setHistory($_POST["history"]);


        if ($event->save()>0)
        {
            $_SESSION['maya_notice']='Successfully saved '.$event->getId();
		    $base_path = $_REQUEST['base_path'];
			$object = $_REQUEST['object'];
			$len = strlen($object);
			if (substr($object,-1)=='y')
			{
				$path = 'List '.ucfirst(substr($object,0,$len-1)).'ies.php';
			} else
			{
				$path = 'List '.ucfirst($object).'s.php';
			}
			header('Location: ?command=display_php&base_path='.$base_path.'&path='.$path);
			return $event;
        } else
        {
           $_SESSION['maya_notice']='SQL Error! '.$_SESSION['error'];
           return false;
        }
    }

    public function editAction()     
    {
		$id = $_REQUEST['id'];
		$event= new Event();
		$event = $event->find(array('id'=>$id));
        
        $event->setName($_POST["name"]);
        $event->setDescription($_POST["description"]);
        $event->setUserIdAuthor($_POST["user_id_author"]);
        $event->setDate($_POST["date"]);
        $event->setStatus($_POST["status"]);
        $event->setHistory($_POST["history"]);


        if ($event->save()>0)
        {
           $_SESSION['maya_notice']='Successfully saved '.$event->getId();
           return $event;
        } else
        {
           $_SESSION['maya_notice']='SQL Error! '.$_SESSION['error'];
           return false;
        }
    }
    
    public function deleteAction()     
    {
		$id = $_REQUEST['id'];
		$event= new Event();
		$event = $event->find(array('id'=>$id));

        if ($event->getId()>0)
        {
            if ($event->delete()==1)
            {
                $_SESSION['maya_notice']='Successfully deleted '.$id;
                $base_path = $_REQUEST['base_path'];
                $object = $_REQUEST['object'];
                $len = strlen($object);
                if (substr($object,-1)=='y')
                {
                    $path = 'List '.ucfirst(substr($object,0,$len-1)).'ies.php';
                } else
                {
                    $path = 'List '.ucfirst($object).'s.php';
                }
                header('Location: ?command=display_php&base_path='.$base_path.'&path='.$path);
                return $user;
            } else
            {
                $_SESSION['maya_notice']='SQL Error! '.$_SESSION['error'];
                return false;
            }
        } else
        {
            $_SESSION['maya_notice']='Cannot find event with id='.$id;
            return false;            
        }
    }    

    public function uploadImageAction()
    {
        $id = $_REQUEST['id'];
		$event= new Event();
		$event = $event->find(array('id'=>$id));
        $name = str_replace(',','_',str_replace(' ','',$event->getName()));
        $paths = new Paths();
        $ext = strtolower(strrchr($_FILES["file"]["name"], '.'));
        $image = new MayaImage();
        $filename=$id.'_'.strtolower($name);
        $image->setFilename($filename.$ext);
        $image->setPath($paths->user_image."events/");
        
        if ($image->save())
        {
            $image->setWidth('180');
			$image->setHeight('180');
			$image->setExt('.png');
			$image->resize("force_width");
			$image->createIcon('64','64',$id.'_'.strtolower($name).'_icon.png');
            return $user;
        } else
        {
            $_SESSION['maya_notice'].='Cannot save image!';
            return false;
        }
    }
}
