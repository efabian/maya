<?php 
/**  
 * [DESCRIPTION]
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */

require_once 'MayaFinancesController.php';
class FinancesController extends MayaFinancesController 
{ 
    public function getSummaryAction()
    {
        $name = $_GET['name'];
        
        $finance = new Finance();
        $expenses = $finance->getAll(array('filter'=>array('source'=>$name),'limit'=>10000,'select'=>array('id','expense','source')));
        $expenses_totol = 0;
        
        foreach($expenses as $exp)
        {
            $expenses_totol = $expenses_totol + $exp->getExpense();
        }
        
        $income = $finance->getAll(array('filter'=>array('destination'=>$name),'limit'=>10000,'select'=>array('id','income','destination')));
        $income_total = 0;
        
        foreach($income as $inc)
        {
            $income_total = $income_total + $inc->getIncome();
        }
        $total = 0;
        $total = $income_total - $expenses_totol;
        echo 'P '.number_format($total,2); 
        exit();
    }   
    
}
