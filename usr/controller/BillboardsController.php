<?php 
require_once 'MayaSecurity.php';
require_once 'security.cfg.php';
require_once 'MayaImage.php';
require_once 'Billboard.php';
class BillboardsController 
{
	    private $security;
    
    public function __construct()
    {
        $this->security = new MayaSecurity();
    }

    public function findById($id)
    {
		$db = new db();
        $sql="SELECT * from billboards WHERE id='$id' LIMIT 1";

        $res = $mysqli->query($sql);

        $billboard='';
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->affected_rows>0)
        {
			while ($row = $res->fetch_array())
            {
                $billboard=new Billboard();
				
                $billboard->setId($row["id"]);
                $billboard->setName($row["name"]);
                $billboard->setContent($row["content"]);
                $billboard->setStatus($row["status"]);
                $billboard->setUserIdAuthor($row["user_id_author"]);

				continue;
			}
        } else
        {

        }

        return $billboard;
    }

    public function find($arr)
    {
		$db = new db();
        $sql="SELECT * from billboards WHERE ";
        $i=1;
        $len=count($arr);
        if ($len>0)
        {
			foreach($arr as $key=>$value)
			{
				if ($i<$len)
				{
					$sql.="`$key`='$value' AND";
				} else
				{
					$sql.="`$key`='$value' ;";
				}
			}
		} else
		{
			$sql.=" `id` > 0; ";
		}
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);

        $res = $mysqli->query($sql);
    
        $billboards = array();
        
        if ($mysqli->affected_rows>0)
        {
            while ($row = $res->fetch_array())
            {
                $billboard= new Billboard();
                
                $billboard->setId($row["id"]);
                $billboard->setName($row["name"]);
                $billboard->setContent($row["content"]);
                $billboard->setStatus($row["status"]);
                $billboard->setUserIdAuthor($row["user_id_author"]);
                $billboards[]=$billboard;
            }
        } else
        {

        }

        return $billboards;
    }
    

    public function newAction()
    {
		$billboard= new Billboard();
		
		
        $billboard->setName($_POST["name"]);
        $billboard->setContent($_POST["content"]);
        $billboard->setStatus($_POST["status"]);
        $billboard->setUserIdAuthor($_POST["user_id_author"]);


        if ($billboard->save()>0)
        {
            $_SESSION['maya_notice']='Successfully saved '.$billboard->getId();
		    $base_path = $_REQUEST['base_path'];
			$object = $_REQUEST['object'];
			$len = strlen($object);
			if (substr($object,-1)=='y')
			{
				$path = 'List '.ucfirst(substr($object,0,$len-1)).'ies.php';
			} else
			{
				$path = 'List '.ucfirst($object).'s.php';
			}
			header('Location: ?command=display_php&base_path='.$base_path.'&path='.$path);
			return $billboard;
        } else
        {
           $_SESSION['maya_notice']='SQL Error! '.$_SESSION['error'];
           return false;
        }
    }

    public function editAction()     
    {
		$id = $_REQUEST['id'];
		$billboard= new Billboard();
		$billboard = $billboard->find(array('id'=>$id));
        
        $billboard->setName($_POST["name"]);
        $billboard->setContent($_POST["content"]);
        $billboard->setStatus($_POST["status"]);
        $billboard->setUserIdAuthor($_POST["user_id_author"]);


        if ($billboard->save()>0)
        {
           $_SESSION['maya_notice']='Successfully saved '.$billboard->getId();
           return $billboard;
        } else
        {
           $_SESSION['maya_notice']='SQL Error! '.$_SESSION['error'];
           return false;
        }
    }
    
    public function deleteAction()     
    {
		$id = $_REQUEST['id'];
		$billboard= new Billboard();
		$billboard = $billboard->find(array('id'=>$id));

        if ($billboard->getId()>0)
        {
            if ($billboard->delete()==1)
            {
                $_SESSION['maya_notice']='Successfully deleted '.$id;
                $base_path = $_REQUEST['base_path'];
                $object = $_REQUEST['object'];
                $len = strlen($object);
                if (substr($object,-1)=='y')
                {
                    $path = 'List '.ucfirst(substr($object,0,$len-1)).'ies.php';
                } else
                {
                    $path = 'List '.ucfirst($object).'s.php';
                }
                header('Location: ?command=display_php&base_path='.$base_path.'&path='.$path);
                return $user;
            } else
            {
                $_SESSION['maya_notice']='SQL Error! '.$_SESSION['error'];
                return false;
            }
        } else
        {
            $_SESSION['maya_notice']='Cannot find billboard with id='.$id;
            return false;            
        }
    }    

    public function uploadImageAction()
    {
        $id = $_REQUEST['id'];
		$billboard= new Billboard();
		$billboard = $billboard->find(array('id'=>$id));
        $name = str_replace(',','_',str_replace(' ','',$billboard->getName()));
        $paths = new Paths();
        $ext = strtolower(strrchr($_FILES["file"]["name"], '.'));
        $image = new MayaImage();
        $filename=$id.'_'.strtolower($name);
        $image->setFilename($filename.$ext);
        $image->setPath($paths->user_image."billboards/");
        
        if ($image->save())
        {
            $image->setWidth('180');
			$image->setHeight('180');
			$image->setExt('.png');
			$image->resize("force_width");
			$image->createIcon('64','64',$id.'_'.strtolower($name).'_icon.png');
            return $user;
        } else
        {
            $_SESSION['maya_notice'].='Cannot save image!';
            return false;
        }
        
    }

}

?>