<?php 
/**  
 * [DESCRIPTION]
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */

require_once 'MayaInformationsController.php';
require_once 'MayaSecurity.php';
class InformationsController extends MayaInformationsController 
{ 
    public function encryptAllCron()
    {
        $key = 'IkPzBilS6R3GfU6vIKQFbw==';
        $inf = new Information();
        $informations=$inf->findAll();
        $security = new MayaSecurity();
        $i=0;
        foreach($informations as $info)
        {
            $token = $info->getToken();
            $value = $info->getValue();
            
            //echo 'token='.$security->encrypt($token,$key)."\r\n";
            //echo 'value='.$security->encrypt($value,$key)."\r\n";
            //echo "---------------\r\n";
            $info->setToken($security->encrypt($token,$key));
            $info->setValue($security->encrypt($value,$key));
            $info->save();
            
            
            $i=$i+1;
        }  
        echo "$i processed\r\n";
    }    
    public function decryptCron()
    {
        $key = 'IkPzBilS6R3GfU6vIKQFbw==';
        $info = new Information();
        $info->find(array('id'=>88));
        $security = new MayaSecurity();

        $token = $info->getToken();
        $value = $info->getValue();
        
        echo $info->getId().':88 Token: "'.$security->decrypt($security->decrypt($info->raw_token,$key),$info->getSalt()).':"'.$info->raw_token."\r\n";
        echo '88 Value:'.$info->getValue()."\r\n";

    }      
}?>
