<?php

require_once 'UserViewBase.php';
class UserView extends UserViewBase
{
    public function renderActions($user,$class="",$edit=TRUE)
    {
        parent::renderActions($user,$class,$edit);
        $role = new Role();
        if ($role->isUserAuthorizedAs('admin'))
        {
            echo "<br><a class='confirm mx-' mx-confirm='Are you sure to assume as ".$user->getName()."?'  href='#' mx-redirect='?command=assumePerson&object=User&new_user_id=".$user->getId()."' ><li class='fa fa-user-secret'></li> Assume</a>";
        }
    }
}
