<?php

require_once 'InformationViewBase.php';
class InformationView extends InformationViewBase
{
    public function getValue($information)
    {
		$doc = new DOMDocument();
		$doc->loadHTML($information->getValue());
		echo $doc->saveHTML();
    }    
}
