<?php 
require_once 'database.cfg.php';
require_once 'UserProtected.php';
class User extends UserProtected
{
    //override setRole
    public function setRole($role)
    {
        if (is_array($role))
        {
            $rol = new Role();
            $role = $rol->arrayToIntegerEncoded($role);
        }
        $this->role = $role;
    }
}

