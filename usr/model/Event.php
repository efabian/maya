<?php 
require_once 'database.cfg.php';
class Event
{

    //@var int
    protected $id;

    //@var varchar
    protected $name;

    //@var varchar
    protected $description;

    //@var int
    protected $user_id_author;

    //@var date
    protected $date;

    //@var enum
    protected $status;

    //@var varchar
    protected $history;

    public function getImage()
    {
        $paths = new Paths();
        $name = str_replace(',','_',str_replace(' ','',$this->getName()));
        $absolute_path = $paths->user_image.'events/'.$this->id.'_'.strtolower($name).'.png';
        if (file_exists($absolute_path))
        {
            return $absolute_path;
        } else
        {
            return $paths->user_image."events/none.png";
        }
    }
    
    public function getImageIcon()
    {
        $paths = new Paths();
        $name = str_replace(',','_',str_replace(' ','',$this->getName()));
        $absolute_path = $paths->user_image.'events/'.$this->id.'_'.strtolower($name).'_icon.png';
        if (file_exists($absolute_path))
        {
            return $absolute_path;
        } else
        {
            return $paths->user_image."events/none_icon.png";
        }
    }
    public function setId($id)
    {
       $this->id=$id;
       return $this;
    }
    public function getId()
    {
       return $this->id;
    }

    public function setName($name)
    {
       $this->name=$name;
       return $this;
    }
    public function getName()
    {
       return htmlentities($this->name,ENT_QUOTES);
    }

    public function setDescription($description)
    {
       $this->description=$description;
       return $this;
    }
    public function getDescription()
    {
       return htmlentities($this->description,ENT_QUOTES);
    }

    public function setUserIdAuthor($user_id_author)
    {
       $this->user_id_author=$user_id_author;
       return $this;
    }
    public function getUserIdAuthor()
    {
       return $this->user_id_author;
    }

    public function setDate($date)
    {
       $this->date=$date;
       return $this;
    }
    public function getDate()
    {
       return $this->date;
    }

    public function setStatus($status)
    {
       $this->status=$status;
       return $this;
    }
    public function getStatus()
    {
       return $this->status;
    }

    public function setHistory($history)
    {
       $this->history=$history;
       return $this;
    }
    public function getHistory()
    {
       return htmlentities($this->history,ENT_QUOTES);
    }

    public  function insert()
    {
        $db = new db();
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        
        $name=$mysqli->real_escape_string($this->name);
        $description=$mysqli->real_escape_string($this->description);
        $user_id_author=$mysqli->real_escape_string($this->user_id_author);
        $date=$mysqli->real_escape_string($this->date);
        $status=$mysqli->real_escape_string($this->status);
        $history=$mysqli->real_escape_string($this->history);
        $sql="INSERT INTO events (name,description,user_id_author,date,status,history) VALUES ('$name','$description','$user_id_author','$date','$status','$history');";
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        }
        $mysqli->close();
        return true;
        

    }
    public  function update()
    {
        $db = new db();
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        
        $name=$mysqli->real_escape_string($this->name);
        $description=$mysqli->real_escape_string($this->description);
        $user_id_author=$mysqli->real_escape_string($this->user_id_author);
        $date=$mysqli->real_escape_string($this->date);
        $status=$mysqli->real_escape_string($this->status);
        $history=$mysqli->real_escape_string($this->history);
        $sql="UPDATE events SET name='$name',description='$description',user_id_author='$user_id_author',date='$date',status='$status',history='$history' WHERE id='$this->id'";
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        }
        $mysqli->close();
        return true;
        

    }
    public  function save()
    {
        if  ($this->id)
        {
            return $this->update();
        } else
        {
            return $this->insert();
        }
    }

    public  function delete()
    {
        $db = new db();
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        if ($mysqli->connect_errno) 
        {
            $_SESSION['maya_notice'].="Connect failed: ".$mysqli->connect_error." \r\n";
            return false;
        } 
        
        $sql="DELETE FROM events WHERE id='$this->id'";
        $mysqli->query($sql);
        if ($mysqli->error)
        {
            $_SESSION['maya_notice'].="Errormessage: ".$mysqli->error."  \r\n";
            return false;
        }
        $mysqli->close();
        return true;
        

    }

    
    public function find($arr)
    {
        $db = new db();
        $sql="SELECT * from events WHERE ";
        $i=1;
        $len=count($arr);
        foreach($arr as $key=>$value)
        {
            if ($i<$len)
            {
                $sql.="`$key`='$value' AND";
            } else
            {
                $sql.="`$key`='$value' ";
            }
        }
        $sql.=" LIMIT 1 ;";
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        
        $res = $mysqli->query($sql);
    
        if ($mysqli->affected_rows>0)
        {
            $row = $res->fetch_array();
            
            $this->id=$row['id'];
            $this->name=$row['name'];
            $this->description=$row['description'];
            $this->user_id_author=$row['user_id_author'];
            $this->date=$row['date'];
            $this->status=$row['status'];
            $this->history=$row['history'];

        } else
        {
            
        }

        return $this;
    }   
    
     
    public function findAll($arr,$orders,$where='')
    {
        $db = new db();
        $sql="SELECT * from events ";
        $i=1;
        foreach ($arr as $id=>$val)
        {
            if (trim($arr[$id]))
            {
            }
            else
            {
                unset($arr[$id]);
            }
        }
        foreach ($orders as $id=>$val)
        {
            if (trim($orders[$id]))
            {
            }
            else
            {
                unset($orders[$id]);
            }
        }        
        $len=count($arr);
        if ($len>0)
        {
            foreach($arr as $key=>$value)
            {
                if ($i==1)
                {
                    $sql.="WHERE ";
                }

                if ($i<$len)
                {
                    $sql.="`$key`='$value' AND ";
                } else
                {
                    $sql.="`$key`='$value' ";
                }

                $i=$i+1;
            }
        } else
		{
			$sql.="WHERE `id` > 0 ";
		}
        if ($where)
        {
            $sql.=' AND '.$where;
        }
        $i=1;
        $olen=count($orders);
        if ($olen>0)
        {
            $sql.="ORDER BY ";
            foreach($orders as $key=>$value)
            {
                if ($i<$olen)
                {
                    $sql.="$key $value, ";
                } else
                {
                    $sql.="$key $value ";
                }
                $i=$i+1;
            }
        }
        $sql.=";";
        
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        
        $res = $mysqli->query($sql);
    
        $events = array();
        
        if ($mysqli->affected_rows>0)
        {
            while ($row = $res->fetch_array())
            {
                $event= new Event();
                
            $event->id=$row['id'];
            $event->name=$row['name'];
            $event->description=$row['description'];
            $event->user_id_author=$row['user_id_author'];
            $event->date=$row['date'];
            $event->status=$row['status'];
            $event->history=$row['history'];

                $events[]=$event;
            }
        } else
        {

        }

        return $events;
    } 
    
    public function findAllLike($arr,$orders)
    {
        $db = new db();
        $sql="SELECT * from events ";
        $i=1;
        foreach ($arr as $id=>$val)
        {
            if (trim($arr[$id]))
            {
            }
            else
            {
                unset($arr[$id]);
            }
        }
        foreach ($orders as $id=>$val)
        {
            if (trim($orders[$id]))
            {
            }
            else
            {
                unset($orders[$id]);
            }
        }        
        $len=count($arr);
        if ($len>0)
        {
            foreach($arr as $key=>$value)
            {
                if ($i==1)
                {
                    $sql.="WHERE ";
                }

                if ($i<$len)
                {
                    $sql.="`$key` LIKE '%$value%' AND ";
                } else
                {
                    $sql.="`$key` LIKE '%$value%' ";
                }

                $i=$i+1;
            }
        } else
		{
			$sql.="WHERE `id` > 0 ";
		}
        $i=1;
        $olen=count($orders);
        if ($olen>0)
        {
            $sql.="ORDER BY ";
            foreach($orders as $key=>$value)
            {
                if ($i<$olen)
                {
                    $sql.="$key $value, ";
                } else
                {
                    $sql.="$key $value ";
                }
                $i=$i+1;
            }
        }
        $sql.=";";
        
        $mysqli = new mysqli($db->host,$db->user, $db->password, $db->name);
        
        $res = $mysqli->query($sql);
    
        $events = array();
        
        if ($mysqli->affected_rows>0)
        {
            while ($row = $res->fetch_array())
            {
                $event= new Event();
                
            $event->id=$row['id'];
            $event->name=$row['name'];
            $event->description=$row['description'];
            $event->user_id_author=$row['user_id_author'];
            $event->date=$row['date'];
            $event->status=$row['status'];
            $event->history=$row['history'];

                $events[]=$event;
            }
        } else
        {

        }

        return $events;
    }
    
     


}
