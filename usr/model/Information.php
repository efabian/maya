<?php 
/**  
 * [DESCRIPTION]
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */

require_once 'database.cfg.php';
require_once 'InformationProtected.php';
require_once 'MayaSecurity.php';
class Information extends InformationProtected
{
    public $security;
     
    public function getValue()
    {
       return $this->value;
    }
    
    public function setSalt($any='')
    {
        if (strlen(trim($this->salt))==0)
        {
            $this->salt = $this->security->generateSalt(10);
        }
    }
    
    public function setToken($token)
    {
        if (strlen(trim($this->salt))==0)
        {
            $this->salt = $this->security->generateSalt(10);
        }        
        $this->token = $this->security->encrypt($token,$this->salt);
    }
    
    public function getToken()
    {
        if ($this->salt)
        {
            return $this->security->decrypt($this->token,$this->salt);
        }
        else
        {
            return $this->token;
        }
    }
    
    
}

