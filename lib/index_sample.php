<?php
session_start();
ini_set('display_errors', 'On');
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING); 
require_once 'maya/Maya.php';

$maya = new Maya();
$rolw = new Role();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Maya Framework</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <?php $maya->view->initializeCssAndJquery();?>
</head>

<body>

  <header id="header" class="header header-hide">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="#body" class="scrollto"><img src="img/maya.png" alt="" style="height:40px;vertical-align:middle" title="" /><span>M</span>aya</a></h1>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
            <li ><a href="#" class="mx-" mx-container="div.page"  mx-click="?command=display_rest&path=home.pub.htm" ><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#" class="mx-" mx-container="div.page"  mx-click="?command=display_rest&path=advance.pub.htm"><i class="fa fa-star"></i> Advance Topic </a></li>
        <?php 
        if (!$role->isUserLoggedIn())
        {
        ?>
            <li class="dropdown"> <a data-toggle="dropdown" href="#" onclick="showLogin()" ><i class="fa fa-user-plus"></i> LOGIN</a></li>
        <?php
        } else 
        {
        ?>
            <li><a href="#" class='mx-' mx-container='div.page' mx-click="?command=display_rest&path=Admin.dir/Informations.dir/List Informations.rest.php"><i class="fa fa-database"></i> Informations</a></li>
            <li><a href="#" class='mx-' mx-container='div.page' mx-click="?command=display_rest&path=Admin.dir/Finances.dir/List Finances.rest.php"><i class="fa fa-dollar"></i> Finances</a></li>
            <li><a href="#"><i class="fa fa-user" ></i> <?php echo $_SESSION['user_name'];?></a>
                <ul>
                <li><a href="#" class='mx-' mx-container='div.page' mx-click="?command=display_rest&path=Admin.dir/Users.dir/List Users.rest.php"><i class="fa fa-group"></i> Users</a></li>
                <li><a href="#" class='mx-' mx-container='div.page' mx-click="?command=display_rest&path=Admin.dir/Tickets.dir/List Tickets.rest.php"><i class="fa fa-bug"></i> Tickets</a></li>    
                <li><a href="?command=logout" class="" style=""><i class="fa fa-user-times"></i>LOGOUT</a></li>
                </ul>
            </li>
      <?php 
        }?> 
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <?php $maya->view->generateLoginAndNotice(); ?>

  <section class="<?php echo ($role->isUserLoggedIn()?'padd-section':'');?> text-center wow fadeInUp"  style="margin-top:100px">
    <div class="container page">
    <?php
    $maya->controller->display()
    ?>
    </div>
  </section>

  <footer class="footer">
    <div class="copyrights">
      <div class="container">
        <p>&copy; Copyrights Maya. All rights reserved. Author: Edgardo Fabian      Since: 2007</p>
        <div class="credits">
          Framework powered by <a href="https://maya.docph.net">Maya</a>
          Html Template by <a href="https://bootstrapmade.com/">BootstrapMade</a> 
        </div>
      </div>
    </div>

  </footer>



  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>


  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>
<?php 
$maya->view->initializeScripts(array('mathjax'=>false,'speak'=>false,'chosen'=>true,'wysiwyg'=>true,'datetimepicker'=>true));
?>
</body>
</html>
