<?php
session_start();
ini_set('display_errors', 'On');
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING); 

require_once 'maya/Maya.php';
$maya = new Maya();
$role= new Role();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Maya Framework</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Csss File -->
  <link href="css/xxxx.css" rel="stylesheet">

  <?php $maya->view->initializeCssAndJquery();?>
  
</head>

<body>

<div>SECTION FOR LOGO MENU HEADERS</div>

<div>
<!--- FOR LOGIN FORMS AND MESSAGES -->
<?php   $maya->view->generateLoginAndNotice(); ?>
</div>

<div class="container page">
<!--- Main section for displaying contents of the site -->
    <?php
    $maya->controller->display()
    ?>
</div>

<div>FOOTER AREA</div>
<!-- Javascript Files -->
  <script src="js/main.js"></script>
<?php 
$maya->view->initializeScripts(array('mathjax'=>false,'speak'=>false,'chosen'=>true,'wysiwyg'=>true,'datetimepicker'=>true));
?>
</body>
</html>
