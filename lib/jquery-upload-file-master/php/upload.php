<?php
session_start();
if (isset($_SESSION['upload_directory']))
{
    $output_dir = $_SESSION['upload_directory'].'/';
}else
{
    $output_dir = __DIR__."/upload/";
}
if(isset($_FILES["myfile"]))
{
	$ret = array();
	
//	This is for custom errors;	
/*	$custom_error= array();
	$custom_error['jquery-upload-file-error']="File already exists";
	echo json_encode($custom_error);
	die();
*/
	$error =$_FILES["myfile"]["error"];
	//You need to handle  both cases
	//If Any browser does not support serializing of multiple files using FormData() 
	if(!is_array($_FILES["myfile"]["name"])) //single file
	{
 	 	$fileName = $_FILES["myfile"]["name"];
        if ((!$_SESSION['upload_overwrite']) && file_exists($output_dir.$fileName))
        {
            $custom_error= array();
            $custom_error['jquery-upload-file-error']="File already exists";
            echo json_encode($custom_error);
            die();
        } else
        {
            move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$fileName);
            $ret[]= $fileName;
        }
	}
	else  //Multiple files, file[]
	{
	  $fileCount = count($_FILES["myfile"]["name"]);
      $overwrite_message='';      
	  for($i=0; $i < $fileCount; $i++)
	  {
        if ((!$_SESSION['upload_overwrite']) && file_exists($output_dir.$fileName))
        {
            if ($overwrite_message)
            {
                $overwrite_message.=', '.$output_dir.$fileName;
            } else
            {
                $overwrite_message.=$output_dir.$fileName;
            }
        } else
        {          
            $fileName = $_FILES["myfile"]["name"][$i];
            move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName);
            $ret[]= $fileName;
        }
	  }
      
      if ($overwrite_message)
      {
            $custom_error= array();
            $custom_error['jquery-upload-file-error']="File/s already exists (".$overwrite_message.")";
            echo json_encode($custom_error);
            die();          
      }
	
	}
    echo json_encode($ret);
 }
 ?>
