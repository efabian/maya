<?php
require_once 'MayaSecurity.php';
class SampleViewBase 
{ 
    public $security;
    public $sample;
    public $page;
    public $path;
    public $samples;
    public $container;
    public $item_per_page;
    public $pages;
    public $search_text;
    public $select_filters;
    public $sort_filters;
    public $sort_orders;
    public function renderId($sample,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        //actual code removed
    }
    public function renderName($sample,$edit=true,$readonly=false,$hidden=false,$attributes=array())
    {
        //actual code removed
    }
    public function renderValueTextarea($sample,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'jwysiwyg'))
    {
        //actual code removed
    }
    public function renderDescriptionTextarea($sample,$edit=true,$readonly=false,$hidden=false,$attributes=array('class'=>'jwysiwyg'))
    {
        //actual code removed
    }
    public function init_edit()
    { 
        //actual code removed
    } 
    public function init_new()
    { 
        //actual code removed
    } 
    public function init_list()
    { 
        //actual code removed
    } 
    public function renderPaginator()
    { 
        include 'PaginatorView.php'; 
        include 'PaginatorController.php'; 
    } 
    public function renderCsrf()
    { 
        echo '<input type="hidden" name="'.$this->security->getCsrfName().'" value="'.$this->security->getCsrfValue().'"/>'; 
    } 
    public function renderActions($sample,$class="",$edit=TRUE)
    {
        if ($edit)
        {
            echo "<a class='click_action $class' container='".$this->container."' url='?command=display_rest&base_path=admin.dir/Samples.dir&path=Edit Sample.hid.rest.php&id=".$sample->getId()."&object=Sample' href='#' ><li class='fa fa-pencil'></li> edit</a>"; 
        } else
        {
            echo "<a class='click_action $class' container='".$this->container."' url='?command=display_rest&base_path=admin.dir/Samples.dir&path=View Sample.hid.rest.php&id=".$sample->getId()."&object=Sample' href='#' ><li class='fa fa-eye'></li> view</a>"; 
        }
    }
    
	public function renderEditControls($next_after_update='',$next_after_delete='next="?command=display_rest&path=admin.dir/Samples.dir/List Samples.rest.php"')
    {
		echo '<div class="control" style="height:80px">';
		echo '<a class="button click_action" url="?command=editRest&object=Sample"  href="#" container="div.page" '.$next_after_update.' ><li class="fa fa-save"></li> Update</a>';
		echo '<a class="button click_action confirm" url="?command=deleteRest&object=Sample" href="#" container="div.page" '.$next_after_delete.' ><li class="fa fa-trash" ></li> Delete </a></form>';
		echo '</div>';
	}    
	
	public function renderNewControls($next_after_create='next="?command=display_rest&path=admin.dir/Samples.dir/List Samples.rest.php"')
	{
		echo '<div class="control" style="height:80px">';
		echo '<a class="button click_action" url="?command=newRest&object=Sample" href="#" '.$next_after_create.' container="div.page"><li class="fa fa-plus"></li> Create </a>';
		echo '</div>';
	}
    
    public function renderCustomEdit()
    {
    }

    public function renderCustomNew()
    {
    }

    public function renderCustomlist()
    {
    }
}
