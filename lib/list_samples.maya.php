<?php
require_once 'Sample.php';
require_once 'SampleView.php';
require_once 'Role.php'; 
if (!isset($allowed)) $allowed=array("admin");
if (!isset($allowed_edit)) $allowed_edit=array("admin");
$role = new Role();
if (!$role->isUserAnyOf($allowed))
{

	$role->showUnauthorizedMessage($allowed,"?command=display_php&base_path=usr/view&path=List Samples.rest.php");
} else
{
$sample_view = new SampleView();
 if (!isset($headers)) $headers = ["id","name","value","description"];
$sample_view->init_list();
?>
<form id='form_list' <?php echo $sample_view->list_form_attributes;?> class='form list' action="?command=display_php&base_path=Admin.dir/Samples.dir&path=List Samples.rest.php" method="POST">
<table class='odd_even_row_alternate_color full_width data' >
<?php $sample_view->renderCsrf();?>
<?php $sample_view->renderCustomList();?>
<input class='field_pages' field_pages='<?php echo $sample_view->pages; ?>' type='hidden' value='<?php echo $sample_view->pages; ?>' name='pages' />
<input class='field_page' field_page='<?php echo $sample_view->page; ?>' type='hidden' value='<?php echo $sample_view->page; ?>' name='page' /> 
<thead><tr><th>#</th><th>Actions</th>
<?php if (in_array('id',$headers)) { ?> <th id='header_id'  field='id'><a class='toggle_sort fa fa-sort' href='#'> id</a>             <input id="sort_id" type="hidden" field="id" class="sort" name="sort_filters[id]" value='<?php echo strtoupper($sample_view->sort_filters['id']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('name',$headers)) { ?> <th id='header_name'  field='name'><a class='toggle_sort fa fa-sort' href='#'> name</a>             <input id="sort_name" type="hidden" field="name" class="sort" name="sort_filters[name]" value='<?php echo strtoupper($sample_view->sort_filters['name']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('value',$headers)) { ?> <th id='header_value'  field='value'><a class='toggle_sort fa fa-sort' href='#'> value</a>             <input id="sort_value" type="hidden" field="value" class="sort" name="sort_filters[value]" value='<?php echo strtoupper($sample_view->sort_filters['value']);?>' style="display:none;">
</th> <?php } ?>
<?php if (in_array('description',$headers)) { ?> <th id='header_description'  field='description'><a class='toggle_sort fa fa-sort' href='#'> description</a>             <input id="sort_description" type="hidden" field="description" class="sort" name="sort_filters[description]" value='<?php echo strtoupper($sample_view->sort_filters['description']);?>' style="display:none;">
</th> <?php } ?>
</tr>
<tr><th></th><th><button id="clear_search_and_sort" type="button" class="button"><i class="fa fa-times"></i> <span style="color:#DDDDDD;text-shadow:none"><i class="fa fa-sort"><i class="fa fa-search"></span></button></th><?php if (in_array('id',$headers)) { ?> <th>    <div class="search_filter_header">
    <input class="search_text" type="text" name="search_texts[id]" 
    value="<?php if (isset($sample_view->search_texts) && array_key_exists('id',$sample_view->search_texts)) echo $sample_view->search_texts['id'];?>"></input><a class="search_text_button" ><li class="fa fa-search"></li></a>
    </div>
</th> <?php } ?>
<?php if (in_array('name',$headers)) { ?> <th>    <div class="search_filter_header">
    <input class="search_text" type="text" name="search_texts[name]" 
    value="<?php if (isset($sample_view->search_texts) && array_key_exists('name',$sample_view->search_texts)) echo $sample_view->search_texts['name'];?>"></input><a class="search_text_button" ><li class="fa fa-search"></li></a>
    </div>
</th> <?php } ?>
<?php if (in_array('value',$headers)) { ?> <th>    <div class="search_filter_header">
    <input class="search_text" type="text" name="search_texts[value]" 
    value="<?php if (isset($sample_view->search_texts) && array_key_exists('value',$sample_view->search_texts)) echo $sample_view->search_texts['value'];?>"></input><a class="search_text_button" ><li class="fa fa-search"></li></a>
    </div>
</th> <?php } ?>
<?php if (in_array('description',$headers)) { ?> <th>    <div class="search_filter_header">
    <input class="search_text" type="text" name="search_texts[description]" 
    value="<?php if (isset($sample_view->search_texts) && array_key_exists('description',$sample_view->search_texts)) echo $sample_view->search_texts['description'];?>"></input><a class="search_text_button" ><li class="fa fa-search"></li></a>
    </div>
</th> <?php } ?>
</tr></thead>
<tbody>
 <?php
    //some actual code remove to reduce file
    $i=0;
    foreach ($sample_view->samples as $sample)
    { 
        $i=$i+1;
?>
<tr>
<td><?php echo $i;?></td><td><?php $sample_view->renderActions($sample,"",$role->isUserAnyOf($allowed_edit));?></td><?php if (in_array('id',$headers)) { ?><td><a class='click_action' container='<?php echo $sample_view->container;?>' url='?command=display_rest&base_path=Admin.dir/Samples.dir&path=Edit Sample.hid.rest.php&id=<?php echo $sample->getId();?>&object=Sample' href='#' ><?php echo $sample->getId();?></a></td> <?php }?>

<?php if (in_array('name',$headers)) { ?>
	<td><?php echo $sample->getName();?></td> <?php }?>
<?php if (in_array('value',$headers)) { ?>
	<td><?php echo $sample->getValue();?></td> <?php }?>
<?php if (in_array('description',$headers)) { ?>
	<td><?php echo $sample->getDescription();?></td> <?php }?>
</tr>
<?php } ?>
</tbody>
</table><input class="sort_order" type="hidden" name="sort_order" value="" />
<?PHP 
        $sample_view->renderPaginator();
?>
</form>
<script>
//actual code to handle sorting removed
</script>

<script>
//actual code to handle page remove
</script>
<div class="control" style="height:80px">
<a class='button click_action' container='<?php echo $sample_view->container;?>' url='?command=display_rest&base_path=<?php echo $_REQUEST["base_path"];?>&path=New Sample.rest.php' href='#' ><li class='fa fa-plus'></li> New</a>

</div>
<?php
}?>
