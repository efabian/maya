<?php
    require_once __DIR__.'/../config/paths.cfg.php';
    $paths = new Paths();
    set_include_path($paths->includes);
    require_once 'User.php';
    $absolute_path='/var/www/kyle/usr/uploads/image/none.png';
    $id=$_GET['id'];
    $user=new User();
    $user->find(array('id'=>$id));
    if ($id>0)
    {
		if ($user->getSecurekey()==$_REQUEST['key'])
		{
			$absolute_path = $_GET['image'];
		}
    }

    //display image
    header("Content-type: image/png");
    readfile($absolute_path);
?>
