<?php
/** Object for roles of maya framework 
 * Object contaiting elements of a maya framework role intended to be hardcoded values.
 * This page is intended to be edited by programmers to fit their needs
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package Role
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */
require_once 'MayaRole.php';
class Role extends MayaRole
{    
    public function __construct()
    {
        $this->rolenames = array(0x00=>'guest',0x01=>'patient',0x02=>'secretary',0x04=>'doctor',0x08=>'admin');
        $this->security = new Secure_config();
        $this->loadRoles();        
    }
    
    public function getAllRolesWithDoctorValues()
    {
        $doc_vals=array();
        for($i=0;$i<=15;$i++)
        {
            if ($i & array_search('doctor',$this->rolenames))
            {
                $doc_vals[] = $i;
            }
        }
        return $doc_vals;
    }
    
    public function getAllRolesWithPatientValues()
    {
        $pat_vals=array();
        for($i=0;$i<=15;$i++)
        {
            if ($i & array_search('patient',$this->rolenames))
            {
                $pat_vals[] = $i;
            }
        }
        return $pat_vals;
    }    
}

