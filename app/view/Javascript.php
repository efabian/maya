<?php 
class Javascript
{
    public function renderAjaxLoader()
    {
        ;
    }
    
    public function addFunctions() //these are scripts that are loaded to the loadScript() function that is loaded when a page is loaded
    {
?>
        reloadChosen();
        loadScripts();
        reloadSpeak();
<?php
    }  

    public function renderFunctions()
    {?> //javascript code functions here
        
        
    function initEditMode()
    {
        if ($('#toggle_edit').hasClass('edit_mode'))
        {
            /* do not toggle initially*/
        }
        else
        {
            $('body').find('.edit_input').each( function()
            {
                $(this).parents('td').find('div.chosen-container').css('width','100%').hide();
                $(this).hide();
            });      
        }        
    }
    function toggleEdit()
    {
        toggleThisEdit('body','td','#toggle_edit');
    }
    function toggleThisEdit(container,field_container,edit_button)
    {
        $(container).find('.view_input').each( function()
        {
            $(this).toggle();
        });
        $(container).find('.edit_input').each( function()
        {
            if ($(this).hasClass('jwysiwyg'))
            {
                $(this).parents('div.wysiwyg').toggle();
            } else if ($(this).hasClass('chosen'))
            {
                $(this).parents(field_container).find('div.chosen-container').toggle().css('width','100%');
            }
            else
            {
                $(this).toggle();
            }
            if ($('#toggle_edit').hasClass('edit_mode'))
            {
                var val='';
                if ($(this).is('select'))
                {
                    val=$(this).find("option:selected").html();
                } else if ($(this).is('span')||$(this).is('div')||$(this).is('label'))
                {
                    $(this).find('input[type="checkbox"]').each( function()
                    {
                        if ($(this).is(':checked'))
                        {
                            if (val.length>0) val=val+', '+$(this).parents('label').text();
                            else val=$(this).parents('label').text();
                        }
                    });
                    $(this).find('input[type="radio"]').each( function()
                    {
                        if ($(this).is(':checked'))
                        {
                            if (val.length>0) val=val+', '+$(this).parents('label').text();
                            else val=$(this).parents('label').text();
                        }
                    });                    
                } else
                {
                    val=$(this).val();
                }
                
                $(this).parents('td').find('.view_input').html(val);
            }
        });
        if ($(edit_button).hasClass('edit_mode'))
        {
            var edit_name='Enable Edit';
            if ( typeof ($(edit_button).attr('edit_name')) != 'undefined') edit_name=$(edit_button).attr('edit_name');
            $(edit_button).html('<li class="fa fa-edit"></li> '+edit_name);
            $(edit_button).removeClass('edit_mode');
        } else
        {
            var lock_name='Lock';
            if ( typeof ($(edit_button).attr('lock_name')) != 'undefined') lock_name=$(edit_button).attr('lock_name');
            $(edit_button).html('<li class="fa fa-lock"></li>'+lock_name);
            $(edit_button).addClass('edit_mode');
        }
    }        

    function loadScripts()
    {
        $('div.page').find('button.add_checkbox').each( function()
        {
            $(this).unbind('click');
            $(this).click( function()
            {
                button = $('<input type="checkbox" style="margin-right:10px" />');
                $(this).closest('div.assistive').find('div.checkbox').append(button);
            });   
            
        }); 
        $('div.page').find('button.del_checkbox').each( function()
        {
            $(this).unbind('click');
            $(this).click( function()
            {
                $(this).closest('div.assistive').find('div.checkbox input:first-child').remove();
            });   
            
        });       
        $('div.page').find('.sortable').each( function()
        {
            $div = $(this);
            $div.height($(this).height());
            $(this).sortable(
            {
                stop: function(event, ui) 
                {
                    var data = "";
                    var name = $(this).attr('name');
                    $(this).find('span').each( function(i, el)
                    {
                        var p = $(el).html();/*.replace(" ", "");*/
                        if (data.length==0)
                        {
                            data = p;
                        } else
                        {
                            data += "|" + p;
                        }
                        /*$div.draggable({ cancel: $(this) });*/
                    });
                    $(this).next('input').val(data);
                    
                }
            });
            $(this).disableSelection();
            
        });
        $('div.page').find('.underlinable').each( function()
        {
            $div = $(this);
            $div.height($(this).height());
            var data='';
            $(this).find('span'). each( function()
            {
                $this_span = $(this);
                $this_span.unbind('click');
                $this_span.click( function() 
                {
                    $click_span = $(this);
                    var underlined = $(this).css('text-decoration');
                    if (underlined.indexOf('underline')>=0)
                    {
                        
                        $(this).css('text-decoration','');
                        $(this).css('border','0px solid transparent');
                    } else
                    {
                        
                        $(this).css('text-decoration','underline');
                        $(this).css('border','1px solid gray');
                    }
                    var data ='';
                    $(this).closest('.underlinable').find('span'). each( function()
                    {
                        var decor = $(this).css('text-decoration');
                        if (decor.indexOf('underline')>=0)
                        {
                            var p = $(this).html().replace(" ", "");
                            if (data.length==0)
                            {
                                data = p;
                            } else
                            {
                                data += " " + p;
                            }
                            
                        }
                    });
                    
                    $(this).closest('.underlinable').next('input').val(data);
                });
                
            });
            $(this).disableSelection();
            
        });
        $('div.page').find('.fillable').each( function()
        {
            $div = $(this);
            /*$div.height($(this).height());*/
            var data='';
            $(this).find('input'). each( function()
            {
                $this_span = $(this);
                $this_span.unbind('change');
                $this_span.change( function() 
                {
                    $click_span = $(this);
                    var data ='';
                    $(this).closest('.fillable').find('input'). each( function()
                    {
                        var p = $(this).val().replace(" ", "");
                        if ($(this).parents('table').hasClass('math'))
                        {
                            p='`'+p+'`';//make ascii math
                        }
                        if (data.length==0)
                        {
                            data = p;
                        } else
                        {
                            data += "_:_"+p;
                        }
                    });
                    $(this).closest('.fillable').next('input').val(data);
                });
                
            });
            $(this).disableSelection();
            
        });
        $('table.math tr.input').find('td').each( function()
        {
            $(this).keyup( function(event)
            {
                var max_idx = $(this).parent('table.math tr.input').children('td').length - 1;
                var idx=$(this).index();
                if (event.keyCode==37)
                {
                    if (idx>0) idx = idx-1;
                    $(this).parent('table.math tr.input').children('td').eq(idx).find('input').focus();
                }
                else if (event.keyCode==39)
                {
                    if (idx<max_idx) idx = idx+1;
                    $(this).parent('table.math tr.input').children('td').eq(idx).find('input').focus();
                }
                else
                {
                    debouncedMathInputRender(idx,$(this).find('input').val(),$(this));
                }
            });
        });
        
        $('.math.fraction_blocks').find('input.no_submit_enter').each( function()
        {
            $(this).keyup( function(event)
            {
                debounceShowFractionBlocks($(this),event);
            });
            showFractionBlocks($(this));
        });
        initEditMode();
    }


    function reloadSpeak() 
    {
        //console.log("reloadspeak");
        $("body").find(".speak").each( function()
        {
            $(this).unbind('click');
            $(this).click( function()
            {
                if ($(this).is("input") || $(this).is("button"))
                {
                    speak($(this).val(),{ amplitude: 100, wordgap: 2, pitch: 50, speed: 175 });
                } else
                {
                 if ($(this).attr("speak_this"))
                 {
                    speak($(this).attr("speak_this"),{ amplitude: 100, wordgap: 2, pitch: 50, speed: 175 });
                 } else
                 {
                    speak($('<p></p>').html($(this).html()).text(),{ amplitude: 100, wordgap: 2, pitch: 50, speed: 175 });
                 }
                }
            });
        });
        
        $("body").find(".speak_phonic").each( function()
        {
            $(this).unbind('click');
            $(this).click( function()
            {
                var chars = '';
                var vowels = ["a", "e", "i", "o", "u"];
                var semi_consonant = ["f", "n", "m"];

                if ($(this).is("input") || $(this).is("button"))
                {
                    chars = $(this).val().trim();
                    
                } else
                {
                 if ($(this).attr("speak_this"))
                 {
                    chars = $(this).attr("speak_this").trim();
                 } else
                 {
                    chars = $('<p></p>').html($(this).html()).text().trim();
                 }
                }
                var phonics = '';
                for(var i=0;i<chars.length;i++)
                 {
                     if (vowels.includes(chars.charAt(i).toLowerCase()))
                     {
                        if (chars.charAt(i).toLowerCase()=='u') phonics=phonics+'ooh ';
                        else if (chars.charAt(i).toLowerCase()=='i') phonics=phonics+'eeh ';
                        else phonics=phonics+chars.charAt(i)+'h ';
                     } else if (semi_consonant.includes(chars.charAt(i).toLowerCase()))
                     {
                         phonics=phonics+chars.charAt(i)+'uh ';
                     }
                     else if (chars.charAt(i).toLowerCase()=='q') phonics=phonics+'quah ';
                     else phonics=phonics+chars.charAt(i)+'ah ';
                }
                speak(phonics,{ amplitude: 100, wordgap: 150, pitch: 50, speed: 290 });
                 
            });
        });
    };


    var debounceMath;
    
    function debouncedMathInputRender(index,value,me)
    {
        clearTimeout(debounceMath);  
        debounceMath = setTimeout(function()
        { 
            me.parents('table.math').find('tr.output td').eq(index).html('`'+value+'`');
            MathJax.Hub.Queue(["Typeset",MathJax.Hub,me.parents('table.math').attr('id')]);
        },1000);
        
    }
    
    function debounceShowFractionBlocks(me,ev)
    {
        clearTimeout(debounceMath);
        debounceMath = setTimeout(function()
        {
            var idx=me.parents('.math.fraction_blocks').find('input.no_submit_enter').index(me);
            var max_idx = me.parents('.math.fraction_blocks').find('input.no_submit_enter').length - 1;
            
            if (ev.keyCode==37)
            {
                if (idx>0) idx = idx-1;
                me.parents('.math.fraction_blocks').find('input.no_submit_enter').eq(idx).focus();
            }
            else if (ev.keyCode==39)
            {
                if (idx<max_idx) idx = idx+1;
                me.parents('.math.fraction_blocks').find('input.no_submit_enter').eq(idx).focus();
            }
            else
            {
                showFractionBlocks(me);
            }
             
        },1000);
    }
    function showFractionBlocks(me)
    {
        
        var div = parseInt(me.parents('.math.fraction_blocks').find('input.no_submit_enter').eq(0).val());
        var divisor = parseInt(me.parents('.math.fraction_blocks').find('input.no_submit_enter').eq(1).val());
        var num_blocks = parseInt(me.parents('.math.fraction_blocks').find('input.no_submit_enter').eq(2).val());
        
        
        var block = 0;
        if (divisor > 0)
        {
            block = div/divisor;
        }
        
        if ((div>0) && (divisor>0))
        {
            me.parents('.math.fraction_blocks').find('.each_block_fraction').html('`'+div+'/'+divisor+'=`');
        }
        
        if (block>0)
        {
            me.parents('.math.fraction_blocks').find('.each_block').html(block);
        }
        
        var html_blocks='';
        for(var i=0;i<num_blocks;i++)
        {
            html_blocks=html_blocks+'<span class="bordered">'+block+'</span>';
        }
        
        me.parents('.math.fraction_blocks').find('div.input').html(html_blocks);    
        MathJax.Hub.Queue(["Typeset",MathJax.Hub,me.parents('.math.fraction_blocks').attr('id')]);    
        
        var answer_blocks = parseInt(me.parents('.math.fraction_blocks').find('input.no_submit_enter').eq(3).val());
        
        var answer = 0;
        if (answer_blocks>0) 
        {
            answer = answer_blocks * block;
            me.parents('.math.fraction_blocks').find('.answer').html(answer);
        }
    }

<?php
    }
}?>
    
