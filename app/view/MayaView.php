<?php
/** Main view of maya framework 
 * Object contaiting view functions for main maya actions. It passes to user view when not in the scope of actions
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package MayaView 
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */
require_once 'MayaMayaView.php';
class MayaView extends MayaMayaView
{
    public function __construct(MayaController $controller)
    {   
        parent::__construct($controller);
    }
    public function generateImageUploadForm()
    {
		if ($_SESSION['user_id']>0)
        {
			$filename = $this->model->getSelectedMenu()->getPath();
			$selected = $this->model->getSelectedMenu()->getId();
			echo "<div id='upload_maya' title='Image Upload' style='display:none' >";
			echo "<form action='?command=upload_image&selected=$selected' class='round-corners' method='post' style='margin:0px;padding:0px; float:center; display:block' enctype='multipart/form-data' > ";
			echo '<input type="file" name="file" id="file" />';
			echo "<input type='hidden' name='filename' value='$filename' />";
			echo '<textarea id="upload_content" type="submit" style="display:none" name="upload_content" value=""></textarea>';
			echo '<input type="submit" name="submit" value="Submit" />';
			echo '</form>';
			echo '</div>';
			$htm ="
			<script>
				$(function() 
				{
					$( '#upload_maya' ).dialog(
					{
						autoOpen: false,
						modal: true
					});					
				});
				function showUpload()
				{
					$( '#upload_maya' ).dialog('open');
				}
				$('#file').change(function() 
				{
                    $( 'div.content' ).find('a.edit.temp').each( function()
                    {
                        $(this).remove();
                    });
					var data = $( 'div.content' ).html();
					var name = '';
					if ($('#file').val().indexOf('fakepath'))
					{
						name = $('#file').val().replace(".'"C:\\\\fakepath\\\\'.'"'.",'');
					} else name = $('#file').val();
					var filename = '".'"usr/uploads/image/'."'+name+'".'"'."';
					var final_data = '<img src='+filename+' />'+data;
					$( '#upload_content' ).val(final_data);
				});
				function hideUpload()
				{
					$( '#upload_maya' ).dialog('close');
				}
						
			</script>";
			echo $htm;
		}
	}
}
