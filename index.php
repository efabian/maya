<?php
session_start();
/** index.php 
 * Main entry point of maya framework sample code. 
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package Maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link https://maya.docph.net 
 */
ini_set('display_errors', 'On');
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING); 

require_once 'maya/Maya.php';

$maya = new Maya();
$user= new User();
$role= new Role();
$paths = new Paths();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Maya Framework</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <!--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i" rel="stylesheet">-->

  <!-- Bootstrap css -->
  <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    

  <!-- Libraries CSS Files -->
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet">
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/modal-video/css/modal-video.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <?php $maya->view->initializeCssAndJquery();?>
  
  
  <link rel="stylesheet" href="js/jquery-ui.css" /> 
 
  <!-- Essential jquery needed by the framework -->
  <script src="js/jquery-1.12.4.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script type="text/javascript" src="js/jquery.mask.js"></script>

 
</head>

<body>

  <header id="header" class="header header-hide">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="#body" class="scrollto"><img src="img/maya.png" alt="" style="height:40px;vertical-align:middle" title="" /><span>M</span>aya</a></h1>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
            <li ><a href="#" class="mx-" mx-container="div.page"  mx-click="?command=display_rest&path=home.pub.htm" ><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#" class="mx-" mx-container="div.page"  mx-click="?command=display_rest&path=advance.pub.htm"><i class="fa fa-star"></i> Advance Topic </a></li>
        <?php 
        if (!$role->isUserLoggedIn())
        {
        ?>
            <li class="dropdown"> <a data-toggle="dropdown" href="#" onclick="showLogin()" ><i class="fa fa-user-plus"></i> LOGIN</a></li>
        <?php
        } else 
        {
        ?>
            <li><a href="#" class='mx-' mx-container='div.page' mx-click="?command=display_rest&path=Admin.dir/Informations.dir/List Informations.rest.php"><i class="fa fa-database"></i> Informations</a></li>
            <li><a href="#" class='mx-' mx-container='div.page' mx-click="?command=display_rest&path=Admin.dir/Finances.dir/List Finances.rest.php"><i class="fa fa-dollar"></i> Finances</a></li>
            <li><a href="#"><i class="fa fa-user" ></i> <?php echo $_SESSION['user_name'];?></a>
                <ul>
                <li><a href="#" class='mx-' mx-container='div.page' mx-click="?command=display_rest&path=Admin.dir/Users.dir/List Users.rest.php"><i class="fa fa-group"></i> Users</a></li>
                <li><a href="#" class='mx-' mx-container='div.page' mx-click="?command=display_rest&path=Admin.dir/Tickets.dir/List Tickets.rest.php"><i class="fa fa-bug"></i> Tickets</a></li>    
                <li>
                <?php
                    if ($_SESSION['user_id_orig'] && $_SESSION['user_role_orig'])
                    {?>
                        <li>
                        <a class="mx-" mx-redirect='?command=resumeOriginalRole&object=User'><i class="fa fa-user-secret"></i> Resume</a>
                        </li>
                    <?php
                    }?>
                </li>
                <li><a href="?command=logout" class="" style=""><i class="fa fa-user-times"></i>LOGOUT</a></li>
                </ul>
            </li>
      <?php 
        }?> 
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

<?php   
   $maya->view->generateLoginAndNotice(array('login'=>array('class'=>'login_form','attributes'=>"style='display:block'"),'notice'=>array('color'=>'rgb(139, 118, 75)')));?>
  <!--==========================
    Hero Section
  ============================-->
  <!--==========================
    Features Section
  ============================-->

  <section class="<?php echo ($role->isUserLoggedIn()?'padd-section':'');?> text-center wow fadeInUp"  style="margin-top:100px">
    <div class="container page">
    <?php
    $maya->controller->display()
    ?>
    </div>
  </section>

  <!--==========================
    Footer
  ============================-->
  <footer class="footer">
    <div class="copyrights">
      <div class="container">
        <p>&copy; Copyrights Maya. All rights reserved. Author: Edgardo Fabian      Since: 2007</p>
        <div class="credits">
          Framework powered by <a href="https://maya.docph.net">Maya</a>
          Html Template by <a href="https://bootstrapmade.com/">BootstrapMade</a> 
        </div>
      </div>
    </div>

  </footer>



  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <!--<script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>-->
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/modal-video/js/modal-video.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>


  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>
<?php 
$maya->view->initializeScripts(array('mathjax'=>false,'speak'=>false,'chosen'=>true,'wysiwyg'=>true,'datetimepicker'=>true));
?>
</body>
</html>
