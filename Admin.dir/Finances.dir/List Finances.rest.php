<?php
require_once 'FinanceView.php';
$headers = ["id","description","income","expense","source","destination","expense_type","income_type","transaction_date","created"];
echo '<h3 class="heading">List Finances</h3>';
$finance_view=new FinanceView();
$finance_view->init_list();
?>
<div>
<h4>Summary</h4>
<form id="summary">
<?php $finance_view->renderCsrf();?>
<div><label>BPI Talamban:</label> <span id="bpi_talamban_balance"></span> <button type="button" class="button click_action" container="#bpi_talamban_balance" url="?command=getSummary&object=Finance&name=BPI Talamban"><i class="fa fa-update"></i> Update</button></div>
<div><label>Cash on Hand:</label> <span id="coh_balance"></span> <button type="button" class="button click_action" container="#coh_balance" url="?command=getSummary&object=Finance&name=COH"><i class="fa fa-update"></i> Update</button></div>
<div><label>Amore Card:</label> <span id="amore_balance"></span> <button type="button" class="button click_action" container="#amore_balance" url="?command=getSummary&object=Finance&name=Amore Card"><i class="fa fa-update"></i> Update</button></div>
<div><label>Emergency Money:</label> <span id="emergency_balance"></span> <button type="button" class="button click_action" container="#emergency_balance" url="?command=getSummary&object=Finance&name=Emergency Money"><i class="fa fa-update"></i> Update</button></div>
</form>
</div>
<?php
include __DIR__.'/../../usr/view/finances/list_finances.rest.php';
?>
