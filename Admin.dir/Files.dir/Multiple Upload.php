<?php
ini_set('display_errors', 'On');
//error_reporting(E_ALL ^ E_WARNING); 
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
require_once 'User.php';
require_once 'Role.php'; 
if (!isset($_SESSION['upload_directory']))
{
    $_SESSION['upload_directory']=getcwd();
}
if (!isset($_SESSION['upload_overwrite']))
{
    $_SESSION['upload_overwrite']=0;
}
$checked='';
if ($_SESSION['upload_overwrite']) $checked='checked="true"';
if (!isset($container)) $container='div.page';
if (!isset($allowed)) $allowed=array("admin");
$role = new Role();
if (!$role->isUserAnyOf($allowed))
{

	$role->showUnauthorizedMessage($allowed,"?command=display_php&base_path=usr/view&path=List Informations.rest.php");
	exit();
}
else
{?>
<link href="lib/jquery-upload-file-master/css/uploadfile.css" rel="stylesheet">
<script src="lib/jquery-upload-file-master/js/jquery.uploadfile.min.js"></script>
<div id="fileuploader_path" style="margin-bottom:20px">
</div>
<div style="margin-top:20px"><label><input id="overwrite" type="checkbox" name="overwrite" <?php echo $checked;?> />Overwrite</label></div>
<div id="fileuploader">Upload</div>
<script>
function showDirs(fpath)
{
    var form_data = {file_path: fpath};
    var url = '?command=listDirsRest&object=File';
    $.post(encodeURI(url),form_data,function(data)
    {
        if (data.success)
        {
            var dirs = data.dirs;
            var i = 0;
            $('#base_path_p').remove();
            $('#fileuploader_path').append($('<p id="base_path_p"><i class="fa fa-folder-open"></i><span id="base_path">'+data.path+'</span></p>'));
            $('#fileuploader_path').find('div.file_path_div').remove();
            for (var name in dirs)
            {
               var finput = $('<div class="file_path_div"><label><input type="radio" class="file_path" name="file_path" value="'+dirs[name]+'" /><i class="fa fa-folder"></i> '+name+'</label></div>');
               if (name=='..')
               {
                   finput = $('<div class="file_path_div"><label><input type="radio" class="file_path" name="file_path" value="'+dirs[name]+'" /><i class="fa fa-arrow-up"></i> '+name+'</label></div>');
               }
               $('#fileuploader_path').append(finput);
            }
            linkToForm();
        } else
        {
            alert(data.message);
        }
        
    });
}
function linkToForm()
{
    $('#fileuploader_path').find('input.file_path').each( function()
    {
        $(this).click( function()
        {
            var full_path = $(this).val();
            showDirs(full_path);
        });
    });
}
$(document).ready(function() 
{
	$("#fileuploader").uploadFile({
		url:"lib/jquery-upload-file-master/php/upload.php",
		fileName:"myfile",
        showStatusAfterError: true,
        showError:true,
        multiple:true,
        returnType:'json'
	});

    showDirs('<?php echo $_SESSION["upload_directory"];?>');    

    $('#overwrite').click( function()
    {
        var form_data = {overwrite: 0};
        if ($(this).is(":checked"))
        {
            form_data = {overwrite: 1};
        }
        var url = '?command=overwriteSetRest&object=File';
        $.post(encodeURI(url),form_data,function(data)
        {
            if (data.success)
            {
                alert(data.message);
            }
            
        });
    });

});
</script>
<?php
}?>
