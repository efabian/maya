<?php
require_once 'TicketView.php';
$readonly_status=TRUE;
$readonly_user_id_submitter=TRUE;
$ticket_view = new TicketView();
$ticket_view->init_new();
$ticket_view->ticket->setStatus('active');
$ticket_view->ticket->setUserIdSubmitter($_SESSION['user_id']);
$allowed=array('admin','doctor','secretary');
echo '<h3 class="heading">New Ticket</h3>';
$description_attributes=array("class"=>"jwysiwyg","placeholder"=>"Description ");
include __DIR__.'/../../usr/view/tickets/new_ticket.rest.php';


