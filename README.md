### Develop Websites in 3 Easy Steps ###
1) Design your database acccording to maya db rules.<br>
2) Generate automatically the model, view, controllers.<br>
3) Modify the generated codes to fit your purpose.<br>

See the maya api documentation here https://maya.docph.net/
See the maya sample code running here https://maya.docph.net/
For inquiries, please email me at edgardo.fabian@gmail.com. I will try (with my busy schedule) with the best I can to answer your inquiries.

### Installation ###

note: This assumes that you have basic knowledge of installing web servers with php support such as apache,nginx,lighttpd etc. Therefore the installation assumes you have a mysql db and php support already installed.<br>
1) Download or clone the files from https://gitlab.com/efabian/maya.git and make sure you put it in your server<br>
2) You may rename the 'maya' folder to your desired application directory.<br>
3) Open the file config/paths.cfg.php and change to $this->app_path to your application path relative to your system root folder. Please note that system root folder not www document root folder.<br>
4) Open the file config/database.cfg.php and change the database host $this->host = "localhost", database name $this->name = "[db name]", user name for db $this->user = "root" and password for db $this->password = "[db password]";<br>
5) If needed you may change the default roles needed by your users at app/model/Role.php by overriding the default roles of user and admin.<br>
6) If needed you may also change the extensions or files that will be ignored by the auto menu generation of maya at app/model/MayaModel.php using the $this->file_filters="".<br>
Now you are ready to design the db tables<br>
7) If you want to try the sample db data, first fill the $db->host, $db->name, $db->user, $db->password at config/database.cfg.php first with the right data. Then go to util directory and execute<br>
php init_sample_db.php
or visit through the site [site url]/util/init_sample_db.php

The above generates a sample user with email: maya.admin@gmail.com and password:maya


### Step 1. Design Database ###

#### Sample users table ####
<pre>
field	        type	        description
id	            unsigned int	Primary key
firstname	    varchar	        Firstname of the user
lastname	    varchar	        Lastname of the user
role	        int 	        Integer values corresponding to certain user roles tied to their rights.
salt	        varchar	        Salt in hashing the password
password	    varchar	        Hashed password
created	        timestamp	    Defaults to timestamp as db insertion
</pre>
#### Sample subjects table linked to users ####
<pre>
field	        type	        description
id	            unsigned int	Primary key
name	        varchar	        Name of this subject
description	    varchar	        Description of this subject
user_id_teacher	unsigned int	This is a foreign key from the 'users' table, which will be named/labeled 'teacher' in tables and inputs
status	        unsigned int	active=1 or not_active=0
created	        timestamp	    Defaults to timestamp as db insertion
</pre>
Sample enrolees table linked to users and subjects
<pre>
field	            type	        description
id	                unsigned int	Primary key
subject_id	        unsigned int	This is a foreign key pointing to the subjects table
users_ids_enrollees	unsigned int	An array of enrollee id pointing to the users table
created	            timestamp	    Defaults to timestamp as db insertion
</pre>

#### Rules in db creation. ####

1) All names of database table names must be in small letters and plural form. For example users, billboards, subjects etc. This is because the model created by the generateModel.php utility will be singular.

2) Foreign key relation shall be linked according to the name of the field for example 'user_id_teacher' means an id that points to the a user in the 'users' which is named as 'teacher'. Setting of foreign key relation in db is not required.

3) Foreign key relation n to n relation shall be linked according to the name of the field but plural form such as 'users_ids_enrollees'. This means any number of users id (array) that points to users in 'users' table that should be named 'enrollees' in tables and forms.

4) All tables should have an 'id' as a primary key which must be an unsigned integer.

5) All fields must be in small letters like id, name, description etc.

### Step 2. Generate Model, View, Controllers ###

#### Generate Model ####

1) Open your linux terminal

2) Got the ../util folder

3) run 'php generateModel.php (table name) [link_image]

For example 'php generateModel.php users link_image', if the users table exist, this command will generate three files namely ../usr_generated/model/UserProtected.php, ../usr_generated/model/UserPublic.php and ../usr/model/User.php . The ../user/model/User.php includes a User class that will extend the UserProtected class at ../usr_generated/model/UserProtected.php/. The UserProtected class will contain all the get and set methods to access the db fields.

#### Generate Controllers ####

1) Open your linux terminal

2) Got the ../util folder

3) run 'php generateController.php (table name) [link_image] [login_register]

For example 'php generateController.php users link_image login_register', if the users table exist, this command will generate two files namely ../usr_generated/controller/MayaUsersController.php, and ../usr/controller/UsersController.php . The ../usr/controllers/UsersController.php includes a UsersController class that will extend the MayaUsersController class at ../usr_generated/controller/MayaUsersController.php/. The UsersController will no longer be generated if it already existed to prevent ovewriting user codes.

link_image is an optional parameter to include uploadImageAction()

login_register is an optional parameter to include loginRestAction and registerRestAction which are only applicable for users table

#### Generate View ####

1) Open your linux terminal

2) Got the ../util folder

3) run 'php generateView.php (table name) [link_image] [login_register]

For example 'php generateView.php users link_image login_register', if the users table exist, this command will generate three files namely ../usr/view/List Users.rest.php, ../usr/view/New User.rest.php and ../usr/view/Edit User.rest.php . Please note that this will overwrite whatever views previously modified. In case you overwrite by accident a backup is generated at ../usr/view_bak/.

link_image is an optional parameter to include uploadImageAction() view support

login_register is an optional parameter to include login and register view support


### Step 3. Modify generated code to fit your needs ###

#### Folder Structure ####

    index.php (edit) Main structure of each page
    home.php (edit) Content of home page
    config (edit) Contains configuration files
        paths.cfg.php
        database.cfg.php
        security.cfg.php
    app (edit) MVCs that extends the core maya MVCs
        model
            MayaModel.php
            MenuElement.php
            Role.php
        view
        controller
        Maya.php
    usr (edit) MVCs that extends the core generated models from tables
        model
        view
        controller
        view_bak
        uploads (files uploaded by the MVC above will be placed here)
    Admin.dir (edit) Admin's List, View, Edit interface for all tables
        Users.dir
            List Users.rest.php - Mostly a sym link to usr/view/List Users.rest.php
            Edit User.hid.rest.php - Mostly a sym link to usr/view/Edit Users.rest.php
            New User.rest.php - Mostly a sym link to usr/view/New User.rest.php
        [Other Tables]
    css (edit)
    js (edit) For jquery and javascript plugins
    lib (edit) For php plugins
    images (edit) Images needed by developer for website.
    img (edit) Images default to maya
    usr_generated (do not edit) Generated MVC from tables
        model
        controller
        model_bak
        controller_bak
    maya (do not edit) Core MVC of maya
        model
        view
        controller
    util (do not edit) Utilities for generating MVCs from tables

#### Rules which files should be edited ####

1) Files from index.php down to the images are the files that can be edited

2) Foldes from img,usr_generated,maya and util should not be edited

3) usr_generated are files automatically generated by the utilities which will be discussed later.

#### Modify Models ####

1) You should only modify the extended versions of models at usr/model folder. This is to insure that when you change your db and update your models the changes you added will not be overwritten by generaModel.php. The generateModel.php will only overwrite the models at usr_generate/model folders

2) You may override functions from the usr_generated/model folder

3) You should add user functions only to the models at usr/model folder.

#### Modify Controllers ####

1) You should only modify the extended versions of controllers at usr/controller folder. This is to insure that when you change your db and update your controllers the changes you added will not be overwritten by generaController.php. The generateController.php will only overwrite the controllers at usr_generate/model folders

2) You may override functions from the usr_generated/controller folder

3) You should add user controller functions only to the controllers at usr/controller folder.

#### Modify Views ####

1) Views (List, Edit, New) are automatically generated at usr/view folder by generateView.php.

2) If you want to put a page that is exactly the same as the 'List Users.rest.php' in the Admin.dir/Users.dir/ directory then it is recommended to generate a relative symbolic link.

ln -s '../../usr/view/List Users.rest.php' 'List Users.rest.php'

The above is terminal command in linux that assumes the folder structure defined above executed at ../Admin.dir/Users.dir/ directory

3) If you want to put a page that is similar to for example 'New User.rest.php' that is for example named 'Register User.rest.php', then you must copy ../usr/view/New User.rest.php and modify the contents to fit your desired page. 
