<script>
    <?php
    if (!isset($this->container)) $this->container = 'div.page';
    if (!isset($this->form_list)) $this->form_list = 'form_list';
    ?>
    function paginate(page) 
    {
        var form = $("#<?php echo $this->form_list;?>");
        
        var target = "<?php echo $this->container;?>";
        
        var url_submit = form.attr('action');        
   
        if (url_submit.indexOf('command=display_php')>=0)
        {
            url = url_submit.replace('command=display_php','command=display_rest');
        } else 
        {
			url = url_submit;
		}
        
        form.find('input.field_page').val(page);
        if (typeof loadPost == 'function') 
        { 
            loadPost(url,form,target); 
        } else
        {
            form.attr('action',url_submit);
            form.submit();
        }
        
    };
    $(function() 
    {

    });
</script>
