<?php
/** Object for program file identification of maya framework 
 * Object contaiting functions to segreggate the information of files contained within the maya framework website
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package FileProperties
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */
require_once 'MenuElement.php';
require_once 'Role.php';

class MayaFileProperties
{
    protected $name;
    protected $extension;
    protected $order;
    protected $role_required;
    protected $hidden;
    protected $rest;
    protected $link;
    
    public function __construct($path)
    {
        $this->link=false;
        if (strpos($path,'/')>0)
		{ 
			$filenames = explode('/',$path);
			$num = count($filenames)-1;
            
			$file = $filenames[$num];
		} else $file = $path;
        
        $ext_pos = strrpos($file,".")+1;
		if ($ext_pos<strlen($file))
		{
			$this->extension =strtolower(substr($file,$ext_pos));
		} else 
        {
            $this->extension = '';
        }
        
        $segments = explode('.',$file);
        $this->hidden = false;
        $this->rest = false;
        if (count($segments)>0)
        {
            if (!(strpos($file,'www.')===FALSE))
            {
                $this->name = $file;
                $this->link = true;
            } else if (!(strpos($file,'http.')===FALSE))
            {
                $this->name = str_replace('http.','',$file);
                $this->link = true;
            } else if (!(strpos($file,'https.')===FALSE))
            {
                $this->name = str_replace('https.','',$file);
                $this->link = true;
            } else
            {
                $this->name = $segments[0];
            }
            $this->name = str_replace('__','/',$this->name);
            
            $this->name=preg_replace('/^_[0-9][0-9]/','',$this->name);
            
            for($i=1;$i<count($segments);$i++)
            {
                $str = $segments[$i];
                if ($i==1)
                {
                    if (is_numeric($str))
                    {
                        $this->order =$str;
                    }
                }
                $start=strpos($str,'(');
                $end=strpos($str,')');
                if (!($start===false) && ($end>$start))
                {
					$roles = explode(',',substr($str,$start+1,$end-$start-1));
					if ($roles[0])
					{
						$role = new Role();
						$role_required = $role->getValueFromArrayNames($roles);
						$this->role_required = $role_required;
					}
                } else if (strpos($str,'hid')===0)
                {
                    $this->hidden = true;
                } else if (strpos($str,'rest')===0)
                {
                    $this->rest = true;
                } else if (strpos($this->name,'admin')===0)
                {
                    $role = new Role();
                    $role_required = $role->getValueFromArrayNames(array('admin'));
                    $this->role_required = $role_required;
				}
                if ($i==(count($segments)-1))
                {
                    $this->extension = $str;
                }
            }
        } else
        {
            $this->name=$file;
            $this->extension='';
            $this->defined_page='';
            $this->role_required='';
        }
                
    }

	public function getExtension()
	{
		return $this->extension;
	}	
    
    public function getRoleRequired()
	{
		return $this->role_required;
	}	

	public function getOrder()
	{
		return $this->order;
	}	
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getHidden()
    {
        return $this->hidden;
    }   
    
    public function getRest()
    {
        return $this->rest;
    }    
    
    public function isDisplayDir($file)
	{
		if (strpos(strtolower($file),'.dir'))
		{
			return true;
		} else if (strpos(strtolower($file),'.pvt'))
		{
			return true;
		} else if (strpos(strtolower($file),'.maya'))
		{
			return true;
		} else if (strpos(strtolower($file),'.page'))
		{
			return true;
		} else
		{
			return false;
		}
	}
    
    public function isLink()
    {
        return $this->link;
    }
    
	public function getLinkPath($filename)
	{
        $file = str_replace('__','/',$filename);
		
		if (!(strpos($file,'www.')===false))
		{ 
			return "http://".$file;
		} else if (!(strpos($file,'http.')===false))
		{ 
			return str_replace('http.',"http://",$file);
		} else if (!(strpos($file,'https.')===false))
		{ 
			return str_replace('https.',"https://",$file);
		} 
	}    
}
