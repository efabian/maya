<?php
/** Main model of maya framework 
 * Object contaiting model objects for main maya framework. 
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package Maya
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */
require_once 'MayaModel.php';
require_once 'MenuElement.php';
require_once 'EmailsController.php';
require_once 'FilesController.php';
require_once 'User.php';
require_once 'generate.php';
require_once 'MayaSecurity.php';
require_once 'UsersController.php';

/*foreach (glob(__DIR__."/../../usr/controller/*.php") as $filename)
{
    require_once $filename;
}*/
class MayaNest
{
    public $user;
    public $usersController;
    protected $data;
    public $model;
    protected $security;
    
    public function __construct($model)
    {
        $this->user = new User();
        $this->usersController = new UsersController();
        $this->model = $model;
        $this->security = new MayaSecurity();
    }
    
    public function receive($command)
    {
        $paths=new Paths();
        if ($command === 'login')
        {
			if (!$this->security->checkLoggedOutCsrf())
            {
                $_SESSION['maya_notice']='Security Error! Please submit only through this site!';
                header("Location: ?clear=zzz");
                exit();
            }
            $user = $this->usersController->loginAction('email');  
            if ($user) 
            {
                if (array_key_exists('target_url',$_REQUEST) && $_REQUEST['target_url'])
                {
                    $this->model->setPath($_REQUEST['target_url']);
                } else
                {
                    $this->model->setPath($paths->home);
                }
                $this->model->setCommand('display_php');
            }
        } else if ($command === 'forgotPasswordSendLink')
        {
            if (!$this->security->checkLoggedOutCsrf())
            {
                $_SESSION['maya_notice']='Security Error! Please submit only through this site!';
                header("Location: ?clear=zzz");
                exit();
            }
            $user = $this->usersController->forgotPasswordSendLinkAction();            
        } else if ($command === 'testDrive')
        {
            if ((!$this->security->checkLoggedOutCsrf()) || (!defined('ENABLE_TEST_DRIVE') || (!ENABLE_TEST_DRIVE)))
            {
                if (array_key_exists('HTTP_X_REQUESTED_WITH',$_SERVER) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest')
                {
                    $response = array('message'=>"Security Error! Please submit only through this site!",'success'=>false);
                    header('Content-Type: application/json');
                    echo json_encode($response);
                    exit();
                }
                else
                {
                    $_SESSION['maya_notice']='Security Error! Please submit only through this site!';
                    header("Location:?command=home");
                    exit();
                }
            }
            if (defined('ENABLE_TEST_DRIVE') && ENABLE_TEST_DRIVE)
            {
                $user = $this->usersController->testDriveAction();            
            }
        }  else if ($command === 'register')
        {
            if (!$this->security->checkLoggedOutCsrf())
            {
                $_SESSION['maya_notice']='Security Error! Please submit only through this site!';
                header("Location: ?clear=zzz");
                exit();
            }
            $user = $this->usersController->registerAction($_POST['confirm_password']);            
        } else if ($command === 'logout')
        {
            $this->usersController->logoutAction();           
        } else if ($_REQUEST['object'])
        {
            $objectName = $_REQUEST['object'];
            $objectController = generateTableNameFromElement($objectName).'Controller';
            require_once $objectController.'.php';
            
            if (class_exists($objectController))
            {
                $object = new  $objectController();
            
                if (isset($_REQUEST['EXTERNAL_API']) && ($_REQUEST['EXTERNAL_API']))
                {
                    $commandAction =  $command.'ExternalApi';
                }
                else
                {
                    if (!$this->security->checkCsrf())
                    {
                        if (array_key_exists('HTTP_X_REQUESTED_WITH',$_SERVER) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest')
                        {
                            $response = array('message'=>"Security Error! Please submit only through this site!",'success'=>false);
                            header('Content-Type: application/json');
                            echo json_encode($response);
                            exit();
                        }
                        else
                        {
                            $_SESSION['maya_notice']='Security Error! Please submit only through this site!';
                            header("Location:?command=home");
                            exit();
                        }
                    }
                    $commandAction = $_REQUEST['command'].'Action';
                }
                if (method_exists($object,$commandAction))
                {
                    $ret = $object->$commandAction();
                    if ($ret)
                    {
                        $this->model->setCommand('get');
                        $this->model->setPath($ret);
                    }
                } else
                {
                   $response = array('message'=>"Method Unsupported!",'success'=>false);
                   header('Content-Type: application/json');
                   echo json_encode($response);
                   exit();
                }

            } else
            {
               $response = array('message'=>"Object does not exist!",'success'=>false);
               header('Content-Type: application/json');
               echo json_encode($response);
               exit();
            }
        }
        return $this->model;
    }

    public function process($model)
    {
		
    }
    
    public function getData()
    {
        return $this->data;
    }
}
