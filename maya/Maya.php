<?php
/** Main object maya framework 
 * Object instantiating the main model,view, and controller functions for maya framework
 * 
 * Copyright (C) 20014-2020 Edgardo Fabian <edgardo.fabian@gahum.com> 
 * LICENSE: This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 3 
 * of the License, or (at your option) any later version. 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://opensource.org/licenses/gpl-license.php>;. 
 * @package Maya 
 * @author Edgardo Fabian <edgardo.fabian@gmail.com> 
 * @link http://www.gahum.com/maya 
 */
require_once __DIR__.'/../config/paths.cfg.php';
$paths = new Paths();
set_include_path($paths->includes);
require_once 'MayaModel.php';
require_once 'MayaView.php';
require_once 'MayaController.php';
require_once 'User.php';
require_once 'Role.php';

class Maya
{
    public $model;
    public $view;
    public $controller;
    
    public function __construct($first_word_last_level=FALSE,$enable_external_links=TRUE,$include_menu=array(),$icons=array(),$additional_menus=array(),$enable_site_map=TRUE,$container='div.page',$enable_login=TRUE)
    {
        session_start();
        
        $this->model = new MayaModel();
        $this->controller = new MayaController($this->model);
        $this->controller->receive();
        $this->controller->prepareMenu($include_menu,$enable_external_links);
        
        //die(json_encode($this->controller->model->getMenus()));
        
        if (count($additional_menus)>0)
        {
            foreach($additional_menus as $amenu)
            {
                $this->controller->model->addMenu($amenu);
            }
        }
        if (count($icons)>0)
        {
            $this->controller->model->addIcons($icons);
        }
        
        if ($enable_login)
        {
            $login_menu = new MenuElement('Login','');
            $login_menu->setAction('#');
            $login_menu->attributes='onClick="showLogin()"';
            $login_menu->icon='<i class="fa fa-sign-in"></i> ';
            $login_menu->id='login_button';
            $login_menu->url='#';
            $this->controller->model->addMenu($login_menu);
        }
        
        $this->view  = new MayaView($this->controller); 

        $this->view->enable_login=$enable_login;       
        
        if ($enable_site_map)
        {
            $menu_level=1;
            $main_menu=TRUE;
            $this->view->createSiteMapFromMenu(array(),$main_menu,$menu_level,$enable_external_links,$include_menu,$container,$first_word_last_level); 
        }
        
        $this->view->prepareAdminMenu($this->controller->model->getMenus());
        
        $this->model = $this->controller->getModel(); 
        
    }

}

