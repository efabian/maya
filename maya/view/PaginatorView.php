<?php
if ($this->pages>1)
{
?>
<div align='center' class='paginator' style='<?php echo $visibility; ?>' >
<input class='page_first first ' type='button' value='1' name='first' onclick='paginate(1)' align='center'></input> 
<?php
if ($this->page>2)
{?>
    <input type='button'  value='<?php echo ($this->page-1);?>' name='prev_page' class='left ' onclick='paginate(<?php echo ($this->page-1);?>)' align='center'></input>
<?php
} else
{?>
    <input type='button'  value='' name='page' class='left '  align='center'></input>
<?php
}?>
<input class='no_hover_effect center' type='button' disabled class='' value='<?php echo $this->page;?>'></input>
<?php
if ($this->page<$this->pages-1)
{?>
    <input class='page_next right' type='button' name='next_page' align='center' value='<?php echo ($this->page+1);?>' onclick='paginate(<?php echo ($this->page+1);?>)' ></input>
<?php
} else
{?>
    <input class='page_next right' type='button' name='page' align='center' value=''  ></input>
<?php
}?>    
<input type='button' class='page_last last ' value='<?php echo $this->pages;?>'  name='last_page' onclick='paginate(<?php echo $this->pages;?>)' align='center'></input>
<input type='hidden' page='1' name="current_page" />
</div>
<?php
}
?>
