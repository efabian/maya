<?php 
require_once 'Javascript.php';
require_once 'MayaSecurity.php';
$js = new Javascript();
?>
<form id="mx-hidden-form">
<?php
$security=new MayaSecurity();
echo '<input type="hidden" name="'.$security->getCsrfName().'" value="'.$security->getCsrfValue().'"/>';
?>
</form>
<script type="text/javascript">
    var ajax_supported;
    
    var loader_img='<?php ($js->renderAjaxloader());?>';
    
    if (loader_img.trim().length==0) //use default if not provided
    {
        loader_img='<svg class="ajax_loader" width="32" height="32" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet" viewBox="0 0 32 32"><ellipse cx="16" cy="16" rx="14" ry="14" stroke="lightblue" stroke-width="2" fill="none"  /><line x1="16" y1="16" x2="16" y2="4" stroke="lightblue" stroke-width="2" /><animateTransform attributeType="xml" attributeName="transform" type="rotate"  from="0 0 0"   to="360 0 0"  dur="4s"  repeatCount="indefinite"/> </svg>';
    }
    

    $( function()
    {
        if (window.XMLHttpRequest) 
        {
            ajax_supported = true;
        } else 
        {
            ajax_supported = false; 
        }
    });
    
    var insert_id = 0;

    
    function capitalFirst(str)
    {
		return str.substring(0,1).toUpperCase()+str.substring(1);
	}
    
    function checkRequired(form_selector)
    {
		var msg='';
		$(form_selector).find('input.mx-required').each(function()
		{
            var ms = capitalFirst($(this).attr('name'))+" is required. \r\n";;
            if ($(this)[0].hasAttribute('mx-required_name'))
            {
                ms = replaceDynamicVariables($(this).attr('mx-required_name'));
            }
			if ($(this).val()=='') msg=msg+ms;    
		});
		$(form_selector).find('select.mx-required').each(function()
		{
            var ms = capitalFirst($(this).attr('name'))+" is required. \r\n";;
            if ($(this)[0].hasAttribute('mx-required_name'))
            {
                ms = replaceDynamicVariables($(this).attr('mx-required_name'));
            }
			if ($(this).val()=='') msg=msg+ms;  
		});		
		$(form_selector).find('textarea.mx-required').each(function()
		{
            var ms = capitalFirst($(this).attr('name'))+" is required. \r\n";;
            if ($(this)[0].hasAttribute('mx-required_name'))
            {
                ms = replaceDynamicVariables($(this).attr('mx-required_name'));
            }
			if ($(this).val()=='') msg=msg+ms;  
		});		
        $(form_selector).find('input.email').each( function()
        {
            if (checkEmail($(this).val())) {  ;} else {msg=msg+$(this).attr('name')+" is invalid email. \r\n";}
        });
		return msg;
	}

    function checkEmail(email)
    {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,6})+$/;
        return regex.test(email);
    }
    
    function replaceDynamicVariables(text)
    {
        var txt = text;
        while (txt.indexOf("{{") >= 0)
        { 
            var st=txt.indexOf("{{");
            var en=txt.indexOf("}}");
            var dyn = txt.substring(st+2, en).split(";");
            var val='';
            if (dyn[1]=='html')
            {
                val = $(dyn[0]).html();
                if (val == undefined) alert('Undefined $('+dyn[0]+'html()!');
            }
            else if (dyn[1]=='value')
            {
                val = $(dyn[0]).val();
                if (val == undefined) alert('Undefined $('+dyn[0]+'val()!');
            }
            else if (dyn[1]=='text')
            {
                val = $(dyn[0]).text();
                if (val == undefined) alert('Undefined $('+dyn[0]+'text()!');
            }
            else 
            {
                val = $(dyn[0]).attr(dyn[1]);
                if (val == undefined) alert('Undefined $('+dyn[0]+'attr('+dyn[1]+')!');
            }
            
            var left = txt.substring(0,st);
            var right = txt.substring(en+2);
            txt = left+val+right;
        }
        
        return txt;
    }
    
    function loadActions(selector)
    {
        if (selector.length==0) selector='body';
        $(selector).find('.mx-').each( function()
        {
            if ($(this).length)
            {
                $(this).off("click");
                
                $(this).click( function()
                {
                    var form_selector = '';
                    if ($(this)[0].hasAttribute('mx-form'))
                    {
                        if ($(this).attr('mx-form').length >0)
                        {
                            form_selector = $(this).attr('mx-form');
                        } 
                    } 
                    if (form_selector.length==0)
                    {
                        if ($(this).closest('form').length>0)
                        {
                            if ($(this).closest('form')[0].hasAttribute('id'))
                            {
                                form_selector = '#'+$(this).closest('form').attr('id');
                            }
                        } 
                    }
                    if (form_selector.length==0)
                    {
                        form_selector = "#mx-hidden-form";
                    }
                    
                    
                    if ($(this)[0].hasAttribute('mx-no_required'))
                    {
                    }
                    else
                    {
                        var msg='';
                        msg=checkRequired(form_selector);
                        
                        if (msg.length>0)
                        {
                            alert(msg);
                            return false;
                        }
                    }
                    
                    
                    if ($(this)[0].hasAttribute('mx-confirm'))
                    {
                        var conf = 'Are you sure?';
                        if ($(this).attr('mx-confirm').length >0)
                        {
                            conf = replaceDynamicVariables($(this).attr('mx-confirm')); 
                        }
                        if (!confirm(conf))
                        {
                            return false;
                        }
                    }
                    
                    if ($(this)[0].hasAttribute('mx-redirect'))
                    {
                        window.location.href=$(this).attr('mx-redirect');
                    }
                    
                    if ($(this)[0].hasAttribute('mx-new_window'))
                    {
                        window.open($(this).attr('mx-new_window'));
                    }
                    
                    var url = $(this).attr('mx-click');
                    var container = '';
                    if ($(this)[0].hasAttribute('mx-container'))
                    {
						container=$(this).attr('mx-container');
					}
                   
                    
                    if (ajax_supported)
                    {
                        var success={url:'',container:container,clear:0,next_silent:0,confirm:''};
                        if ($(this)[0].hasAttribute('mx-next'))
                        {
                            if ($(this)[0].hasAttribute('mx-next_confirm'))
                            {
                                if ($(this).attr('mx-next_confirm').length>0)
                                {
                                    success.confirm = $(this).attr('mx-next_confirm');
                                }
                            }

                            var next_url = $(this).attr('mx-next');
                            if (typeof next_url !== typeof undefined && next_url !== false) 
                            {
                                success.url=next_url;
                                var next_container = $(this).attr('mx-next_container');
                                if (typeof next_container !== typeof undefined && next_container !== false) 
                                {
                                    success.container = next_container;						
                                }
                                else
                                {
                                    success.container = container;
                                }
                            }
                            
                            if ($(this)[0].hasAttribute('mx-next_form'))
                            {
                                if ($(this).attr('mx-next_form').length>0)
                                {
                                    success.form = $(this).attr('mx-next_form');
                                }
                                else success.form = form_selector;
                            }
                            else success.form = form_selector;
                        }
                        if ($(this).hasClass('mx-clear_after'))
                        {
                            success.clear=1;
                        }
                        if ($(this).hasClass('mx-next_silent'))
                        {
                            success.silent=1;
                        }
                        loadToSelector(url,container,form_selector,success,loader_img);
                    } else
                    {
                        $(form_selector).attr('action',url);
                        $(form_selector).attr('method','POST');
                        $(form_selector).submit();
                    }
                    return false;
                });
            }
        });        
    }
       
	function loadToSelector(url,selector,form_selector,success_action,loader_img)
	{
        var form_data;
        
		if (!$(form_selector).length)
		{
			form_selector = "#mx-hidden-form";
		}
		if ($(form_selector).length)
        {
			form_data = $(form_selector).serialize();
		}
		
		if (selector.trim().length>0)
		{
			form_data = form_data+'&mx-container='+selector;
		}
		loadToSelector_form_data(url,selector,form_data,success_action,loader_img);
	}	
   
	function loadToSelector_form_data(url,selector,form_data,success_action,loader_img)
	{
        if (selector.trim().length>0)
        {
            $(selector).append(loader_img);
        }
        else
        {
			$('body').append(loader_img);
		}
            
		$.post(encodeURI(url),form_data,function(data)
		{
			var json=1;
			var json_data;
			
			if(typeof data =='object')
			{
				json_data = data;
			}
			else
			{
				try 
				{
					json_data = JSON.parse(data);
				} catch (e) 
				{
					json=0;
				}
			}
			if (json)
			{
				// It is JSON
				if (json_data.success)
				{
					if (json_data.hasOwnProperty('next'))
					{
						if (typeof json_data.next =='object')
						{
							success_action = json_data.next;
						}
					}
					
					if (json_data.hasOwnProperty('html'))
					{
						if (json_data.html.length > 0)
						{
							if (selector.trim().length>0)
							{
								$(selector).html(json_data.html);
							}
						}
					}
					
					if (json_data.hasOwnProperty('message'))
					{
						if (json_data.message.length > 0)
						{
							alert(json_data.message);
						}
					}
					
					if ((typeof success_action =='object') && (success_action.url.trim().length>0))
					{
						perform_next(success_action,selector,json_data.message);
					}
				} else
				{
					if (json_data.hasOwnProperty('message'))
					{
						if (json_data.message.length > 0)
						{
							alert(json_data.message);
						}
					}
				}  
				var container = 'body';
				if (selector.trim().length>0) container=selector;
				 
				$(container).find('.ajax_loader').each( function()
				{
					$(this).remove();
				});
			}
			else
			{
				if (selector.trim().length>0)
				{
					$(selector).html(data);
					reloadJs(selector);
				}
			}
			
		});
   }
   
   <?php
   $js->renderFunctions();
   ?>
   
   function reloadJs(selector)
   {
        loadActions(selector);
        <?php
        $js->addFunctions();
        ?>
   }

    
    function successCallback(data)
    {
        alert('Success!');
    }


    
    function perform_next(success_action,selector,message,loader_img)
    {
        if (success_action.clear)
        {
            if (success_action.form.length>0)
            {
                $(success_action.form).find('input').each( function()
                {
                    if ($(this).attr('type')!='hidden')
                    {
                        $(this).attr('value','');
                        $(this).val('');
                    }
                });
                $(success_action.form).find('select').each( function()
                {
                    $(this).find('option').each( function()
                    {
                        $(this).removeAttr('selected');
                    });
                    $(this).val('');
                });
                $(success_action.form).find('textarea').each( function()
                {
                    $(this).html('');
                    $(this).val('');
                });
            }
        }
        if (success_action.url.length>0)
        {
            if (success_action.confirm.length>0)
            {
                var msg;
                if (message.length) msg = message+'. '+success_action.confirm;
                else msg=success_action.confirm;
                if (!confirm(msg))
                {
                    $(selector).find('.ajax_loader').each( function()
                    {
                        $(this).remove();
                    });
                    return false;
                }
            }
            
            

            var url = success_action.url;
            
            loadToSelector(url,success_action.container,success_action.form,'',loader_img);
        }        
    }
    
    
    function loadJsonMessageToSelector(url,form,selector,callback) 
    {
        $.post(encodeURI(url),$(form).serialize(),function(data)
        {
			if (data.success)
			{
				$(selector).html(data.message);
			}
            var param='';
            if(data.hasOwnProperty('callback_param'))
            {
                param = data.callback_param;
            }
			callback(param);
        });
        return false;   
    }    
    
    function center(div)
    {
        var view_width = $(window).width();
        left = ($(window).width() - div.width())/2;
        div.css('left',left);
    }
    

    $(document).ready(function() 
    {
        if (typeof jQuery != "undefined") 
        {
        } else
        {
            //alert("jQuery library is not found!");
        }        
        var containers = <?php echo json_encode($_SESSION[PROJECT.'_containers']);?>;
        for (var container in containers) 
        {
            if ((container.length>0)  && (containers[container].hasOwnProperty('url')) && (containers[container].url.length>0))
            {
				if ($(container).length)
				{
					loadToSelector_form_data(containers[container].url,container,containers[container].data,'',loader_img);
				}
            }
        }
        reloadJs('body');
    });    
</script>
