<?php
date_default_timezone_set('Asia/Manila');

//ini_set('display_errors', 'Off');
ini_set('display_errors', 'On');
//error_reporting(E_ALL ^ E_WARNING); 
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING); 
//error_reporting(E_ALL);
require_once __DIR__.'/config/paths.cfg.php';
$paths = new Paths();
set_include_path($paths->includes);

require_once __DIR__.'/config/database.cfg.php';
require_once __DIR__.'/config/security.cfg.php';

if ((array_key_exists("REMOTE_ADDR",$_SERVER) && $_SERVER["REMOTE_ADDR"]))
{
    echo "<p>Cannot execute from web browser</p>";
}
else if ((array_key_exists('argv',$_SERVER)) && (count($_SERVER['argv'])>2))
{
    //php cron.hid.php <table> <action> 
    //<action>Cron() will be called on ucfirst(<table>)Controller
    $name = ucfirst($_SERVER['argv'][1]);
    $action = $_SERVER['argv'][2].'Cron';
    
    $controller_name = $name.'Controller';
    
    require_once $controller_name.'.php';
    
    
    $controller = new $controller_name();
    
    $controller->$action();
}
else
{
    echo "error \r\n";
}

?>
